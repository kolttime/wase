//
//  TableHeaderFooterViewModel.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 19/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

public protocol TableHeaderFooterViewModel {
    
    var viewType: TableHeaderFooterView.Type { get }
}
