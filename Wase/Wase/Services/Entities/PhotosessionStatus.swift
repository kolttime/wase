//
//  UserType.swift
//  OMG
//
//  Created by Andrew Oparin on 15/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

public enum PhotosessionStatus: String {
    
    case inbox = "INBOX"
    case waiting = "WAITING"
    case created = "CREATED"
    case selection = "SELECTION"
    case prepayment = "PREPAYMENT"
}

extension PhotosessionStatus: Codable {
    
    //    enum Key: CodingKey {
    //        case rawValue
    //    }
    //
    //    init(from decoder: Decoder) throws {
    //        let container = try decoder.container(keyedBy: Key.self)
    //        let rawValue = try container.decode(String.self, forKey: .rawValue)
    //        self = UserType(rawValue: rawValue)!
    //    }
    //
    //    func encode(to encoder: Encoder) throws {
    //        var container = encoder.container(keyedBy: Key.self)
    //        try container.encode(self.rawValue, forKey: .rawValue)
    //    }
    
}


