//
//  CredentialStorage.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 19/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import KeychainSwift

public class CredentialStorage {
    
    private let keyChain: KeychainSwift
    private var token: String?
    private let key: String
    
    public init(key: String) {
        self.key = key
        self.keyChain = KeychainSwift()
        self.token = self.loadToken()
    }
    
    public func save(token: String) {
        self.token = token
        self.keyChain.set(token, forKey: self.key)
    }
    
    public func deleteToken() {
        self.token = nil
        keyChain.delete(self.key)
    }
    
    public func loadToken() -> String? {
        return keyChain.get(self.key)
    }
    
    public var unsafeToken: String {
        guard let token = self.token, !token.isEmpty else {
            fatalError("Token not found 😱")
        }
        return token
    }
    
    public func getToken() -> String? {
        guard let token = self.token, !token.isEmpty else {
            return nil
        }
        return token
    }
}
