//
//  TableViewController.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 19/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

open class TableViewController: UITableViewController, TableInteraction, ScrollInteraction {
    
    
    public var onApprove : (([String]) -> Void)?
    public var onReject : (([String]) -> Void)?
    public var scrollView: UIScrollView! { return self.tableView }
    var id : String? = nil
    public var viewModels: [TableSectionViewModel] = []
    public var onScroll: ((UIScrollView) -> Void)?
    
    public init() {
        super.init(style: .grouped)
        self.tableView.sectionIndexTrackingBackgroundColor = UIColor.clear
        self.tableView.sectionIndexBackgroundColor = UIColor.clear
        self.tableView.estimatedRowHeight = 0
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.clear
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModels.count
    }
    
    
    open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModels[section].cellModels.count
    }
    
    open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = self.viewModels[indexPath.section].cellModels[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: viewModel.cellType),
                                                 for: indexPath) as! TableViewCell
        cell.layoutSubviews()
        
        cell.layoutIfNeeded()
        cell.configure(viewModel: viewModel)
        return cell
    }
    
    open override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let viewModel = self.viewModels[section].headerViewModel else { return nil }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: viewModel.viewType)) as! TableHeaderFooterView
        view.configure(viewModel: viewModel)
        return view
    }
    
    open override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let viewModel = self.viewModels[section].footerViewModel else { return nil }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: viewModel.viewType)) as! TableHeaderFooterView
        view.configure(viewModel: viewModel)
        return view
    }
    
    open override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard let viewModel = self.viewModels[section].footerViewModel else { return CGFloat.leastNonzeroMagnitude }
        return viewModel.viewType.height(for: viewModel, tableView: tableView)
    }
    
    open override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let viewModel = self.viewModels[section].headerViewModel else { return CGFloat.leastNonzeroMagnitude }
        return viewModel.viewType.height(for: viewModel, tableView: tableView)
    }
    
    open override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? TableViewCell
        cell?.didSelect()
    }
    
    open override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell = tableView.cellForRow(at: indexPath) as? TableViewCell
        cell?.willSelect()
        return indexPath
    }
    
    open override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let viewModel = self.viewModels[indexPath.section].cellModels[indexPath.row]
        return viewModel.cellType.height(for: viewModel, tableView: tableView)
    }
    
    open override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.onScroll?(scrollView)
    }
   
}
