//
//  LargeTitleRootScrollViewController.swift
//  OMG
//
//  Created by Andrew Oparin on 16/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

open class LargeTitleRootScrollViewController: AirdronViewController {
    
    private lazy var largeTitleView = LargeTitleView()
    public lazy var containerBarView = UIView()
    lazy var underlayView = UIView()
    private lazy var scrollIndicatorOverlayView = UIView()
    private lazy var separatorView = UIView()
    
    private var underlayHeightConstraint: Constraint?
    
    public var largeTitle: NSAttributedString? {
        didSet {
            self.largeTitleView.text = self.largeTitle
        }
    }
    
    public var actionView: UIView? {
        didSet {
            self.largeTitleView.actionView = self.actionView
        }
    }
    
    public var navigationBarAreaHeight: CGFloat {
        if Device.isIphoneX || Device.isIphoneXSmax {
            return 100
        } else if Device.isIphone5 {
            return 70
        } else {
            return 75
        }
    }
    
    var minNavigationAndLargeTitleHeight: CGFloat {
        return self.navigationBarAreaHeight + LargeTitleView.contentHeight
    }
    
    public var backgroundBarColor: UIColor? {
        didSet {
            self.containerBarView.backgroundColor = self.backgroundBarColor
            self.largeTitleView.backgroundColor = self.backgroundBarColor
            self.underlayView.backgroundColor = self.backgroundBarColor
            self.scrollIndicatorOverlayView.backgroundColor = self.backgroundBarColor
        }
    }
    
    private weak var scrollInteraction: ScrollInteraction?
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.view.addSubview(self.underlayView)
        self.initialSetup()
        self.view.addSubview(self.scrollIndicatorOverlayView)
        self.view.addSubview(self.containerBarView)
        self.view.addSubview(self.largeTitleView)
        self.separatorView.backgroundColor = Color.light.value
        self.separatorView.alpha = 0
        self.view.addSubview(self.separatorView)
        self.setupConstraints()
        self.view.setNeedsUpdateConstraints()
        
    }
    
    func setupScroll(scrollInteraction: ScrollInteraction, refreshControl: UIRefreshControl?) {
        self.scrollInteraction = scrollInteraction
        self.scrollInteraction?.scrollView.refreshControl = refreshControl
        self.scrollInteraction?.scrollView.refreshControl?.bounds.origin.y = 40
        self.scrollInteraction?.scrollView.contentInsetAdjustmentBehavior = .never
        self.scrollInteraction?.scrollView.contentInset.top = self.minNavigationAndLargeTitleHeight
        self.scrollInteraction?.scrollView.contentOffset.y = -self.minNavigationAndLargeTitleHeight
        
        self.scrollInteraction?.onScroll = { [unowned self] scrollView in
            let y = scrollView.contentOffset.y + self.minNavigationAndLargeTitleHeight
            self.underlayHeightConstraint?.update(offset: max(self.minNavigationAndLargeTitleHeight - y, self.navigationBarAreaHeight))
            
            let scaleY = y / 2
            let coefficient = min(1.3, max(1, (self.navigationBarAreaHeight - scaleY) / self.navigationBarAreaHeight))
            
            self.largeTitleView.titleLabel.transform = CGAffineTransform.identity
            let titleX = self.largeTitleView.titleLabel.frame.origin.x
            self.largeTitleView.titleLabel.transform = CGAffineTransform.identity.scaledBy(x: coefficient,
                                                                                           y: coefficient)
            let newTitleX = self.largeTitleView.titleLabel.frame.origin.x
            let translateTransform = CGAffineTransform.init(translationX: titleX - newTitleX, y: 0)
            self.largeTitleView.titleLabel.transform = self.largeTitleView.titleLabel.transform.concatenating(translateTransform)
            if y >= LargeTitleView.contentHeight - CGFloat.leastNonzeroMagnitude {
                self.separatorView.alpha = 1
            } else {
                self.separatorView.alpha = 0
            }
        }
    }
    
    open func initialSetup() {
        
    }
    
    open func setupConstraints() {
        self.underlayView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            self.underlayHeightConstraint = $0.height.equalTo(self.minNavigationAndLargeTitleHeight).constraint
        }
        self.containerBarView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(self.navigationBarAreaHeight)
        }
        self.separatorView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalTo(self.containerBarView.snp.bottom)
            $0.height.equalTo(1)
        }
        self.largeTitleView.snp.makeConstraints {
            $0.left.equalTo(self.underlayView.snp.left)
            $0.right.equalTo(self.underlayView.snp.right)
            $0.bottom.equalTo(self.underlayView.snp.bottom)
        }
        self.scrollIndicatorOverlayView.snp.makeConstraints {
            $0.top.equalTo(self.underlayView.snp.top)
            $0.right.equalTo(self.underlayView.snp.right)
            $0.bottom.equalTo(self.underlayView.snp.bottom)
            $0.width.equalTo(10)
        }
    }
    
    @objc
    private func backTap() {
        self.navigationController?.popViewController(animated: true)
    }
}
