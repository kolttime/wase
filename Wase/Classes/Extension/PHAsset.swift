//
//  PHAsset.swift
//  MDW
//
//  Created by Andrew Oparin on 07/09/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import Photos

typealias PHAssetEditingObject = (input: PHContentEditingInput?, error: NSError?, tempUrl: URL?, fullImage: UIImage?)

extension PHAsset {
    
    var fileName: String? {
        let resources = PHAssetResource.assetResources(for: self)
        guard let resource = resources.first else { return nil }
        return resource.originalFilename
    }
    
    func resolveImage(withOptions options: PHImageRequestOptions,
                      completion: @escaping (UIImage?) -> Void)
    {
        let targetSize = CGSize(
            width: self.pixelWidth,
            height: self.pixelHeight
        )
        
        PHImageManager.default().requestImage(
            for: self,
            targetSize: targetSize,
            contentMode: .default,
            options: options) { (image, _) in
                completion(image)
        }
    }
    
    // MARK: completion are called on arbitrary queue in ios 10 and erlier versions. from ios 10 and later one are called on main queue
    @discardableResult
    func resolveEditingImageAndCopy(completion: ((PHAssetEditingObject) -> Void)?) -> PHContentEditingInputRequestID {
        let options: PHContentEditingInputRequestOptions = {
            let options = PHContentEditingInputRequestOptions()
            options.isNetworkAccessAllowed = true
            return options
        }()
        let id = self.requestContentEditingInput(with: options) { input, info in
            
            if let error = info[PHContentEditingInputErrorKey] as? NSError {
                completion?(PHAssetEditingObject(input: nil,
                                                 error: error,
                                                 tempUrl: nil,
                                                 fullImage: nil))
                return
            }
            
//            guard let imageOriginalUrl = input?.fullSizeImageURL else {
//                completion?(PHAssetEditingObject(input: nil,
//                                                 error: AirdronError.dataIsEmptyError() as NSError,
//                                                 tempUrl: nil,
//                                                 fullImage: nil))
//                return
//            }
            
            guard let imageData = input?.displaySizeImage?.jpegData(compressionQuality: 0.7) else {
                completion?(PHAssetEditingObject(input: nil,
                                                 error: AirdronError.dataIsEmptyError() as NSError,
                                                 tempUrl: nil,
                                                 fullImage: nil))
                return
            }
            
            let temp = URL(fileURLWithPath: NSTemporaryDirectory(),
                           isDirectory: true).appendingPathComponent(self.fileName ?? UUID.init().uuidString)
            
            
            if !FileManager.default.fileExists(atPath: temp.path) {
                do {
                    try imageData.write(to: temp)
                } catch {
                    completion?(PHAssetEditingObject(input: nil,
                                                     error: error as NSError,
                                                     tempUrl: nil,
                                                     fullImage: nil))
                }
            }
            
            completion?(PHAssetEditingObject(input: input,
                                             error: nil,
                                             tempUrl: temp,
                                             fullImage: input?.displaySizeImage))
        }
        return id
    }
}
