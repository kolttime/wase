//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Artem Myachkov on 07/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class PhoneCodeViewController: SimpleNavigationBar{
    
    private lazy var codeFiled: OMGCodeTextField = {
        let codeFiled = OMGCodeTextField()
        codeFiled.delegate = self
        return codeFiled
    }()
    
    var onCodeField : (() -> Void)?
    
    var onCompletion : Action?
    
    private let keyboardObserver = KeyboardObserver()
    
    var phoneNumber : String
    
    var mainLabel = UILabel()
    
    var numberInfo = UILabel()
    
    var timerLabel = UILabel()
    
    var pushSMS = UIButton()
    
    var timer:Timer?
    
    var timeLeft = 60
    
    var wrongLabel = UILabel()
    
    
    private var apiService : ApiService
    private var sessionManager : TaokaUserSessionManager
    private var key : String
    init(phoneNumber : String, apiService : ApiService, key : String, sessionManager : TaokaUserSessionManager) {
        self.phoneNumber = phoneNumber
        self.apiService = apiService
        self.sessionManager = sessionManager
        self.key = key
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func initialSetup() {
        
        super.initialSetup()
    
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
    
        self.view.addSubview(mainLabel)
        self.view.addSubview(numberInfo)
        self.view.addSubview(codeFiled)
        self.view.addSubview(timerLabel)
        self.view.addSubview(wrongLabel)
        self.view.addSubview(pushSMS)
        
        pushSMS.tintColor = .blue
        pushSMS.setTitle("Получить новый код", for: .normal)
        pushSMS.isHidden = true
        pushSMS.setTitleColor(.blue, for: .normal)
        pushSMS.titleLabel?.font = UIFont.systemFont(ofSize: 17) // кнопка

        mainLabel.text = "Введите код из SMS"
        mainLabel.textColor = .black
        mainLabel.font = UIFont.systemFont(ofSize: 35)
        mainLabel.numberOfLines = 0
        self.setLargeTitleHidden()
        
//        numberInfo.text = "*Код был отправлен на номер \n\(phoneNumber)"
//        numberInfo.textColor = .gray
//        numberInfo.font = UIFont.systemFont(ofSize: 17)
//        numberInfo.numberOfLines = 0
        
        numberInfo.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Код был отправлен на номер \n\(phoneNumber)")
        numberInfo.numberOfLines = 0

        
//        wrongLabel.text = "*Неверный код"
//        wrongLabel.textColor = .red
//        wrongLabel.font = UIFont.systemFont(ofSize: 15)
//        wrongLabel.isHidden = true
        
        wrongLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.red.value).make(string: "Неверный код")
        wrongLabel.isHidden = true
        
        
//        timerLabel.font = UIFont.systemFont(ofSize: 17)
//        timerLabel.numberOfLines = 0
        
        timerLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "")
        timerLabel.textColor = Color.grey.value //?????
        timerLabel.numberOfLines = 0
        
        
        
        self.view.backgroundColor = Color.white.value
        self.SetupConstraints()
        
    }
    
    @objc func textCodeFull(){
        print("done")
        self.onCodeField?()
    }
    
    private func SetupConstraints(){
        mainLabel.snp.makeConstraints{

            $0.top.equalToSuperview().offset(self.getTopOffset())
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
        numberInfo.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(32)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-81)
            
        }
        
        codeFiled.snp.makeConstraints{
            $0.top.equalTo(numberInfo.snp.bottom).offset(32)
            $0.left.equalToSuperview()
        }
        
        timerLabel.snp.makeConstraints{
            $0.top.equalTo(codeFiled.snp.bottom).offset(6)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
        wrongLabel.snp.makeConstraints{
            $0.top.equalTo(timerLabel.snp.bottom).offset(8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
        pushSMS.snp.makeConstraints{
            $0.top.equalTo(wrongLabel.snp.bottom).offset(8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
    }
    
    @objc func onTimerFires()
    {
        timeLeft -= 1
        timerLabel.text = "Получить новый код 0:\(timeLeft)"
        
        if timeLeft <= 0 {
            timer?.invalidate()
            timer = nil
            pushSMS.isHidden = false
            timerLabel.isHidden = true
        }
    }
    
 
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.keyboardObserver.isSuspended = true
        wrongLabel.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.keyboardObserver.isSuspended = false
        self.codeFiled.firstResponder()
        timeLeft = 60
        onTimerFires()
    }
    
}
extension PhoneCodeViewController: CodeFieldViewDelegate{
    func codeDidChage(_ code: String) {
    }
    
    func didEnterCode(_ code: String) {
        self.apiService.verification(byKey: self.key, code: code) {[weak self] result in
            switch result {
            case .success(let response):
                print("success")
                if let token = response.token {
                    self?.sessionManager.save(token: token)
                    if let user = response.user{
                        self?.sessionManager.save(user: user)
                        self?.sessionManager.setFirstLaunch()
                        self?.onCompletion?()
                    } else {
                        self?.onCodeField?()
                    }
                } else {
                    self?.wrongLabel.isHidden = false
                    self?.codeFiled.showError()
                    self?.codeFiled.firstResponder()
                }
            case .failure(let error):
                print(error)
                self?.wrongLabel.isHidden = false
                self?.codeFiled.showError()
                self?.codeFiled.firstResponder()
            }
            
        }

    }
}

