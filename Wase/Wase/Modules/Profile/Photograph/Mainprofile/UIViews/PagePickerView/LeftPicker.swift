//
//  LeftPicker.swift
//  Taoka
//
//  Created by Minic Relocusov on 11/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class LeftPicker : AirdronView {
    
    
    public var picked : Bool = false
    
    private lazy var views  = [UIView(), UIView(), UIView(), UIView(), UIView(), UIView(), UIView(), UIView(), UIView()]
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = Color.light.value
        for view in self.views {
            view.layer.cornerRadius = 0.5
            view.layer.masksToBounds = true
            view.backgroundColor = Color.greyL.value
            
            self.addSubview(view)
        }
        self.setupConstraints()
    }
    
    func normalState(){
        for view in self.views{
            view.backgroundColor = Color.greyL.value
        }
        self.picked = false
    }
    func pickedState(){
        for view in self.views {
            view.backgroundColor = Color.black.value
        }
        self.picked = true
    }
    
    override func setupConstraints(){
        super.setupConstraints()
        
        // Там циклом конечно было бы круто сделать, но в пизду как - то...
        
        views[0].snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.height.equalTo(4)
            $0.width.equalTo(4)
        }
        views[1].snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalTo(self.views[0].snp.right).offset(2)
            $0.height.equalTo(4)
            $0.width.equalTo(4)
        }
        views[2].snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalTo(self.views[1].snp.right).offset(2)
            $0.height.equalTo(4)
            $0.width.equalTo(4)
        }
        views[3].snp.makeConstraints{
            $0.top.equalTo(self.views[0].snp.bottom).offset(2)
            $0.left.equalToSuperview()
            $0.height.equalTo(4)
            $0.width.equalTo(4)
        }
        views[4].snp.makeConstraints{
            $0.top.equalTo(self.views[0].snp.bottom).offset(2)
            $0.left.equalTo(self.views[3].snp.right).offset(2)
            $0.height.equalTo(4)
            $0.width.equalTo(4)
        }
        views[5].snp.makeConstraints{
            $0.top.equalTo(self.views[0].snp.bottom).offset(2)
            $0.left.equalTo(self.views[4].snp.right).offset(2)
            $0.height.equalTo(4)
            $0.width.equalTo(4)
        }
        views[6].snp.makeConstraints{
            $0.top.equalTo(self.views[3].snp.bottom).offset(2)
            $0.left.equalToSuperview()
            $0.height.equalTo(4)
            $0.width.equalTo(4)
        }
        views[7].snp.makeConstraints{
            $0.top.equalTo(self.views[3].snp.bottom).offset(2)
            $0.left.equalTo(self.views[6].snp.right).offset(2)
            $0.height.equalTo(4)
            $0.width.equalTo(4)
        }
        views[8].snp.makeConstraints{
            $0.top.equalTo(self.views[3].snp.bottom).offset(2)
            $0.left.equalTo(self.views[7].snp.right).offset(2)
            $0.height.equalTo(4)
            $0.width.equalTo(4)
        }

    }
}
