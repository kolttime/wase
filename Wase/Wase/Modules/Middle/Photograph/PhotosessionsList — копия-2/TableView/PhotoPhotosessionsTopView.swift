//
//  PhotoPhotosessionsTopView.swift
//  Taoka
//
//  Created by Minic Relocusov on 23/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotoPhotosessionsTopView : AirdronView {
    
    private lazy var avatarImageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = Color.greyL.value
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.isUserInteractionEnabled = true
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    
    let dateLabel = UILabel()
    let nameLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.dateLabel)
        self.addSubview(self.avatarImageView)
        self.addSubview(self.nameLabel)
        self.backgroundColor = Color.white.value
        self.setupConstraints()
    }
    
    func set(avatarUrl : String, name : String , date : String){
        self.nameLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: name)
        self.avatarImageView.kf.setImage(with: URL(string : avatarUrl))
        self.dateLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: date)
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.avatarImageView.snp.makeConstraints{
            $0.left.equalToSuperview().offset(12)
            $0.centerY.equalToSuperview()
            $0.height.equalTo(20)
            $0.width.equalTo(20)
        }
        self.dateLabel.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.right.equalToSuperview().offset(-12)
        }
        self.nameLabel.snp.makeConstraints{
            $0.left.equalTo(self.avatarImageView.snp.right).offset(4)
            $0.centerY.equalToSuperview()
        }
    }
}
