//
//  UIView.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 10/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

public extension UIView {
    
    public var topLayoutEdge: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp.top
        } else {
            return self.snp.top
        }
    }
    
    public var bottomLayoutEdge: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp.bottom
        } else {
            return self.snp.bottom
        }
    }
    
    public var leftLayoutEdge: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp.left
        } else {
            return self.snp.left
        }
    }
    
    public var rightLayoutEdge: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp.right
        } else {
            return self.snp.right
        }
    }
    
    func animateLayout(duration: Double, completion: Action? = nil) {
        UIView.animate(withDuration: duration, animations: { self.layoutIfNeeded() }) { _ in
            completion?()
        }
    }
    
    func animateHidden(duration: Double = 0.2, completion: Action? = nil) {
        UIView.animate(withDuration: duration, animations: { self.alpha = 0.0 }) { _ in
            completion?()
        }
    }
    
    func animateShowing(duration: Double = 0.2, completion: Action? = nil) {
        UIView.animate(withDuration: duration, animations: { self.alpha = 1.0 }) { _ in
            completion?()
        }
    }
}

public extension UIView {
    
    static func makeViewWith(backgroundColor: UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = backgroundColor
        return view
    }
    
    public static let defaultShakeValues: [Any] = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
    public static let shakeDuration: Double = 0.6
    public func shake(values: [Any] = UIView.defaultShakeValues) {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = UIView.shakeDuration
        animation.values = values
        layer.add(animation, forKey: "shake")
    }
}

public extension UIView {
    
    public func setHugging(priority: UILayoutPriority) {
        self.setContentHuggingPriority(priority, for: .horizontal)
        self.setContentHuggingPriority(priority, for: .vertical)
    }
    
    public func setResistance(priority: UILayoutPriority) {
        self.setContentCompressionResistancePriority(priority, for: .horizontal)
        self.setContentCompressionResistancePriority(priority, for: .vertical)
    }
}

extension UIView {
    
    func blink() {
        self.alpha = 0.2
        UIView.animate(withDuration: 1,
                       delay: 0.0,
                       options: [.curveLinear, .repeat, .autoreverse],
                       animations: { self.alpha = 1.0 })
    }
}
