//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class ToDoViewController : AirdronViewController {
    
    var onPhoto : (() -> Void)?
    var onMakeUp : (() -> Void)?
    var onOther : (() -> Void)?
    var mainLabel = UILabel()
    
    var firstLabel = checkPlanView()
    
    var secondLabel = checkPlanView()
    
    var thirdLabel = checkPlanView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        view.addSubview(mainLabel)
        view.addSubview(firstLabel)
        view.addSubview(secondLabel)
        view.addSubview(thirdLabel)
        
        mainLabel.font = UIFont.systemFont(ofSize: 35)
        mainLabel.textColor = .black
        mainLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.make(string: "Чем вы занимаетесь?")
        mainLabel.numberOfLines = 0
        
        firstLabel.nameLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "Я фотограф")
        firstLabel.descrLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Люди смогут присылать вам предложения о заказе фотосессий")
        
        secondLabel.nameLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "Я визажист")
        secondLabel.descrLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Формулировка, зачем визажисту сервис")
        
        thirdLabel.nameLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "Другой специалист")
        thirdLabel.descrLabel.text = ""
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.nextView))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.nextView2))
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.nextView3))
        
        firstLabel.addGestureRecognizer(tap1)
        secondLabel.addGestureRecognizer(tap2)
        thirdLabel.addGestureRecognizer(tap3)
        
        self.setUpConstraints()
    }
    
    @objc func nextView(){
        self.onPhoto?()
    }
    @objc func nextView2(){
        self.onMakeUp?()
    }
    @objc func nextView3(){
        self.onOther?()
    }
    func setUpConstraints(){
        
        mainLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        
        
        firstLabel.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(62)
            $0.top.equalTo(mainLabel.snp.bottom).offset(32)
        }
        
        secondLabel.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(62)
            $0.top.equalTo(firstLabel.snp.bottom).offset(30)
        }
        
        thirdLabel.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(62)
            $0.top.equalTo(secondLabel.snp.bottom).offset(30)
        }
        
    }
}

