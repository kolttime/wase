//
//  LargeTitleView.swift
//  MDW
//
//  Created by Andrew Oparin on 26/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

open class LargeTitleView: AirdronView {
    
    private var sideOffset: CGFloat { return Device.isIphone5 ? 15 : 20 }
    public static var contentHeight: CGFloat {
        if Device.isIphone5 {
            return 50
        }
        return 55
    }
    
    public var titleLabel = UILabel()
    public var actionView: UIView? {
        willSet {
            self.actionView?.snp.removeConstraints()
            self.titleLabel.snp.removeConstraints()
            self.actionView?.removeFromSuperview()
        }
        didSet {
            if let actionView = self.actionView {
                self.addSubview(actionView)
                actionView.setResistance(priority: .defaultHigh)
                actionView.setHugging(priority: .defaultHigh)
                self.makeConstraintForTitleAndAction()
            } else {
                self.makeConstraintForTitle()
            }
        }
    }
    
    open override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: LargeTitleView.contentHeight)
    }
    
    open override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.titleLabel)
        self.invalidateIntrinsicContentSize()
        self.setNeedsUpdateConstraints()
    }
    
    open override func setupConstraints() {
        super.setupConstraints()
        if let _ = self.actionView {
            self.makeConstraintForTitleAndAction()
        } else {
            self.makeConstraintForTitle()
        }
    }
    
    private func makeConstraintForTitle() {
        self.titleLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(self.sideOffset).priority(ConstraintPriority.high)
            $0.right.equalToSuperview().offset(-self.sideOffset)
            $0.centerY.equalToSuperview()
        }
    }
    
    private func makeConstraintForTitleAndAction() {
        self.titleLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(self.sideOffset).priority(ConstraintPriority.high)
            $0.centerY.equalToSuperview()
        }
        self.actionView?.snp.makeConstraints {
            $0.right.equalToSuperview().offset(-self.sideOffset)
            $0.centerY.equalToSuperview()
            $0.left.equalTo(self.titleLabel.snp.right).offset(8)
        }
    }
    
    public var text: NSAttributedString? {
        didSet {
            self.titleLabel.attributedText = self.text
        }
    }
}
