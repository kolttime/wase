//
//  Specializations.swift
//  Taoka
//
//  Created by Роман Макеев on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct Budget: Codable {
    
    var name : String?
    var value : [Int]?
    
    
    init() {
        self.name = nil
        self.value = nil
    }
    
    init(name : String?, value : [Int]) {
        self.name = name
    }
    
    
    //    func toDictionary() -> JsonDictionary {
    //
    //        var dict : [String : Any] = [:]
    //        dict["type"] = self.type?.rawValue
    //        return dict
    //
    //    }
}

extension Budget {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Budget.self, from: data)
    }
}

extension Array where Element == Budget {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Budget].self, from: data)
    }
}




