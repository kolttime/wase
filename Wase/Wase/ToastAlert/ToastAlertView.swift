//
//  ToastAlertView.swift
//  MDW
//
//  Created by Andrew Oparin on 16/09/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class ToastAlertView: AirdronView {
    
    private var parentWidth: CGFloat = 0
    private var textAttributes = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.white.value)
    private let offset = ViewSize.sideOffset
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = Color.grey.value
        self.addSubview(self.textLabel)
        self.setHugging(priority: .required)
        self.invalidateIntrinsicContentSize()
        self.setNeedsUpdateConstraints()
    }
    
    func configure(text: String, parentWidth: CGFloat, backgroundColor: UIColor) {
        self.parentWidth = parentWidth
        self.textLabel.attributedText = self.textAttributes.make(string: text)
        self.backgroundColor = backgroundColor
        self.invalidateIntrinsicContentSize()
        self.setNeedsLayout()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.textLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(self.offset)
            $0.top.equalToSuperview().offset(10)
            $0.right.equalToSuperview().offset(-self.offset)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        var textHeight = self.textLabel.attributedText?.height(width: CGFloat.greatestFiniteMagnitude) ?? 0
        let textWidth = self.parentWidth - 2 * self.offset
        textHeight = self.textLabel.attributedText?.height(width: textWidth) ?? 0
        return CGSize(width: textWidth, height: textHeight + 20)
    }
}
