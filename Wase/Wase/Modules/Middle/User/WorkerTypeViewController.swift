//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class WorkerTypeViewController : AirdronViewController {
    
    
    var Photograph = checkPlanView()
    var PHandVizage = checkPlanView()
    
    var onPhotograph : (() -> Void)?
    
    var onPhotoandVizage : (() -> Void)?
    
    
    let mainLabel = UILabel()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        Photograph.nameLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "Фотограф")
        Photograph.descrLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Запечатлит лучшие мгновения")
        
        PHandVizage.nameLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "Фотограф и визажист")
        PHandVizage.descrLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Помогут выглядеть потрясающе")
     
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.onPhoto))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.onPhotoandOther))
        Photograph.addGestureRecognizer(tap1)
        PHandVizage.addGestureRecognizer(tap2)
        
        
        
        self.mainLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.colored(color: Color.black.value).make(string: "Кто вам нужен для фотосессии?")
        
        mainLabel.numberOfLines = 0
        
        
        
        self.view.addSubview(mainLabel)
        self.view.addSubview(Photograph)
        self.view.addSubview(PHandVizage)
        
        self.setUpConstraints()
    }
    
    @objc func onPhoto(){
        self.onPhotograph?()
        self.Photograph.backgroundColor = Color.light.value
        self.PHandVizage.backgroundColor = Color.white.value
    }
    @objc func onPhotoandOther(){
        self.PHandVizage.backgroundColor = Color.light.value
        self.Photograph.backgroundColor = Color.white.value
        self.onPhotoandVizage?()
    }
    
    func setUpConstraints(){
        self.mainLabel.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        Photograph.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.height.equalTo(64)
            $0.right.equalToSuperview()
            $0.top.equalTo(mainLabel.snp.bottom).offset(24)
        }
        PHandVizage.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.height.equalTo(64)
            $0.right.equalToSuperview()
            $0.top.equalTo(Photograph.snp.bottom).offset(14)
        }
    }
}






