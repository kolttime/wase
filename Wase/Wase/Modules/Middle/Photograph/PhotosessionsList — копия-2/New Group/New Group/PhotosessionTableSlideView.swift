//
//  PhotosessionTableSlideView.swift
//  Taoka
//
//  Created by Роман Макеев on 05/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotosessionTableSlideView : SlideView {
    
    
    
    private lazy var tableViewController = TableViewController()
    var titleLabel = UILabel()
    var selection : (() -> Void)?
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.titleLabel)
        self.titleLabel.attributedText = CustomFont.bodyBold20.attributesWithParagraph.make(string: "Подскажите пожалуйста, почему не заинтересовало?"
        )
        self.titleLabel.numberOfLines = 0
        self.tableViewController.tableView.register(cellClass: PhotosessionSliddeTableViewCell.self)
        self.addSubview(self.tableViewController.view)
        self.setupConstraintss()
        
        self.render()
    }
    func render(){
        let viewModels = [PhotosessionSliddeTableViewCellModel(name: "Особой причины нет", selectionHandler: self.selection), PhotosessionSliddeTableViewCellModel(name: "Неудобное время", selectionHandler: self.selection), PhotosessionSliddeTableViewCellModel(name: "Неудобное место", selectionHandler: self.selection), PhotosessionSliddeTableViewCellModel(name: "Другое", selectionHandler: self.selection) ]
        let section = DefaultTableSectionViewModel(cellModels: viewModels)
        self.tableViewController.update(viewModels: [section])
    }
     func setupConstraintss() {
        //super.setupConstraints()
        self.titleLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(54)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.tableViewController.view.snp.makeConstraints{
            $0.top.equalTo(self.titleLabel.snp.bottom).offset(5)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
}
