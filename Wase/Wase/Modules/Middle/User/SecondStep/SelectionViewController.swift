//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class SelectionViewController : SimpleNavigationBar {
    
    var onBack : Action?
    var onNext : (() -> Void)?
    var onUser : ((String) -> Void)?
    lazy var tableViewController = TableViewController()
    private var selectionTypes: [SelectionCellModel] = []
    
    private var apiService : ApiService
    private var type : UserType
    private var id : String
    init(apiService : ApiService, type : UserType, id : String){
        self.apiService = apiService
        self.type = type
        self.id = id
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.onBack?()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(tableViewController.tableView)
        self.tableViewController.tableView.register(cellClass: SelectionCell.self)
        self.toastPresenter.targetView = self.view
        //self.setBackTitle(title: "Фотографы")
    
        self.setTitle(title: "Подбор")
        self.view.backgroundColor = Color.white.value
        self.setTabBarHidden()
        self.setLargeTitleHidden()
        
        self.setUpConstraints()
        self.fetchData()
        
    }
    
    func update(id : String){
        for model in self.selectionTypes {
            if model.id == id {
                let index = self.selectionTypes.index(where : {$0.id == id})
                self.selectionTypes.remove(at: index!)
                let section = DefaultTableSectionViewModel(cellModels: self.selectionTypes)
                self.tableViewController.update(viewModels : [section])
                break
            }
        }
    }
    func fetchData(){
        self.apiService.getCandidates(type: self.type, id: self.id) {[weak self] response in
            switch response {
            case .success(let users):
                print("Ебой \n\n\(users)\n\n")
                self?.render(users: users)
            case .failure(let error):
                print(error)
                self?.showToastErrorAlert(error)
                
            }
        }
    }
    
    func render(users : [UserShort]){
        var viewModels : [SelectionCellModel] = []
        for user in users {
            let selection : ((String) -> Void)? = {[weak self] id in
                self?.onUser?(id)
            }
            let viewModel = SelectionCellModel(mainLabel: "\(user.givenName) \(user.familyName)", countLabel: "5 фотосессий", id: user.id, image: user.avatar, selectionHandler: selection)
            viewModels.append(viewModel)
        }
        let section = DefaultTableSectionViewModel(cellModels: viewModels)
        self.tableViewController.update(viewModels: [section])
    }

    @objc func nextView(){
        print("~~~~~")
        self.onNext?()
    }
    
    override func setUpConstraints(){
        
        tableViewController.tableView.snp.makeConstraints{
        $0.top.equalToSuperview().offset((self.navigationController?.navigationBar.bounds.size.height ?? 0) + UIApplication.shared.statusBarFrame.height + 10) //?
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
    }
}




