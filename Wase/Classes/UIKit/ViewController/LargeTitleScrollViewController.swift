//
//  LargeTitleTableViewController.swift
//  MDW
//
//  Created by Andrew Oparin on 29/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

open class LargeTitleScrollViewController: AirdronViewController {

    lazy var largeTitleView = LargeTitleView()
    private lazy var navigationBarView = NavigationBarView()
    private lazy var containerBarView = UIView()
    lazy var underlayView = UIView()
    private lazy var scrollIndicatorOverlayView = UIView()
    private lazy var separatorView = UIView()

    private var underlayHeightConstraint: Constraint?
    
    public var largeTitle: NSAttributedString? {
        didSet {
            self.largeTitleView.text = self.largeTitle
        }
    }
    
    public var actionView: UIView? {
        didSet {
            self.largeTitleView.actionView = self.actionView
        }
    }
    
    private var navigationBarAreaHeight: CGFloat {
        if Device.isIphoneX || Device.isIphoneXSmax {
            return 95
        } else if Device.isIphone5 {
            return 60
        } else {
            return 70
        }
    }
    
    var minNavigationAndLargeTitleHeight: CGFloat {
        return self.navigationBarAreaHeight + LargeTitleView.contentHeight
    }
    
    public var backgroundBarColor: UIColor? {
        didSet {
            self.containerBarView.backgroundColor = self.backgroundBarColor
            self.navigationBarView.backgroundColor = self.backgroundBarColor
            self.largeTitleView.backgroundColor = self.backgroundBarColor
            self.underlayView.backgroundColor = self.backgroundBarColor
            self.scrollIndicatorOverlayView.backgroundColor = self.backgroundBarColor
        }
    }
    
    public var navigationTitle: NSAttributedString? {
        didSet {
            self.navigationBarView.title = self.navigationTitle
        }
    }
    
    public var backButtonTitle: NSAttributedString? {
        didSet {
            self.navigationBarView.backButton.text = self.backButtonTitle
        }
    }
    
    public var backButtonTintColor: UIColor? {
        didSet {
            self.navigationBarView.backButton.imageTintColor = self.backButtonTintColor
        }
    }
    
    public var backButtonImage: UIImage? {
        didSet {
            self.navigationBarView.backButton.image = self.backButtonImage
        }
    }
    
    public var leftAction: UIView? {
        didSet {
            self.navigationBarView.leftAction = self.leftAction
        }
    }
    
    public var rightActions: [UIView] = [] {
        didSet {
            self.navigationBarView.rightActions = self.rightActions
        }
    }
    
    private weak var scrollInteraction: ScrollInteraction?
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.view.addSubview(self.underlayView)
        self.initialSetup()
        self.view.addSubview(self.scrollIndicatorOverlayView)
        self.view.addSubview(self.largeTitleView)
        self.view.addSubview(self.containerBarView)
        self.separatorView.backgroundColor = Color.light.value
        self.separatorView.alpha = 0
        self.view.addSubview(self.separatorView)
        self.containerBarView.addSubview(self.navigationBarView)
        self.navigationBarView.setTitleHidden(value: true, withAnimation: false)
        self.setupConstraints()
        self.view.setNeedsUpdateConstraints()
        self.navigationBarView.backButton.addTarget(self, action: #selector(backTap), for: .touchUpInside)
    }
    
    func setupScroll(scrollInteraction: ScrollInteraction, refreshControl: UIRefreshControl?) {
        self.scrollInteraction = scrollInteraction
        self.scrollInteraction?.scrollView.refreshControl = refreshControl
        self.scrollInteraction?.scrollView.refreshControl?.bounds.origin.y = 40
        self.scrollInteraction?.scrollView.contentInsetAdjustmentBehavior = .never
        self.scrollInteraction?.scrollView.contentInset.top = self.minNavigationAndLargeTitleHeight
        self.scrollInteraction?.scrollView.contentOffset.y = -self.minNavigationAndLargeTitleHeight
        
        self.scrollInteraction?.onScroll = { [unowned self] scrollView in
            let y = scrollView.contentOffset.y + self.minNavigationAndLargeTitleHeight
            self.underlayHeightConstraint?.update(offset: max(self.minNavigationAndLargeTitleHeight - y, self.navigationBarAreaHeight))
            
            let scaleY = y / 2
            let coefficient = min(1.3, max(1, (self.navigationBarAreaHeight - scaleY) / self.navigationBarAreaHeight))
            
            self.largeTitleView.titleLabel.transform = CGAffineTransform.identity
            let titleX = self.largeTitleView.titleLabel.frame.origin.x
            self.largeTitleView.titleLabel.transform = CGAffineTransform.identity.scaledBy(x: coefficient,
                                                                                           y: coefficient)
            let newTitleX = self.largeTitleView.titleLabel.frame.origin.x
            let translateTransform = CGAffineTransform.init(translationX: titleX - newTitleX, y: 0)
            self.largeTitleView.titleLabel.transform = self.largeTitleView.titleLabel.transform.concatenating(translateTransform)
            if y >= LargeTitleView.contentHeight - CGFloat.leastNonzeroMagnitude {
                self.navigationBarView.setTitleHidden(value: false, withAnimation: true)
                self.separatorView.alpha = 1
            } else {
                self.navigationBarView.setTitleHidden(value: true, withAnimation: true)
                self.separatorView.alpha = 0
            }
        }
    }
    
    open func initialSetup() {
        
    }
    
    open func setupConstraints() {
        self.underlayView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            self.underlayHeightConstraint = $0.height.equalTo(self.minNavigationAndLargeTitleHeight).constraint
        }
        self.containerBarView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(self.navigationBarAreaHeight)
        }
        self.separatorView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalTo(self.containerBarView.snp.bottom)
            $0.height.equalTo(1)
        }
        self.navigationBarView.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        }
        self.largeTitleView.snp.makeConstraints {
            $0.left.equalTo(self.underlayView.snp.left)
            $0.right.equalTo(self.underlayView.snp.right)
            $0.bottom.equalTo(self.underlayView.snp.bottom)
        }
        self.scrollIndicatorOverlayView.snp.makeConstraints {
            $0.top.equalTo(self.underlayView.snp.top)
            $0.right.equalTo(self.underlayView.snp.right)
            $0.bottom.equalTo(self.underlayView.snp.bottom)
            $0.width.equalTo(10)
        }
    }
    
    @objc
    private func backTap() {
        self.navigationController?.popViewController(animated: true)
    }
}
