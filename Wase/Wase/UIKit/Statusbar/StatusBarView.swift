//
//  StatusBarView.swift
//  Taoka
//
//  Created by Minic Relocusov on 19/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit



class StatusBarView : AirdronView {
    
    
    
    var height : Float
    var prop : Float = 6.06
    var width : Float
    let firstLayer = UIView()
    let secondLayer = UIView()
    let thirdLayer = UIView()
    let fourthLayer = UIView()
    var stage : Int
    init(height : Float, stage : Int){
        self.height = height
        self.width = height / prop
        self.stage = stage
        super.init(frame: .zero)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
        self.backgroundColor = Color.light.value
        self.addSubview(self.firstLayer)
        self.addSubview(self.secondLayer)
        self.addSubview(self.thirdLayer)
        self.addSubview(self.fourthLayer)
        self.firstLayer.layer.cornerRadius = 4
        self.firstLayer.layer.masksToBounds = true
        self.firstLayer.backgroundColor = Color.white.value
        self.secondLayer.layer.cornerRadius = 4
        self.secondLayer.layer.masksToBounds = true
        self.secondLayer.layer.backgroundColor = Color.greyL.value.cgColor
        self.thirdLayer.layer.cornerRadius = 4
        self.thirdLayer.layer.masksToBounds = true
        self.thirdLayer.backgroundColor = Color.white.value
        self.fourthLayer.layer.cornerRadius = 4
        self.fourthLayer.layer.masksToBounds = true
        self.fourthLayer.backgroundColor = Color.accent.value
        self.setupConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.firstLayer.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
            $0.width.equalTo((self.width * Float(stage + 1)) + 10)
        }
        self.secondLayer.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.width.equalTo((self.width * Float(stage + 1))  )
        }
        self.thirdLayer.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.bottom.equalToSuperview()
            var width = (self.width * Float(stage)) + 10
            if self.stage == 0 {
                width = 0
            }
            $0.width.equalTo(width)
        }
        self.fourthLayer.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.width.equalTo(self.width * Float(self.stage))
        }
    }
    
}
