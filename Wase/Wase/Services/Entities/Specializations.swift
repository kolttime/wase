//
//  Specializations.swift
//  Taoka
//
//  Created by Роман Макеев on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct Specialization: Codable, Equatable {
    
    let id : String?
    let name : String?
    let description : String?
    
    init(id : String?, name : String?, description : String?) {
        self.id = id
        self.name = name
        self.description = description
    }
    
    
    func toDictionary() -> JsonDictionary {
        guard let id = self.id,
        let name = self.name,
            let description = self.description else {return [:]}
        var dict : [String : Any] = [:]
        dict["id"] = id
        dict["name"] = name
        dict["description"] = description
        return dict
    }
}

extension Specialization {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Specialization.self, from: data)
    }
}

extension Array where Element == Specialization {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Specialization].self, from: data)
    }
}

