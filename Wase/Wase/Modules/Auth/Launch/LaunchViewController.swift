//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Artem Myachkov on 07/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit

class LaunchViewController: SimpleNavigationBar{
    

    var label = UILabel()
    private lazy var imageView = UIImageView(image: .init(UIImage(named: "A"))!)
    var onButtonClicked : (() -> Void)?
    var numberButton =  authButtons()
    var instButton = authButtons()
    var onInstagram : Action?
    
    var versionLabel = UILabel()
    
    private let apiService: ApiService
    
    init(apiService: ApiService) {
    self.apiService = apiService
    super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        
     
        super .initialSetup()
        self.setTitle(title: "")
        self.view.addSubview(self.imageView)
        self.view.addSubview(label)
        self.view.addSubview(versionLabel)
        self.view.addSubview(self.numberButton)
        self.view.addSubview(self.instButton)
        self.setBackTitle(title: "Назад")
        self.imageView.contentMode = .scaleAspectFill
        
        self.label.attributedText = CustomFont.largeTitle.attributesWithParagraph.make(string: "Wase")
        
        versionLabel.text = "0.1 alpha (3)"
        versionLabel.font = UIFont.systemFont(ofSize: 13)
        versionLabel.textColor = .gray
        
       
        self.view.backgroundColor = Color.white.value
        self.SetupConstraints()
        
        numberButton.textLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "Войти по номеру телефона")
        
        instButton.textLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "Войти через Instagram")
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        let numberTap = UITapGestureRecognizer(target: self, action: #selector(self.onNumberTap))
        self.numberButton.addGestureRecognizer(numberTap)
        
        let instTap = UITapGestureRecognizer(target: self, action: #selector(self.onInstTap))
        self.instButton.addGestureRecognizer(instTap)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @objc func onNumberTap(){
        print("number")
        self.onButtonClicked?()
    }
    
    @objc func onInstTap(){
        print("inst")

        self.onInstagram?()



    }
    
    private func SetupConstraints(){
        label.snp.makeConstraints{

            $0.left.equalToSuperview().offset(24)
            $0.bottom.equalTo(self.imageView.snp.bottom).offset(-24)
        }
        
        self.imageView.snp.makeConstraints {
            $0.width.equalToSuperview()
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-315)
        }
        
        self.numberButton.snp.makeConstraints{
            $0.top.equalTo(self.imageView.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(62)
        }
        
        self.instButton.snp.makeConstraints{
            $0.top.equalTo(self.numberButton.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(62)
        }
        
        self.versionLabel.snp.makeConstraints{
            
            $0.bottom.equalToSuperview().offset(-34)
            $0.left.equalToSuperview().offset(24)
        }
        

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}
