//
//  UIViewController.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 19/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

public extension UIViewController {
    
    public var topLayoutEdge: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.view.safeAreaLayoutGuide.snp.top
        } else {
            return self.topLayoutGuide.snp.bottom
        }
    }
    
    public var bottomLayoutEdge: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.view.safeAreaLayoutGuide.snp.bottom
        } else {
            return self.bottomLayoutGuide.snp.top
        }
    }
    
    public var leftLayoutEdge: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.view.safeAreaLayoutGuide.snp.left
        } else {
            return self.view.snp.left
        }
    }
    
    public var rightLayoutEdge: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.view.safeAreaLayoutGuide.snp.right
        } else {
            return self.view.snp.right
        }
    }
    
    public var topLayoutEdgeInset: CGFloat {
        if #available(iOS 11.0, *) {
            return self.view.safeAreaInsets.top
        } else {
            return self.topLayoutGuide.length
        }
    }
    
    public var bottomLayoutEdgeInset: CGFloat {
        if #available(iOS 11.0, *) {
            return self.view.safeAreaInsets.bottom
        } else {
            return self.bottomLayoutGuide.length
        }
    }
}

extension UIViewController {
    
    func add(_ child: UIViewController) {
        self.addChild(child)
        self.view.addSubview(child.view)
        child.view.frame = self.view.bounds
        child.didMove(toParent: self)
    }
    func removeFromParentWithView() {
        guard self.parent != nil else {
            return
        }
        self.willMove(toParent: nil)
        self.removeFromParent()
        self.view.removeFromSuperview()
    }
}
