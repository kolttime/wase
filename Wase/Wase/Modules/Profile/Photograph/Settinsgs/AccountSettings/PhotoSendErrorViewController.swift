//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotoSendErrorViewController : SimpleNavigationBar {
    
    var onNext : (() -> Void)?
    var crutchTF = OMGTextField.makeDate()
    var mainLabel = UILabel()
    
    override init(){
        super.init()
    }
    
    var scrollViewController = ScrollViewController()
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setRighttitle(title: "Отправить", completion: nil)
        self.crutchTF.isHidden = true
        self.view.backgroundColor = Color.white.value
        self.mainLabel.font = UIFont.boldSystemFont(ofSize: 30)
        self.mainLabel.textColor = .black
        self.mainLabel.text = "Расскажите, пожалуйста"
        self.mainLabel.numberOfLines = 0
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        self.view.addSubview(self.scrollViewController.scrollView)
        self.scrollViewController.scrollView.addSubview(mainLabel)
        self.scrollViewController.scrollView.addSubview(crutchTF)
        
        self.setTabBarHidden()
        self.setLargeTitleHidden()
        
        
        self.setUpConstraints()
        
        
    }
    
    @objc func nextView(){
        print("~~~~~")
        self.onNext?()
    }
    
    override func setUpConstraints(){
        
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        self.mainLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalToSuperview()
        }
        
        self.crutchTF.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
    }
}
}




