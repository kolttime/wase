//
//  FileManager.swift
//  MDW
//
//  Created by Andrew Oparin on 07/09/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

extension FileManager {
    public func clearTempDirectory() {
        do {
            let tempPath = NSTemporaryDirectory()
            let tempDirectory = try self.contentsOfDirectory(atPath: tempPath)
            let tempURL = URL(fileURLWithPath: tempPath)
            try tempDirectory.forEach { file in
                let fileUrl = tempURL.appendingPathComponent(file)
                try removeItem(atPath: fileUrl.path)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
