//
//  Device.swift
//  AirdronCore
//
//  Created by andrew oparin on 03/02/2018.
//  Copyright © 2018 airdron. All rights reserved.
//

import Foundation
import UIKit

public struct Device {
    public static let isIpad = UIDevice.current.userInterfaceIdiom == .pad
    public static let isIphone = UIDevice.current.userInterfaceIdiom == .phone
    public static let isRetina = UIScreen.main.scale >= 2.0
    
    public static let screenWidth = Int(UIScreen.main.bounds.size.width)
    public static let screenHeight = Int(UIScreen.main.bounds.size.height)
    public static let screenMaxLength = Int(max(screenWidth, screenHeight))
    public static let screenMinLength = Int(min(screenWidth, screenHeight))
    
    public static let isIphone5 = isIphone && screenMaxLength == 568
    public static let isIphone6 = isIphone && screenMaxLength == 667
    public static let isIphone6p = isIphone && screenMaxLength == 736
    public static let isIphoneX = isIphone && screenMaxLength == 812
    public static let isIphoneXSmax = isIphone && screenMaxLength == 896

    public static let isIpadMini = isIpad && screenMaxLength == 1024
    public static let isIpadMid = isIpad && screenMaxLength == 1112
    public static let isIpadMax = isIpad && screenMaxLength == 1366
}
