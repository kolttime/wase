//
//  ResponsePhotosessionView.swift
//  Taoka
//
//  Created by Роман Макеев on 05/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class ResponsePhotosessionView : AirdronView {
    
    
    var typeLabel = UILabel()
    var whereLabel = UILabel()
    var whenlabel = UILabel()
    var withLabel = UILabel()
    var separator1 = UIView()
    var separator2 = UIView()
    var whenTitle = UILabel()
    override func initialSetup() {
        super.initialSetup()
        
        self.addSubview(self.typeLabel)
        self.addSubview(self.whenlabel)
        self.addSubview(self.whereLabel)
        self.addSubview(self.withLabel)
        self.addSubview(self.separator1)
        self.addSubview(self.separator2)
        self.addSubview(self.whenTitle)
        self.whenlabel.numberOfLines = 0
        self.whereLabel.numberOfLines = 0
        
        self.separator1.backgroundColor = Color.light.value
        self.separator2.backgroundColor = Color.light.value
       // self.setupConstraints()
    }
    
    
    func set(type : String, whereText : String, when : String, with : String, whereTitle : String) {
        self.typeLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: type)
        self.whenlabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: when)
        self.whereLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: whereText)
        self.withLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: with)
        self.whenTitle.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: whereTitle)
        self.setupConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.separator1.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
        }
        self.separator2.snp.makeConstraints{
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
        }
        self.typeLabel.snp.makeConstraints{
            $0.top.equalTo(self.separator1.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.whenlabel.snp.makeConstraints{
            $0.top.equalTo(self.typeLabel.snp.bottom)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            
        }
        self.whenTitle.snp.makeConstraints{
            $0.top.equalTo(self.whenlabel.snp.bottom).offset(16)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.whereLabel.snp.makeConstraints{
            $0.top.equalTo(self.whenTitle.snp.bottom)
            $0.left.equalTo(self.whenlabel)
            $0.right.equalTo(self.whenlabel)
            if self.withLabel.text == "" {
                $0.bottom.equalTo(self.separator2.snp.top).offset(-15)
            }
        }
        if self.withLabel.text != "" {
            self.withLabel.snp.makeConstraints{
                $0.top.equalTo(self.whereLabel.snp.bottom).offset(16)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.bottom.equalTo(self.separator2.snp.top).offset(-15)
            }
        }
        
    }
    
}
