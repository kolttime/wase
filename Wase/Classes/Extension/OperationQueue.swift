//
//  OperationQueue.swift
//  OMG
//
//  Created by Andrew Oparin on 18/11/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

extension OperationQueue {
    
    func waitCurrentUploadsOperation(queue: DispatchQueue = .main, _ block: @escaping Action) -> Operation? {
        guard !self.operations.isEmpty else { block(); return nil}
        let operation = BlockOperation { queue.async { block() } }
        self.operations.forEach { operation.addDependency($0) }
        self.addOperation(operation)
        return operation
    }
}
