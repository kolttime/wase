//
//  UserPhotoMiddleView.swift
//  Taoka
//
//  Created by Minic Relocusov on 19/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class UserPhotoMiddleView : AirdronView {
    
    
    var onTouch : Action?
    private lazy var avatarImageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = Color.light.value
        view.layer.cornerRadius = 36 / 2
        view.clipsToBounds = true
        view.isUserInteractionEnabled = true
        view.contentMode = .scaleAspectFill
        return view
    }()
    let topLabel = UILabel()
    let bottomLabel = UILabel()
    let arrow = UIImageView()
    let separatorView = UIView()
    
    
    func set(count : String, proffesionName : String, avatarURL : String){
        self.topLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: count)
        self.bottomLabel.attributedText = CustomFont.tech11.attributesWithParagraph.make(string: proffesionName)
        self.avatarImageView.kf.setImage(with: URL(string: avatarURL))
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.avatarImageView)
        self.addSubview(self.topLabel)
        self.addSubview(self.bottomLabel)
        self.addSubview(self.arrow)
        self.addSubview(self.separatorView)
        self.backgroundColor = Color.white.value
        self.separatorView.backgroundColor = Color.grey.value
        self.arrow.image = UIImage(named: "Arrow2")!
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.touched))
        self.addGestureRecognizer(tap)
        self.setupConstraints()
    }
    
    @objc func touched(){
        self.onTouch?()
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        self.avatarImageView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(12)
            $0.width.equalTo(36)
            $0.height.equalTo(36)
        }
        self.topLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalTo(self.avatarImageView.snp.right).offset(6)
            $0.right.equalToSuperview().offset(-20)
        }
        self.bottomLabel.snp.makeConstraints{
            $0.top.equalTo(self.topLabel.snp.bottom)
            $0.left.equalTo(self.avatarImageView.snp.right).offset(6)
            $0.right.equalToSuperview().offset(-20)
        }
        self.arrow.snp.makeConstraints{
            $0.top.equalToSuperview().offset(15)
            $0.right.equalToSuperview().offset(-17)
        }
    }
    
}
