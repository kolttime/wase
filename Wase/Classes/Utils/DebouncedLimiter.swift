//
//  DebouncedLimiter.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 19/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

public final class DebouncedLimiter {
    
    let limit: TimeInterval
    
    private var item: DispatchWorkItem?
    
    public init(limit: TimeInterval = 0.5) {
        self.limit = limit
    }
    
    public func invalidate() {
        self.item?.cancel()
    }
    
    public func execute(action: @escaping Action) {
        self.item?.cancel()
        let item = DispatchWorkItem {
            action()
        }
        self.item = item
        DispatchQueue.main.asyncAfter(deadline: .now() + limit,
                                      execute: item)
    }
}
