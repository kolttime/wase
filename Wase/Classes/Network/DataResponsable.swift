//
//  DataResponsable.swift
//  AirdronCoreTests
//
//  Created by andrew oparin on 27/11/2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation
import Alamofire

public protocol DataResponsible {
    
    @discardableResult
    func response(queue: DispatchQueue?, completionHandler: @escaping (Alamofire.DefaultDataResponse) -> Void) -> Self
}

extension Alamofire.DataRequest: DataResponsible { }

