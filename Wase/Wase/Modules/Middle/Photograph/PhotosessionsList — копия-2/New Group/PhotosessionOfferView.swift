//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotosessionOfferView : SimpleNavigationBar {

    
    var tableSlideView = PhotosessionTableSlideView()
    var addCard = SampleButton()
    var scrollViewController = ScrollViewController()
    var keyObserver = KeyboardObserver()
    var avatarInfo = AvatarView()
    var commentView = CommentView(top: true)
    var photosessionView = ResponsePhotosessionView()
    var costView = PhotosessionResponseCostView()
    var activeField = UIView()
    var readyView = PhotosessionResponseReadyView()
    var buttonLabel = UILabel()
    var button = UIView()
    var backCancel : Action?
    var apiService : ApiService
    private var photoId : String
    private var me : User
    
    init(apiService : ApiService, photoId : String, me : User){
        self.apiService = apiService
        self.photoId = photoId
        self.me = me
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private var helperView = UIView()
    private let loadingView = TaokaLoadingView()
    override func initialSetup() {
        
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.scrollViewController.scrollView.layoutIfNeeded()
        self.view.addSubview(self.scrollViewController.scrollView)
        self.scrollViewController.scrollView.addSubview(avatarInfo)
        self.scrollViewController.scrollView.addSubview(self.commentView)
        self.scrollViewController.scrollView.addSubview(self.helperView)
        self.scrollViewController.scrollView.addSubview(self.photosessionView)
        self.scrollViewController.scrollView.addSubview(self.costView)
        self.scrollViewController.scrollView.addSubview(self.button)
        self.button.backgroundColor = Color.light.value
        self.view.addSubview(self.loadingView)
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        //self.toastPresenter = self.view
        self.loadingView.isHidden  = true
        self.button.addSubview(self.buttonLabel)
        self.buttonLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: "Откликнуться")
        self.button.layer.cornerRadius = 10
        self.button.layer.masksToBounds = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.buttonClicked))
        self.button.addGestureRecognizer(tap)
        self.photosessionView.layoutSubviews()
        self.setLargeTitleHidden()
        self.setTabBarHidden()
        self.fetchData()
        self.toastPresenter.targetView = self.view
        self.view.addSubview(self.tableSlideView)
        self.tableSlideView.setRecognizer(height: 385)
        self.tableSlideView.animate = {[weak self] in
            self?.view.animateLayout(duration: 0.5)
        }
       // self.setUpConstraints()
        self.setuphandlers()
        var cancel : Action? = {
            [weak self] in
            print("cancel")
            self?.tableSlideView.open?()
        }
        self.tableSlideView.snp.makeConstraints{
            self.tableSlideView.topConstr = $0.top.equalTo(self.view.snp.bottom).constraint
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(385)
        }
        var slideCancel : Action? = {[weak self] in
            self?.tableSlideView.close?()
        }
        self.tableSlideView.setLeftTitle(title: "Отмена", completion: slideCancel)
        
        self.setRighttitle(title: "Отклонить", completion: cancel)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.setRighttitle(title: "", completion: nil)
    }
    @objc func cancelTouch(){
        print("отменить")
        self.backCancel?()
    }
    var commentString = ""
    var stage = 0
    @objc func buttonClicked(){
        print("click")
        if self.commentString != "" {
            if self.stage == 0 {
                self.stage = 1
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.cancelTouch))
                
                let rightlabel = UILabel()
                rightlabel.addGestureRecognizer(tap)
                rightlabel.isUserInteractionEnabled = true
                rightlabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.red.value).make(string: "Отменить")
                self.setRightButton(view: rightlabel)
                //self.setRighttitle(title: "Отменить", color: Color.red.value, completion: self.backCancel)
                self.buttonLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Ожидается утверждение")
                self.button.backgroundColor = Color.white.value
                self.render2()
            }
        }
    }
    func render2(){
        self.costView.isHidden = true
        self.scrollViewController.scrollView.addSubview(self.readyView)
        self.readyView.set(cost: "3600 ₽", comment: self.commentString, time: "3 часа", avatarUrl: "")
        self.readyView.layoutSubviews()
        self.readyView.snp.makeConstraints{
            $0.top.equalTo(self.photosessionView.snp.bottom).offset(20)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        }
        self.readyView.alpha = 0
        UIView.animate(withDuration: 0.5) {
            self.readyView.alpha = 1
        }
        self.scrollViewController.scrollView.layoutSubviews()
        self.view.layoutSubviews()
        print("reayViewMaxY : \(self.readyView.frame)")
        self.button.snp.removeConstraints()
        print("contentheight : \(self.scrollViewController.scrollView.contentSize.height)")
        self.button.snp.makeConstraints{
            $0.top.equalTo(self.readyView.snp.bottom).offset(HelpFunctions.setButtonOffset(view: self.view, scrollView: self.scrollViewController.scrollView, frameView: self.readyView, navBar: self.navigationController!.navigationBar))
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
            $0.bottom.equalToSuperview().offset(-40)
        }
       // self.view.animateLayout(duration: 0.5)
        
    }
    func setScrollOffset(scrollView : UIScrollView, view : UIView ,activeView : UIView, keyHeight : CGFloat = 500, offset : CGFloat = 0){
        let textMaxY = self.costView.frame.maxY + offset
        let keyMinHeight = (UIScreen.main.bounds.size.height) - keyHeight
        print("textMaxY : \(textMaxY)\nKeyHeight : \(keyHeight)\nkeyMinHeigh : \(keyMinHeight)\nUIScreen : \(UIScreen.main.bounds.size.height)")
        
        if textMaxY > keyMinHeight {
            let scrollOffset = textMaxY - keyMinHeight
            print("scrollOffset : \(scrollOffset)")
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollOffset), animated: true)
        } else {
            scrollView.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    var keyFrame  = CGRect.init(x: 0, y: 0, width: 0, height: 451)
    func setuphandlers(){
        self.keyObserver.onKeyboardWillShow = {[weak self] frame, jj in
            self?.keyFrame = frame
            guard let self = self else {return}
            self.setScrollOffset(scrollView: self.scrollViewController.scrollView, view: self.view, activeView: self.activeField, keyHeight: self.keyFrame.height)
        }
        self.keyObserver.onKeyboardWillHide = {[weak self] gg, ggfd in
            self?.scrollViewController.scrollView.setContentOffset(CGPoint.zero, animated: true)
        }
        self.costView.commentField.onTextDidChanged = {[weak self] response in
            self?.commentString = response.text
        
        }
        self.costView.commentField.onBeginEditing = {[weak self] field in
            self?.activeField = field
            //self?.setScrollOffset()
            
        }
        
    }
    var hasComment = true
    func fetchData(){
        self.loadingView.isHidden = false
        self.apiService.getPhotoPhotosession(id: self.photoId) {[weak self] response in
            switch response {
            case .success(let photosession):
                if photosession.description == nil  || photosession.description == ""
                {self?.hasComment = false}
                self?.loadingView.isHidden = true
                self?.render(photo: photosession)
                print("photosession : \(photosession)\ndesc \(self?.hasComment)")
            case .failure(let error):
                print(error)
                self?.showToastErrorAlert(error)
            }
        }
    }
    func render(photo : LongPhotosession){
        self.avatarInfo.set(imageUrl: photo.author!.avatar?.original.absoluteString ?? "", nameLabel: "\(photo.author!.givenName) \(photo.author!.familyName)")
       // self.scrollViewController.scrollView.layoutSubviews()
       // self.avatarInfo.layoutSubviews()
        self.commentView.set(text: "\(photo.description ?? "")", time: "")
        self.scrollViewController.scrollView.layoutSubviews()
        self.commentView.layoutSubviews()
        var whereText = ""
        switch photo.place.type {
        case .home:
            whereText = "Дома\n\(photo.place.address!)"
        case .outdoors:
            whereText = "На выезду\n\(photo.place.district!.name!)"
        case .studio:
            whereText = "В студии"
        
        }
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        dateFormatter.locale =  Locale(identifier: "ru_MD")
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let date = dateFormatter.date(from: photo.date)!
        
        dateFormatter.dateFormat = "dd MMMM"
        let whereTitle = dateFormatter.string(from: date)
        let weekDay = calendar.component(.weekday, from: date)
        var weekString = ""
        switch weekDay {
        case 1:
            weekString = "воскресенье"
        case 2:
            weekString = "понедельник"
        case 3:
            weekString = "вторник"
        case 4:
            weekString = "среда"
        case 5:
            weekString = "четверг"
        case 6:
            weekString = "пятница"
        case 7:
            weekString = "суббота"
            
        default:
            weekString = ""
        }
        var durationString = ""
        switch photo.duration {
        case 1:
            durationString = "1 час"
        case 2:
            durationString = "2 часа"
        case 3:
            durationString = "3 часа"
        case 4:
            durationString = "4 часа"
        default:
            durationString = "\(photo.duration) часов"
        }
        var withText = ""
        if photo.participantsTypes.count != 0 {
            if me.type == .makeUpArtist {
                withText = "С фотографом"
            } else {
                withText = "С визажистом"
            }
        }
        self.photosessionView.set(type: "\(photo.specialization.name!)", whereText: whereText, when: "\(weekString)\nc \(photo.time),\n\(durationString)", with: withText, whereTitle: whereTitle)
        self.costView.set(cost: "\(photo.price!) ₽", comission: "0 ₽ после комиссии")
        self.commentString = photo.description ?? ""
        self.photosessionView.layoutSubviews()
        self.costView.layoutSubviews()
        self.setUpConstraints()
    }

    override func setUpConstraints(){
        super.setUpConstraints()
        
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.helperView.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.avatarInfo.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview()
            $0.height.equalTo(36)
        }
        let height = (self.commentView.textLabel.text?.height(width: UIScreen.main.bounds.size.width - 52  - (2 * 12) - ViewSize.sideOffset, font: CustomFont.bodyRegular17.font) ?? 0 ) + 16
        print(height)
        print(height)
        if !self.hasComment {
            self.commentView.isHidden = true
            self.commentView.removeFromSuperview()
        } else {
            self.commentView.snp.makeConstraints{
                $0.top.equalTo(self.avatarInfo.snp.bottom).offset(16)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-52)
                $0.height.equalTo(height)
                //$0.bottom.equalToSuperview()
            }
        }
        self.photosessionView.snp.makeConstraints{
            if self.hasComment {
                $0.top.equalTo(self.commentView.snp.bottom).offset(20)
            } else {
                $0.top.equalTo(self.avatarInfo.snp.bottom).offset(20)
            }
            
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        }
        self.costView.snp.makeConstraints{
            $0.top.equalTo(self.photosessionView.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            //$0.bottom.equalToSuperview()
        }
        self.scrollViewController.scrollView.layoutSubviews()
        self.button.snp.makeConstraints{
            $0.top.equalTo(self.costView.snp.bottom).offset(HelpFunctions.setButtonOffset(view: self.view, scrollView: self.scrollViewController.scrollView, frameView: self.costView, navBar: self.navigationController!.navigationBar))
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
            $0.bottom.equalToSuperview().offset(-40)
        }
        self.buttonLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
       
        
    }
}


extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
}
}
