//
//  Album.swift
//  Taoka
//
//  Created by Роман Макеев on 16/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct Album: Codable {
    
    
    let id: String
    let title : String
    let images : [Image]
    let date : String
    let storage : String?
    let visibility : Visibility?
    let specialization : Specialization
    
    init(id : String, title : String, images : [Image], date : String, storage : String? , visibility : Visibility?, specialization : Specialization){
        self.id = id
        self.title = title
        self.images = images
        self.date = date
        self.storage = storage
        self.visibility = visibility
        self.specialization = specialization
    }
}
public enum Visibility: String {
    
    case publiC = "PUBLIC"
    case privatE = "PRIVATE"
}

extension Visibility: Codable {
    
   
    
}

extension Album {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Album.self, from: data)
    }
}

extension Array where Element == Album {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Album].self, from: data)
    }
}
