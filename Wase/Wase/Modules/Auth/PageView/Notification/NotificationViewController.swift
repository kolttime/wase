//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class NotificationViewController : AirdronViewController {
    
    
  
    
    var onNext : (() -> Void)?
    
    var mainLabel = UILabel()
    
    var descrNotifications = UILabel()
    
    var doNotifications = UILabel()
    
    var notificationsInfo = UILabel()
    
    let switchOnOff = UISwitch(frame:CGRect(x: 150, y: 150, width: 0, height: 0))
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        view.addSubview(mainLabel)
        view.addSubview(descrNotifications)
        view.addSubview(doNotifications)
        view.addSubview(notificationsInfo)
        view.addSubview(switchOnOff)

        
        switchOnOff.onTintColor = Color.accent.value
        
        
        mainLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.make(string: "Включите уведомления")
        mainLabel.numberOfLines = 0
        
        
        switchOnOff.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
       
        descrNotifications.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Так вы не упустите ничего важного")
        descrNotifications.numberOfLines = 0
        
        
        doNotifications.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "Получать уведомления")
        doNotifications.numberOfLines = 0
        
        
        notificationsInfo.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Уведомления будут появляться вне зависимости от того, заблокирован или разблокирован телефон. Все как привычнно.")
        notificationsInfo.numberOfLines = 0
        
        self.setUpConstraints()
        
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
        self.onNext?()
    }
    
    @objc func nextView(){
        print("~~~~~")
        self.onNext?()
    }
    
    func setUpConstraints(){
        
        mainLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        descrNotifications.snp.makeConstraints{
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-27)
            $0.top.equalTo(mainLabel.snp.bottom).offset(12)
        }
        doNotifications.snp.makeConstraints{
            $0.left.equalToSuperview().offset(24)
            $0.top.equalTo(descrNotifications.snp.bottom).offset(185)
        }
        notificationsInfo.snp.makeConstraints{
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-27)
            $0.top.equalTo(doNotifications.snp.bottom).offset(16)
        }
        switchOnOff.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-24)
            $0.top.equalTo(descrNotifications.snp.bottom).offset(185)
        }
        
    }
}

