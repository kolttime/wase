//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotographSettingsViewController : SimpleNavigationBar {
    
    var goAccount : (() -> Void)?
    var goServices : (() -> Void)?
    var goFinance : (() -> Void)?
    var goErrors : (() -> Void)?
    var goUserAgreement: (() -> Void)?
    var goTermsOfUse: (() -> Void)?
    var goNotif: (() -> Void)?
    
    var isChanged = false
    
    var onBack : ((User) -> Void)?
    
    var accountView : FourViewClass
    var notifView : FourViewClass
    var servicesView : FourViewClass
    var financeView : FourViewClass
    
    var supportView : SupportAndVersionClass
    var appView : SupportAndVersionClass
    
    var personalAgreement : UserInfoClass
    
    var conditions : UserInfoClass
    
    var scrollViewController = ScrollViewController()
    
    var userModel : User
    var apiService : ApiService
    init(userModel : User, apiService : ApiService){
        self.userModel = userModel
        self.apiService = apiService
        self.accountView = FourViewClass(textLabel: "Аккаунт", textDescr: "@"+userModel.nickname)
        self.notifView = FourViewClass(textLabel: "Уведомления", textDescr: "Чтобы ничего не пропустить")
        self.servicesView = FourViewClass(textLabel: "Услуги", textDescr: "Ваши услуги, время работы и опции")
        self.financeView = FourViewClass(textLabel: "Финансы", textDescr: "Счет и история транзакций")
        self.supportView = SupportAndVersionClass(textLabel: "Поддержка", textDescr: "Сообщить о проблеме", versionLabel: "")
        self.appView = SupportAndVersionClass(textLabel: "Приложение", textDescr: "Версия", versionLabel: "0.1 alpha (3)")
        self.personalAgreement = UserInfoClass(textLabel: "Пользовательское соглашение")
        self.conditions = UserInfoClass(textLabel: "Условия использования")
        super.init()
    }
    
    func set(){
        self.accountView.set(textLabel: "Аккаунт", descLabel: "@\(self.userModel.nickname)")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.view.backgroundColor = Color.white.value

        
        self.setLargeTitle()
        self.setTitle(title: "Настройки")
        
        self.setBackTitle(title: "Настройки")
        
        self.setTabBarHidden()
        
        supportView.versionLabel.isHidden = true
        appView.image.isHidden = true
        
        let tap0 = UITapGestureRecognizer(target: self, action: #selector(self.Finance))
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.Service))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.Account))
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.Errors))
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(self.Ussser))
        let tap5 = UITapGestureRecognizer(target: self, action: #selector(self.Notif))
        let tap6 = UITapGestureRecognizer(target: self, action: #selector(self.Conditions))
        financeView.addGestureRecognizer(tap0)
        servicesView.addGestureRecognizer(tap1)
        accountView.addGestureRecognizer(tap2)
        supportView.addGestureRecognizer(tap3)
        personalAgreement.addGestureRecognizer(tap4)
        notifView.addGestureRecognizer(tap5)
        conditions.addGestureRecognizer(tap6)
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        self.view.addSubview(self.scrollViewController.scrollView)
        
        scrollViewController.scrollView.addSubview(accountView)
        scrollViewController.scrollView.addSubview(notifView)
        scrollViewController.scrollView.addSubview(servicesView)
        scrollViewController.scrollView.addSubview(financeView)
        scrollViewController.scrollView.addSubview(supportView)
        scrollViewController.scrollView.addSubview(appView)
        scrollViewController.scrollView.addSubview(personalAgreement)
        scrollViewController.scrollView.addSubview(conditions)
        
        scrollViewController.scrollView.showsHorizontalScrollIndicator = false

        
        self.setUpConstraints()
    }
    
    func doInsert(){
        
    }
    
    @objc func Ussser(){
        print("finance")
        self.goUserAgreement?()
    }
    
    @objc func Notif(){
        print("finance")
        self.goNotif?()
    }
    
    @objc func Finance(){
        print("finance")
        self.goFinance?()
    }
    
    @objc func Errors(){
        print("errors~~~~~")
        self.goErrors?()
    }
    
    @objc func Service(){
        print("ssservice")
        self.goServices?()
    }
    
    @objc func Account(){
        print("~~~~~")
        self.goAccount?()
    }
    
    @objc func Conditions(){
        print("conditions")
        self.goTermsOfUse?()
    }
    
    
    override func setUpConstraints(){
        super.setUpConstraints()
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }

        
        accountView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.height.equalTo(84)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        notifView.snp.makeConstraints{
            $0.top.equalTo(accountView.snp.bottom)
            $0.height.equalTo(84)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        servicesView.snp.makeConstraints{
            $0.top.equalTo(notifView.snp.bottom)
            $0.height.equalTo(84)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        financeView.snp.makeConstraints{
            $0.top.equalTo(servicesView.snp.bottom)
            $0.height.equalTo(84)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        supportView.snp.makeConstraints{
            $0.top.equalTo(financeView.snp.bottom)
            $0.height.equalTo(100)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        appView.snp.makeConstraints{
            $0.top.equalTo(supportView.snp.bottom)
            $0.height.equalTo(100)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        personalAgreement.snp.makeConstraints{
            $0.top.equalTo(appView.snp.bottom)
            $0.height.equalTo(44)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        conditions.snp.makeConstraints{
            $0.top.equalTo(personalAgreement.snp.bottom)
            $0.height.equalTo(44)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-20)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //onBack?(userModel)
    }
    
}



