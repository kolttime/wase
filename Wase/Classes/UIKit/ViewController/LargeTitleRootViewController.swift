//
//  LargeTitleRootViewController.swift
//  MDW
//
//  Created by Andrew Oparin on 29/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

open class LargeTitleRootViewController: AirdronViewController {
    
    lazy var containerTitleView = UIView()
    private lazy var largeTitleView = LargeTitleView()
    
    var titleAreaHeight: CGFloat {
        if Device.isIphoneX || Device.isIphoneXSmax {
            return 100
        } else if Device.isIphone5 {
            return 70
        } else {
            return 75
        }
    }
    
    public var largeTitle: NSAttributedString? {
        didSet {
            self.largeTitleView.text = self.largeTitle
        }
    }
    
    public var largeTitleActionView: UIView? {
        didSet {
            self.largeTitleView.actionView = self.largeTitleActionView
        }
    }
    
    public var backgroundTitleColor: UIColor? {
        didSet {
            self.containerTitleView.backgroundColor = self.backgroundTitleColor
        }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.initialSetup()
        self.view.addSubview(self.containerTitleView)
        self.containerTitleView.addSubview(self.largeTitleView)
        self.setupConstraints()
        self.view.setNeedsUpdateConstraints()
    }
    
    open func initialSetup() {
        
    }
    
    open func setupConstraints() {
        self.containerTitleView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalToSuperview()
            $0.height.equalTo(self.titleAreaHeight)
        }
        self.largeTitleView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
}
