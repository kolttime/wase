//
//  ThreadSafeDict.swift
//  OMG
//
//  Created by Andrew Oparin on 21/11/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

class ThreadSafeDict<K: Hashable, V> {
    
    private var dict = [K: V]()
    
    private let queue = DispatchQueue(label: "Airdron.threadSafeDict", attributes: .concurrent)
    
    subscript(key: K) -> V? {
        get {
            var item: V?
            self.queue.sync {
                item = self.dict[key]
            }
            return item
        }
        
        set {
            self.queue.async(flags: .barrier) {
                self.dict[key] = newValue
            }
        }
    }
    
    func reset() {
        self.queue.async(flags: .barrier) {
            self.dict.removeAll()
        }
    }
    
    var count: Int {
        var count: Int = 0
        self.queue.sync {
            count = self.dict.count
        }
        return count
    }
    
    func forEach(_ block: (K, V) -> Void) {
        var dict = [K: V]()
        self.queue.sync {
            dict = self.dict
        }
        dict.forEach(block)
    }
}
