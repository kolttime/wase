
//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class AccountBindingViewController : SimpleNavigationBar {

    
    override func initialSetup() {
        super.initialSetup()
        self.view.backgroundColor = Color.white.value
        self.setTabBarHidden()
        self.setTitle(title: "Привязка счета для выплат")
        self.setLargeTitle()
        
        for navItem in(self.navigationController?.navigationBar.subviews)! {
            for itemSubView in navItem.subviews {
                if let largeLabel = itemSubView as? UILabel {
                    largeLabel.text = self.title
                    largeLabel.numberOfLines = 0
                    largeLabel.lineBreakMode = .byWordWrapping
                }
            }
        }
        
        self.setUpConstraints()
    }
    
    override func setUpConstraints(){
        super.setUpConstraints()
        
    }
}


