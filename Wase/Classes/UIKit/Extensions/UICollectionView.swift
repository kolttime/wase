//
//  UICollectionView.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 19/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

public extension UICollectionView {
    
    func register(cellClass: UICollectionViewCell.Type) {
        self.register(cellClass,
                      forCellWithReuseIdentifier: String(describing: cellClass))
    }
    
    func registerHeader(headerClass: UICollectionReusableView.Type) {
        self.register(headerClass,
                      forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                      withReuseIdentifier: String(describing: headerClass))
    }
    
    func registerFooter(footerClass: UICollectionReusableView.Type) {
        self.register(footerClass,
                      forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                      withReuseIdentifier: String(describing: footerClass))
    }
}

public extension CollectionCellViewModel {
    
    var reuseIdentifier: String {
        return String(describing: self.cellType)
    }
}

public extension UICollectionView {
    
    func obtainCell(for viewModel: CollectionCellViewModel,
                    atIndexPath indexPath: IndexPath) -> CollectionViewCell {
        let cell = self.dequeueReusableCell(withReuseIdentifier: viewModel.reuseIdentifier,
                                            for: indexPath) as! CollectionViewCell
        cell.configure(viewModel: viewModel)
        return cell
    }
    
    func obtainHeader(for viewModel: CollectionHeaderFooterViewModel,
                      atIndexPath indexPath: IndexPath) -> UICollectionReusableView {
        let view = self.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                         withReuseIdentifier: viewModel.reuseIdentifier,
                                                         for: indexPath) as! CollectionHeaderFooterView
        view.configure(viewModel: viewModel)
        return view
    }
    
    func obtainFooter(for viewModel: CollectionHeaderFooterViewModel,
                      atIndexPath indexPath: IndexPath) -> UICollectionReusableView {
        let view = self.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter,
                                                         withReuseIdentifier: viewModel.reuseIdentifier,
                                                         for: indexPath) as! CollectionHeaderFooterView
        view.configure(viewModel: viewModel)
        return view
    }
    
    func obtainEmptyHeader(atIndexPath indexPath: IndexPath) -> UICollectionReusableView {
        let view = self.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                         withReuseIdentifier: String(describing: EmptyCollectionHeaderView.self),
                                                         for: indexPath)
        return view
    }
    
    func obtainEmptyFooter(atIndexPath indexPath: IndexPath) -> UICollectionReusableView {
        let view = self.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter,
                                                         withReuseIdentifier: String(describing: EmptyCollectionFooterView.self),
                                                         for: indexPath)
        return view
    }
}
