//
//  UIColor.swift
//  MDW
//
//  Created by Andrew Oparin on 01/08/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    public convenience init(hashRgbaValue: UInt32) {
        let red = CGFloat((hashRgbaValue >> 24) & 0xff) / 255.0
        let green = CGFloat((hashRgbaValue >> 16) & 0xff) / 255.0
        let blue = CGFloat((hashRgbaValue >> 8) & 0xff) / 255.0
        let alpha = CGFloat((hashRgbaValue) & 0xff) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
