//
//  ApproveView.swift
//  OMG
//
//  Created by Minic Relocusov on 21/04/2019.
//  Copyright © 2019 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit


class checkPlanView : AirdronView {
    
    var nameLabel  = UILabel()
    var descrLabel  = UILabel()
    var image = UIImageView(image: .init(UIImage(named: "Arrow2")?.withRenderingMode(.alwaysTemplate))!)
  
    
    override func initialSetup() {
        super.initialSetup()
        self.image.tintColor = Color.greyL.value
        self.backgroundColor = Color.white.value
        self.addSubview(self.nameLabel)
        self.addSubview(self.descrLabel)
        self.addSubview(self.image)
        
        descrLabel.numberOfLines = 0
        nameLabel.numberOfLines = 0
        
        self.setupConstraints()
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.nameLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(24)
        }
        
        self.descrLabel.snp.makeConstraints{
            $0.top.equalTo(nameLabel.snp.bottom).offset(2)
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        
        self.image.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-22)
            $0.top.equalToSuperview().offset(15)
            $0.width.equalTo(8)
            $0.height.equalTo(13)
        }
        
    }
    
    override var intrinsicContentSize : CGSize{
        let width = UIView.noIntrinsicMetric
        return CGSize(width: width, height: 50)
    }
}


