//
//  ApproveView.swift
//  OMG
//
//  Created by Minic Relocusov on 21/04/2019.
//  Copyright © 2019 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit



class SwitchView : AirdronView {
    var onSwitchOn : Action?
    var onSwitchOff : Action?
    var added = false
    
    var textLabel  = UILabel()
    private lazy var image = UIImageView(image: .init(UIImage(named: "Arrow2"))!)
    var switchView = UISwitch(frame:CGRect(x: 150, y: 150, width: 0, height: 0))
    
    init (textLabel : String) {
        self.textLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: textLabel)
        super.init(frame: .zero)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.switchView.addTarget(self, action: #selector(toggleSwitch), for: UIControl.Event.valueChanged)
        self.switchView.onTintColor = .blue
        
        self.backgroundColor = Color.white.value
        self.addSubview(self.textLabel)
        self.addSubview(switchView)
        self.setupConstraints()
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.textLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        self.switchView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
    }
  
    @objc func toggleSwitch(sender: UISwitch) {
        if(switchView.isOn) {
            self.added = true
            self.onSwitchOn?()
            
        } else {
            self.added = false
            self.onSwitchOff?()
        }
    }
    
    
    override var intrinsicContentSize : CGSize{
        let width = UIView.noIntrinsicMetric
        return CGSize(width: width, height: 50)
    }
}



