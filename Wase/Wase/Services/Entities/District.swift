//
//  Specializations.swift
//  Taoka
//
//  Created by Роман Макеев on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct District: Codable {
    
    var id : String?
    var type : DistrictType?
    var name : String?
    
    
    init() {
        self.id = nil
        self.type = nil
        self.name = nil
    }
    
    init(id : String?, type : DistrictType, name : String?) {
        self.id = id
        self.type = type
        self.name = name
    }
    
    
//    func toDictionary() -> JsonDictionary {
//        
//        var dict : [String : Any] = [:]
//        dict["type"] = self.type?.rawValue
//        return dict
//        
//    }
}

extension District {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(District.self, from: data)
    }
}

extension Array where Element == District {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([District].self, from: data)
    }
}



