//
//  CircularProgressView.swift
//  OMG
//
//  Created by Andrew Oparin on 26/11/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

class CircularProgressView: AirdronView {
    
    private lazy var circleLayer: CAShapeLayer = CAShapeLayer()
    
    var startAngle: CGFloat = -90.0
    
    var minValue: CGFloat = 0.0
    
    var maxValue: CGFloat = 1.0
    
    var animate: Bool = false
    
    var lineWidth: CGFloat = 2 {
        didSet {
            self.circleLayer.lineWidth = lineWidth
        }
    }
    
    var color: UIColor = UIColor.white {
        didSet {
            self.circleLayer.strokeColor = color.cgColor
        }
    }
    
    private var value: CGFloat = 0.0
    
    convenience public init(side: CGFloat) {
        self.init(frame: CGRect(x: 0, y: 0, width: side, height: side))
    }
    
    override  func layoutSubviews() {
        super.layoutSubviews()
        self.circleLayer.frame = self.bounds
        self.circleLayer.path = makePath()
    }
    
    private func makePath() -> CGPath {
        let center = CGPoint(x: self.bounds.width / 2.0, y: self.bounds.height / 2.0)
        let radius = (self.bounds.width - 2) / 2
        return UIBezierPath(arcCenter: center, radius: radius, startAngle: 0.0, endAngle: CGFloat.pi * 2.0, clockwise: true).cgPath
    }
    
    func beginRotation() {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat.pi * 2
        rotateAnimation.duration = 2
        rotateAnimation.repeatCount = .infinity
        rotateAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        rotateAnimation.isRemovedOnCompletion = false
        circleLayer.add(rotateAnimation, forKey: "circleRotation")
    }
    
    func endRotation() {
        circleLayer.removeAnimation(forKey: "circleRotation")
    }
    
    func stopProgress() {
        self.circleLayer.removeAnimation(forKey: "circleStroke")
    }
    
    override  func initialSetup() {
        self.backgroundColor = UIColor.clear
        self.layer.addSublayer(self.circleLayer)
        
        self.circleLayer.frame = self.bounds
        self.circleLayer.fillColor = UIColor.clear.cgColor
        self.circleLayer.strokeColor = self.color.cgColor
        self.circleLayer.lineWidth = self.lineWidth
        
        self.circleLayer.strokeEnd = 0
        self.circleLayer.path = makePath()
        
        self.circleLayer.lineCap = CAShapeLayerLineCap.round
        self.circleLayer.lineJoin = CAShapeLayerLineJoin.round
    }
    
    func setProgress(_ newValue: CGFloat, animated: Bool) {
        guard newValue != self.value else { return }
        guard animated else {
            self.circleLayer.strokeEnd = newValue
            self.value = newValue
            self.setNeedsLayout()
            return
        }
        
        let strokeAnimation = CABasicAnimation(keyPath: "strokeEnd")
        strokeAnimation.fromValue = self.value
        strokeAnimation.toValue = newValue
        strokeAnimation.duration = TimeInterval(newValue - self.value)
        strokeAnimation.fillMode = CAMediaTimingFillMode.forwards
        strokeAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        strokeAnimation.isRemovedOnCompletion = false
        
        self.value = newValue
        self.circleLayer.strokeEnd = newValue
        
        self.circleLayer.add(strokeAnimation, forKey: "circleStroke")
    }
}
