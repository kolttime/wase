//
//  CoordinatorFactory.swift
//  OMG
//
//  Created by Andrew Oparin on 06/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

protocol CoordinatorPresentable {
    
    var coordinatorFactory: CoordinatorFactory { get set }
}

protocol CoordinatorFactory {
    
    func makeAuthCoordinator(presenter: AirdronNavigationController,
                             coordinatorFactory: CoordinatorFactory) -> AuthCoordinator
    
    func makeUserReviewCoordinator(presenter: AirdronNavigationController,
                             coordinatorFactory: CoordinatorFactory) -> UserReviewCoordinator
    
    func makeUserMiddleCoordinator(presenter: AirdronNavigationController,
                                    coordinatorFactory: CoordinatorFactory) -> UserMiddleCoordinator
    
    func makeUserProfileCoordinator(presenter: AirdronNavigationController,
                                   coordinatorFactory: CoordinatorFactory) -> UserProfileCoordinator
    
    func makePhotographReviewCoordinator(presenter: AirdronNavigationController,
                                   coordinatorFactory: CoordinatorFactory) -> PhotographReviewCoordinator
    
    func makePhotographMiddleCoordinator(presenter: AirdronNavigationController,
                                          coordinatorFactory: CoordinatorFactory) -> PhotographMiddleCoordinator
    
    func makePhotographProfileCoordinator(presenter: AirdronNavigationController,
                                    coordinatorFactory: CoordinatorFactory) -> PhotographProfileCoordinator
    
    func makeUserMenuCoordinator(presenter: AirdronNavigationController,
                                          coordinatorFactory: CoordinatorFactory) -> UserMenuCoordinator
    
    func makePhotographMenuCoordinator(presenter: AirdronNavigationController,
                                 coordinatorFactory: CoordinatorFactory) -> PhotographMenuCoordinator
    
    
}

final class CoordinatorFactoryImp: CoordinatorFactory {

    
    private let moduleBuilderFactory: ModuleBuilderFactory
    
    init(moduleBuilderFactory: ModuleBuilderFactory) {
        self.moduleBuilderFactory = moduleBuilderFactory
    }
    
    func makeAuthCoordinator(presenter: AirdronNavigationController,
                             coordinatorFactory: CoordinatorFactory) -> AuthCoordinator {
        return AuthCoordinator(presenter: presenter,
                               moduleBuilder: self.moduleBuilderFactory.makeAuthModuleBuilder(),
                               coordinatorFactory: self)
    }
    
    func makeUserReviewCoordinator(presenter: AirdronNavigationController,
                             coordinatorFactory: CoordinatorFactory) -> UserReviewCoordinator {
        return UserReviewCoordinator(presenter: presenter,
                                     moduleBuilder: self.moduleBuilderFactory.makeUserReviewModuleBuilder(),
                               coordinatorFactory: self)
    }
    
    func makeUserMiddleCoordinator(presenter: AirdronNavigationController,
                                   coordinatorFactory: CoordinatorFactory) -> UserMiddleCoordinator {
        return UserMiddleCoordinator(presenter: presenter,
                                     moduleBuilder: self.moduleBuilderFactory.makeUserMiddleModuleBuilder(),
                                     coordinatorFactory: self)
    }
    
    
    func makeUserProfileCoordinator(presenter: AirdronNavigationController,
                                    coordinatorFactory: CoordinatorFactory) -> UserProfileCoordinator {
        return UserProfileCoordinator(presenter: presenter,
                                      moduleBuilder: self.moduleBuilderFactory.makeUserProfileModuleBuilder(),
                                      coordinatorFactory: self)
    }
    
    func makePhotographReviewCoordinator(presenter: AirdronNavigationController,
                                   coordinatorFactory: CoordinatorFactory) -> PhotographReviewCoordinator {
        return PhotographReviewCoordinator(presenter: presenter,
                                     moduleBuilder: self.moduleBuilderFactory.makePhotographReviewModuleBuilder(),
                                     coordinatorFactory: self)
    }
    
    func makePhotographMiddleCoordinator(presenter: AirdronNavigationController,
                                         coordinatorFactory: CoordinatorFactory) -> PhotographMiddleCoordinator {
        return PhotographMiddleCoordinator(presenter: presenter,
                                           moduleBuilder: self.moduleBuilderFactory.makePhotographMiddleModuleBuilder(),
                                           coordinatorFactory: self)
    }
    
    func makePhotographProfileCoordinator(presenter: AirdronNavigationController,
                                    coordinatorFactory: CoordinatorFactory) -> PhotographProfileCoordinator {
        return PhotographProfileCoordinator(presenter: presenter,
                                      moduleBuilder: self.moduleBuilderFactory.makePhotographProfileModuleBuilder(),
                                      coordinatorFactory: self)
    }
    
    func makeUserMenuCoordinator(presenter: AirdronNavigationController,
                                          coordinatorFactory: CoordinatorFactory) -> UserMenuCoordinator {
        return UserMenuCoordinator(presenter: presenter,
                                            moduleBuilder: self.moduleBuilderFactory.makeUserMenuModuleBuilder(),
                                            coordinatorFactory: self)
    }
    
    func makePhotographMenuCoordinator(presenter: AirdronNavigationController,
                                 coordinatorFactory: CoordinatorFactory) -> PhotographMenuCoordinator {
        return PhotographMenuCoordinator(presenter: presenter,
                                   moduleBuilder: self.moduleBuilderFactory.makePhotographMenuModuleBuilder(),
                                   coordinatorFactory: self)
    }
    
}
