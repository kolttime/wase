//
//  ManagedObject.swift
//  AirdronCore
//
//  Created by andrew oparin on 10/02/2018.
//  Copyright © 2018 airdron. All rights reserved.
//

import Foundation
import CoreData

open class ManagedObject: NSManagedObject {
    
}

public protocol ManagedObjectType: NSFetchRequestResult {
    
    static var entityName: String { get }
    static var defaultSortDescriptors: [NSSortDescriptor] { get }
    static var defaultPredicate: NSPredicate { get }
}

public protocol DefaultManagedObjectType: ManagedObjectType {}

public extension DefaultManagedObjectType {
    public static var defaultPredicate: NSPredicate { return NSPredicate(value: true) }
}

public extension ManagedObjectType {
    public static var entityName: String { return String(describing: self) }
}

public extension ManagedObjectType {
    
    public static var defaultSortDescriptors: [NSSortDescriptor] {
        return []
    }
    
    public static var sortedFetchRequest: NSFetchRequest<Self> {
        let request = NSFetchRequest<Self>(entityName: entityName)
        request.sortDescriptors = defaultSortDescriptors
        request.predicate = defaultPredicate
        return request
    }
    
    public static func sortedFetchRequestWithPredicate(predicate: NSPredicate) -> NSFetchRequest<Self> {
        let request = sortedFetchRequest
        guard let existingPredicate = request.predicate else { fatalError("must have predicate") }
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [existingPredicate, predicate])
        return request
    }
    
    public static func sortedFetchRequestWithPredicateFormat(format: String, args: CVarArg...) -> NSFetchRequest<Self> {
        let predicate = withVaList(args) { NSPredicate(format: format, arguments: $0) }
        return sortedFetchRequestWithPredicate(predicate: predicate)
    }
    
    public static func predicateWithFormat(format: String, args: CVarArg...) -> NSPredicate {
        let predicate = withVaList(args) { NSPredicate(format: format, arguments: $0) }
        return predicateWithPredicate(predicate: predicate)
    }
    
    public static func predicateWithPredicate(predicate: NSPredicate) -> NSPredicate {
        return NSCompoundPredicate(andPredicateWithSubpredicates: [defaultPredicate, predicate])
    }
}

public extension ManagedObjectType where Self: ManagedObject {
    
    public static func findOrCreateInContext(moc: NSManagedObjectContext,
                                             matchingPredicate predicate: NSPredicate,
                                             configure: ((Self) -> Void)? = nil) -> Self {
        guard let obj = findOrFetchInContext(moc: moc, matchingPredicate: predicate) else {
            return self.createInContext(moc: moc, configure: configure)
        }
        return obj
    }
    
    public static func createInContext(moc: NSManagedObjectContext,
                                       configure: ((Self) -> Void)? = nil) -> Self {
        let newObject: Self = moc.insertObject()
        configure?(newObject)
        return newObject
    }
    
    public static func findOrFetchInContext(moc: NSManagedObjectContext,
                                            matchingPredicate predicate: NSPredicate) -> Self? {
        guard let obj = materializedObjectInContext(moc: moc, matchingPredicate: predicate) else {
            return fetchInContext(context: moc) { request in
                request.predicate = predicate
                request.returnsObjectsAsFaults = false
                request.fetchLimit = 1
                }.first
        }
        return obj
    }
    public static func fetchInContext(context: NSManagedObjectContext,
                                      matchingPredicate predicate: NSPredicate) -> [Self] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Self.entityName)
        request.sortDescriptors = Self.defaultSortDescriptors
        request.predicate = predicate
        guard let result = try! context.fetch(request) as? [Self] else {
            fatalError("Fetched objects have wrong type")
        }
        return result
    }
    
    public static func fetchInContext(context: NSManagedObjectContext,
                                      configurationBlock: (NSFetchRequest<NSFetchRequestResult>) -> Void = { _ in }) -> [Self] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Self.entityName)
        configurationBlock(request)
        do {
            let fetchResult = try context.fetch(request)
            guard let result = fetchResult as? [Self] else { fatalError("Fetched objects have wrong type") }
            return result
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    public static func countInContext(context: NSManagedObjectContext,
                                      configurationBlock: (NSFetchRequest<NSFetchRequestResult>) -> Void = { _ in }) -> Int {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Self.entityName)
        configurationBlock(request)
        do {
            let result = try context.count(for: request)
            return result
        } catch {
            let error = error as NSError
            fatalError("Failed to execute fetch request: \(error)")
        }
        
    }
    
    internal static func materializedObjectInContext(moc: NSManagedObjectContext,
                                                     matchingPredicate predicate: NSPredicate) -> Self? {
        for obj in moc.registeredObjects where !obj.isFault && !obj.isDeleted {
            guard let res = obj as? Self, predicate.evaluate(with: res) else { continue }
            return res
        }
        return nil
    }
}
