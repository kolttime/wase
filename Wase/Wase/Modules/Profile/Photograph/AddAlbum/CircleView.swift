//
//  CircleView.swift
//  OMG
//
//  Created by Andrew Oparin on 17/11/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

class CircleView: AirdronView {
    //fdshufghuisdjfbjds
    public var mainColor: UIColor = Color.light.value {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    public override func initialSetup() {
        super.initialSetup()
        self.contentMode = .redraw
        self.backgroundColor = UIColor.clear
    }
    
    public override func draw(_ rect: CGRect) {
        let path = UIBezierPath(ovalIn: rect)
        self.mainColor.setFill()
        path.fill()
    }
}
