//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Artem Myachkov on 07/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit
import Gallery

class UserProfileViewController: SimpleNavigationBar{
    
    
    
    var onImages : (([Image], Int) -> Void)?
    var onAlbum : ((String) -> Void)?
    var imagesModel : [Image]?
    var onPhotosession : Action?
    var onSelectCell : Action?
    var onSettings : ((User) -> Void)?
    var userModel : User?
    var albumsModel : [ShortAlbum]?
    
    private lazy var loadingView = TaokaLoadingView()
    private lazy var loadingViewTable = TaokaLoadingView()
    private lazy var tableViewController = ALTableViewController()
    private lazy var scrollViewController = ScrollViewController()
    private lazy var collectionViewController = CollectionWaterfallViewController(layout: self.collectionViewLayout)
    private var collectionView: UICollectionView { return self.collectionViewController.collectionView! }
    private lazy var collectionViewLayout: AirdronCollectionViewWaterfallLayout = {
        let layout = AirdronCollectionViewWaterfallLayout()
        layout.minimumColumnSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return layout
    }()
    private var profileHeader : TakoaProfileView?
   
    private lazy var pickerView = PagePickerView()
    var albumsFetched = false
    var imagesFetched = false
    var apiService : ApiService
    init(apiService : ApiService){
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateInfo(userModel : User) {
        self.userModel = userModel
        self.profileHeader?.set(givenName : userModel.givenName, familyName : userModel.familyName, city : userModel.city.name, descryption : userModel.description ?? "", imgUrl : userModel.avatar?.original.absoluteString ?? "")
    }
    var getSetting : Action?
    override func initialSetup() {
        super.initialSetup()
        
        // self.add(self.tableViewController)
        //self.add(self.scrollViewController)
        self.setLargeTitleHidden()
        self.getSetting = {[weak self] in
            self?.onSettings?((self?.userModel!)!)
        }
        self.view.addSubview(self.scrollViewController.scrollView)
        
        // self.scrollViewController.scrollView.addSubview(self.tableViewController.view)
        self.tableViewController.tableView.register(cellClass: SubTableViewCell.self)
        self.collectionViewController.collectionView.register(cellClass: SubCollectionViewCell.self)
        //self.tableViewController.tableView.estimatedRowHeight = 400
        //self.tableViewController.tableView.rowHeight = UITableView.automaticDimension
        self.tableViewController.tableView.contentSize.height = 4000
        self.scrollViewController.scrollView.alwaysBounceHorizontal = false
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        //self.render()
        self.setRighttitle(title: "Настройки", completion : getSetting)
        
        self.setBackTitle(title: "Профиль")
        
        self.scrollViewController.scrollView.layer.layoutIfNeeded()
        self.tableViewController.tableView.layer.layoutIfNeeded()
        self.tableViewController.tableView.backgroundColor = .purple
        self.tableViewController.tableView.isScrollEnabled  = false
        self.collectionViewController.collectionView.isScrollEnabled = false
       
      
        self.pickerView.layer.cornerRadius = 10
       
        self.pickerView.layer.masksToBounds = true
        self.pickerView.onLeftPick = {[weak self] in
            self?.render2()
        }
        self.pickerView.onRightPick = {[weak self] in
            self?.render()
        }
      
        // self.pickerView.rightPick()
        //  self.render()
        self.view.addSubview(self.loadingViewTable)
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = true
        self.loadingViewTable.isHidden = true
        //self.setUpConstraints()
        //////////////////////////////////////// self.firstFetch()
        // self.render2()
        // self.tempFetch()
        self.firstFetch()
    }
   
    func tempFetch(){
//        self.loadingView.isHidden = false
//        self.apiService.getProfile(){[weak self] response in
//            switch response {
//            case .success(let user):
//                self?.userModel = user
//                self?.loadingView.isHidden = true
//
//            case .failure(let error):
//                print(error)
//            }
//        }
    }
    
    func firstFetch(){
        var userFetched = false
        var albumsFetched = false
        self.loadingViewTable.isHidden = false
        self.loadingView.isHidden = false
        self.apiService.getProfile(userId : nil) {[weak self] result in
            switch result {
            case .success(let user):
                print(user)
                
                guard let self = self else {return}
                self.userModel = user
               
                self.profileHeader = TakoaProfileView.init(givenName: user.givenName, familyName: user.familyName, city: "\(user.city.name)", descryption: user.description ?? "", imgUrl: user.avatar?.original.absoluteString ??  "")
                self.scrollViewController.scrollView.addSubview(self.profileHeader!)
             
                self.scrollViewController.scrollView.addSubview(self.pickerView)
                self.scrollViewController.scrollView.isScrollEnabled = true
                self.setUpConstraints()
                //self.loadingView.isHidden = true
                userFetched = true
                if albumsFetched {
                    self.pickerView.rightPick()
                    self.loadingView.isHidden = true
                    userFetched = false
                }
            case .failure(let error):
                print(error)
                
                
            }
            
        }
        
        self.apiService.getAlbums(id : nil){[weak self] response in
            switch response {
            case .success(let albums):
                print(albums)
                self?.albumsModel = albums
                albumsFetched = true
                if userFetched {
                    
                    self?.pickerView.rightPick()
                    self?.loadingView.isHidden = true
                    albumsFetched = false
                }
                
            case .failure(let error):
                print(error)
                
                
            }
        }
        self.scrollViewController.scrollView.layer.layoutIfNeeded()
        self.tableViewController.tableView.layer.layoutIfNeeded()
    }
    func render(){
        self.collectionViewController.collectionView.removeFromSuperview()
        self.collectionViewController.collectionView.snp.removeConstraints()
        self.tableViewController.tableView.removeFromSuperview()
        self.tableViewController.tableView.snp.removeConstraints()
        self.scrollViewController.scrollView.addSubview(self.tableViewController.tableView)
        
        var selectionHandler : ((String) -> Void)? = {[weak self] id in
            self?.onAlbum?(id)
        }
        var viewModels : [SubTableViewCellModel] = []
        
        for album in self.albumsModel! {
            let viewModel = SubTableViewCellModel.init(imgUrl: album.images[0].original.absoluteString, nameText: album.title, dateText: album.date, id: album.id, selectionHandler: selectionHandler)
            viewModels.append(viewModel)
        }
        let section = DefaultTableSectionViewModel(cellModels: viewModels)
        self.tableViewController.update(viewModels: [section])
        self.setUpTableConstraints(count : viewModels.count)
        self.tableViewController.tableView.contentSize.height = 4000
    }
    func render2(){
        self.collectionViewController.collectionView.removeFromSuperview()
        self.collectionViewController.collectionView.snp.removeConstraints()
        self.tableViewController.tableView.removeFromSuperview()
        self.tableViewController.tableView.snp.removeConstraints()
        self.scrollViewController.scrollView.addSubview(self.collectionViewController.collectionView)
        // let viewModels = [SubCollectionViewCellModel(imgUrl: "https://bloody-disgusting.com/wp-content/uploads/2019/02/hotline-miami-header-e1551364402366.jpg", selectionHandler: self.onSelectCell), SubCollectionViewCellModel(imgUrl: "https://bloody-disgusting.com/wp-content/uploads/2015/04/HOT2_2.jpg", selectionHandler: self.onSelectCell), SubCollectionViewCellModel(imgUrl: "https://www.datocms-assets.com/9739/1552560427-hotlinemiami1.jpg"), SubCollectionViewCellModel(imgUrl: "https://img4.goodfon.ru/wallpaper/nbig/f/24/toni-hotline-miami-hotline-miami-tony-igra-art-risunok-tigr.jpg", selectionHandler: self.onSelectCell), SubCollectionViewCellModel(imgUrl: "https://static1.squarespace.com/static/58cafb2db8a79b4189b3c76c/t/5c7c3e2a9140b7a1d3c6719e/1553114719598/hotline-miami-game-retro-style-dark-life-cityscape-5k-su.jpg", selectionHandler: self.onSelectCell)]
        var viewModels2 : [SubCollectionViewCellModel] = []
        if self.imagesModel == nil {
            
            self.apiService.getImages(id : nil) {[weak self] response in
                switch response {
                case .success(let images):
                    
                    self?.imagesModel = images
                    var i = 0
                    let selection : ((Int) -> Void)? = {[weak self] index in
                        self?.onImages?(self?.imagesModel! ?? [], index)
                    }
                    for image in self?.imagesModel ?? [] {
                        let viewModel = SubCollectionViewCellModel(imgUrl: image.original.absoluteString, index: i, selectionHandler: selection)
                        viewModels2.append(viewModel)
                        i += 1
                    }
                    let section = DefaultCollectionSectionViewModel(cellModels: viewModels2)
                    self?.collectionViewController.update(viewModels: [section])
                    self?.setUpColelctionConstraints(count: viewModels2.count)
                case .failure(let error):
                    print(error)
                    return
                }
            }
        } else {
            self.setUpColelctionConstraints(count: self.imagesModel!.count)
        }
        
        
        
        // self.tableViewController.tableView.contentSize.height = 4000
    }
    func setUpTableConstraints(count : Int){
        self.tableViewController.tableView.snp.makeConstraints{
            $0.top.equalTo(self.pickerView.snp.bottom).offset(8)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.height.equalTo(count * 176)
        }
    }
    func setUpColelctionConstraints(count : Int){
        let height = (UIScreen.main.bounds.size.width - 2) / 3.0
        
        let counted : Int = self.roundNumber(num: Float(count) / 3.0)
        let finalHeight = ( Float(height) * Float(counted) ) + Float(counted - 1)
        print("Height: \(height)\ncounted: \(counted)\nfinalHeight: \(finalHeight)\nПромежуток: \((Float(height) / Float(count)) / 3.0)" )
        self.collectionViewController.collectionView.snp.makeConstraints{
            $0.top.equalTo(self.pickerView.snp.bottom).offset(16)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.height.equalTo(finalHeight)
        }
    }
    override func setUpConstraints() {
        super.setUpConstraints()
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.width.equalToSuperview()
        }
        self.profileHeader!.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        self.pickerView.snp.makeConstraints{
            $0.top.equalTo(self.profileHeader!.snp.bottom).offset(12)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.height.equalTo(44)
        }
        self.loadingView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.right.equalToSuperview()
        }
        //        self.loadingViewTable.snp.makeConstraints{
        //
        //        }
        
    }
    func roundNumber(num : Float) -> Int{
        let full : Int = Int(num)
        if Float(full) == num {
            return full
        } else {
            return full + 1
        }
    }
}



