//
//  UploadManager.swift
//  AirdronCore
//
//  Created by andrew oparin on 10/02/2018.
//  Copyright © 2018 airdron. All rights reserved.
//

import Foundation
import Alamofire

public protocol Uploadable {
    
    func multipart() -> (MultipartFormData) -> Void
}

public extension Uploadable {
    
    var videoFileName: String { return "video.mov" }
    var videoName: String { return "video" }
    var videoMimeType: String { return "video/mov" }
    
    var imageFileName: String { return "image.jpg" }
    var imageName: String { return "image" }
    var imageMimeType: String { return "image/jpeg" }
}


public struct AirdronUrlResource {
    
    public enum Meta {
        case image(fileURL: URL)
        case video(fileURL: URL)
    }
    
    public let meta: AirdronUrlResource.Meta
}

extension AirdronUrlResource: Uploadable {
    
    public func multipart() -> (MultipartFormData) -> Void {
        let multipartFormData: (MultipartFormData) -> Void = { multipartFormData in
            switch self.meta {
            case .image(let fileURL):
                self.imageMultipart(multipartFormData, fileURL: fileURL)
            case .video(let fileURL):
                self.videoMultipart(multipartFormData, fileURL: fileURL)
            }
        }
        return multipartFormData
    }
    
    private func videoMultipart(_ multipartFormData: MultipartFormData, fileURL: URL) {
        multipartFormData.append(fileURL,
                                 withName: self.videoName,
                                 fileName: self.videoFileName,
                                 mimeType: self.videoMimeType)
    }
    
    private func imageMultipart(_ multipartFormData: MultipartFormData, fileURL: URL)  {
        multipartFormData.append(fileURL,
                                 withName: self.imageName,
                                 fileName: self.imageFileName,
                                 mimeType: self.imageMimeType)
    }
}

public class UploadManager {
    
    private let sessionManager: Alamofire.SessionManager
    private static let defaultUploadBackgroundIdentifier = "com.airdron.upload.backround"

    public init(configuration: URLSessionConfiguration) {
        self.sessionManager = Alamofire.SessionManager(configuration: configuration)
        self.sessionManager.startRequestsImmediately = false
    }
    
    public func upload(request: AirdronRequest,
                       data: Uploadable,
                       serializer: ResponseSerializer,
                       completionHandler: ((Result<Any>) -> Void)?,
                       requestCompletionHandler: ((Result<AirdronDataRequestWrapper>) -> Void)? = nil,
                       progressHandler: ((Double) -> Void)? = nil) {
        let multipartFormData = data.multipart()
        
        let encodingCompletion: ((SessionManager.MultipartFormDataEncodingResult) -> Void)? = { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                requestCompletionHandler?(.success(AirdronDataRequestWrapper(dataRequest: upload)))
                let request = upload.response { response in
                    if let innerError = response.error as NSError? {
                        completionHandler?(.failure(AirdronError(error: innerError)))
                        return
                    }
                    
                    if let innerError = response.error {
                        completionHandler?(.failure(AirdronError(error: innerError)))
                        return
                    }
                    
                    guard let statusCode = response.response?.statusCode else {
                        completionHandler?(.failure(AirdronError.unknownError()))
                        return
                    }
                    
                    if statusCode >= 200 && statusCode < 300 {
                        do {
                            let responseData = try serializer.deserialize(data: response.data)
                            completionHandler?(.success(responseData))
                        } catch let error as AirdronError {
                            completionHandler?(.failure(error))
                        } catch let error as NSError {
                            completionHandler?(.failure(AirdronError(error: error)))
                        } catch {
                            completionHandler?(.failure(AirdronError(error: error)))
                        }
                    } else {
                        completionHandler?(.failure(AirdronError.backendError(statusCode, responseData: response.data)))
                    }
                }
                upload.uploadProgress(queue: DispatchQueue.main, closure: { (progress) in
                    let progress = Double(progress.completedUnitCount) / Double(progress.totalUnitCount)
                    progressHandler?(progress)
                })
                request.resume()
            case .failure(let encodingError):
                completionHandler?(.failure(AirdronError.unknownError()))
                requestCompletionHandler?(.failure(AirdronError.unknownError()))
            }
        }
        
        self.sessionManager.upload(multipartFormData: multipartFormData,
                                  to: request.endpoint,
                                  headers: request.headers,
                                  encodingCompletion: encodingCompletion)
    }
    
    public func upload<T>(request: AirdronRequest,
                          data: Uploadable,
                          processData: @escaping ((_ responseData: Data) throws -> T),
                          completionHandler: ((Result<T>) -> Void)?,
                          requestCompletionHandler: ((Result<AirdronDataRequestWrapper>) -> Void)? = nil,
                          progressHandler: ((Double) -> Void)? = nil) {
        let multipartFormData = data.multipart()
        
        let encodingCompletion: ((SessionManager.MultipartFormDataEncodingResult) -> Void)? = { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                requestCompletionHandler?(.success(AirdronDataRequestWrapper(dataRequest: upload)))
                let request = upload.response { response in
                    if let innerError = response.error as NSError? {
                        completionHandler?(.failure(AirdronError(error: innerError)))
                        return
                    }
                    
                    if let innerError = response.error {
                        completionHandler?(.failure(AirdronError(error: innerError)))
                        return
                    }
                    
                    guard let statusCode = response.response?.statusCode else {
                        completionHandler?(.failure(AirdronError.unknownError()))
                        return
                    }
                    
                    if statusCode >= 200 && statusCode < 300 {
                        do {
                            guard let data = response.data else { throw AirdronError.dataIsEmptyError() }
                            let response = try processData(data)
                            completionHandler?(.success(response))
                        } catch let error as AirdronError {
                            completionHandler?(.failure(error))
                        } catch let error as NSError {
                            completionHandler?(.failure(AirdronError(error: error)))
                        } catch {
                            completionHandler?(.failure(AirdronError(error: error)))
                        }
                    } else {
                        completionHandler?(.failure(AirdronError.backendError(statusCode, responseData: response.data)))
                    }
                }
                upload.uploadProgress(queue: DispatchQueue.main, closure: { (progress) in
                    let progress = Double(progress.completedUnitCount) / Double(progress.totalUnitCount)
                    progressHandler?(progress)
                })
                request.resume()
            case .failure(let encodingError):
                completionHandler?(.failure(AirdronError.unknownError()))
                requestCompletionHandler?(.failure(AirdronError.unknownError()))
            }
        }
        
        self.sessionManager.upload(multipartFormData: multipartFormData,
                                   to: request.endpoint,
                                   headers: request.headers,
                                   encodingCompletion: encodingCompletion)
    }
}

extension UploadManager {
    
    public static func `default`() -> UploadManager {
        return UploadManager(configuration: URLSessionConfiguration.default)
    }
    
    public static func background() -> UploadManager {
        let configuration = URLSessionConfiguration.background(withIdentifier: self.defaultUploadBackgroundIdentifier)
        return UploadManager(configuration: configuration)
    }
}
