//
//  AddAlbumViewController.swift
//  Taoka
//
//  Created by Minic Relocusov on 29/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit
import Gallery
import Photos


class AddAlbumViewController : SimpleNavigationBar, UIScrollViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.specializations.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.specializations[row].name!
       
    }
    var count = 1 {
        didSet {
            print("COUNT \(count)")
        }
    }
    var onBackWithImages : (([Image]) -> Void)?
    var onBackWithAlbum : ((Album) -> Void)?
    let addLabel = UILabel()
    var typeString = ""
    var dateString = ""
    var isExporting = true
    let datePicker = UIDatePicker()
    let customPicker = UIPickerView()
    var activeField = UIView()
    private lazy var slideView = SlideView()
    var interactor: AddPhotoInteractorInput!
    private var viewModels : [SubCollectionViewCellModel] = []
    private lazy var nameTextField = OMGTextField.makeAlbumName()
    private lazy var dateField = TimeViewClass.makeDateAlbum()
    private lazy var typeField = TimeViewClass.makeTypeAlbum()
    let switchView = SwitchView.init(textLabel: "Собрать в альбом")
    var testView = UIView()
    var helperView = UIView()
    private lazy var addButton = SampleButton.init(textLabel: "Добавить")
    let loadingView = TaokaLoadingView()
    private lazy var scrollViewController = ScrollViewController()
    private lazy var collectionViewController = CollectionWaterfallViewController(layout: self.collectionViewLayout)
    private var collectionView: UICollectionView { return self.collectionViewController.collectionView! }
    private lazy var collectionViewLayout: AirdronCollectionViewWaterfallLayout = {
        let layout = AirdronCollectionViewWaterfallLayout()
        layout.minimumColumnSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -2)
        return layout
    }()
    

    private var apiService : ApiService
    init(apiService : ApiService) {
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func initialSetup() {
        super.initialSetup()
        self.setTitle(title: "Загрузка")
        self.setLargeTitleHidden()
        //self.view.addSubview(self.scrollViewController.scrollView)
        self.setTabBarHidden()
        //fbhdsjgfalsdfjdsabjfdsnbjfsda
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.scrollViewController.scrollView)
        self.view.addSubview(self.loadingView)
        self.view.addSubview(self.slideView)
        self.slideView.setRecognizer(height: 304)
        self.scrollViewController.scrollView.addSubview(self.switchView)
        self.scrollViewController.scrollView.addSubview(self.addButton)
        self.scrollViewController.scrollView.alwaysBounceHorizontal = false
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.collectionViewController.collectionView.isScrollEnabled = false
        self.collectionViewController.collectionView.register(cellClass: SubCollectionViewCell.self)
        self.collectionViewController.collectionView.register(cellClass: AddAlbumCollectionViewCell.self)
        self.collectionViewController.collectionView.register(cellClass: AddPhotoCollectionViewCell.self)
        self.scrollViewController.scrollView.addSubview(self.collectionViewController.collectionView)
        self.addButton.backgroundColor = Color.light.value
        self.dateField.setDefault(text: "Дата фотосессии")
        self.typeField.setDefault(text: "Тип фотосессии")
        self.scrollViewController.scrollView.addSubview(self.helperView)
        self.helperView.isHidden = true
        self.scrollViewController.scrollView.addSubview(self.nameTextField)
        self.scrollViewController.scrollView.addSubview(self.dateField)
        self.scrollViewController.scrollView.addSubview(self.typeField)
        self.addButton.mainLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.black.value).make(string: "Добавить")
        self.addButton.layer.cornerRadius = 10
        self.addButton.layer.masksToBounds = true
        self.toastPresenter.targetView = self.view
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.customPicker.delegate = self
        self.customPicker.dataSource = self
        self.slideView.animate = {
            [weak self] in
            guard let self = self else {return}
            self.view.animateLayout(duration: 0.5)
        }
      //  self.slideView.open?()
        self.addButton.addSubview(self.addLabel)
        self.addLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.black.value).make(string: "Добавить")
        self.nameTextField.alpha = 1
        self.dateField.alpha = 1
        self.typeField.alpha = 1
        self.nameTextField.isUserInteractionEnabled = true
        self.dateField.isUserInteractionEnabled = true
        self.typeField.isUserInteractionEnabled = true
        self.switchView.switchView.setOn(true, animated: false)
        self.switchView.onSwitchOn = {[weak self] in
            guard let self = self else {return}
            UIView.animate(withDuration: 0.5, animations: {
                
                self.nameTextField.alpha = 1
                self.typeField.alpha = 1
                self.dateField.alpha = 1
                self.nameTextField.isUserInteractionEnabled = true
                self.dateField.isUserInteractionEnabled = true
                self.typeField.isUserInteractionEnabled = true
            })
            self.setUpConstraints(fields: true)
            self.view.animateLayout(duration: 0.3)
        }
        self.switchView.onSwitchOff = {[weak self] in
            guard let self = self else {return}
            UIView.animate(withDuration: 0.5, animations: {
           
                self.nameTextField.alpha = 0
                self.typeField.alpha = 0
                self.dateField.alpha = 0
                self.nameTextField.isUserInteractionEnabled = false
                self.dateField.isUserInteractionEnabled = false
                self.typeField.isUserInteractionEnabled = false
                
            })
            self.setUpConstraints(fields: false)
            self.view.animateLayout(duration: 0.3)
        }
        
       // private var selectedRow : Int = 0
        self.slideView.setRightTitle(title: "Выбрать", completion: {[weak self] in
            guard let self = self else {return}
            if self.activeField == self.dateField {
                var text : String
                text = self.datePicker.date.month
                self.dateField.setText(text: text)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "YYYY-MM-dd"
                
                self.dateString = dateFormatter.string(from: self.datePicker.date)
                print(self.dateString)
                self.slideView.close?()
            } else if self.activeField == self.typeField {
                self.typeField.setText(text: self.specializations[self.customPicker.selectedRow(inComponent: 0)].name!)
                self.typeString = self.specializations[self.customPicker.selectedRow(inComponent: 0)].id!
                self.slideView.close?()
            }
            
        })
        self.slideView.setLeftTitle(title: "Отмена", completion: {[weak self] in
            guard let self = self else {return}
            self.slideView.close?()
        })
       // self.collectionViewController.collectionView.backgroundColor = Color.red.value
//        self.addButton.center.y = 100
        self.loadingView.isHidden = false
        self.scrollViewController.scrollView.layer.layoutIfNeeded()
        self.setupHandlers()
        self.slideView.addSubview(self.datePicker)
        self.slideView.addSubview(self.customPicker)
        self.datePicker.isHidden = true
        self.customPicker.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.uploadTouch))
        self.addButton.addGestureRecognizer(tap)
        self.render()
        
    }
    
    
    func configureTime(){
        self.datePicker.isHidden = false
        self.customPicker.isHidden = true
        self.datePicker.datePickerMode = .date
       // let calendar = Calendar.current
       // var components = DateComponents()
        
        //let maxDate = calendar.date(from: components)
        self.datePicker.maximumDate = Date()
      //  self.datePicker.minimumDate = Date()
    }
    
    func setupHandlers(){
        
        
        self.nameTextField.onBeginEditing = {
            [weak self] field in
            guard let self = self else {return}
            self.activeField = field
        }
        
        self.dateField.onEdditing = {
            [weak self] in
            guard let self = self else {return}
            self.customPicker.isHidden = true
            self.datePicker.isHidden = false
            self.nameTextField.resignResponder()
            self.activeField = self.dateField
            self.typeField.onEndEdditing?()
            self.configureTime()
            self.slideView.open?()
        }
        self.typeField.onEdditing = {[weak self] in
            guard let self = self else {return}
            self.datePicker.isHidden = true
            self.customPicker.isHidden = false
            self.activeField = self.typeField
            self.dateField.onEndEdditing?()
            self.nameTextField.resignResponder()
            self.configureCustom()
            self.slideView.open?()
        }
    }
    func configureCustom()
    {
       
        self.customPicker.reloadAllComponents()
    }
    func add(images: [Gallery.Image]) {
     //   self.isExporting = true
      //  self.uploadButton?.update(state: .disabled)
        let viewModels = self.interactor.makeViewModels(images: images)
        self.collectionViewController.collectionView.performBatchUpdates({
            viewModels.forEach { viewModel in
                self.collectionViewController.insert(viewModel,
                                                     for: self.collectionViewController.collectionView.numberOfItems(inSection: 0) - 1,
                                                     in: 0)
            }
        },
                                                                         completion: nil)
    }
    var onAddImages : Action?
    private func initialViewModels() {
        let addViewModel = AddAlbumCollectionViewCellModel { [weak self] in
            self?.onAddImages?()
        }
        let section = DefaultCollectionSectionViewModel(cellModels: [addViewModel])
        self.collectionViewController.update(viewModels: [section])
        
    }
    
    
    @objc func uploadTouch(){
        var canUpload = true
        canUpload = !isExporting
        if self.switchView.switchView.isOn {
            guard let text = self.nameTextField.text else {canUpload = false
                return
            }
            if text == "" {canUpload = false}
            if !self.dateField.didSet {
                canUpload = false
            }
            if !self.typeField.didSet {
                canUpload = false
            }
            
            if canUpload {
                self.loadingView.isHidden = false
                self.interactor.uploadImages(album: AlbumUpload.init(title: self.nameTextField.text!, specialization: self.typeString, date: self.dateString))
            }
            
        } else {
            if canUpload {
                self.loadingView.isHidden = false
                self.interactor.uploadImages(album: nil)
            }
            
        }
        
        
        self.isExporting = true
    }
   
    private var specializations : [Specialization] = []
    func render(){
        self.initialViewModels()
        self.apiService.specialization() {[weak self] result in
            switch result {
            case .success(let response):
                self?.specializations = response
                self?.loadingView.isHidden = true
            case .failure(let error):
                print(error)
                self?.showToastErrorAlert(error)
            }
        }
        self.setUpConstraints(fields: true)
    }
    
    func setUpConstraints(fields: Bool) {
        super.setUpConstraints()
        let count = self.count
        let height = (UIScreen.main.bounds.size.width - 2) / 3.0
        
        let counted : Int = roundNumber(num: Float(count) / 3.0)
        let finalHeight = ( Float(height) * Float(counted) ) + Float(counted - 1)
        
        
        let maxHeight = UIScreen.main.bounds.size.height
        var fHeight : CGFloat = CGFloat(finalHeight + 64)

            
        if Device.isIphoneXSmax || Device.isIphoneX {
            fHeight += 60
        }
            if fields {
                fHeight += 173
            }
        
        let btnHeight : CGFloat = 56 + 58
        var offset = (maxHeight - fHeight - btnHeight) < 40 ? 40 : (maxHeight - fHeight - btnHeight - 58)
        print("Height: \(height)\ncounted: \(counted)\nfinalHeight: \(finalHeight)\nПромежуток: \((Float(height) / Float(count)) / 3.0)" )
        print("OFFSET \(offset)")
        if offset < 40 {
            offset = 40
        }
        self.scrollViewController.scrollView.snp.removeConstraints()
        self.loadingView.snp.removeConstraints()
        self.helperView.snp.removeConstraints()
        self.addButton.snp.removeConstraints()
        self.nameTextField.snp.removeConstraints()
        self.dateField.snp.removeConstraints()
        self.typeField.snp.removeConstraints()
        self.collectionViewController.collectionView.snp.removeConstraints()
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.width.equalToSuperview()
            
            
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.helperView.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalToSuperview()
            $0.width.equalToSuperview().offset(0)
            
        }
        self.collectionViewController.collectionView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(finalHeight)
          //  $0.bottom.equalToSuperview()
        }
        self.switchView.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalTo(self.collectionView.snp.bottom).offset(32)
            $0.height.equalTo(32)
        }
        self.nameTextField.snp.makeConstraints{
            $0.top.equalTo(self.switchView.snp.bottom).offset(7)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.dateField.snp.makeConstraints{
            $0.top.equalTo(self.nameTextField.snp.bottom).offset(7)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(60)
        }
        self.typeField.snp.makeConstraints{
            $0.top.equalTo(self.dateField.snp.bottom).offset(7)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(60)
        }
        self.slideView.snp.makeConstraints{
            self.slideView.topConstr =  $0.top.equalTo(self.view.snp.bottom).constraint
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(304)
        }
        self.addButton.snp.makeConstraints{
            if !fields {
                $0.top.equalTo(self.switchView.snp.bottom).offset(offset)
            } else {
                $0.top.equalTo(self.typeField.snp.bottom).offset(offset)
            }
           // $0.centerY.equalToSuperview().offset(200)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
            $0.bottom.equalToSuperview().offset(-58)
        }
        self.datePicker.snp.makeConstraints{
            $0.left.equalTo(self.slideView.snp.left).offset(30)
            $0.top.equalTo(self.slideView.snp.top).offset(54)
            $0.right.equalTo(self.slideView.snp.right).offset(-30)
        }
        
        self.customPicker.snp.makeConstraints{
            $0.left.equalTo(self.slideView.snp.left).offset(30)
            $0.top.equalTo(self.slideView.snp.top).offset(54)
            $0.right.equalTo(self.slideView.snp.right).offset(-30)
        }
        self.addLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        
    }
}
extension AddAlbumViewController: AddPhotoInteractorOutput {
  
    
    
    
    func didUpdateViewModels(count: Int) {
        self.count += count
        self.setUpConstraints(fields: self.switchView.switchView.isOn)
        self.view.animateLayout(duration: 0.5)
    }
    
    func didUploadAlbum(album: Album) {
        print(album)
        self.onBackWithAlbum?(album)
    }
    
    func didUploadImagesInAlbum(images: [Image]) {
        print(images)
        self.onBackWithImages?(images)
    }
    
    
    
    
    func didReceive(error: AirdronError) {
        //self.uploadButton?.update(state: .normal)
       // self.showToastErrorAlert(error)
        print("ERROR HERE \n\n \(error) \n\n")
        self.isExporting = false
    }
    
    func didClear(asset: PHAsset, cell: UICollectionViewCell) {
        guard let index = self.collectionView.indexPath(for: cell) else { return }
        self.collectionViewController.delete(index.item, in: index.section)
        self.count -= 1
        self.setUpConstraints(fields: self.switchView.switchView.isOn)
    }
    
    func didSelect(image: UIImage) {
      //  self.onFullPhoto?(image)
    }
    
    func didUpdateUpload(count: Int) {
        
    }
    
    func didUploadUnpaidImages(images: [Image]) {
        //self.onUploadedImages?(response, images)
        print(images)
    }
    
    func didUploadPaidImages(images: [Image]) {
        //self.onPayment?(self.albumShort, images)
    }
    
    func didExportPhotos() {
       self.isExporting = false
    
    }
}
