
//
//  ApproveView.swift
//  OMG
//
//  Created by Minic Relocusov on 21/04/2019.
//  Copyright © 2019 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit


class ServiceCheckView : AirdronView {
    
    var textLabel  = UILabel()
    var textDecsr = UILabel()
    let switchOnOff = UISwitch(frame:CGRect(x: 150, y: 150, width: 0, height: 0))
    
    init(textLabel : String, textDescr : String) {
        self.textLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.black.value).make(string: textLabel)
        self.textDecsr.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: textDescr)
        super.init(frame: .zero)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.textLabel.numberOfLines = 0
        self.textDecsr.numberOfLines = 0
        switchOnOff.onTintColor = .blue
        self.backgroundColor = Color.white.value
        self.addSubview(self.textLabel)
        self.addSubview(self.textDecsr)
        self.addSubview(self.switchOnOff)
        
        self.setupConstraints()
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.textLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(25)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
        self.textDecsr.snp.makeConstraints{
            $0.top.equalTo(textLabel.snp.bottom).offset(8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-74)
            $0.bottom.equalToSuperview()
        }
        
        self.switchOnOff.snp.makeConstraints{
            $0.top.equalToSuperview().offset(22)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
    }
    
    override var intrinsicContentSize : CGSize{
        let width = UIView.noIntrinsicMetric
        return CGSize(width: width, height: 50)
    }
}



