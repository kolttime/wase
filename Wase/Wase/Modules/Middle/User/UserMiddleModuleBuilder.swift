//
//  AuthModuleBuilder.swift
//  Taoka
//
//  Created by Artem Myachkov on 07/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit

class UserMiddleModuleBuilder{
    
    
    private let apiService: ApiService
    private let sessionManager: TaokaUserSessionManager
    
    init(apiService: ApiService,
         sessionManager: TaokaUserSessionManager) {
        self.apiService = apiService
        self.sessionManager = sessionManager
    }
    
    func makePhotographProfileModule(id : String, photoId : String) -> PhotographProfileViewController {
        return PhotographProfileViewController(apiService: self.apiService, userId : id, photoId: photoId)
    }
    
    func makePhotoAlbum(images : [Image]?, id : String?, scrollTo : Int?, userId : String) -> PhotoAlbumViewController {
        return PhotoAlbumViewController(userId : userId ,images : images,  id: id, sctollTo: scrollTo , apiService: self.apiService )
    }
    
    func makeLaunchModule() -> UserMiddleViewController {
        return UserMiddleViewController(apiService: self.apiService)
    }
    
    func makeUserMiddleModule() -> UserMiddleViewController {
        return UserMiddleViewController(apiService: self.apiService)
    }
    
    func mskeUserCreateModule() -> UserCreatePhotoViewController {
        return UserCreatePhotoViewController(apiService: self.apiService)
    }
    
    func makeTineModule() -> TimeViewController {
        return TimeViewController()
    }
    
    func makePlaceModule() -> BringPlaceViewController {
        return BringPlaceViewController(apiService: self.apiService, userModel: self.sessionManager.user!)
    }
    
    func makeWorkerModule() -> WorkerTypeViewController {
        return WorkerTypeViewController()
    }
    
    func makeBudgetModule(photoCreation : CreatePhotosession) -> BudgetViewController {
        return BudgetViewController(photoCreation: photoCreation, apiService: self.apiService, userModel: self.sessionManager.user!)
    }
    
    func makeSecondStepModule(photoCreation : CreatePhotosession) -> ReadySecondStepViewController {
        return ReadySecondStepViewController(photoCreation: photoCreation, apiService: self.apiService)
    }
    
    func makeSelectionModule(type : UserType, id : String) -> SelectionViewController {
        return SelectionViewController(apiService: self.apiService, type: type, id: id)
    }
    
    func makePageUserCreateViewModule(initialViewControllers : [AirdronViewController], count : Int) -> AirdronPageViewController {
        let PageViewControllers = AirdronPageViewController(count: count)
        PageViewControllers.setViewContrtollers(viewControllers: initialViewControllers, count: count)
        return PageViewControllers
    }
    
}


