//
//  SearchableCountryTableViewCell.swift
//  OMG
//
//  Created by Andrew Oparin on 13/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

class SelectionCell: TableViewCell {
    var selection : ((String) -> Void)?
    var selectModel: SelectionCellModel?
    var id : String?
    var imageq = UIImageView()
    
    private lazy var mainLabel = UILabel()
    private lazy var countLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.imageq)
        self.contentView.addSubview(self.mainLabel)
        self.contentView.addSubview(self.countLabel)
        self.imageq.backgroundColor = Color.light.value
        self.imageq.contentMode = .scaleAspectFill
        self.imageq.layer.cornerRadius = 22
        self.imageq.layer.masksToBounds = true
        self.setupConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.imageq.snp.makeConstraints{
            $0.left.equalToSuperview().offset(16)
            $0.top.equalToSuperview().offset(10)
            $0.height.equalTo(44)
            $0.width.equalTo(44)
        }
        self.mainLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(13)
            $0.left.equalTo(imageq.snp.right).offset(8)
        }
        
        self.countLabel.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(4)
            $0.left.equalTo(imageq.snp.right).offset(8)
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! SelectionCellModel
        self.selectModel = viewModel
        self.imageq.kf.setImage(with: (viewModel.image?.original) ?? URL(string: ""))
        self.id = viewModel.id
        self.selection = viewModel.selectionHandler
        self.mainLabel.text = viewModel.mainLabel
        self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: self.mainLabel.text ?? "")
        self.countLabel.text = viewModel.countLabel
        self.countLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: self.countLabel.text ?? "")
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 64
    }
    
    override func didSelect() {
        self.selection?(self.id!)
    }
}

struct SelectionCellModel : TableCellViewModel {
    var cellType: TableViewCell.Type { return SelectionCell.self }
    var mainLabel: String
    var countLabel : String
    var id : String
    var image : Image?
    let selectionHandler: ((String) -> Void)?
}




