//
//  PhotoPhotosessionsMiddleView.swift
//  Taoka
//
//  Created by Minic Relocusov on 23/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit



class PhotoPhotosessionsMiddleView : AirdronView {
    
    
    
    let topLabel = UILabel()
    
    let costLabel = UILabel()
    
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.topLabel)
        self.addSubview(self.costLabel)
        self.topLabel.numberOfLines = 0
        self.setupConstraints()
        self.backgroundColor = Color.white.value
    }
    
    
    func set(top : String, middle : String, bottom : String, cost : String){
        self.topLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: "\(top)\n\(middle)\n\(bottom)")
        self.costLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: cost)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.topLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(9)
            $0.left.equalToSuperview().offset(12)
        }
        self.costLabel.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-12)
            $0.bottom.equalToSuperview().offset(-12)
        }
    }
}
