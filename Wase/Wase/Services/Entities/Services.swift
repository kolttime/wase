//
//  Services.swift
//  Taoka
//
//  Created by Роман Макеев on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct Services: Codable {
    
    
    var specializations: [Specialization]!
    var workHours: String
    var workDays: [Int]
    var price: Int?
    var vacant: Bool
    var mobile: Bool?
    
    
    init(specializations : [Specialization]!, workHours : String, workDays : [Int], price : Int, vacant : Bool, mobile : Bool){
        self.specializations = specializations
        self.workDays = workDays
        self.workHours = workHours
        self.price = price
        self.vacant = vacant
        self.mobile = mobile
    }
    
}

extension Services {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Services.self, from: data)
    }
}

extension Array where Element == Services {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Services].self, from: data)
    }
}
