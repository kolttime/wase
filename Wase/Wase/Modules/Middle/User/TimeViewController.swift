//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class TimeViewController : AirdronViewController, UIScrollViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var actualArray = [String]()
    
    var onNext : ((Date?, String, String) -> Void)?
    
    public var fullFields = false
    
    var buffer = [0,0]

    var fullArray = [false,false,false]
    
    var firstIndex = Int()
    var secondIndex = Int()
    
    var fullHours = ["1 час", "2 часа", "3 часа", "4 часа", "5 часов", "6 часов", "7 часов", "8 часов", "9 часов", "10 часов", "11 часов", "12 часов"]
    var actualHours = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"]
    
    var itemSelectedOne = String()
    var itemSelectedTwo = String()
    
 
    let datePicker = UIDatePicker()
    var date = TimeViewClass()
    var begin = TimeViewClass()
    var timing = TimeViewClass()
    var slideView = SlideView()
    var scrollViewController = ScrollViewController()
    var activeField = 0
    
    var customPicker = UIPickerView()
    
    
    let mainLabel = UILabel()
    let helperView = UIView()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        firstIndex = 0
        secondIndex = 0
        
        self.date.textLabel.isHidden = true
        self.begin.textLabel.isHidden = true
        self.timing.textLabel.isHidden = true
        
        self.view.backgroundColor = Color.white.value
        
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.slideView.setRecognizer(height: 304)
        let fff  : Action? = {[weak self] in
            switch self?.activeField {
            case 1:
                self?.date.textDecsr.text = self?.datePicker.date.month
                self?.date.textLabel.isHidden = false
                self?.fullArray[0] = true
                print("First")
            case 2:
                self?.begin.textDecsr.text = self!.itemSelectedOne
                self?.buffer[0] = self!.firstIndex
                self?.begin.textLabel.isHidden = false
                self?.fullArray[1] = true
                print("Second")
            case 3:
                self?.timing.textDecsr.text = self!.itemSelectedTwo
                self?.buffer[1] = self!.secondIndex
                self?.timing.textLabel.isHidden = false
                self?.fullArray[2] = true
                print("Third")
            default:
                print("fff")
            }
            
            if self?.fullArray[0] == true && self?.fullArray[1] == true && self?.fullArray[2] == true {
                self?.fullFields = true
            }
            
            self?.slideView.close?()
        }
        
        let closeAll : Action? = {[weak self] in
            
            switch self?.activeField {
            case 1:
                print("11")
            case 2:
                print("22")
            case 3:
                print("33")
            default:
                print("boop")
            }
            
            self?.slideView.close?()
            
        }
        
        customPicker.delegate = self
        customPicker.dataSource = self
        
        self.slideView.addSubview(customPicker)
        self.slideView.addSubview(datePicker)
        
        self.itemSelectedOne = actualHours[0]
        self.itemSelectedTwo = fullHours[0]
        
        self.slideView.setLeftTitle(title: "Отмена", completion: closeAll)
        self.slideView.setRightTitle(title: "Выбрать", completion: fff)
        
        self.mainLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.colored(color: Color.black.value).make(string: "Когда хотите провести?")
        
        mainLabel.numberOfLines = 0
        
        helperView.isHidden = true
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        self.view.addSubview(self.scrollViewController.scrollView)
        scrollViewController.scrollView.showsHorizontalScrollIndicator = false
        scrollViewController.scrollView.addSubview(mainLabel)
        scrollViewController.scrollView.addSubview(timing)
        scrollViewController.scrollView.addSubview(begin)
        scrollViewController.scrollView.addSubview(date)
        scrollViewController.scrollView.delegate = self
        scrollViewController.scrollView.addSubview(self.helperView)
        self.slideView.animate = {[weak self] in
            self?.view.animateLayout(duration: 0.5)
        }
        
        self.view.addSubview(self.slideView)
        
        self.slideView.defaultCenter = self.view.frame.maxY + (100)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.opppenWheel))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.opppenBegin))
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.opppenTiming))
        
        self.date.textLabel.attributedText =  CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "День")
        self.date.textDecsr.attributedText =  CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "День")
        self.date.addGestureRecognizer(tap1)
        
        self.begin.textLabel.attributedText =  CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Начало")
        self.begin.textDecsr.attributedText =  CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Начало")
        self.begin.addGestureRecognizer(tap2)
        
        self.timing.textLabel.attributedText =  CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Продолжительность")
        self.timing.textDecsr.attributedText =  CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Продолжительность")
        self.timing.addGestureRecognizer(tap3)
        
        self.datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)
        
        self.setupHandlers()
        self.setUpConstraints()
    }
    
    
    func configureBegin(){
        actualArray = actualHours
        self.customPicker.selectRow(self.buffer[0], inComponent: 0, animated: true)
        self.datePicker.isHidden = true
        self.customPicker.isHidden = false
        self.customPicker.reloadAllComponents()
    }
    
    func configureTiming(){
        actualArray = fullHours
        self.customPicker.selectRow(self.buffer[1], inComponent: 0, animated: true)
        self.customPicker.reloadAllComponents()
        self.datePicker.isHidden = true
        self.customPicker.isHidden = false
    }
    
    func configureTime(){
        self.datePicker.isHidden = false
        self.customPicker.isHidden = true
        self.datePicker.datePickerMode = .date
        let calendar = Calendar.current
        var components = DateComponents()
        components.day = 31
        components.month = 12
        components.year = 2020
        components.hour = 23
        components.minute = 59
        let maxDate = calendar.date(from: components)
        self.datePicker.maximumDate = maxDate
        self.datePicker.minimumDate = Date()
    }
    @objc func opppenWheel(){
        self.slideView.open?()
        self.date.separator.backgroundColor = Color.black.value
        self.begin.separator.backgroundColor = Color.light.value
        self.timing.separator.backgroundColor = Color.light.value
        self.configureTime()
        print("A")
        activeField = 1
    }
    
    @objc func opppenBegin(){
        self.slideView.open?()
        self.date.separator.backgroundColor = Color.light.value
        self.begin.separator.backgroundColor = Color.black.value
        self.timing.separator.backgroundColor = Color.light.value
        self.configureBegin()
        print("B")
        activeField = 2
    }
    
    @objc func opppenTiming(){
        self.slideView.open?()
        self.date.separator.backgroundColor = Color.light.value
        self.begin.separator.backgroundColor = Color.light.value
        self.timing.separator.backgroundColor = Color.black.value
        print("C")
        self.configureTiming()
        activeField = 3
    }
    
    @objc func datePickerChanged(picker: UIDatePicker) {
      
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return actualArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return actualArray[row]
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if activeField == 2 {
            firstIndex = row
            itemSelectedOne = actualHours[row]
        }
        
        if activeField == 3 {
            secondIndex = row
            itemSelectedTwo = fullHours[row]
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.slideView.close?()
    }
    
     func setUpConstraints(){
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.mainLabel.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
        self.slideView.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            self.slideView.topConstr = $0.top.equalTo(self.view.snp.bottom).constraint
            $0.height.equalTo(304)
        }
        self.helperView.snp.makeConstraints{
            $0.top.equalTo(self.mainLabel.snp.bottom).offset(50)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        
        self.date.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(ViewSize.sideOffset)
            $0.height.equalTo(67)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        self.begin.snp.makeConstraints{
            $0.top.equalTo(date.snp.bottom)
            $0.height.equalTo(67)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        self.timing.snp.makeConstraints{
            $0.top.equalTo(begin.snp.bottom)
            $0.height.equalTo(67)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        self.datePicker.snp.makeConstraints{
            $0.left.equalTo(self.slideView.snp.left).offset(30)
            $0.top.equalTo(self.slideView.snp.top).offset(54)
            $0.right.equalTo(self.slideView.snp.right).offset(-30)
        }
        
        self.customPicker.snp.makeConstraints{
            $0.left.equalTo(self.slideView.snp.left).offset(30)
            $0.top.equalTo(self.slideView.snp.top).offset(54)
            $0.right.equalTo(self.slideView.snp.right).offset(-30)
        }
        
    }
    
    func setupHandlers(){
    }
    
}

extension Date {
    var month: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale =  Locale(identifier: "ru_MD")
        dateFormatter.dateFormat = "d MMMM"
        return dateFormatter.string(from: self)
    }
    var dateString : String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale =  Locale(identifier: "ru_MD")
        dateFormatter.dateFormat = "YYYY-MM-dd"
        return dateFormatter.string(from: self)
    }
}



