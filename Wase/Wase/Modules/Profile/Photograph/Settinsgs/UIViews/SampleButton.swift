//
//  AddButton.swift
//  Taoka
//
//  Created by Minic Relocusov on 11/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class SampleButton : UIButton {
    
    var mainLabel = UILabel()
    
    override init(frame : CGRect){
        super.init(frame: frame)
        self.initialSetup()
    }
    
    init(textLabel : String) {
        self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.white.value).make(string: textLabel)
        super.init(frame: .zero)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialSetup(){
        self.addSubview(self.mainLabel)
        self.backgroundColor = Color.light.value
       
        self.setupConstraints()
    }
    
    func setupConstraints(){
        self.mainLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
    }
}

