//
//  Types.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 10/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

public typealias Identifier = Int64
