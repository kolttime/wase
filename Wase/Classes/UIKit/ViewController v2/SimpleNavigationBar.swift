//
//  SimpleNavigationBar.swift
//  Taoka
//
//  Created by Minic Relocusov on 09/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit

class SimpleNavigationBar : AirdronViewController, ToastAlertPresentable {
    
    
    var toastPresenter: ToastAlertPresenter = ToastAlertPresenter()
    
    
    
    var tabBarHidden : Bool = {
        
       return false
    }()
    var onLargetitle : Action?
    var onNormalTitle : Action?
    
    var largeTitle : Bool = true
    
  
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.layoutMargins.left = ViewSize.sideOffset
    
        self.view.backgroundColor = Color.white.value
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = Color.accent.value
        self.navigationController?.navigationBar.barTintColor = Color.white.value
        self.tabBarController?.tabBar.isHidden = false
       
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.backBarButtonItem?.setTitleTextAttributes(CustomFont.bodyRegular17.attributes, for: .normal)
        self.initialSetup()
    }
    
    var large = false
    var normal = false
    
    func setOfff(_ scrollView: UIScrollView) {
        guard let navHeight = self.navigationController?.navigationBar.frame.height else {return}
        if navHeight > CGFloat(44) && !self.large{
            self.large = true
            self.normal = false
            self.onLargetitle?()
        } else if navHeight <= 44 && !self.normal{
            self.normal = true
            self.large = false
            self.onNormalTitle?()
        }
    }
    func setTabBarHidden(){
        guard let tabBarController = self.tabBarController else {return}
        tabBarController.tabBar.isHidden = true
        self.tabBarHidden = true
    }
    func setTabBar(){
        guard let tabBarController = self.tabBarController else {return}
        tabBarController.tabBar.isHidden = false
        self.tabBarHidden = false
    }
    func setLargeTitle(subTitle : String? = nil){
        self.largeTitle = true
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController!.navigationBar.largeTitleTextAttributes = CustomFont.largeTitle.attributes
        self.navigationController!.navigationBar.sizeToFit()
        self.navigationController?.navigationBar.autoresizesSubviews = true
        self.navigationController?.navigationBar.layoutSubviews()
        
//        for navItem in(self.navigationController?.navigationBar.subviews)! {
//            for itemSubView in navItem.subviews {
//                if let largeLabel = itemSubView as? UILabel {
//                    let combination : NSMutableAttributedString = NSMutableAttributedString()
//                    print("HERE \(largeLabel.text)")
//                    largeLabel.numberOfLines = 0
//                    largeLabel.attributedText = combination
//                    largeLabel.lineBreakMode = .byWordWrapping
//                }
//            }
//        }
        
    }
    
    
    func getBottomOffset() -> CGFloat{
        return self.tabBarController?.tabBar.frame.height ?? 0
    }
    
    func setLargeTitleHidden(){
        self.largeTitle = false
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    override func viewDidAppear(_ animated: Bool) {
        if self.tabBarHidden {
            self.setTabBarHidden()
        } else {
            self.setTabBar()
        }
        if self.largeTitle {
            self.setLargeTitle()
        } else {
            self.setLargeTitleHidden()
        }
    }
    func setUpConstraints(){
        
      //  self.view.frame = CGRect.init(x: 0, y: (self.navigationController?.navigationBar.bounds.size.height ?? 0 + (self.navigationController?.toolbar.bounds.size.height ?? 0)), width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - (self.navigationController?.navigationBar.bounds.size.height ?? 0 + (self.navigationController?.toolbar.bounds.size.height ?? 0)))
    }
    func setTitle(title: String) {
        self.title = title
    }
    
    func setBackTitle(title: String){
        let button = UIBarButtonItem.init(title: title, style: .done, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = button
        self.navigationItem.backBarButtonItem?.setTitleTextAttributes(CustomFont.bodyRegular17.attributes.colored(color: Color.accent.value), for: .normal)
    }
    func setRightButton(view : UIView){
        let button = UIBarButtonItem.init(customView: view)
        self.navigationItem.rightBarButtonItem = button
    }
    var middleView : UIView?
    func setMiddleButton(view : UIView, count : Int){
        let halfWidth = UIScreen.main.bounds.size.width / 2
        let halfViewWidth = CGFloat(  ((8 * count) + (10 * (count - 1))) / 2 )
        let offset = halfWidth - halfViewWidth
        self.middleView = view
        self.navigationController?.navigationBar.addSubview(middleView!)
        view.snp.makeConstraints{
            $0.left.equalToSuperview().offset( offset)
            $0.centerY.equalToSuperview()
        }
    }
    
    var onRight : Action?
    func setRighttitle(title : String, color : UIColor? = nil, completion : Action?){
        var cColor : UIColor = Color.accent.value
        if color == nil{
            cColor = Color.accent.value
        }
        let button = UIBarButtonItem.init(title: title, style: .done, target: self, action: #selector(self.onRightButton))
        self.onRight = completion
        self.navigationItem.rightBarButtonItem = button

    self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(CustomFont.bodyRegular17.attributes.colored(color: cColor), for: .normal)
        
    }
    @objc func onRightButton(){
        self.onRight?()
    }
    func deleteMiddleButtons(){
        self.middleView?.removeFromSuperview()
        }
    func initialSetup(){
      //  self.setUpConstraints()
    }
    func getTopOffset() -> CGFloat {
        return (self.navigationController?.navigationBar.bounds.size.height ?? 0) + ( UIApplication.shared.statusBarFrame.height)
    }
    @objc func some() {
        
    }
}


class AirdronNavigationBar : UINavigationBar {
    
    
    var onLargeTitle : Action?
    var onNormalTitle : Action?
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        updateLargeTitle()
    }
    private func updateLargeTitle() {
        if #available(iOS 11.0, *) {
            if frame.height >= 44 {
                // code logic when large title is visible
            } else {
                // code logic when large title is hidden
            }
        }
    }
}
