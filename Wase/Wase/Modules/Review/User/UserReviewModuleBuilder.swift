//
//  AuthModuleBuilder.swift
//  Taoka
//
//  Created by Artem Myachkov on 07/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit

class UserReviewModuleBuilder{
    
    func makeUserReviewModule() -> UserReviewViewController {
        return UserReviewViewController()
    }
    
}
