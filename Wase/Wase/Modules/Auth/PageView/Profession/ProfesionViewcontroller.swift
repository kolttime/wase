//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class ProfessionViewController : AirdronViewController {
    
    var onNext : (() -> Void)?
    
    var onUserType : Action?
    
    var mainLabel = UILabel()
    
    var firstLabel = checkPlanView()
    
    var secondLabel = checkPlanView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        view.addSubview(mainLabel)
        view.addSubview(firstLabel)
        view.addSubview(secondLabel)
        
        mainLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.make(string: "Подскажите, какой у вас план?")
        mainLabel.numberOfLines = 0
        
        firstLabel.nameLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "Хочу заказать фотосессию")
        firstLabel.descrLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Вы можете найти подходящих профессионалов и заказaть у них проект")
        
        secondLabel.nameLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "Хочу предоставлять услуги")
        secondLabel.descrLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Клиенты сами смогут находить и обращаться к вам с заказами")
        
        self.setUpConstraints()
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.onUser))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.nextView))
        
        firstLabel.addGestureRecognizer(tap1)
        secondLabel.addGestureRecognizer(tap2)
        
    }
    
    
    @objc func onUser(){
        self.onUserType?()
    }
    @objc func nextView(){
        print("~~~~~")
        self.onNext?()
    }
    
    func setUpConstraints(){
       
        mainLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.top.equalToSuperview()
        }
        
        firstLabel.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(84)
            $0.top.equalTo(mainLabel.snp.bottom).offset(32)
        }
        
        secondLabel.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(84)
            $0.top.equalTo(firstLabel.snp.bottom).offset(ViewSize.sideOffset)
        }
        
    }
}
