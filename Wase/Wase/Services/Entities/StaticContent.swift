//
//  userCreation.swift
//  Taoka
//
//  Created by Роман Макеев on 14/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct StaticContent: Codable {
    
    
    var name : String?
    var title: String?
    var text: String?
    
    
   
    
    init(name : String?, title : String?, text : String?) {
        self.name = name
        self.title = title
        self.text = text
    }
    
}
extension StaticContent {
     init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(StaticContent.self, from: data)
    }
}

extension Array where Element == StaticContent {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([StaticContent].self, from: data)
    }
}
