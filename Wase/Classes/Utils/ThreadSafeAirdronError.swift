//
//  ThreadSafeAirdronError.swift
//  OMG
//
//  Created by Andrew Oparin on 18/11/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

class ThreadSafeAirdronError {
    
    private var error: AirdronError?
    private static let queue = DispatchQueue(label: "Airdron.threadSafeError", attributes: .concurrent)
    
    func set(error: AirdronError?) {
        ThreadSafeAirdronError.queue.async(flags: .barrier) {
            self.error = error
        }
    }
    
    func getError() -> AirdronError? {
        var error: AirdronError?
        ThreadSafeAirdronError.queue.sync {
            error = self.error
        }
        return error
    }
    
    func reset() {
        ThreadSafeAirdronError.queue.async(flags: .barrier) {
            self.error = nil
        }
    }
}
