//
//  SubCollectionViewCell.swift
//  Taoka
//
//  Created by Роман Макеев on 13/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotoPhotosessionsCollectionViewCell : CollectionViewCell {
    
    var onSelect : Action?
    let nameLabel = UILabel()
    let containerView = UIView()
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.nameLabel)
       // self.containerView.addSubview(self.nameLabel)
        self.backgroundColor = Color.white.value
        self.containerView.backgroundColor = Color.white.value
        self.contentView.layoutIfNeeded()
       // self.layoutIfNeeded()
        self.setupConstraints()
    }
    
    override func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! PhotoPhotosessionsCollectionViewCellModel
        self.onSelect = viewModel.selectionHandler
        self.nameLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: viewModel.name)
    }
    override func setupConstraints() {
        super.setupConstraints()
        
        self.nameLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
    }
    
    override func didSelect() {
        self.onSelect?()
    }
}
extension PhotoPhotosessionsCollectionViewCell: WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel, width: CGFloat) -> CGFloat {
        return 40
    }
    
}
struct PhotoPhotosessionsCollectionViewCellModel : CollectionCellViewModel {
    var cellType: CollectionViewCell.Type { return PhotoPhotosessionsCollectionViewCell.self }
    var name : String
    var selectionHandler : Action?
}
