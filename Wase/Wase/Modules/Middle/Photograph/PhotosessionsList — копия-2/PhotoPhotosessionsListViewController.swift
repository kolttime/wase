//
//  PhotoPhotosessionsListViewController.swift
//  Taoka
//
//  Created by Minic Relocusov on 23/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotographMiddleViewController : SimpleNavigationBar {
    
    
    
    private var inboxModels : [PhotoPhotosessionsCellModelType1]?
    private var waitingModels : [PhotoPhotosessionsCellModelType1]?
    private var activeModels : [PhotoPhotosessionsCellModelType1]?
    private var endedModels : [PhotoPhotosessionsCellModelType1]?
    
    let headerView = UserPhotosessionsScrollView()
    private lazy var tableViewController = TableViewController()
    let separator = UIView()
    let activeView = UIView()
    var onNext : ((String) -> Void)?
    private var loadingView = TaokaLoadingView()
    var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 16
        layout.minimumLineSpacing = 16
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    private var apiService : ApiService
     init(apiService : ApiService) {
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func initialSetup() {
        super.initialSetup()
        self.add(self.tableViewController)
        self.tableViewController.tableView.register(cellClass: PhotoPhotosessionsCellType1.self)
        
        
        self.collectionView.register(PhotoPhotosessionsCollectionViewCell.self, forCellWithReuseIdentifier: "hello")
        self.toastPresenter.targetView = self.view
        self.collectionView.backgroundColor = Color.white.value
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        //self.collectionView.setContentOffset(CGPoint(x: 24, y: 0), animated: false)
        //self.tableViewController.tableView.setContentOffset(CGPoint(x: 0, y: 50), animated: false)
        self.setTitle(title: "Фотосессии")
        self.setBackTitle(title: "Фотосессии")
        self.setLargeTitle()
        //self.tableViewController.view.addSubview(self.headerView)
        self.tableViewController.tableView.contentInset = UIEdgeInsets(top: 48, left: 0, bottom: 0, right: 0)
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: ViewSize.sideOffset, bottom: 0, right: ViewSize.sideOffset)
        //self.collectionViewController
        //self.headerView.backgroundColor = Color.red.value
      //  self.tableViewController.tableView.tableHeaderView = self.headerView
        self.collectionView.showsVerticalScrollIndicator = false
        self.collectionView.showsHorizontalScrollIndicator = false
        let width : CGFloat = {
            let lbl = UILabel()
            lbl.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Входящие")
            return lbl.textWidth()
        }()
        self.separator.backgroundColor = Color.grey.value
        self.collectionView.addSubview(self.separator)
        //self.separator.layer.zPosition = 999999
        self.collectionView.bringSubviewToFront(self.separator)
        self.collectionView.addSubview(self.activeView)
        self.activeView.frame = CGRect(x: 0, y: 38, width: width, height: 2)
        self.activeView.backgroundColor = Color.black.value
        self.collectionView.bringSubviewToFront(self.activeView)
        self.navigationController!.view.addSubview(self.collectionView)
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        self.view.addSubview(self.loadingView)
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.loadingView.isHidden = true
        self.setUpConstraints()
        
        let selection = {[weak self] in
            self?.onNext?("fdsfds")
            print("FFFFFFFFFFFFFF")
        }
        
        let viewModels = [PhotoPhotosessionsCollectionViewCellModel(name: "Входящие", selectionHandler: selection), PhotoPhotosessionsCollectionViewCellModel(name: "Ожидаемые", selectionHandler: selection), PhotoPhotosessionsCollectionViewCellModel(name: "Активные", selectionHandler: nil), PhotoPhotosessionsCollectionViewCellModel(name: "Завершенные", selectionHandler: nil)]
        let section = DefaultCollectionSectionViewModel(cellModels: viewModels)
       // self.collectionViewController.update(viewModels: [section])
        
       // self.view.addSubview(self.collectionView)
        self.collectionView.layoutSubviews()
        self.collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .right)
        self.render(tabIndex: .inbox)
       // self.render()
    }
    
    
    func render(tabIndex : TabIndex) {
        
        var viewModels : [TableCellViewModel] = []
        
        self.loadingView.isHidden = false
        switch tabIndex {
        case .inbox:
            if self.inboxModels != nil {
                viewModels = self.inboxModels!
                let section = DefaultTableSectionViewModel(cellModels: viewModels)
                self.tableViewController.update(viewModels: [section])
                self.loadingView.isHidden = true
                break
            }
            else {
                self.apiService.getPhotoPhotosessions(tabIndex: tabIndex) {[weak self] response in
                    switch response {
                    case .success(let photosessions):
                        print("PHOTOSESSIONS : \(photosessions.count)")
                        self?.loadingView.isHidden = true
                        //self?.inboxModels = photosessions
                        var inboxModels : [PhotoPhotosessionsCellModelType1] = []
                        for model in photosessions {
                            let viewModel = PhotoPhotosessionsCellModelType1(photo: model, selectionHandler: { (photo) in
                                self?.onNext?(photo.id)
                            })
                            viewModels.append(viewModel)
                            inboxModels.append(viewModel)
                        }
                        
                        self?.inboxModels = inboxModels
                        let section = DefaultTableSectionViewModel(cellModels: viewModels)
                        self?.tableViewController.update(viewModels: [section])
                    case .failure(let error):
                        print(error)
                        self?.showToastErrorAlert(error)
                    }
                }
            }
        case .waiting:
            if self.waitingModels != nil {
                viewModels = self.waitingModels!
                let section = DefaultTableSectionViewModel(cellModels: viewModels)
                self.tableViewController.update(viewModels: [section])
                self.loadingView.isHidden = true
                break
            }
            else {
                self.apiService.getPhotoPhotosessions(tabIndex: tabIndex) {[weak self] response in
                    switch response {
                    case .success(let photosessions):
                        print("")
                        self?.loadingView.isHidden = true
                        //self?.waitingModels = photosessions
                        var waitingModels : [PhotoPhotosessionsCellModelType1] = []
                        for model in photosessions {
                            let viewModel = PhotoPhotosessionsCellModelType1(photo: model, selectionHandler: { (photo) in
                                self?.onNext?(photo.id)
                            })
                            viewModels.append(viewModel)
                            waitingModels.append(viewModel)
                        }
                        
                        self?.waitingModels = waitingModels
                        let section = DefaultTableSectionViewModel(cellModels: viewModels)
                        self?.tableViewController.update(viewModels: [section])
                    case .failure(let error):
                        print(error)
                        self?.showToastErrorAlert(error)
                    }
                }
            }
        default:
            print("fff")
        }
        
        
    }
    func render(){
        
        let selection : (() -> Void) = {[weak self] in
            print("FFFFFFFF")
            //self?.onNext?()
        }

        
//        let viewModels : [PhotoPhotosessionsCellModelType1] = [PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection), PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection), PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection), PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection), PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection), PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection), PhotoPhotosessionsCellModelType1(name: "Ангелина", avatar: "", date: "2ч. назад", selectionHandler: selection)]
//        let section = DefaultTableSectionViewModel(cellModels: viewModels)
//        self.tableViewController.update(viewModels: [section])
        
    }
    
    
    override func setUpConstraints() {
        super.setUpConstraints()
        
        self.tableViewController.view.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.collectionView.snp.makeConstraints{
            $0.top.equalTo(self.navigationController!.navigationBar.snp.bottom)
           // $0.top.equalToSuperview().offset(self.getTopOffset())
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            //$0.centerX.equalToSuperview()
            $0.height.equalTo(40)
        }
        self.separator.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview().offset(20)
            $0.bottom.equalToSuperview()
            $0.height.equalTo(1)
            $0.width.equalToSuperview()
            //$0.left.equalToSuperview()
            //$0.right.equalToSuperview()
            
        }
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.collectionView.removeFromSuperview()
        self.collectionView.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.collectionView.isHidden = false
    }
    
}


extension PhotographMiddleViewController : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "hello", for: indexPath) as! PhotoPhotosessionsCollectionViewCell
        let viewModel : PhotoPhotosessionsCollectionViewCellModel
        if indexPath.item == 0 {
            viewModel = PhotoPhotosessionsCollectionViewCellModel(name: "Входящие", selectionHandler: nil)
        } else if indexPath.item == 1 {
            viewModel = PhotoPhotosessionsCollectionViewCellModel(name: "Ожидаемые", selectionHandler: nil)
        } else if indexPath.item == 2 {
            viewModel = PhotoPhotosessionsCollectionViewCellModel(name: "Активные", selectionHandler: nil)
        } else {
            viewModel = PhotoPhotosessionsCollectionViewCellModel(name: "Завершенные", selectionHandler: nil)
        }
        cell.configure(viewModel: viewModel)
        return cell
        
    }
    func change(item : Int){
        for i in 0...3 {
            guard self.collectionView.cellForItem(at: IndexPath(item: i, section: 0)) != nil else {continue}
            let cell = self.collectionView.cellForItem(at: IndexPath(item: i, section: 0)) as! PhotoPhotosessionsCollectionViewCell
            let name = cell.nameLabel.text!
            if i != item {
                cell.nameLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: name)
            } else {
                cell.nameLabel.attributedText = CustomFont.bodySemobold15.attributesWithParagraph.make(string: name)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //collectionView.setContentOffset(CGPoint(x: -24, y: 0), animated: false)
    
            //self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -24)
            self.collectionView.scrollToItem(at: indexPath, at: .right, animated: true)
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoPhotosessionsCollectionViewCell
        self.change(item: indexPath.item)
        
        let width = cell.nameLabel.textWidth()
        UIView.animate(withDuration: Double(0.25),
                       delay: 0,
                       // 6
            options: UIView.AnimationOptions.curveEaseOut,
            animations: {[self.activeView.frame = CGRect(x: cell.center.x - (width/2), y: 38, width: width, height: 2)]},
            completion: nil)
    
        print(indexPath.item)
        if indexPath.item == 0 {
            render(tabIndex: .inbox)
        } else if indexPath.item == 1{
            render(tabIndex: .waiting)
        } else if indexPath.item == 2 {
            render(tabIndex: .active)
        }else {
            render(tabIndex: .ended)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthLabel = UILabel()
        
        if indexPath.item == 0 {
            widthLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Входящие")
            
        } else if indexPath.item == 1 {
            widthLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Ожидаемые")
        }
        else if indexPath.item == 2 {
            widthLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Активные")
        }
        else if indexPath.item == 3 {
            widthLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: "Завершенные")
        }
        return CGSize(width: widthLabel.textWidth(), height: 40)
    }
}

