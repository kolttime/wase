//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class PhotoServiceSettingsViewController : SimpleNavigationBar, UIPickerViewDelegate, UIPickerViewDataSource{
    
    var scrollViewController = ScrollViewController()
    var onBack : ((User, Bool) -> Void)?
    var timePicker = UIPickerView()
    var isChanged = false
    var cativeField = 0
    var specializations : [Specialization] = []
    private var SpecTypes: [ChangeSpecializationCellModel] = []
    private var testModel : User
    lazy var tableViewController = TableViewController()
    var workDays : [Int] = []
    var selected = ["0:00","0:00"]
    
    var selectedSpecs : [Specialization] = []
    
    private lazy var specializationTextField = TimeViewClass()
    private lazy var costTextField = OMGTextField.costField()
    private var lookForOrders : ServiceCheckView
    private var toOtherCity : ServiceCheckView
    
    private lazy var time = TimeViewClass()
    private var weeks = TimeViewClass()
    
    var slideView = SlideView()
    private var full_txt = String()
    

    var swithViewArray = [SwitchView(textLabel: "Понедельник"), SwitchView(textLabel: "Вторник"), SwitchView(textLabel: "Среда"), SwitchView(textLabel: "Четверг"), SwitchView(textLabel: "Пятница"), SwitchView(textLabel: "Суббота"), SwitchView(textLabel: "Воскресенье")]
    
    var labelTiming = UILabel()
    
    var userModel : User
    public var apiService : ApiService
    
    init(apiService : ApiService,userModel: User){
        self.apiService = apiService
        self.userModel = userModel
        self.lookForOrders = ServiceCheckView(textLabel: "Ищу заказы", textDescr: "Если отключить, клиенты не смогут предлагать вам заказы")
        self.toOtherCity = ServiceCheckView(textLabel: "Выезжаю в другой город", textDescr: "К вам смогут обращаться клиенты со всей России")
        self.testModel = userModel
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
  
    
    override func initialSetup() {
        super.initialSetup()
        self.view.backgroundColor = Color.white.value
        self.workDays = self.userModel.services?.workDays ?? []
        self.setLargeTitle()
        self.setTitle(title: "Услуги")
        
        self.setTabBarHidden()
        for scec in self.userModel.services?.specializations ?? [] {
            if self.boolSpec(specc: scec) {
                self.selectedSpecs.append(scec)
            }
        }
        let fff  : Action? = {[weak self] in
        
            self?.full_txt.removeAll()
            
            for i in self!.swithViewArray{
                if i.added == true {
                    self!.full_txt += self!.renameWeeks(name: i.textLabel.text!) + ", "
                }
            }
            
            
            
            guard let self = self else {return}
            self.full_txt = String(self.full_txt.dropLast(2))
            print(self.full_txt)
            
            switch self.cativeField{
            
            case 1:
                print(self.selected)
                print("hours")
                if self.selected == ["0:00","0:00"]{
                    self.time.textLabel.isHidden = true
                    self.time.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Рабочие часы")
                } else {
                    self.time.textLabel.isHidden = false
                    self.time.textLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Рабочие часы")
                    self.time.textDecsr.text = self.selected[0] + "-" + self.selected[1]
                }
            case 2:
                print("days")
                self.userModel.services?.workDays = []
                for i in 0...(self.swithViewArray.count)-1{
                    if self.swithViewArray[i].switchView.isOn == true {
                        //self?.workDays.append(i+1)
                        self.userModel.services?.workDays.append(i + 1)
                        
                    }
                }
                var daysString = ""
                for model in self.userModel.services?.workDays ?? []{
                    if model == 1 {
                        daysString.append("Пн, ")
                        self.swithViewArray[0].switchView.setOn(true, animated: false)
                    }
                    if model == 2 {
                        daysString.append("Вт, ")
                        self.swithViewArray[1].switchView.setOn(true, animated: false)
                    }
                    if model == 3 {
                        daysString.append("Ср, ")
                        self.swithViewArray[2].switchView.setOn(true, animated: false)
                    }
                    if model == 4 {
                        daysString.append("Чт, ")
                        self.swithViewArray[3].switchView.setOn(true, animated: false)
                    }
                    if model == 5 {
                        daysString.append("Пт, ")
                        self.swithViewArray[4].switchView.setOn(true, animated: false)
                    }
                    if model == 6 {
                        daysString.append("Сб, ")
                        self.swithViewArray[5].switchView.setOn(true, animated: false)
                    }
                    if model == 7 {
                        daysString.append("Вс, ")
                        self.swithViewArray[6].switchView.setOn(true, animated: false)
                    }
                }
                daysString = String(daysString.dropLast(2))
                if daysString == ""{
                    self.weeks.textLabel.isHidden = true
                    self.weeks.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Рабочие дни")
                } else {
                    self.weeks.textLabel.isHidden = false
                    self.weeks.textLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Рабочие дни")
                    self.weeks.textDecsr.text = daysString
                }
                //self.userModel.services?.workDays = self.workDays
            case 3:
                print("specializations")
                print(self.selectedSpecs)
                var specString = ""
                for spec in self.userModel.services?.specializations ?? [] {
                    
                    specString.append("\(spec.name!), ")
                }
            specString = String(specString.dropLast(2))
                if specString == ""{
                    self.specializationTextField.textLabel.isHidden = true
                    self.specializationTextField.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Специализации")
                } else {
                    self.specializationTextField.textLabel.isHidden = false
                    self.specializationTextField.textLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Специализации")
                    self.specializationTextField.textDecsr.text = specString
                }
            default:
                print("buugg")
            }

            self.slideView.close?()
        }
        
        self.slideView.setRightTitle(title: "Готово", completion: fff)
        self.slideView.addSubview(tableViewController.tableView)
        self.tableViewController.tableView.register(cellClass: ChangeSpecializationCell.self)
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        self.view.addSubview(self.scrollViewController.scrollView)
        scrollViewController.scrollView.addSubview(specializationTextField)
        scrollViewController.scrollView.addSubview(costTextField)
        scrollViewController.scrollView.addSubview(time)
        scrollViewController.scrollView.addSubview(lookForOrders)
        scrollViewController.scrollView.addSubview(toOtherCity)
        scrollViewController.scrollView.addSubview(weeks)
        
        self.costTextField.text = "\(userModel.services?.price ?? 0)"
    
        self.slideView.setRecognizer(height: 304)
        
        self.slideView.addSubview(labelTiming)
        self.slideView.addSubview(timePicker)
        
        for i in swithViewArray{
            self.slideView.addSubview(i)
        }
        
        
        self.slideView.animate = {[weak self] in
            self?.view.animateLayout(duration: 0.5)
        }

        self.timePicker.delegate = self
        self.timePicker.dataSource = self
        
        self.workDays = userModel.services?.workDays ?? []
        self.time.descText = userModel.services?.workHours
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.openDays))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.openHours))
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.openSpecialization))
        self.weeks.addGestureRecognizer(tap1)
        self.time.addGestureRecognizer(tap2)
        self.specializationTextField.addGestureRecognizer(tap3)
        
        self.view.addSubview(self.slideView)
    
        self.slideView.defaultCenter = self.view.frame.maxY + (230)
        
        self.weeks.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Рабочие дни")
        self.time.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Рабочие часы")
        self.specializationTextField.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Специализации")
        
        self.time.textDecsr.text = userModel.services?.workHours
        
        self.lookForOrders.switchOnOff.isOn = userModel.services?.vacant ?? false
        self.toOtherCity.switchOnOff.isOn = userModel.services?.mobile ?? false
        var specString = ""
        for spec in self.userModel.services?.specializations ?? [] {
            
            specString.append("\(spec.name!), ")
        }
        specString = String(specString.dropLast(2))
        //self.specializationTextField.set(text: specString)
        if specString == ""{
            self.specializationTextField.textLabel.isHidden = true
            self.specializationTextField.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Специализации")
        } else {
            self.specializationTextField.textLabel.isHidden = false
            self.specializationTextField.textLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Специализации")
            self.specializationTextField.textDecsr.text = specString
        }
        var daysString = ""
        for model in self.userModel.services?.workDays ?? []{
            if model == 1 {
                daysString.append("Пн, ")
                self.swithViewArray[0].switchView.setOn(true, animated: false)
            }
            if model == 2 {
                daysString.append("Вт, ")
                self.swithViewArray[1].switchView.setOn(true, animated: false)
            }
            if model == 3 {
                daysString.append("Ср, ")
                self.swithViewArray[2].switchView.setOn(true, animated: false)
            }
            if model == 4 {
                daysString.append("Чт, ")
                self.swithViewArray[3].switchView.setOn(true, animated: false)
            }
            if model == 5 {
                daysString.append("Пт, ")
                self.swithViewArray[4].switchView.setOn(true, animated: false)
            }
            if model == 6 {
                daysString.append("Сб, ")
                self.swithViewArray[5].switchView.setOn(true, animated: false)
            }
            if model == 7 {
                daysString.append("Вс, ")
                self.swithViewArray[6].switchView.setOn(true, animated: false)
            }
        }
        daysString = String(daysString.dropLast(2))
        if daysString == ""{
            self.weeks.textLabel.isHidden = true
            self.weeks.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Рабочие дни")
        } else {
            self.weeks.textLabel.isHidden = false
            self.weeks.textLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Рабочие дни")
            self.weeks.textDecsr.text = daysString
        }
        
        self.setUpConstraints()
        self.setupHandlers()
        self.fetchData()
    }
    
    func fetchData2(){
        if userModel.type == UserType(rawValue: "PHOTOGRAPHER") as! UserType{
            print("DA DA YA")
        }
    }
    
    func fetchData(){
        self.apiService.specialization(){[weak self] response in
            switch response {
            case .success(let rr):
                self?.specializations = rr
                self?.render()
                self?.tableViewController.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func boolSpec(specc : Specialization) -> Bool {
            for i in self.userModel.services?.specializations ?? []{
                if i.id == specc.id{
                    return true
                }
            }
            return false
    }

    //renderWeeks!
    
    func render(){
        let selection : ((Bool?, Specialization?) -> Void)? = {[weak self] sostoyanie, spec in
            guard let self = self else {return}
            if sostoyanie == true {
                if self.boolSpec(specc: spec!) == false {
                    self.userModel.services?.specializations.append(spec!)
                }
            } else {
                if self.boolSpec(specc: spec!) == true{
                    if self.userModel.services?.specializations.count ?? 0 < 2 {
                        self.userModel.services?.specializations.removeAll()
                    } else {
                        guard let index = self.userModel.services?.specializations.lastIndex(where: {$0.id == spec?.id}) else { return}
                        self.userModel.services?.specializations.remove(at: index)
                    }
                }
            }
            print(self.userModel.services?.specializations.count)
            print(self.userModel.services?.specializations)
        }
        for i in self.specializations{
            var hasSpec : Bool
            if self.boolSpec(specc: i) {
                hasSpec = true
            } else {
                hasSpec = false
            }
            let mm = ChangeSpecializationCellModel.init(specialization: i, hasSpec: hasSpec, selectionHandler: selection)
            self.SpecTypes.append(mm)
        }
        let section = DefaultTableSectionViewModel(cellModels: SpecTypes, headerViewModel: nil, footerViewModel: nil)
        self.tableViewController.update(viewModels: [section])
        
    }
    
    @objc func openSpecialization(){

        self.tableViewController.tableView.isHidden = false
        self.labelTiming.attributedText = CustomFont.bodyBold20.attributesWithParagraph.colored(color: Color.black.value).make(string: "Специализации")
        self.cativeField = 3
        self.slideView.setHeight(height: 457)
        for i in swithViewArray{
            i.isHidden = true
        }
        self.timePicker.isHidden = true
        self.slideView.open?()
    }
    
    @objc func openDays(){
        self.tableViewController.tableView.isHidden = true
        self.labelTiming.attributedText = CustomFont.bodyBold20.attributesWithParagraph.colored(color: Color.black.value).make(string: "Рабочие дни")
        self.cativeField = 2
        self.slideView.setHeight(height: 457)
        for i in swithViewArray{
            i.isHidden = false
        }
        self.timePicker.isHidden = true
        self.slideView.open?()
    }
    
    @objc func openHours(){
        self.tableViewController.tableView.isHidden = true
        self.labelTiming.attributedText = CustomFont.bodyBold20.attributesWithParagraph.colored(color: Color.black.value).make(string: "Рабочие часы")
        self.cativeField = 1
        self.slideView.setHeight(height: 304)
        for i in swithViewArray{
            i.isHidden = true
        }
        self.timePicker.isHidden = false
        self.slideView.open?()
    }

    
    func renameWeeks(name : String) -> String{
        switch name{
            case "Понедельник" :
                return "Пн"
            case "Вторник" :
                return "Вс"
            case "Среда" :
                return "Ср"
            case "Четверг" :
                return "Чт"
            case "Пятница" :
                return "Пт"
            case "Суббота" :
                return "Сб"
            case "Воскресенье" :
                return "Вс"
            default:
                return ""
            }
    }
    
    override func setUpConstraints(){
        super.setUpConstraints()
        
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        self.specializationTextField.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.height.equalTo(45)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        self.costTextField.snp.makeConstraints{
            $0.top.equalTo(specializationTextField.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        self.time.snp.makeConstraints{
            $0.top.equalTo(costTextField.snp.bottom).offset(20)
            $0.height.equalTo(45)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
    
        self.weeks.snp.makeConstraints{
            $0.top.equalTo(time.snp.bottom).offset(20)
            $0.height.equalTo(45)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        self.labelTiming.snp.makeConstraints{
            $0.top.equalTo(slideView.snp.top).offset(54)
            $0.left.equalTo(slideView.snp.left).offset(ViewSize.sideOffset)
        }
        
        self.lookForOrders.snp.makeConstraints{
            $0.top.equalTo(weeks.snp.bottom).offset(6)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        }
        self.slideView.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            self.slideView.topConstr = $0.top.equalTo(self.view.snp.bottom).constraint
            $0.height.equalTo(457)
        }
        self.toOtherCity.snp.makeConstraints{
            $0.top.equalTo(lookForOrders.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-20)
        }
        
        self.swithViewArray[0].snp.makeConstraints{
            $0.height.equalTo(28)
            $0.top.equalTo(labelTiming.snp.bottom).offset(15)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        }
        self.swithViewArray[1].snp.makeConstraints{
            $0.height.equalTo(28)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalTo(self.swithViewArray[0].snp.bottom).offset(ViewSize.sideOffset)
        }
        self.swithViewArray[2].snp.makeConstraints{
            $0.height.equalTo(28)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalTo(self.swithViewArray[1].snp.bottom).offset(ViewSize.sideOffset)
        }
        self.swithViewArray[3].snp.makeConstraints{
            $0.height.equalTo(28)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalTo(self.swithViewArray[2].snp.bottom).offset(ViewSize.sideOffset)
        }
        self.swithViewArray[4].snp.makeConstraints{
            $0.height.equalTo(28)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalTo(self.swithViewArray[3].snp.bottom).offset(ViewSize.sideOffset)
        }
        self.swithViewArray[5].snp.makeConstraints{
            $0.height.equalTo(28)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalTo(self.swithViewArray[4].snp.bottom).offset(ViewSize.sideOffset)
        }
        self.swithViewArray[6].snp.makeConstraints{
            $0.height.equalTo(28)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalTo(self.swithViewArray[5].snp.bottom).offset(ViewSize.sideOffset)
        }
        self.timePicker.snp.makeConstraints{
            $0.left.equalTo(self.slideView.snp.left).offset(27)
            $0.top.equalTo(self.slideView.snp.top).offset(64)
            $0.right.equalTo(self.slideView.snp.right).offset(-23)
        }
        self.tableViewController.tableView.snp.makeConstraints{
            $0.top.equalTo(self.labelTiming.snp.top).offset(40)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
    
    var xx = ["0:00", "1:00", "2:00", "3:00", "4:00", "5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"]
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0{
            selected[0] = xx[row]
            print(xx[row])
        }
        if component == 1{
            selected[1] = xx[row]
            print(xx[row])
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0{
            return 24
        }
        if component == 1{
            return 24
        }
        return xx.count
    }
    
    var specBool = false
    
    func checkSpecChanged () -> Bool{
        for i in selectedSpecs{
            if ((userModel.services?.specializations.contains(i))!){
                print("yes")
                specBool = true
            }
        }
        return specBool
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0{
            return xx[row]
        }
        if component == 1{
            return xx[row]
        }
        return ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("ъеъ2")
        onChange()
        fetchData2()
        print("\n\n\n\n\nUSERDAYS\n\n\(self.userModel.services?.workDays)\n\n\n\n")
        if HelpFunctions.userModelChanged(first: self.userModel, second: self.testModel) {
            self.apiService.patchProfile(userModel: self.userModel, completion: nil)
        }
        onBack?(userModel, isChanged)
    }
    
    
    func onChange(){
        
        
        self.userModel.services?.price = ((self.costTextField.text ?? "0") as NSString).integerValue
        
      
        
        if userModel.services?.workHours != time.textDecsr.text {
            userModel.services?.workHours = time.textDecsr.text ?? ""
            self.isChanged = true
        }
        
        self.userModel.services?.vacant = self.lookForOrders.switchOnOff.isOn
        self.userModel.services?.mobile = self.toOtherCity.switchOnOff.isOn
        
    }
    
}


extension PhotoServiceSettingsViewController{
    func setupHandlers(){
        self.costTextField.onReturnHandler = {[weak self] in
            self?.view.endEditing(true)
            self?.openHours()
        }
    }
}


