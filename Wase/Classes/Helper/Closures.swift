//
//  Closures.swift
//  AirdronCore
//
//  Created by andrew oparin on 01.11.2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation

public typealias Action = (() -> Void)
