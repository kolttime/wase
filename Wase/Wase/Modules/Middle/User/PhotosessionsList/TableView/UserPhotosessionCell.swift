//
//  UserPhotosessionCell.swift
//  Taoka
//
//  Created by Minic Relocusov on 18/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class UserPhotosessionsCellType1 : TableViewCell {
    
    var id : String?
    var selection : ((UserType, String) -> Void)?
    let baseView = UIView()
    let separatorView = UIView()
    let nameLabel = UILabel()
    let dateLabel = UILabel()
    let containerView = UIView()
    let firstMiddle = UserPhotoMiddleView()
    let secondMiddle = UserPhotoMiddleView()
    var topView = UserphotoTopView()
    var statusBarView = UserPhotoStatusBarView(stage: 1)
    override func initialSetup() {
        super.initialSetup()
        
        self.backgroundColor = Color.white.value
        // self.contentView.addSubview(self.label)
        self.contentView.addSubview(self.baseView)
        // self.contentView.addSubview(self.mainImageView)
        
        baseView.backgroundColor = UIColor.clear
        baseView.layer.shadowColor = UIColor.black.cgColor
        baseView.layer.shadowOffset = CGSize(width: 0, height: 0)
        baseView.layer.shadowOpacity = 0.1
        baseView.layer.shadowRadius = 8.0
        self.containerView.frame = baseView.bounds
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.masksToBounds = true
        baseView.addSubview(self.containerView)
      //  baseView.layer.shadowPath = UIBezierPath(roundedRect: baseView.bounds, cornerRadius: 10).cgPath
        baseView.layer.shouldRasterize = true
        baseView.layer.rasterizationScale = UIScreen.main.scale
        self.containerView.backgroundColor = Color.light.value
        self.firstMiddle.onTouch = {[weak self] in
            guard let self = self else {return}
            self.selection?(UserType.photographer, self.id!)
        }
        
        self.containerView.addSubview(self.topView)
        self.containerView.addSubview(self.firstMiddle)
        self.containerView.addSubview(self.statusBarView)
        self.contentView.backgroundColor = Color.white.value
        //self.containerView.dropShadow()
        
        self.setUpConstarints()
        
        // set the shadow properties
        
        
       // self.setupConstraints()
    }
    var first1 = true
    var first2 = true
    override func configure(viewModel: TableViewCell.ViewModelType) {
        var viewModel = viewModel as! UserPhotosessionsCellModelType1
        self.id = viewModel.id
        self.selection = viewModel.selection
        self.topView.set(nameLabel: viewModel.name, dateLabel: viewModel.date, img: true)
        var zayavka = ""
        switch viewModel.count {
        case 0:
            zayavka = "Заявок"
        case 1:
            zayavka = "Заявка"
        case 2:
            zayavka = "Заявки"
        case 3:
            zayavka = "Заявки"
        case 4:
            zayavka = "Заявки"
        default:
            zayavka = "Заявок"
        }
        self.firstMiddle.set(count: "\(viewModel.count) \(zayavka)", proffesionName: "Фотограф", avatarURL: viewModel.urls![0])
        self.statusBarView.removeFromSuperview()
        self.statusBarView = UserPhotoStatusBarView(stage: viewModel.stage)
        self.containerView.addSubview(self.statusBarView)
        self.statusBarView.snp.makeConstraints{
            $0.top.equalTo(self.firstMiddle.snp.bottom).offset(1)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }

    }
    func setUpConstarints(){
        self.baseView.snp.makeConstraints{
            $0.top.equalTo(self.contentView).offset(8)
            $0.left.equalTo(self.contentView).offset(ViewSize.sideOffset)
            $0.right.equalTo(self.contentView).offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.contentView).offset(-8)
           // $0.height.equalTo(169)
        }
        self.containerView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.topView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(60)
        }
        self.firstMiddle.snp.makeConstraints{
            $0.top.equalTo(self.topView.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(56)
        }
        self.statusBarView.snp.makeConstraints{
            $0.top.equalTo(self.firstMiddle.snp.bottom).offset(1)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
    
    
    
    
    
    
    
    
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        return 185
        
    }
    
    
    
    
}
struct UserPhotosessionsCellModelType1 : TableCellViewModel {
    var cellType: TableViewCell.Type {return UserPhotosessionsCellType1.self}
    var name : String
    var date : String
    var urls : [String]?
    var type : String
    var count : Int
    var stage : Int
    var id : String
    var selection : ((UserType, String) -> Void)?
}

