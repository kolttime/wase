//
//  AuthCoordinator.swift
//  OMG
//
//  Created by Andrew Oparin on 11/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import Gallery

protocol UserProfileCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startUserProfileFlow(presenter: AirdronNavigationController)
}

extension UserProfileCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startUserProfileFlow(presenter: AirdronNavigationController) {
        let coordinator = self.coordinatorFactory.makeUserProfileCoordinator(presenter: presenter,
                                                                            coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.start()
    }
}

class UserProfileCoordinator: BaseCoordinator {
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: UserProfileModuleBuilder
    private let navigationPresenter = AirdronNavigationController()
    var onCompletion: Action?
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: UserProfileModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        self.showUserProfileModule()
    }
    
    func showUserProfileModule() {
        let viewController = self.moduleBuilder.makeUserProfileModule()
        viewController.onSettings = {[weak self] user in
            self?.showUserProfileSettingModule(userModel: user, vc : viewController)
        }
        viewController.onAlbum = {[weak self] id in
            self?.showPhotoAlbumModule(images: nil, id: id, scrollTo: nil)
        }
        viewController.onImages = {[weak self] images, index in
            self?.showPhotoAlbumModule(images: images, id: nil, scrollTo: index)
        }
        self.presenter.setViewControllers([viewController], animated: false)
    }
    func showPhotoAlbumModule(images : [Image]?, id : String?, scrollTo : Int?){
        let viewController = self.moduleBuilder.makePhotoAlbum(images : images, id: id, scrollTo: scrollTo)
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showUserProfileSettingModule(userModel : User, vc : UserProfileViewController){
        let viewController = self.moduleBuilder.makeUserProfileSettings(userModel: userModel)
        
        
        viewController.backClicked = {[weak self] user in
            vc.updateInfo(userModel: user)
            vc.userModel = user
            self?.presenter.popViewController(animated: true)
        }
        viewController.goAccount = {[weak self] in
            self?.showUserAccountSettingsModule(vc: viewController, mainVc: vc)
        }
        
        viewController.goUserAgreement = {[weak self] in
            self?.showUserAgreement()
        }
        
        viewController.goNotif = {[weak self] in
            self?.showUserNotifSettingsModule()
        }
        
        viewController.goErrors = {[weak self] in
            self?.showUserErrorModule()
        }
        viewController.goTermsOfUse = {[weak self] in
            self?.showUserTermsOfUseModule()
        }
        
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showUserAgreement(){
        let viewController = self.moduleBuilder.makeUserAgreementSettings()
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showUserNotifSettingsModule(){
        let viewController = self.moduleBuilder.makeUserNotifSettings()
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showUserErrorModule(){
        let viewController = self.moduleBuilder.makeUserErrorsSettings()
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showUserTermsOfUseModule(){
        let viewController = self.moduleBuilder.makeUserTermsOfUseSettings()
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showUserAccountSettingsModule(vc : UserSettingsViewController, mainVc : UserProfileViewController){
        let viewController = self.moduleBuilder.makeUserAccountSettings(userModel: vc.userModel)
        viewController.onAvatar = {[weak self] in
            guard let self = self else { return }
            self.showImagePicker(viewController: viewController)
        }
        viewController.onLogout = {[weak self] in
            viewController.apiService.logout()
        }
        
        viewController.onBack = {[weak self] model in
            vc.userModel = model
            vc.set()
            mainVc.userModel = model
            mainVc.updateInfo(userModel: model)
            print(model)

        }
        
        self.presenter.pushViewController(viewController, animated: true)
    }
    func showImagePicker(viewController: PhotographAccountSettingsViewController) {
        let galleryController = GalleryController()
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.Camera.imageLimit = 1
        galleryController.delegate = viewController
        self.presenter.present(galleryController, animated: true) { [unowned galleryController] in
            //Appsee.markView(asSensitive: galleryController.view)
        }
    }
    
}


