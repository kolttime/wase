//
//  Success.swift
//  Taoka
//
//  Created by Minic Relocusov on 03/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct Success: Codable {
    
    let success: Bool
}

extension Success {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Success.self, from: data)
    }
}
