//
//  SlideView.swift
//  Taoka
//
//  Created by Minic Relocusov on 23/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit




class SlideView : AirdronView {
    
    public var topConstr : Constraint?
    var btnView = UIView()
    var leftLabel = UILabel()
    var onRight : Action?
    var onLeft : Action?
    var rightlabel = UILabel()
    var open : Action?
    var close : Action?
    public var centerY = CGFloat.zero
    var tapView = UIView()
    var superView = UIView()
    var height = CGFloat.zero
    var animate : Action?
    func setRecognizer(height : CGFloat){

        self.height = height
        let tap = UIPanGestureRecognizer(target: self, action: #selector(self.onTap(recognizer:)))
        self.tapView.addGestureRecognizer(tap)
    }
    func setHeight(height : CGFloat){
        self.height = height
    }
    override func initialSetup() {
        super.initialSetup()
        
        self.addSubview(self.tapView)
        self.addSubview(self.btnView)
        self.btnView.backgroundColor = Color.light.value
        self.btnView.layer.cornerRadius = 2
        self.reveal = false
        self.btnView.layer.masksToBounds = false
        
        self.open = {[weak self] in
            guard let self = self else {return}
            self.reveal = true
            if self.first {
                self.defaultCenter = self.center.y
                self.first = false
            }
            self.topConstr?.update(offset: -self.height )
            //self.animateLayout(duration: 0.5)
            self.animate?()
        
        
        }
        self.close = {[weak self] in
            guard let self = self else {return}
            self.reveal = false
            self.topConstr?.update(offset: 0 )
           // self.animateLayout(duration: 0.5)
            self.animate?()
            
        }
        self.addSubview(self.leftLabel)
        self.leftLabel.isHidden = true
        self.addSubview(self.rightlabel)
        self.rightlabel.isHidden = true
        self.backgroundColor = Color.white.value
        self.tapView.backgroundColor = Color.white.value
        self.setupConstraints()
    }
    
    
    var defaultCenter : CGFloat?
    var reveal = false {
        didSet {
            print(self.reveal)
        }
    }
    
    func setRightTitle(title : String, completion : Action?){
        self.rightlabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.accent.value).make(string: title)
        self.rightlabel.isHidden = false
        self.onRight = completion
        self.rightlabel.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onRightTap))
        self.rightlabel.addGestureRecognizer(tap)
    }
    
    func setBackgroundColor(color : UIColor){
        self.backgroundColor = color
    }
    
    func setLeftTitle(title : String, completion : Action?){
        self.leftLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.accent.value).make(string: title)
        self.leftLabel.isHidden = false
        self.onLeft = completion
        self.leftLabel.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onLeftTap))
        self.leftLabel.addGestureRecognizer(tap)
    }
    @objc func onLeftTap(){
        self.onLeft?()
    }

    @objc func onRightTap(){
        self.onRight?()
    }
    @objc func onTap(recognizer:UIPanGestureRecognizer){
        self.endEditing(true)
        let translation = recognizer.translation(in: self)
        self.center.y += translation.y
        recognizer.setTranslation(CGPoint.zero, in: self)
        print(self.centerY)
        if recognizer.state == .ended {
            var finalPoint = CGFloat()
            
            if self.reveal {
                if self.center.y > (self.defaultCenter! - self.height + 40) {
                    finalPoint = self.defaultCenter!
                    self.reveal = false
                } else {
                    finalPoint = self.defaultCenter! - height
                    self.reveal = true
                }
            } else {
                if self.center.y < self.defaultCenter! - 30 {
                    finalPoint = self.defaultCenter! - height
                    self.reveal = true
                } else {
                    finalPoint = self.defaultCenter!
                    self.reveal = false
                }
            }
            
            if finalPoint == self.defaultCenter {
                self.center.y = self.defaultCenter!
                self.close?()
            } else {
                self.center.y = self.defaultCenter!
                self.open?()
            }
        }
    }
    
    
    var first = true

    
    override func setupConstraints() {
        super.setupConstraints()
        self.tapView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(50)
        }
        self.rightlabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.leftLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(16)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        self.btnView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(13.5)
            $0.centerX.equalToSuperview()
            $0.height.equalTo(5)
            $0.width.equalTo(36)
        }
    }
    
    
    
}
