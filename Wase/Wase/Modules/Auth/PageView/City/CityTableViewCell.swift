//
//  SearchableCountryTableViewCell.swift
//  OMG
//
//  Created by Andrew Oparin on 13/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

class SearchableCountryTableViewCell: TableViewCell {
    
    var cityModel: SearchableCountryTableCellViewModel?
    var selection : ((String) -> Void)?
    var imageq = UIImageView(image: .init(UIImage(named: "Arrow2"))!)
    
    private lazy var cityLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.cityLabel)
        self.contentView.addSubview(self.imageq)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.cityLabel.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-8)
            $0.left.equalToSuperview().offset(24)
        }
        self.imageq.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-22)
            $0.centerY.equalToSuperview()
            $0.width.equalTo(8)
            $0.height.equalTo(13)
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! SearchableCountryTableCellViewModel
        self.cityModel = viewModel
        self.selection = viewModel.selectionHandler
        self.cityLabel.text = viewModel.cityLabel
        self.cityLabel.font = UIFont.systemFont(ofSize: 20)
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 50
    }
    
    override func didSelect() {
        self.selection?(self.cityLabel.text!)
    }
}

struct SearchableCountryTableCellViewModel: TableCellViewModel {
    var cellType: TableViewCell.Type { return SearchableCountryTableViewCell.self }
    var cityLabel: String
    let selectionHandler: ((String) -> Void)?
}

