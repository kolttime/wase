//
//  PagePickerView.swift
//  Taoka
//
//  Created by Minic Relocusov on 11/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PagePickerView : AirdronView {
    
    
    var onRightPick : Action?
    var onLeftPick : Action?
    private lazy var leftSide = UIView()
    private lazy var rightSide = UIView()
    private lazy var leftIcon = LeftPicker()
    private lazy var rightIcon = RightPicker()
    var rightPicked = true
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = Color.light.value
        self.addSubview(self.leftSide)
        self.addSubview(self.rightSide)
        self.leftSide.addSubview(self.leftIcon)
        self.rightSide.addSubview(self.rightIcon)
        let leftTap = UITapGestureRecognizer(target: self, action: #selector(self.leftPick))
        let rightTap = UITapGestureRecognizer(target: self, action: #selector(self.rightPick))
        self.leftSide.addGestureRecognizer(leftTap)
        self.rightSide.addGestureRecognizer(rightTap)

        self.setupConstraints()
    }
    
    @objc
    func leftPick(){
        if !self.leftIcon.picked {
            self.leftIcon.pickedState()
            self.rightIcon.normalState()
            self.rightPicked = false
            self.onLeftPick?()
        }
    }
    @objc func rightPick(){
        if !self.rightIcon.picked {
            self.leftIcon.normalState()
            self.rightIcon.pickedState()
            self.rightPicked = true
            self.onRightPick?()
        }
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.leftSide.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.width.equalToSuperview().dividedBy(2)
            $0.bottom.equalToSuperview()
        }
        self.rightSide.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview().dividedBy(2)
            $0.bottom.equalToSuperview()
        }
        self.leftIcon.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
            $0.height.equalTo(16)
            $0.width.equalTo(16)
        }
        self.rightIcon.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
            $0.height.equalTo(16)
            $0.width.equalTo(16)
        }
    }
    
}
