
import Foundation
import UIKit

class TaokaTextField: AirdronView {
    
    private let contentHeight: CGFloat = 50
    weak var delegate: CodeFieldViewDelegate? {
        didSet {
            self.codeField.delegate = self.delegate
        }
    }
    private lazy var codeField = CodeFieldView()
    
    func resignResponder() {
        self.codeField.resignFirstResponder()
    }
    
    func firstResponder() {
        self.codeField.becomeFirstResponder()
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.codeField)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.codeFieldTap))
        self.addGestureRecognizer(tap)
        self.invalidateIntrinsicContentSize()
        self.setNeedsUpdateConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.codeField.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-5)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: self.contentHeight)
    }
    
    func showError() {
        self.shake()
        self.codeField.reset()
    }
    
    @objc
    private func codeFieldTap() {
        self.codeField.becomeFirstResponder()
    }
}


