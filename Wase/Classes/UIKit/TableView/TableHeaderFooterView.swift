//
//  TableHeaderFooterView.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 19/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

open class TableHeaderFooterView: UITableViewHeaderFooterView {
    
    public typealias ViewModelType = TableHeaderFooterViewModel
    
    public override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initialSetup()
        self.setupConstraints()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetup()
        self.setupConstraints()
    }
    
    open func initialSetup() {
        
    }
    
    open func configure(viewModel: ViewModelType) {
        
    }
    
    open func setupConstraints() {
        
    }
    
    open class func height(for viewModel: ViewModelType, tableView: UITableView) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
