//
//  ApplicationContainer.swift
//  OMG
//
//  Created by Andrew Oparin on 06/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

class DependencyContainer {
    
    lazy var networkManager = NetworkManager.default()
    
    lazy var uploadManager = UploadManager.default()
    
    lazy var backgroundNetworkManager = NetworkManager.background()
    
    lazy var backgroundUploadManager = UploadManager.background()
    
    lazy var credential = CredentialStorage(key: "Taoka")

    lazy var moduleBuilderFactory: ModuleBuilderFactory = ModuleBuilderFactoryImp(serviceContainer: self.serviceContainer)
    
    lazy var coordinatorFactory: CoordinatorFactory = CoordinatorFactoryImp(moduleBuilderFactory: self.moduleBuilderFactory)
    
    lazy var serviceContainer: ServiceContainer = ServiceContainerImp(networkManager: self.networkManager,
                                                                      uploadManager: self.uploadManager,
                                                                      backgroundNetworkManager: self.backgroundNetworkManager,
                                                                      backgroundUploadManager: self.backgroundUploadManager,
                                                                      credential: self.credential)
    
}
