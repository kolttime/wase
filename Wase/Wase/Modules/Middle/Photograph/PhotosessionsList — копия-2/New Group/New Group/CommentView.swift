//
//  ApproveView.swift
//  OMG
//
//  Created by Minic Relocusov on 21/04/2019.
//  Copyright © 2019 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit



class CommentView : AirdronView {
    
    var textLabel = UILabel()
    var timeText = UILabel()
    
    
   
    
    func set(text : String, time : String){
        self.textLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.black.value).make(string: text)
        self.timeText.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: time)
    }
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var top : Bool
    init(top : Bool) {
        
        self.top = top
        super.init(frame: .zero)
    }
    
    override func initialSetup() {
        super.initialSetup()
        if self.top {
            self.layer.cornerRadius = 10
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
            self.layer.masksToBounds = true
        } else {
            
                self.layer.cornerRadius = 10
                self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
                self.layer.masksToBounds = true
            
        }
        self.backgroundColor = Color.light.value
        self.addSubview(self.textLabel)
        self.addSubview(timeText)
        self.layoutIfNeeded()
        self.textLabel.numberOfLines = 0
        self.setupConstraints()
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()

        self.textLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(12)
            $0.top.equalToSuperview().offset(8)
            $0.right.equalToSuperview().offset(-12)
            $0.bottom.equalToSuperview().offset(-8)
        }
        self.timeText.snp.makeConstraints{
            //$0.top.equalTo(self.textLabel.snp.bottom).offset(-5)
            $0.right.equalToSuperview().offset(-12)
            $0.bottom.equalToSuperview().offset(-8)
        }
        
    }
    
}






