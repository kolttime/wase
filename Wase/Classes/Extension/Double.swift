//
//  Double.swift
//  OMG
//
//  Created by Andrew Oparin on 27/11/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

extension Double {
    
    func removeZerosFromEnd() -> String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 16
        return String(formatter.string(from: number) ?? "")
    }
}
