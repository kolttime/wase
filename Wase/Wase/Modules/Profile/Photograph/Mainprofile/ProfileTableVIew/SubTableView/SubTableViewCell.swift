//
//  SubTableViewCell.swift
//  Taoka
//
//  Created by Minic Relocusov on 12/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class SubTableViewCell : TableViewCell {
    
    
    var id : String?
    var selectionhandler : ((String) -> Void)?
    let nameLabel = UILabel()
    let datelabel = UILabel()
    let mainImageView = UIImageView()
    let containerView = UIView()
    
    override func initialSetup() {
        super.initialSetup()
        
        self.backgroundColor = Color.white.value
       // self.contentView.addSubview(self.label)
        self.contentView.addSubview(self.containerView)
       // self.contentView.addSubview(self.mainImageView)
        self.mainImageView.addSubview(self.nameLabel)
        self.mainImageView.addSubview(self.datelabel)
        self.containerView.addSubview(self.mainImageView)
        
       
        self.containerView.layer.shadowColor = UIColor.black.cgColor
        self.containerView.layer.shadowRadius = 8
        self.containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.containerView.layer.shadowOpacity = 0.2
        self.containerView.layer.cornerRadius = 10
      
        //self.containerView.layer.shadowPath = UIBezierPath(rect: containerView.bounds).cgPath
        self.containerView.clipsToBounds = true
        self.containerView.layer.masksToBounds = false
        self.containerView.layer.shouldRasterize = true
        self.containerView.layer.rasterizationScale = UIScreen.main.scale
        
        
        self.mainImageView.layer.cornerRadius = 10
        self.mainImageView.clipsToBounds = true
        self.mainImageView.contentMode = .scaleAspectFill
        self.nameLabel.textColor = .white
        self.datelabel.textColor = .white
        self.nameLabel.font = UIFont.boldSystemFont(ofSize: 17)
        self.datelabel.font = UIFont.systemFont(ofSize: 13)
        self.contentView.backgroundColor = Color.white.value
        //self.containerView.dropShadow()
        
        self.layoutIfNeeded()
        
        // set the shadow properties
        
        
        self.setupConstraints()
    }
    
   override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! SubTableViewCellModel
        self.mainImageView.kf.setImage(with: URL(string: viewModel.imgUrl))
        self.nameLabel.text = viewModel.nameText
        self.datelabel.text = viewModel.dateText
        self.selectionhandler = viewModel.selectionHandler
        self.id = viewModel.id
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.containerView.snp.makeConstraints{
            $0.top.equalTo(self.contentView).offset(8)
            $0.left.equalTo(self.contentView).offset(ViewSize.sideOffset)
            $0.right.equalTo(self.contentView).offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.contentView).offset(-8)
            $0.height.equalTo(getCardHeight())
        }
        self.mainImageView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.nameLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(12)
            $0.left.equalToSuperview().offset(12)
        }
        self.datelabel.snp.makeConstraints{
            $0.top.equalTo(self.nameLabel.snp.bottom)
            $0.left.equalToSuperview().offset(12)
        }
        
        
        
    }
    
    
    override func didSelect() {
        self.selectionhandler?(self.id!)
    }
    
     override class func height(for viewModel: ViewModelType,
     tableView: UITableView) -> CGFloat {
     //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        if Device.isIphoneX || Device.isIphoneXSmax {
            return 196
        } else {
            return 176
        }
     
     }
    
    
    
    
}
struct SubTableViewCellModel : TableCellViewModel {
    var cellType: TableViewCell.Type {return SubTableViewCell.self}
    var imgUrl : String
    var nameText : String
    var dateText : String
    var id : String
    var selectionHandler : ((String) -> Void)?
}

extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

