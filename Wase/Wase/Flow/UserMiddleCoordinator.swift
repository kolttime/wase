//
//  AuthCoordinator.swift
//  OMG
//
//  Created by Andrew Oparin on 11/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

protocol UserMiddleCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startUserMiddleFlow(presenter: AirdronNavigationController)
}

extension UserMiddleCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startUserMiddleFlow(presenter: AirdronNavigationController) {
        let coordinator = self.coordinatorFactory.makeUserMiddleCoordinator(presenter: presenter,
                                                                         coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.start()
    }
}

class UserMiddleCoordinator: BaseCoordinator {
    
    
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: UserMiddleModuleBuilder
    private let navigationPresenter = AirdronNavigationController()
    var onCompletion: Action?
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: UserMiddleModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    

    
    override func start() {
        self.showUserMiddleModule()
    }
    
    func showUserMiddleModule() {
        let viewController = self.moduleBuilder.makeUserMiddleModule()
        viewController.onNew = {[weak self]  in
            self?.showUserCreatePhoto(vc : viewController)
        }
        viewController.selection = {[weak self] type, id in
            self?.showSelectionModule(type: type, id: id, photoId: id, vc: viewController)
        }
        self.presenter.setViewControllers([viewController], animated: false)
    }
    
    func showSelectionModule(type : UserType, id : String, photoId : String, vc : UserMiddleViewController){
        let selectionStep = self.moduleBuilder.makeSelectionModule(type: type, id: id)
        selectionStep.onUser = {[weak self] id in
            print("HERES IDS \(id)")
            self?.showPhotographProfileModule(userId: id, photoId: photoId, vc : selectionStep)
        }
        selectionStep.onBack = {[weak self] in
            vc.fetchData()
        }
        self.presenter.pushViewController(selectionStep, animated: true)
    }
    
    
    func showPhotographProfileModule(userId : String, photoId : String, vc : SelectionViewController) {
        let viewController = self.moduleBuilder.makePhotographProfileModule(id : userId, photoId: photoId)
        
        viewController.onAlbum = {[weak self] id in
            self?.showPhotoAlbumModule(images: nil, id: id, scrollTo: nil, userId: userId)
        }
        viewController.onImages = {[weak self] images, index in
            self?.showPhotoAlbumModule(images: images, id: nil, scrollTo: index, userId: userId)
        }
        viewController.onAddForUser = {[weak self] id in
            vc.update(id: id)
            self?.presenter.popViewController(animated: true)
        }
       
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    
    func showPhotoAlbumModule(images : [Image]?, id : String?, scrollTo : Int?, userId : String){
        let viewController = self.moduleBuilder.makePhotoAlbum(images : images, id: id, scrollTo: scrollTo, userId: userId)
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    
    
    func showSecondRedyModule(photosessionCreateModel : CreatePhotosession, vc : UserMiddleViewController){
        let secondReadyStep = self.moduleBuilder.makeSecondStepModule(photoCreation: photosessionCreateModel)
        secondReadyStep.onNext = { [weak self] id, photo in
            photosessionCreateModel.title = "tiiitle"
            //self.presenter.pushViewController(selectionStep, animated: true)
            vc.addNew(photosession: photo)
            guard let type = photosessionCreateModel.participantsTypes else {return}
            let userType = UserType.init(rawValue: type[0].rawValue)
            self?.presenter.popViewController(animated: false)
            self?.showSelectionModule(type: userType!, id: id, photoId: photo.id ,vc : vc)
        }
        self.presenter.pushViewController(secondReadyStep, animated: true)
    }
    
    func showUserCreatePhoto(vc : UserMiddleViewController){
        var photosessionCreateModel = CreatePhotosession()
        let userCreate = self.moduleBuilder.mskeUserCreateModule()
        let timeСreate = self.moduleBuilder.makeTineModule()
        let bringPlace = self.moduleBuilder.makePlaceModule()
        let workerCheck = self.moduleBuilder.makeWorkerModule()
        let budgetCreate = self.moduleBuilder.makeBudgetModule(photoCreation : photosessionCreateModel)
        let viewController = self.moduleBuilder.makePageUserCreateViewModule(initialViewControllers: [userCreate, timeСreate, bringPlace, workerCheck, budgetCreate], count: 5)
        self.presenter.pushViewController(viewController, animated: true)
        var comp : Action? = {[weak self] in
            if timeСreate.fullFields == true{
                print("GOOD")
//                print(timeСreate.datePicker.date, timeСreate.itemSelectedOne, timeСreate.buffer[1]+1)
                viewController.next()
                viewController.setBackTitle(title: "Фотосессии")
                viewController.setRighttitle(title: "", completion: nil)
                
                photosessionCreateModel.date = timeСreate.datePicker.date.dateString
                photosessionCreateModel.time = timeСreate.itemSelectedOne
                photosessionCreateModel.duration = timeСreate.buffer[1]+1
                
            } else{
                print("BAd")
            }
        }
        viewController.onBack = {[weak self] index in
    
            if index == 0 {
                self?.presenter.popViewController(animated: true)
            }
            print(index)
            if index == 2 {
                viewController.setRighttitle(title: "Далее", completion: comp)
            } else {
                viewController.setRighttitle(title: "", completion: nil)
            }
        }
        userCreate.onNext = {[weak self] specialization in
            photosessionCreateModel.specialization = specialization
            viewController.next()
            viewController.setRighttitle(title: "Далее", completion: comp)
        }
        
        bringPlace.onnStudio = {[weak self] type in
            photosessionCreateModel.place.type = type
            viewController.next()
        }
        
        bringPlace.onnHome = {[weak self] type, adress in
            photosessionCreateModel.place.type = type
            photosessionCreateModel.place.adress = adress
            viewController.next()
        }
        
        bringPlace.onnAdress = {[weak self] type, district in
            photosessionCreateModel.place.type = type
            photosessionCreateModel.place.district = district
            viewController.next()
        }
        
        workerCheck.onPhotograph = {[weak self] in
            print("photograph")
            photosessionCreateModel.participantsTypes = [ParticipantsTypes.photographer]
            viewController.next()
        }
        
        workerCheck.onPhotoandVizage = {[weak self] in
            print("photo + vizage")
            photosessionCreateModel.participantsTypes = [ParticipantsTypes.photographer, ParticipantsTypes.makeup]
            viewController.next()
        }
        
        budgetCreate.onNext = {[weak self] budget in
            print(budget.value)
            var array : [Int] = []
            for i in budget.value!{
                array.append(i)
            }
            photosessionCreateModel.budget = array
            //self?.presenter.pushViewController(secondReadyStep, animated: true)
            self?.presenter.popViewController(animated: false)
            self?.showSecondRedyModule(photosessionCreateModel: photosessionCreateModel, vc : vc)
        }
    }
}


