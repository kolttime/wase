//
//  CustomFont.swift
//  OMG
//
//  Created by Andrew Oparin on 04/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

enum CustomFont {
    
    case headline1
    case largeTitle
    case titleSecondary
    case headline2
    case headline3Bold
    case headline3regular
    case bodyBold20
    case bodyRegular20
    case bodySemibold17
    case bodyRegular17
    case bodySemobold15
    case bodyRegular15
    case tech13
    case tech11
    
   

    var font: UIFont {
        switch self {
            
        case .headline1:
            return UIFont.systemFont(ofSize: 40, weight: .bold)
        case .largeTitle:
            return UIFont.systemFont(ofSize: 34, weight: .bold)
        case .titleSecondary:
            return .systemFont(ofSize: 34, weight: .regular)
        case .headline2:
            return .systemFont(ofSize: 28, weight: .bold)
        case .headline3Bold:
            return .systemFont(ofSize: 22, weight: .bold)
        case .headline3regular:
            return .systemFont(ofSize: 22, weight: .regular)
        case .bodyBold20:
            return .systemFont(ofSize: 20, weight: .bold)
        case .bodyRegular20:
            return .systemFont(ofSize: 20, weight: .regular)
        case .bodySemibold17:
            return .systemFont(ofSize: 17, weight: .semibold)
        case .bodyRegular17:
            return .systemFont(ofSize: 17, weight: .regular)
        case .bodySemobold15:
            return .systemFont(ofSize: 15, weight: .semibold)
        case .bodyRegular15:
            return .systemFont(ofSize: 15, weight: .regular)
        case .tech13:
            return .systemFont(ofSize: 13, weight: .regular)
        case .tech11:
            return .systemFont(ofSize: 11, weight: .regular)
        @unknown default:
            return .systemFont(ofSize: 15, weight: .regular)
        }
    }
    
    var color: UIColor {
        switch self {
        default:
            return Color.black.value
        }
    }
    
    var letterSpacing: NSNumber {
        switch self {
            
        case .headline1:
            return NSNumber(value: 0.41)
        case .largeTitle:
            return NSNumber(value: 0.41)
        case .titleSecondary:
            return NSNumber(value: 0)
        case .headline2:
            return NSNumber(value: 0.33)
        case .headline3Bold:
            return NSNumber(value: 0.35)
        case .headline3regular:
            return NSNumber(value: 0.35)
        case .bodyBold20:
            return NSNumber(value: 0.38)
        case .bodyRegular20:
            return NSNumber(value: 0.38)
        case .bodySemibold17:
            return NSNumber(value: -0.41)
        case .bodyRegular17:
            return NSNumber(value: -0.41)
        case .bodySemobold15:
            return NSNumber(value: -0.24)
        case .bodyRegular15:
            return NSNumber(value: -0.24)
        case .tech13:
            return NSNumber(value: -0.08)
        case .tech11:
            return NSNumber(value: 0.07)
        @unknown default:
            return NSNumber(value: 0)
        }
    }
    
    var lineHeight: CGFloat {
        switch self {
            
        case .headline1:
            return 48
        case .largeTitle:
            return 38
        case .titleSecondary:
            return 41
        case .headline2:
            return 34
        case .headline3Bold:
            return 28
        case .headline3regular:
            return 28
        case .bodyBold20:
            return 25
        case .bodyRegular20:
            return 25
        case .bodySemibold17:
            return 22
        case .bodyRegular17:
            return 22
        case .bodySemobold15:
            return 20
        case .bodyRegular15:
            return 20
        case .tech13:
            return 18
        case .tech11:
            return 13
        @unknown default:
            return 0
        }
    }
    
    var lineHeightMultiple: CGFloat {
        return self.lineHeight / self.font.lineHeight
    }
    
    var paragraphStyle: NSMutableParagraphStyle {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = self.lineHeightMultiple
        return paragraphStyle
    }
    
    var attributes: [NSAttributedString.Key: Any] {
        return [NSAttributedString.Key.foregroundColor : self.color,
                NSAttributedString.Key.font : self.font,
                NSAttributedString.Key.kern: self.letterSpacing]
    }
    
    var attributesWithParagraph: [NSAttributedString.Key: Any] {
        return [NSAttributedString.Key.foregroundColor : self.color,
                NSAttributedString.Key.font : self.font,
                NSAttributedString.Key.kern: self.letterSpacing,
                NSAttributedString.Key.paragraphStyle: self.paragraphStyle]
    }
}
