//
//  CollectionHeaderFooterView.swift
//  MDW
//
//  Created by Andrew Oparin on 05/09/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

public class CollectionHeaderFooterView: UICollectionReusableView {
    
    public typealias ViewModelType = CollectionHeaderFooterViewModel
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupInitialState()
        
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupInitialState()
    }
    
    private func setupInitialState() {
        self.initialSetup()
        self.setupConstraints()
        self.setNeedsUpdateConstraints()
    }
    
    public func initialSetup() {
        
    }
    
    public func configure(viewModel: ViewModelType) {
        
    }
    
    public class func height(for viewModel: ViewModelType,
                             collectionView: UICollectionView) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    public func setupConstraints() {
        
    }
}

public protocol CollectionHeaderFooterViewModel {
    
    var type: CollectionHeaderFooterView.Type { get }
}

public extension CollectionHeaderFooterViewModel {
    
    public var reuseIdentifier: String {
        return String(describing: self.type)
    }
}

public class EmptyCollectionHeaderView: CollectionHeaderFooterView {}

public class EmptyCollectionFooterView: CollectionHeaderFooterView {}
