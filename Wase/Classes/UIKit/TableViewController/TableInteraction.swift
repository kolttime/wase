//
//  TableInteraction.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 19/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

public protocol TableInteraction: class {
    
    var viewModels: [TableSectionViewModel] { get set }
    var tableView: UITableView! { get set }

}

public extension TableInteraction {
    
    public func update(viewModels: [TableSectionViewModel]) {
        self.viewModels = viewModels
        self.tableView.reloadData()
    }
    
    public func insert(_ cellViewModel: TableCellViewModel,
                       for row: Int,
                       in section: Int,
                       animation: UITableView.RowAnimation = .automatic) {
        self.viewModels[section].cellModels.insert(cellViewModel, at: row)
        let indexPath = IndexPath(row: row, section: section)
        self.tableView.insertRows(at: [indexPath], with: animation)
    }
    
    public func append(_ cellViewModels: [TableCellViewModel],
                       in section: Int,
                       animation: UITableView.RowAnimation = .automatic) {
        let startIndex = self.viewModels[section].cellModels.count
        self.viewModels[section].cellModels.append(contentsOf: cellViewModels)
        let indexPaths = (startIndex..<startIndex + cellViewModels.count).map { return IndexPath(row: $0, section: section) }
        self.tableView.insertRows(at: indexPaths, with: animation)
    }
    
    public func update(_ cellViewModel: TableCellViewModel,
                       at indexPath: IndexPath,
                       animation: UITableView.RowAnimation = .automatic,
                       reload: Bool = true) {
        self.viewModels[indexPath.section].cellModels[indexPath.row] = cellViewModel
        if reload {
            self.tableView.reloadRows(at: [indexPath], with: animation)
        }
    }
    
    public func update(_ cellViewModel: TableCellViewModel,
                       for row: Int,
                       in section: Int,
                       animation: UITableView.RowAnimation = .automatic,
                       reload: Bool = true) {
        let indexPath = IndexPath(row: row, section: section)
        self.update(cellViewModel, at: indexPath, animation: animation, reload: reload)
    }
    
    public func delete(_ row: Int,
                       in section: Int,
                       animation: UITableView.RowAnimation = .fade) {
        self.viewModels[section].cellModels.remove(at: row)
        let indexPath = IndexPath(row: row, section: section)
        self.tableView.deleteRows(at: [indexPath], with: animation)
    }
    
    public func move(from fromRow: Int,
                     in fromSection: Int,
                     to toRow: Int,
                     in toSection: Int) {
        let model = self.viewModels[fromSection].cellModels.remove(at: fromRow)
        self.viewModels[toSection].cellModels.insert(model, at: toRow)
        let fromIndexPath = IndexPath(row: fromRow, section: fromSection)
        let toIndexPath = IndexPath(row: toRow, section: toSection)
        self.tableView.moveRow(at: fromIndexPath, to: toIndexPath)
    }
    
    public func insert(_ sectionViewModel: TableSectionViewModel,
                       in section: Int,
                       animation: UITableView.RowAnimation = .automatic) {
        self.viewModels.insert(sectionViewModel, at: section)
        let indexSet = IndexSet(integer: section)
        self.tableView.insertSections(indexSet, with: animation)
    }
    
    public func update(_ sectionViewModel: TableSectionViewModel,
                       inSection section: Int,
                       animation: UITableView.RowAnimation = .automatic,
                       reload: Bool = true) {
        self.viewModels[section] = sectionViewModel
        let indexSet = IndexSet(integer: section)
        if reload {
            self.tableView.reloadSections(indexSet, with: animation)
        }
    }
    
    public func delete(section: Int,
                       animation: UITableView.RowAnimation = .fade) {
        self.viewModels.remove(at: section)
        let indexSet = IndexSet(integer: section)
        self.tableView.deleteSections(indexSet, with: animation)
    }
}
