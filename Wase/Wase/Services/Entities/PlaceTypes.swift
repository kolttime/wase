//
//  Specializations.swift
//  Taoka
//
//  Created by Роман Макеев on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct PlaceTypes: Codable {
    
    var type : LocationType
    var district : String?
    var adress : String?
    
    
     init() {
        self.type = .home
        self.district = nil
        self.adress = nil
    }
    
    init(type : LocationType, district : String?, adress : String?) {
        self.type = type
        self.district = district
        self.adress = adress
    }
    
    
    func toDictionary() -> JsonDictionary {
        
        var dict : [String : Any] = [:]
        if self.district != nil && self.district != "" {
            dict["district"] = district
        }
        if self.adress != nil && self.adress != "" {
            dict["address"] = adress
        }
        dict["type"] = self.type.rawValue
        return dict
        
    }
}

extension PlaceTypes {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(PlaceTypes.self, from: data)
    }
}

extension Array where Element == PlaceTypes {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([PlaceTypes].self, from: data)
    }
}


