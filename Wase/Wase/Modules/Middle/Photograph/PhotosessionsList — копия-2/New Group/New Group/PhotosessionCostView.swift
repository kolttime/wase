//
//  PhotosessionCostView.swift
//  Taoka
//
//  Created by Роман Макеев on 05/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotosessionResponseCostView : AirdronView {
    
    
    
    
    private var costlabel = UILabel()
    private var comissionLael = UILabel()
    private var changeLabel = UILabel()
    var commentField = OMGTextField.makePhotoCommenTextField()
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.costlabel)
        self.addSubview(self.changeLabel)
        self.addSubview(self.comissionLael)
        self.addSubview(self.commentField)
    }
    
    func set(cost : String, comission : String){
        self.costlabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: cost)
        self.comissionLael.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: comission)
        self.changeLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Изменить")
        self.setupConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.costlabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(20)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        self.changeLabel.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalToSuperview().offset(20)
        }
        self.comissionLael.snp.makeConstraints{
            $0.top.equalTo(self.costlabel.snp.bottom)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
        self.commentField.snp.makeConstraints{
            $0.top.equalTo(self.comissionLael.snp.bottom).offset(40)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(60)
            $0.bottom.equalToSuperview()
        }
    }
    
}
