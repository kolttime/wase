//
//  SubCollectionViewCell.swift
//  Taoka
//
//  Created by Роман Макеев on 13/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class SubCollectionViewCell : CollectionViewCell {
    
    
    var index : Int?
    var onSelect : ((Int) -> Void)?
    let mainimageView = UIImageView()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.mainimageView)
        self.mainimageView.layer.masksToBounds = true
        self.mainimageView.contentMode = .scaleAspectFill
        self.layoutIfNeeded()
        self.setupConstraints()
    }
    
    override func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! SubCollectionViewCellModel
        self.onSelect = viewModel.selectionHandler
        self.mainimageView.kf.setImage(with: URL(string: viewModel.imgUrl))
        self.index = viewModel.index
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.mainimageView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.right.equalToSuperview()
        }
    }
    
    override func didSelect() {
        self.onSelect?(self.index!)
    }
}
extension SubCollectionViewCell: WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel, width: CGFloat) -> CGFloat {
        return width
    }
}
struct SubCollectionViewCellModel : CollectionCellViewModel {
     var cellType: CollectionViewCell.Type { return SubCollectionViewCell.self }
     var imgUrl : String
     var index : Int
     var selectionHandler : ((Int) -> Void)?
}
