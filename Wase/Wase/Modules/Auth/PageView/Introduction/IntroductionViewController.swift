//
//  IntroductionViewController.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class IntroductionViewController : AirdronViewController {
    
    
    var onNext : ((String, String , String) -> Void)?
    let mainLabel = UILabel()
    private lazy var givenNameTextField = OMGTextField.makeGivenName()
    private lazy var familyNameTextField = OMGTextField.makeFamilyName()
    private lazy var nickNametextField = OMGTextField.makeNickName()
    private lazy var bottomLabel = UILabel()
    private lazy var scrollViewController = ScrollViewController()
    private lazy var keyObserver = KeyboardObserver()
    private let textfieldTopOffset: CGFloat = Device.isIphone5 ? 20 : 120
    private var activeField = UIView() {
        didSet {
            if activeField == self.nickNametextField{
                self.setScrollOffset(offset: 100)
            }
            else {
                self.setScrollOffset(offset: (self.navigationController?.navigationBar.bounds.size.height ?? 0) + ( UIApplication.shared.statusBarFrame.height))
            }
        }
    }
    
    private var apiService : ApiService
    
    init(apiService : ApiService) {
        self.apiService = apiService
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
      
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.add(self.scrollViewController)
        self.bottomLabel.attributedText = CustomFont.tech11.attributesWithParagraph.colored(color: Color.grey.value).make(string: "По нему вас смогут быстро найти \nИспользуйте только латиницу, цифры и __")
       
        self.bottomLabel.numberOfLines = 0
        self.mainLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.colored(color: Color.black.value).make(string: "Представьтесь, пожалуйста")
        
        mainLabel.numberOfLines = 0
        self.scrollViewController.scrollView.addSubview(self.givenNameTextField)
        self.scrollViewController.scrollView.addSubview(self.familyNameTextField)
        self.scrollViewController.scrollView.addSubview(self.nickNametextField)
        self.scrollViewController.scrollView.backgroundColor = Color.white.value
        self.scrollViewController.scrollView.addSubview(self.bottomLabel)
        
        self.scrollViewController.scrollView.addSubview(self.mainLabel)
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.setUpConstraints()
        self.setupHandlers()
        
        
        
    }
    
    
    @objc func nextView(){
        self.onNext?(self.givenNameTextField.text!, self.familyNameTextField.text!, self.nickNametextField.text!)
    }
    
    func setUpConstraints(){

        self.mainLabel.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.givenNameTextField.snp.makeConstraints{
            $0.top.equalTo(self.mainLabel.snp.bottom).offset(32)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.familyNameTextField.snp.makeConstraints{
            $0.top.equalTo(self.givenNameTextField.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.nickNametextField.snp.makeConstraints{
            $0.top.equalTo(self.familyNameTextField.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.bottomLabel.snp.makeConstraints{
            $0.top.equalTo(self.nickNametextField.snp.bottom).offset(5)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
    }
    
    var error = false
    func checkValid(){
        if (self.givenNameTextField.text?.isEmpty == false && self.familyNameTextField.text?.isEmpty == false && self.nickNametextField.text?.isEmpty == false) {
            self.apiService.getCheckNickname(nickname: self.nickNametextField.text!) { [weak self] response in
                switch response {
                case .success(let result):
                    print(" Success ")
                    guard let self = self else {return}
                    if result.available {
                        self.onNext?((self.givenNameTextField.text!), (self.familyNameTextField.text!), self.nickNametextField.text!)
                    } else {
                        self.error = true
                        self.nickNametextField.makeError(error: "Увы, такой никнейм уже занят")
                        UIView.animate(withDuration: 0.5, animations: {
                            self.bottomLabel.center.y += 7
                        })
                    }
                case .failure(let error):
                    print(error)
                }
                
            }
            
        }
        
        
    }
    
    var keyFrame = CGRect.init(x: 0.0, y: 451.0, width: 375.0, height: 216.0)
    func setScrollOffset(offset : CGFloat = 0){
        let keyMin = UIScreen.main.bounds.height - self.keyFrame.height
        let textFrame  = self.scrollViewController.scrollView.convert(self.activeField.frame, to: self.view)
       
        print("KeyMin: \(keyMin)\ntextFrame: \(textFrame.maxY)")
        
        if (textFrame.maxY + offset ) > keyMin {
            
            let scrollOffset = (textFrame.maxY + offset) - keyMin
            self.scrollViewController.scrollView.setContentOffset(CGPoint(x: self.scrollViewController.scrollView.contentOffset.x, y: scrollOffset), animated: true)
        } else {
            self.scrollViewController.scrollView.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    func setupHandlers(){
        self.keyObserver.onKeyboardDidShow = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            self?.keyFrame = frame
           // self?.scrollViewController.scrollView.contentSize.height = self?.scrollViewController.scrollView.contentSize.height ?? 0 + y
        }
        
        self.keyObserver.onKeyboardWillHide = { [weak self] frame, duration in
            self?.scrollViewController.scrollView.contentInset.bottom = 0
        }
        self.givenNameTextField.onBeginEditing = {[weak self] textField in
            self?.activeField = textField
        }
        self.familyNameTextField.onBeginEditing = {[weak self] textField in
            self?.activeField = textField
        }
        self.givenNameTextField.onReturnHandler = {[weak self] in
            self?.familyNameTextField.becomeResponder()
        }
        self.familyNameTextField.onReturnHandler = {[weak self] in
            self?.nickNametextField.becomeResponder()
        }
        self.nickNametextField.onTextDidChanged = {[weak self] text in
            guard let self = self else {return}
          
                if self.error{
                    self.nickNametextField.deleteError()
                    self.error = false
                    self.nickNametextField.separatorView.backgroundColor = Color.black.value
                    UIView.animate(withDuration: 0.5, animations: {
                        self.bottomLabel.center.y -= 7
                    })
                }
            
        }
        self.nickNametextField.onBeginEditing = {[weak self] textField in
            guard let self = self else {return}
            self.activeField = self.bottomLabel
            
            
           
        }
        
        self.nickNametextField.onReturnHandler = {[weak self] in
            self?.checkValid()
        }
    }
}
