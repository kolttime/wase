//
//  Image.swift
//  Taoka
//
//  Created by Роман Макеев on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

public struct ImageSrc: Codable {
    
    enum CodingKeys: String, CodingKey {
        case w320 = "320w"
        case w640 = "640w"
        case w800 = "800w"
        case w1100 = "1100w"
        case w1400 = "1400w"
        case w1600 = "1600w"
        case w2000 = "2000w"
        
    }
    
    let w320: URL?
    let w640: URL?
    let w800: URL?
    let w1100: URL?
    let w1400: URL?
    let w1600: URL?
    let w2000: URL?
    
    
}

 struct Image: Codable, Hashable {
    
    static func == (lhs: Image, rhs: Image) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
    }
    
    var base64Preview: String {
        return self.preview.components(separatedBy: ",").last ?? String.empty
    }
    
    let id: String
    let preview: String
    let original: URL
    let width: Int
    let height: Int
    let size: Int
    let srcset: ImageSrc?
    let createdAt : String
}

extension Image {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Image.self, from: data)
    }
}
extension Image: DictionaryMappable {
    
    func toDictionary() -> JsonDictionary {
        var dict = JsonDictionary()
        dict["image"] = self.id
        
        return dict
    }
}

extension Array where Element == Image {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Image].self, from: data)
    }
}
