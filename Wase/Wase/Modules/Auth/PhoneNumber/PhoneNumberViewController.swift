//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Artem Myachkov on 07/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import CoreTelephony
import libPhoneNumber_iOS
import SwiftPhoneNumberFormatter

class PhoneNumberViewController: SimpleNavigationBar{
    
    
    private lazy var loadingView  = TaokaLoadingView()
    
    private lazy var phoneNumberTextField = PhoneFormattedTextField()
    
    private let apiService: ApiService
    
    private let keyboardObserver = KeyboardObserver()
    
    var SMSLabel = UILabel()
    
    var regInfoLabel = UILabel()
    
    var onTextField : ((String, String) -> Void)?
    
    var russia = UILabel()
    
    var scrollViewController = ScrollViewController()
    
    
    private var activeField : UIView?
    private let textfieldTopOffset: CGFloat = Device.isIphone5 ? 20 : 120
    
    private var bottomConstraint: Constraint?

    private var bottomOffset: CGFloat = 20
    private lazy var mainlabel = UILabel()
    
    private let phoneUtil = NBPhoneNumberUtil()
    
    init(apiService: ApiService) {
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        
        super.initialSetup()
        self.setBackTitle(title: "Назад")
        //
//        self.navigationController?.navigationBar.prefersLargeTitles = false
        //self.setTitle(title: "Введите номер телефона")
        self.setLargeTitleHidden()
    
        
        if Device.isIphone5{
            print(5555555555)
        }
        
        
        SMSLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Мы отправим код через СМС для регистрации или входа")
        SMSLabel.numberOfLines = 0
    
        regInfoLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.black.value).make(string: "Регистрируясь, вы принимаете условия пользования и соглашаетесь на обработку персональных данных")
        regInfoLabel.numberOfLines = 0
        
        russia.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: .blue).make(string: "Рoccия") //*
        
        self.view.backgroundColor = Color.white.value
        
        phoneNumberTextField.config.defaultConfiguration = PhoneFormat(defaultPhoneFormat: "### ### ## ##")
        phoneNumberTextField.prefix = "+7 "
        phoneNumberTextField.font = UIFont.systemFont(ofSize: 27)
        
    
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        self.view.addSubview(self.scrollViewController.view)
        
        scrollViewController.scrollView.showsHorizontalScrollIndicator = false
        self.mainlabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.make(string: "Введите номер телефона")
        self.mainlabel.numberOfLines = 0
        self.scrollViewController.scrollView.addSubview(self.mainlabel)
        self.scrollViewController.scrollView.addSubview(self.SMSLabel)
        self.scrollViewController.scrollView.addSubview(self.regInfoLabel)
        self.scrollViewController.scrollView.addSubview(self.phoneNumberTextField)
        self.scrollViewController.scrollView.addSubview(self.russia)
        print("Scroll: \(self.scrollViewController.scrollView.contentOffset)")
        self.loadingView.isHidden = true
        self.scrollViewController.view.addSubview(self.loadingView)
        self.setupHandlers()
        self.SetupConstraints()
        
    }
    

    @objc func textFieldFull(){
        print("done")
        self.loadingView.isHidden = false
        //self.onTextField?(self.phoneNumberTextField.text!)
        var Str = self.phoneNumberTextField.text?.replacingOccurrences(of: " ", with: "")
        let finalStr = String(Str!.dropFirst())
        print(finalStr)
        self.apiService.auth(byPhone: finalStr){[weak self] result in
            switch result{
            case .success(let response):
                print(response)
                self?.loadingView.isHidden = true
                self?.onTextField?((self?.phoneNumberTextField.text!)!, response.key)
            case .failure(let error):
                print(error)
                self?.loadingView.isHidden = false
                }
        
            }

    }
    
    
    
    private func SetupConstraints(){
        
        
        self.scrollViewController.view.snp.makeConstraints{
            $0.top.equalToSuperview().offset(self.getTopOffset())
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.mainlabel.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        SMSLabel.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalTo(self.mainlabel.snp.bottom).offset(12)
        }
        
        regInfoLabel.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-115)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            self.bottomConstraint = $0.bottom.equalTo(self.bottomLayoutEdge).offset(-self.bottomOffset).constraint
        }
        
        phoneNumberTextField.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalTo(SMSLabel.snp.bottom).offset(34)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
        russia.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalTo(phoneNumberTextField.snp.bottom).offset(8)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.keyboardObserver.isSuspended = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.keyboardObserver.isSuspended = false
        phoneNumberTextField.becomeFirstResponder()
    }
    var keyFrame = CGRect.init(x: 0.0, y: 451.0, width: 375.0, height: 216.0)
    func setScrollOffset(offset : CGFloat = 0){
        if Device.isIphone5 {
            let keyMin = UIScreen.main.bounds.height - self.keyFrame.height
            let textFrame  = self.scrollViewController.scrollView.convert(self.regInfoLabel.frame, to: self.view)
            
            print("KeyMin: \(keyMin)\ntextFrame: \(textFrame.maxY)")
            
            if (textFrame.maxY + offset ) > keyMin {
                
                let scrollOffset = (textFrame.maxY + offset) - keyMin
                self.scrollViewController.scrollView.setContentOffset(CGPoint(x: self.scrollViewController.scrollView.contentOffset.x, y: scrollOffset), animated: true)
            } else {
                self.scrollViewController.scrollView.setContentOffset(CGPoint.zero, animated: true)
            }
        }
    }
    
}


extension String {
    internal func substring(start: Int, offsetBy: Int) -> String? {
        guard let substringStartIndex = self.index(startIndex, offsetBy: start, limitedBy: endIndex) else {
            return nil
        }
        
        guard let substringEndIndex = self.index(startIndex, offsetBy: start + offsetBy, limitedBy: endIndex) else {
            return nil
        }
        
        return String(self[substringStartIndex ..< substringEndIndex])
    }
}

extension PhoneNumberViewController{
    func setupHandlers(){
        phoneNumberTextField.textDidChangeBlock = { field in
            if let text = field?.text, text != "" {
                if text.count == 16{
                    self.textFieldFull()
                }
            } else {
                print("No text")
            }
        }
        
        
        self.keyboardObserver.onKeyboardDidShow = { [weak self] frame, duration in
            guard let self = self else {return}
            self.setScrollOffset(offset: self.regInfoLabel.frame.height)
            
            // self?.scrollViewController.scrollView.contentSize.height = self?.scrollViewController.scrollView.contentSize.height ?? 0 + y
        }

        
        self.keyboardObserver.onKeyboardWillShow = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.keyFrame = frame
            strongSelf.bottomConstraint?.update(offset: -frame.height
                - strongSelf.bottomOffset
                + strongSelf.bottomLayoutEdgeInset)
            strongSelf.view.animateLayout(duration: duration)
        }
        self.keyboardObserver.onKeyboardWillHide = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.bottomConstraint?.update(offset: -strongSelf.bottomOffset)
            strongSelf.view.animateLayout(duration: duration)
        }
    }
    
    }

