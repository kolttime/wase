//
//  TableViewCell.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 19/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

open class TableViewCell: UITableViewCell {
    
    public typealias ViewModelType = TableCellViewModel
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initialSetup()
        self.setupConstraints()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialSetup()
        self.setupConstraints()
    }
    
    open func initialSetup() {
        self.selectionStyle = .none
    }
    
    open func configure(viewModel: ViewModelType) {
        
    }
    
    open func willSelect() {
        
    }
    
    open func didSelect() {
        
    }
    
    open func setupConstraints() {
        
    }
    
    open class func height(for viewModel: ViewModelType,
                           tableView: UITableView) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
}
