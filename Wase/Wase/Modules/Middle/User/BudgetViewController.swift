//
//  IntroductionViewController.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class BudgetViewController : AirdronViewController {
    
    
    var onNext : ((Budget) -> Void)?
    lazy var tableViewController = TableViewController()
    private let loadingView = TaokaLoadingView()
    var Budgets : [Budget] = []
    
    private var budgetTypes: [BudgetTableCellViewModel] = []
    
    var photoCreation : CreatePhotosession
    private var apiService : ApiService
    private var userModel : User
    
    init(photoCreation : CreatePhotosession,apiService : ApiService, userModel : User) {
        self.photoCreation = photoCreation
        self.apiService = apiService
        self.userModel = userModel
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.loadingView.isHidden = false
        
        self.view.addSubview(tableViewController.tableView)
        view.addSubview(self.loadingView)
        self.tableViewController.tableView.register(cellClass: BudgetCell.self)
        self.tableViewController.tableView.register(headerFooterViewClass: MainLabelHeaderCell.self)
        
        self.fetchData()

        
        self.view.backgroundColor = Color.white.value
        self.setUpConstraints()
        
    }
    
    func fetchData(){
        var massivStringov : [String] = []
        self.loadingView.isHidden = false
        for type in photoCreation.participantsTypes ?? [] {
            massivStringov.append(type.rawValue)
        }
        self.apiService.getBudgest(specialization: (photoCreation.specialization?.id)!, duration: photoCreation.duration!, type: massivStringov){[weak self] response in
            switch response {
            case .success(let rr):
                self?.loadingView.isHidden = true
                self?.Budgets = rr
                print(rr)
                self?.render()
                self?.tableViewController.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }

    func render(){
        
        let selection : ((Budget?) -> Void)? = {[weak self] ints in
            self?.onNext?(ints!)
        }
        for i in Budgets{
            let mm = BudgetTableCellViewModel.init(budget: i, selectionHandler: selection)
            self.budgetTypes.append(mm)
        }
        let headerModel = MainLabelHeaderViewModel(mainLabel: "Какой у вас бюджет?") //?????
        let section = DefaultTableSectionViewModel(cellModels: budgetTypes, headerViewModel: headerModel, footerViewModel: nil)
        self.tableViewController.update(viewModels: [section])
        
    }
    
    func setUpConstraints(){
        
        tableViewController.tableView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableViewController.tableView.reloadData()
    }
    
}


