//
//  userCreation.swift
//  Taoka
//
//  Created by Роман Макеев on 14/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

class UserCreation: DictionaryMappable {
    
   
    var nickname : String?
    var givenName: String?
    var familyName: String?
    var type: UserType?
    var city: String?
  
    
    init() {
        self.nickname = nil
        self.givenName = nil
        self.familyName = nil
        self.type = nil
        self.city = nil
    }
    
    init(nickname : String?, givenName : String?, familyName : String?, type : UserType?, city : String?) {
        self.nickname = nickname
        self.givenName = givenName
        self.familyName = familyName
        self.type = type
        self.city = city
    }
    
    func toDictionary() -> JsonDictionary {
        guard let nickname = self.nickname,
            let givenName = self.givenName,
            let familyName = self.familyName,
            let type = self.type,
            
            let city = self.city else { return [:] }
        let parameters: [String: Any] = ["nickname": nickname,
                                         "given_name": givenName,
                                         "family_name": familyName,
                                         "type": type.rawValue,
                                         "city": city]
        return parameters
    }
    
}
