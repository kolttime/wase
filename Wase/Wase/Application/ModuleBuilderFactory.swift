//
//  ModuleBuilderFactory.swift
//  OMG
//
//  Created by Andrew Oparin on 06/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

protocol ModuleBuilderFactory {
    
    func makeAuthModuleBuilder() -> AuthModuleBuilder
    
    func makeUserReviewModuleBuilder() -> UserReviewModuleBuilder
    
    func makeUserProfileModuleBuilder() -> UserProfileModuleBuilder
    
    func makeUserMiddleModuleBuilder() -> UserMiddleModuleBuilder
    
    func makePhotographReviewModuleBuilder() -> PhotographReviewModuleBuilder
    
    func makePhotographMiddleModuleBuilder() -> PhotographMiddleModuleBuilder
    
    func makePhotographProfileModuleBuilder() -> PhotographProfileModuleBuilder
    
    func makeUserMenuModuleBuilder() -> UserMenuModuleBuilder
    
    func makePhotographMenuModuleBuilder() -> PhotographMenuModuleBuilder
    
}

final class ModuleBuilderFactoryImp: ModuleBuilderFactory {
    
    private let serviceContainer: ServiceContainer
    
    init(serviceContainer: ServiceContainer) {
        self.serviceContainer = serviceContainer
    }
    
    func makeAuthModuleBuilder() -> AuthModuleBuilder {
        return AuthModuleBuilder(apiService: self.serviceContainer.apiService, sessionManager: self.serviceContainer.sessionManager)
    }
    
    
    func makeUserReviewModuleBuilder() -> UserReviewModuleBuilder{
        return UserReviewModuleBuilder()
    }
    
    func makeUserMiddleModuleBuilder() -> UserMiddleModuleBuilder{
        return UserMiddleModuleBuilder(apiService: self.serviceContainer.apiService, sessionManager: self.serviceContainer.sessionManager)
    }
    
    func makeUserProfileModuleBuilder() -> UserProfileModuleBuilder{
        return UserProfileModuleBuilder(apiService: self.serviceContainer.apiService, sessionManager: self.serviceContainer.sessionManager)
    }
    
    func makePhotographReviewModuleBuilder() -> PhotographReviewModuleBuilder{
        return PhotographReviewModuleBuilder()
    }
    
    func makePhotographMiddleModuleBuilder() -> PhotographMiddleModuleBuilder{
        return PhotographMiddleModuleBuilder(apiService: self.serviceContainer.apiService, sessionManager: self.serviceContainer.sessionManager)
    }
    
    func makePhotographProfileModuleBuilder() -> PhotographProfileModuleBuilder{
        return PhotographProfileModuleBuilder(apiService: self.serviceContainer.apiService, sessionManager: self.serviceContainer.sessionManager)
    }
    
    func makeUserMenuModuleBuilder() -> UserMenuModuleBuilder{
        return UserMenuModuleBuilder()
    }
    
    func makePhotographMenuModuleBuilder() -> PhotographMenuModuleBuilder{
        return PhotographMenuModuleBuilder()
    }

}
