//
//  AirdronViewController.swift
//  MDW
//
//  Created by Andrew Oparin on 20/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

open class AirdronViewController: UIViewController, AirdronTabDataSource {
    
    public weak var airdronTabbarController: AirdronTabbarController?
    public var airdronTabItem = AirdronTabItem()
    
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func needShowTabbar() -> Bool {
        return true
    }
}
