//
//  SubCollectionViewCell.swift
//  Taoka
//
//  Created by Роман Макеев on 13/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class AddAlbumCollectionViewCell : CollectionViewCell {
    
    var onSelect : Action?
    let mainimageView = UIView()
    let plusImage = UIImageView()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.mainimageView)
        self.mainimageView.addSubview(self.plusImage)
        self.mainimageView.layer.masksToBounds = true
        self.plusImage.image = UIImage(named: "plus")!
        self.backgroundColor = Color.light.value
        self.plusImage.contentMode = .scaleAspectFill
        self.layoutIfNeeded()
        self.setupConstraints()
    }
    
    override func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! AddAlbumCollectionViewCellModel
        self.onSelect = viewModel.selectionHandler
        
    }
    override func setupConstraints() {
        super.setupConstraints()
        self.mainimageView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.right.equalToSuperview()
        }
        self.plusImage.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
            $0.width.equalTo(16)
            $0.height.equalTo(16)
        }
    }
    
    override func didSelect() {
        self.onSelect?()
    }
}
extension AddAlbumCollectionViewCell: WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel, width: CGFloat) -> CGFloat {
        return width
    }
}
struct AddAlbumCollectionViewCellModel : CollectionCellViewModel {
    var cellType: CollectionViewCell.Type { return AddAlbumCollectionViewCell.self }
    //var imgUrl : String
    var selectionHandler : Action?
}
