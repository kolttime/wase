//
//  LongPhotosession.swift
//  Taoka
//
//  Created by Роман Макеев on 06/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct LongPhotosession: Codable {
    
    let participantsTypes : [UserType]
    let description : String?
    let freedom : Bool?
    let comment : String?
    let budget : [Int]
    let id : String
    let offers : [Offers]?
    let status : PhotosessionStatus
    let time : String
    let duration : Int
    let title : String
    let date : String
    let author : UserShort?
    let price : Int?
    let createdAt : String
    let specialization : Specialization
    let place : Place
    init(participantsTypes : [UserType], description : String? , freedom : Bool?, comment : String? ,budget : [Int] ,id : String, offers : [Offers], status : PhotosessionStatus, title : String, duration : Int, time : String, date : String, author : UserShort, price : Int, createdAt : String, specialization: Specialization, place : Place) {
        self.participantsTypes =  participantsTypes
        self.description = description
        self.freedom = freedom
        self.comment = comment
        self.budget = budget
        self.id = id
        self.author = author
        self.price = price
        self.offers = offers
        self.status = status
        self.title = title
        self.duration = duration
        self.time = time
        self.date = date
        self.createdAt = createdAt
        self.specialization = specialization
        self.place = place
    }
    
}
extension LongPhotosession {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(LongPhotosession.self, from: data)
    }
}

extension Array where Element == LongPhotosession {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([LongPhotosession].self, from: data)
    }
}


