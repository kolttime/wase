//
//  ApproveView.swift
//  OMG
//
//  Created by Minic Relocusov on 21/04/2019.
//  Copyright © 2019 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit


class SupportAndVersionClass : AirdronView {
    
    var textLabel  = UILabel()
    var textDecsr = UILabel()
    var separator = UIView()
    var image = UIImageView(image: .init(UIImage(named: "Arrow2"))!)
    var versionLabel = UILabel()
    
    init(textLabel : String, textDescr : String, versionLabel: String) {
        self.textLabel.attributedText = CustomFont.bodyBold20.attributesWithParagraph.colored(color: Color.black.value).make(string: textLabel)
        self.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: textDescr)
        self.versionLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: versionLabel)
        super.init(frame: .zero)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = Color.white.value
        self.addSubview(self.textLabel)
        self.addSubview(self.textDecsr)
        self.addSubview(self.separator)
        self.addSubview(self.image)
        self.addSubview(versionLabel)
        self.separator.backgroundColor = .lightGray
        self.setupConstraints()
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.textLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(23.5)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
        self.textDecsr.snp.makeConstraints{
            $0.top.equalTo(textLabel.snp.bottom).offset(16)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
        self.separator.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview()
            $0.height.equalTo(0.5)
            $0.bottom.equalToSuperview()
        }
        
        self.image.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-22)
            $0.top.equalToSuperview().offset(72)
            $0.width.equalTo(8)
            $0.height.equalTo(13)
        }
        
        self.versionLabel.snp.makeConstraints{
            $0.top.equalTo(textLabel.snp.bottom).offset(16)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
    }
    
    override var intrinsicContentSize : CGSize{
        let width = UIView.noIntrinsicMetric
        return CGSize(width: width, height: 50)
    }
}



