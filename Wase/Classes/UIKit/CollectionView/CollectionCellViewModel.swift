//
//  CollectionCellViewModel.swift
//  MDW
//
//  Created by Andrew Oparin on 27/08/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

public protocol CollectionCellViewModel {
    
    var cellType: CollectionViewCell.Type { get }
}

public protocol CollectionSectionViewModel {
    
    var cellModels: [CollectionCellViewModel] { get set }
    var headerViewModel: CollectionHeaderFooterViewModel? { get }
    var footerViewModel: CollectionHeaderFooterViewModel? { get }
}

public struct DefaultCollectionSectionViewModel: CollectionSectionViewModel {
    
    public var cellModels: [CollectionCellViewModel]
    public var headerViewModel: CollectionHeaderFooterViewModel?
    public var footerViewModel: CollectionHeaderFooterViewModel?
    
    public init(cellModels: [CollectionCellViewModel],
                headerViewModel: CollectionHeaderFooterViewModel? = nil,
                footerViewModel: CollectionHeaderFooterViewModel? = nil) {
        self.cellModels = cellModels
        self.headerViewModel = headerViewModel
        self.footerViewModel = footerViewModel
    }
}
