//
//  Endpoints.swift
//  OMG
//
//  Created by Andrew Oparin on 12/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

enum Endpoints {
    
    case auth
    case verification
    case activation
    case profile
    case albums
    case nickname(name: String)
    case specialization
    case cities
    case upload
    case profileImages
    case profileAlbums
    case instagramAuth
    case profileAlbum(id : String)
    case usersAlbums(id : String, albumId : String)
    case pages(name : String)
    case candidates(id : String)
    case instagramRedirect
    case instagramLogin
    case instagramCode
    case photosessions
    case createPhotosession
    case takeDistricts(id: String)
    case takeBudget
    case profileUserImages(id : String)
    case profileUserAlbum(userId : String, id : String)
    case users(id : String)
    case profileUserAlbums(id : String)
    case photosessionsOffers(id : String)
    case longPhotosession(id : String)
    case photoOffer(id : String)
    
    
    static var host: String  { return "http://84.201.185.177:8080/api/v1" }
    static var urlAddress: String  { return "http://84.201.185.177:8080/" }
    static var sharingAddress: String  { return "http://84.201.185.177:8080/" }
    
    var url: String {
        switch self {
        case .auth:
            return Endpoints.host + "/authorization"
        case .verification:
            return Endpoints.host + "/verification"
        case .activation:
            return Endpoints.host + "/activation"
        case .profile:
            return Endpoints.host + "/profile"
        case .albums:
            return Endpoints.host + "/albums"
        case .nickname(let name):
            return Endpoints.host + "/check/nickname/\(name)"
        case .specialization:
            return Endpoints.host + "/static/specializations"
        case .cities:
            return Endpoints.host + "/static/cities"
        case .upload:
            return Endpoints.host + "/upload"
        case .profileImages:
            return Endpoints.host + "/profile/images"
        case .pages(let name):
            return Endpoints.host + "/static/pages/\(name)"
        case .profileAlbums:
            return Endpoints.host + "/profile/albums"
        case .profileAlbum(let id):
            return Endpoints.host + "/profile/albums/\(id)"
        case .usersAlbums(let id, let albumId):
            
            //return Endpoints.host + "/users/\(id)/albums/\(id)"
            
            return Endpoints.host + "/users/\(id)/albums/\(albumId)"
            
            
        case .instagramRedirect:
            return Endpoints.host + "/profile/social_network/instagram"
        case .instagramLogin:
            return Endpoints.host + "/oauth/instagram"
        case .instagramAuth:
            return "https://api.instagram.com/oauth/authorize/"
        case .instagramCode:
            return Endpoints.host + "/profile/social_network/instagram?code"
        case .createPhotosession:
            return Endpoints.host + "/photosessions"
        case .takeDistricts(let id):
            return Endpoints.host + "/static/cities/\(id)/districts"
        case .takeBudget:
            return Endpoints.host + "/budgets"
        case .candidates(let id):
            return Endpoints.host + "/photosessions/\(id)/candidates"
        case .photosessions:
            return Endpoints.host + "/photosessions"
        case .profileUserImages(let id):
            return Endpoints.host + "/users/\(id)/images"
        case .profileUserAlbum(let userId, id: let id):
            return Endpoints.host + "/users/\(userId)/albums/\(id)"
        case .users(let id):
            return Endpoints.host + "/users/\(id)"
        case .profileUserAlbums(let id):
            return Endpoints.host + "/users/\(id)/albums"
            
        case .photosessionsOffers(let id):
            return Endpoints.host + "/photosessions/\(id)/offers"
        case .longPhotosession(let id):
            return Endpoints.host + "/photosessions/\(id)"
        case .photoOffer(let id):
            return Endpoints.host + "/photosessions/\(id)/offer"
            
            
        }
    }
}


