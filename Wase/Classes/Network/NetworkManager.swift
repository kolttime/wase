//
//  NetworkManager.swift
//  AirdronCore
//
//  Created by andrew oparin on 28/11/2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation
import Alamofire
public class NetworkManager {
    
    private static let defaultNetworkBackgroundIdentifier = "com.airdron.network.backround"
    
    private let requestExecuter: RequestExecuter = RequestExecuter()
    private let requestBuilder: DataRequestBuilder
    private let sessionManager: SessionManager
    
    init(sessionManager: SessionManager, requestBuilder: DataRequestBuilder) {
        self.requestBuilder = requestBuilder
        self.sessionManager = sessionManager
    }
    
    public func perform<T>(request: AirdronRequest,
                           processData: @escaping ((_ responseData: Any) throws -> T),
                           serializer: ResponseSerializer = JSONSerializer(),
                           completionHandler: ((Result<T>) -> Void)?,
                           responseHandler: ((HTTPURLResponse?) -> Void)? = nil) {
        let dataRequest = self.requestBuilder.make(request: request)
        self.requestExecuter.execute(dataResponsible: dataRequest,
                                     processData: processData,
                                     serializer: serializer,
                                     completionHandler: completionHandler,
                                     responseHandler: responseHandler)
        dataRequest.resume()
    }
    
    public func perform<T>(request: AirdronRequest,
                           processData: @escaping ((_ responseData: Data) throws -> T),
                           completionHandler: ((Result<T>) -> Void)?,
                           responseHandler: ((HTTPURLResponse?) -> Void)? = nil) {
        let dataRequest = self.requestBuilder.make(request: request)
        self.requestExecuter.execute(dataResponsible: dataRequest,
                                     processData: processData,
                                     completionHandler: completionHandler,
                                     responseHandler: responseHandler)
        dataRequest.resume()
    }
    
    public func performUpload<T>(data: Data,
                              request: AirdronRequest,
                              processData: @escaping ((_ responseData: Any) throws -> T),
                              serializer: ResponseSerializer = JSONSerializer(),
                              completionHandler: ((Result<T>) -> Void)?,
                              responseHandler: ((HTTPURLResponse?) -> Void)? = nil) {
        let dataRequest = self.requestBuilder.makeUpload(data: data, request: request)
        self.requestExecuter.execute(dataResponsible: dataRequest,
                                     processData: processData,
                                     serializer: serializer,
                                     completionHandler: completionHandler,
                                     responseHandler: responseHandler)
        dataRequest.resume()
    }
                              
    
    public func makeWrapperPerformer<T>(request: AirdronRequest,
                                      processData: @escaping ((_ responseData: Any) throws -> T),
                                      serializer: ResponseSerializer = JSONSerializer(),
                                      completionHandler: ((Result<T>) -> Void)?,
                                      responseHandler: ((HTTPURLResponse?) -> Void)? = nil) -> AirdronDataRequestWrapper {
        let dataRequest = self.requestBuilder.make(request: request)
        self.requestExecuter.execute(dataResponsible: dataRequest,
                                     processData: processData,
                                     serializer: serializer,
                                     completionHandler: completionHandler,
                                     responseHandler: responseHandler)
        return AirdronDataRequestWrapper(dataRequest: dataRequest)
    }
    
    public func makeWrapperPerformer<T>(request: AirdronRequest,
                                        processData: @escaping ((_ responseData: Data) throws -> T),
                                        completionHandler: ((Result<T>) -> Void)?,
                                        responseHandler: ((HTTPURLResponse?) -> Void)? = nil) -> AirdronDataRequestWrapper {
        let dataRequest = self.requestBuilder.make(request: request)
        self.requestExecuter.execute(dataResponsible: dataRequest,
                                     processData: processData,
                                     completionHandler: completionHandler,
                                     responseHandler: responseHandler)
        dataRequest.resume()
        return AirdronDataRequestWrapper(dataRequest: dataRequest)
    }
    
    public func cancelAllTasks() {
        self.sessionManager.session.getAllTasks { tasks in
            tasks.forEach { $0.cancel() }
        }
    }
}

extension NetworkManager {
    
    public static func `default`() -> NetworkManager {
        let sessionManager = Alamofire.SessionManager(configuration: URLSessionConfiguration.default)
        sessionManager.startRequestsImmediately = false
        let requestBuilder = DataRequestBuilder(sessionManager: sessionManager)
        return NetworkManager(sessionManager: sessionManager, requestBuilder: requestBuilder)
    }
    
    public static func background() -> NetworkManager {
        let configuration = URLSessionConfiguration.background(withIdentifier: self.defaultNetworkBackgroundIdentifier)
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        sessionManager.startRequestsImmediately = false
        let requestBuilder = DataRequestBuilder(sessionManager: sessionManager)
        return NetworkManager(sessionManager: sessionManager, requestBuilder: requestBuilder)
    }
}
