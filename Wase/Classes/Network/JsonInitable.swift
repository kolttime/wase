//
//  JsonInitable.swift
//  AirdronCoreTests
//
//  Created by andrew oparin on 27/11/2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation

public typealias JsonDictionary = [String: Any]

public protocol JsonInitable {
    
    init(json: JsonDictionary) throws
}
