//
//  Functions.swift
//  Taoka
//
//  Created by Minic Relocusov on 30/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


public func roundNumber(num : Float) -> Int{
    let full : Int = Int(num)
    if Float(full) == num {
        return full
    } else {
        return full + 1
    }
}

public func getCardHeight() -> CGFloat {
    var height : CGFloat = Device.isIphoneXSmax || Device.isIphoneX ? 180 : 160
    if Device.isIphone5 {
        height = 145
    }
    return height
}


class HelpFunctions  {
    static public func setButtonOffset(view : UIView, scrollView : UIView, frameView : UIView, navBar : UIView, btnHeight : CGFloat = 56, bottomOffset : CGFloat = 58, standartOffset : CGFloat = 40) -> CGFloat {
        let navheight = navBar.frame.height + UIApplication.shared.statusBarFrame.height
        let height = UIScreen.main.bounds.height
        let frame : CGRect = view.convert(frameView.frame, from: scrollView)
        let maxY = frameView.frame.maxY
        let interval = height - maxY - bottomOffset - btnHeight - navheight
        print("maxY : \(frameView.frame.maxY)\nheight : \(height)\ninterval : \(interval)\nframe \(frame)\nframeFrameView : \(frameView.frame)")
        if interval < standartOffset {
            return standartOffset
        } else {
            return interval
        }
    }
    
    static public func getNavigationHeight(navBar : UIView) -> CGFloat {
        return UIApplication.shared.statusBarFrame.height + navBar.frame.height
    }
    
    static func setScrollOffset(scrollView : UIScrollView, view : UIView ,activeView : UIView, keyHeight : CGFloat = 500, offset : CGFloat = 0){
        let textMaxY = activeView.frame.maxY + offset
        let keyMinHeight = view.frame.height - keyHeight
        print("textMaxY : \(textMaxY)\nKeyHeight : \(keyHeight)\nkeyMinHeigh : \(keyMinHeight)")
        
        if textMaxY > keyMinHeight {
            let scrollOffset = textMaxY - keyMinHeight
            print("scrollOffset : \(scrollOffset)")
            scrollView.setContentOffset(CGPoint(x: 0, y: -scrollOffset), animated: true)
        } else {
            scrollView.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    
    static public func userModelChanged(first : User, second : User) -> Bool {
        
        if first.givenName != second.givenName {return true}
        if first.familyName != second.familyName {return true}
        if first.nickname != second.nickname {return true}
        if first.avatar != second.avatar {return true}
        if first.city.id != second.city.id {return true}
        if first.description != second.description {return true}
        if checkSpecializations(first: first.services?.specializations ?? [], second: second.services?.specializations ?? []) {
            return true
        }
        if first.services?.workDays != second.services?.workDays {
            return true
        }
        if first.services?.vacant != second.services?.vacant {
            return true
        }
        if first.services?.mobile != second.services?.vacant{
            return true
        }
        if first.services?.price != second.services?.price {
            return true
        }
        if first.services?.workHours != second.services?.workHours {
            return true
        }
        
        return false
    }
    
    static func checkSpecializations(first : [Specialization], second : [Specialization]) -> Bool {
        
        if first.count != second.count {
            return true
        }
        
        for spec in first {
            if !second.contains(spec) {
                return true
            }
        }
        
        return false
    }
}
