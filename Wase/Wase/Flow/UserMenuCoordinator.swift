//
//  MenuCoordinator.swift
//  OMG
//
//  Created by Andrew Oparin on 11/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

protocol UserMenuCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startUserMenuFlow(presenter: AirdronNavigationController)
}

extension UserMenuCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startUserMenuFlow(presenter: AirdronNavigationController) {
        let coordinator = self.coordinatorFactory.makeUserMenuCoordinator(presenter: presenter, coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.start()
    }
}

class UserMenuCoordinator: BaseCoordinator, UserReviewCoordinatorFlowPresentable, UserMiddleCoordinatorFlowPresentable, UserProfileCoordinatorFlowPresentable {
   
    
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: UserMenuModuleBuilder
    private let tabbarViewController = AirdronTabbarController()
    
    enum TabIndex: Int {
        case castings
        case portfolio
        case profile
        
        var image: UIImage {
            switch self {
            case .castings:
                return UIImage(named: "A")!
            case .portfolio:
                return UIImage(named: "A")!
            case .profile:
                return UIImage(named: "A")!
            }
        }
        
        var text: String {
            switch self {
            case .castings:
                return "one"
            case .portfolio:
                return "two"
            case .profile:
                return "three"
            }
        }
        
        static let allValues: [TabIndex] = [.castings, .portfolio, .profile]
    }
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: UserMenuModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        let tabbarViewController = self.makeTabbarController()
        self.presenter.setViewControllers([tabbarViewController], animated: false)
        tabbarViewController.selectTab(atIndex: TabIndex.castings.rawValue)
    }
    

    private func makeTabbarController() -> AirdronTabbarController {
        let tabbarViewController = AirdronTabbarController()
        tabbarViewController.setViewControllers(viewControllers: self.makeTabControllers())
        tabbarViewController.separatorColor = Color.light.value
        tabbarViewController.onSelectedTab = { [weak self, weak tabbarViewController] index in
            guard let navigationController = tabbarViewController?.viewControllers[index] as? AirdronNavigationController else { return }
            switch TabIndex(rawValue: index) {
            case .castings?:
                if navigationController.topViewController == nil {
                    self?.startUserReviewFlow(presenter: navigationController) {
                        tabbarViewController?.selectTab(atIndex: TabIndex.profile.rawValue)
                    }
                }
            case .portfolio?:
                if navigationController.topViewController == nil {
                    self?.startUserMiddleFlow(presenter: navigationController)
                }
            case .profile?:
                if navigationController.topViewController == nil {
                    self?.startUserProfileFlow(presenter: navigationController)
                }
            default:
                break
            }
        }
        return tabbarViewController
    }
    
    
    private func makeTabControllers() -> [AirdronTabResponder] {
        return TabIndex.allValues.map { return self.makeTabWith(image: $0.image,
                                                                text: $0.text) }
        
    }
    
    private func makeTabWith(image: UIImage, text: String) -> AirdronTabResponder {
        let vc = AirdronNavigationController()
        let normalColor = Color.greyL.value
        let selectedColor = Color.black.value
        vc.airdronTabItem.normalImage = image
        vc.airdronTabItem.selectedImage = image
        vc.airdronTabItem.text = text
        vc.airdronTabItem.normalTextColor = normalColor
        vc.airdronTabItem.selectedTextColor = selectedColor
        vc.airdronTabItem.normalImageColor = normalColor
        vc.airdronTabItem.selectedImageColor = selectedColor
        vc.view.backgroundColor = Color.white.value
        return vc
    }
}

