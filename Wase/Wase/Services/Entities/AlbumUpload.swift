//
// User.swift
// OMG
//
// Created by Andrew Oparin on 06/10/2018.
// Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

struct AlbumUpload: Codable {
    
    let title : String
    let specialization : String
    let date : String
    
    init(title : String, specialization : String, date : String) {
        self.title = title
        self.specialization = specialization
        self.date = date
    }
    
    func toDictionary() -> JsonDictionary {
        let parameters : [String : Any] = ["title" : self.title,
                                           "specialization" : self.specialization,
                                           "date" : self.date]
        return parameters
    }
    
}



extension AlbumUpload {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(AlbumUpload.self, from: data)
    }
}

extension Array where Element == AlbumUpload {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([AlbumUpload].self, from: data)
    }
}
