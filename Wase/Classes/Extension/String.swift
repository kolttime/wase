//
//  String.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 19/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

public extension String {
    
    public func height(width: CGFloat, font: UIFont) -> CGFloat {
        let boundingRect = self.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude),
                                             options: .usesLineFragmentOrigin,
                                             attributes: [NSAttributedString.Key.font: font],
                                             context: nil)
        let height = ceil(boundingRect.height)
        return height
    }
    
    public func width(height: CGFloat, font: UIFont) -> CGFloat {
        let boundingRect = self.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: height),
                                             options: .usesLineFragmentOrigin,
                                             attributes: [NSAttributedString.Key.font: font],
                                             context: nil)
        let width = ceil(boundingRect.width)
        return width
    }
}

public extension String {
    
    func hasMatches(forSearchText searchText: String) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: "\\b\(searchText.words().joined(separator: " "))") else {
            return false
        }
        let separatedWords = self.words().joined(separator: " ")
        let nsString = separatedWords as NSString
        return !regex.matches(in: separatedWords, range: NSRange(location: 0, length: nsString.length)).isEmpty
    }
    
    func words() -> [String] {
        var words: [String] = []
        self.lowercased().enumerateSubstrings(in: startIndex..<endIndex, options: .byWords) { string, _, _, _ in
            guard let word = string?.lowercased() else { return }
            words.append(word)
        }
        return words
    }
}

public extension String {
    
    static var empty: String { return "" }
    
    var isEmail: Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}

public extension String {
    
    var formatedForBackTitle: String {
        return self.count > 7 ? String(self.prefix(5)) + ".." : self
    }
}
