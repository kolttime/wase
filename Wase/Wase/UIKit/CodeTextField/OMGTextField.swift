//
//  OMGTextField.swift
//  OMG
//
//  Created by Andrew Oparin on 06/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import InputMask
import CoreTelephony
import libPhoneNumber_iOS

typealias TextFieldResponse = (text: String, mandatoryComplete: Bool)

protocol ModifyTextDelegate: class {
    
    func textField(_ textField: OMGTextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String)
}


class OMGTextField: AirdronView {
    
    private let contentHeight: CGFloat = 50
    var needShowTopLabel = true {
        didSet {
            self.titleLabel.alpha = self.needShowTopLabel ? 1 : 0
        }
    }
    
    var onTextDidChanged: ((TextFieldResponse) -> Void)?
    var onReturnHandler: Action?
    var onBeginEditing: ((OMGTextField) -> Void)?
    
    lazy var titleLabel = UILabel()
    lazy var textField = UITextField()
    lazy var separatorView = UIView()
    
    lazy var detailLabel = UILabel()
    
    // MARK: Can not use with modifyTextFieldDelegate simultaneously
    var maskedTextViewHandler: MaskedTextFieldDelegate? {
        didSet {
            if let handler = self.maskedTextViewHandler {
                self.textField.removeTarget(self, action: #selector(textDidChange), for: .editingChanged)
                handler.listener = self
                self.textField.delegate = handler
            } else {
                if !self.textField.canPerformAction(#selector(textDidChange), withSender: self) {
                    self.textField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
                }
                self.textField.delegate = self
            }
        }
    }
    
    // MARK: Can not use with maskedTextViewHandler simultaneously
    weak var modifyTextFieldDelegate: ModifyTextDelegate?
    
    private let placeholderAttributes = CustomFont.bodyRegular17.attributes.colored(color: Color.grey.value) //*******
    public var inputTextAttributes = CustomFont.bodyRegular17.attributes.colored(color: Color.black.value)
    private let titleAttributes = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value)
    private let detailAttributes = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value)
    
    var textAlignment: NSTextAlignment = .left {
        didSet {
            self.textField.textAlignment = self.textAlignment
        }
    }
    var placeholder: String = String.empty {
        didSet {
            self.textField.attributedPlaceholder = self.placeholderAttributes.make(string: self.placeholder)
            self.titleLabel.attributedText = self.titleAttributes.make(string: self.placeholder)
        }
    }
    var keyboardType: UIKeyboardType = .default {
        didSet {
            self.textField.keyboardType = self.keyboardType
        }
    }
    
    var returnKeyType: UIReturnKeyType = .default {
        didSet {
            self.textField.returnKeyType = self.returnKeyType
        }
    }
    
    var autocorrectionType: UITextAutocorrectionType = .default {
        didSet {
            self.textField.autocorrectionType = self.autocorrectionType
        }
    }
    
    var autocapitalizationType: UITextAutocapitalizationType = .sentences {
        didSet {
            self.textField.autocapitalizationType = self.autocapitalizationType
        }
    }
    
    var isSecureTextEntry: Bool {
        set {
            self.textField.isSecureTextEntry = newValue
        }
        get {
            return self.textField.isSecureTextEntry
        }
    }
    
    var text: String? {
        set {
            self.textField.text = newValue
            guard self.needShowTopLabel else { return }
            if newValue?.isEmpty == false {
                self.titleLabel.alpha = 1
            } else {
                self.titleLabel.alpha = 0
            }
        }
        get {
            return self.textField.text
        }
    }
    
    var detailText: String? {
        didSet {
            if let text = self.detailText {
                self.detailLabel.attributedText = self.detailAttributes.make(string: text)
            } else {
                self.detailLabel.attributedText = nil
            }
        }
    }
    
    func resignResponder() {
        self.textField.resignFirstResponder()
    }
    
    func becomeResponder() {
        self.textField.becomeFirstResponder()
    }
    
    private func handle(text: String, mandatoryComplete: Bool) {
        self.onTextDidChanged?((text: self.textField.text ?? String.empty, mandatoryComplete: mandatoryComplete))
        guard self.needShowTopLabel else { return }
        if self.textField.text?.isEmpty == false {
            UIView.animate(withDuration: 0.2) {
                self.titleLabel.alpha = 1
            }
        } else {
            UIView.animate(withDuration: 0.2) {
                self.titleLabel.alpha = 0
            }
        }
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.titleLabel)
        self.addSubview(self.textField)
        self.addSubview(self.separatorView)
        self.addSubview(self.detailLabel)
        
        self.separatorView.backgroundColor = Color.light.value
        self.textField.defaultTextAttributes = self.inputTextAttributes
        self.titleLabel.alpha = 0
        self.textField.delegate = self
        self.textField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        self.setNeedsUpdateConstraints()
        self.invalidateIntrinsicContentSize()
        
        
    }
    let errorLabel = UILabel()
    func makeError(error : String) {
        self.addSubview(self.errorLabel)
        self.errorLabel.attributedText = CustomFont.tech11.attributesWithParagraph.colored(color: Color.red.value).make(string: error)
        self.errorLabel.snp.makeConstraints{
            $0.top.equalTo(self.separatorView.snp.bottom)
            $0.left.equalTo(self.separatorView)
        }
        self.separatorView.backgroundColor = Color.red.value
    }
    func deleteError(){
        self.errorLabel.removeFromSuperview()
        //self.separatorView.backgroundColor = Color.grey44.value
    }
    @objc
    func textDidChange() {
        self.handle(text: self.textField.text ?? String.empty, mandatoryComplete: true)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(1).priority(ConstraintPriority.high)
            $0.right.equalToSuperview()
        }
        self.textField.snp.makeConstraints {
            $0.left.equalToSuperview().priority(ConstraintPriority.high)
            $0.right.equalToSuperview()
            $0.top.equalTo(self.titleLabel.snp.bottom).offset(5)
        }
        self.separatorView.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
        }
        self.detailLabel.snp.makeConstraints {
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-8)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: self.contentHeight)
    }
    
    func showError() {
        self.shake()
        self.textField.becomeFirstResponder()
        UIView.animate(withDuration: UIView.shakeDuration, animations: { self.separatorView.backgroundColor = Color.black.value })
    }
}

extension OMGTextField: MaskedTextFieldDelegateListener {
    
    func textField(_ textField: UITextField,
                   didFillMandatoryCharacters complete: Bool,
                   didExtractValue value: String) {
        self.handle(text: value, mandatoryComplete: complete)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.onReturnHandler?()
        return true
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let modifyDelegate = self.modifyTextFieldDelegate {
            modifyDelegate.textField(self,
                                     shouldChangeCharactersIn: range,
                                     replacementString: string)
            self.handle(text: self.textField.text ?? String.empty, mandatoryComplete: true)
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.onBeginEditing?(self)
        self.separatorView.backgroundColor = Color.black.value
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.separatorView.backgroundColor = Color.grey.value
    }
    
    // Need because it is implemented in radmadrobots MaskedTextFieldDelegateListener
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        self.separatorView.backgroundColor = Color.grey.value
    }
}

extension OMGTextField {
    
    static func makeInputNumberTextField() -> OMGTextField {
        let field = OMGTextField()
        field.needShowTopLabel = false
        field.autocorrectionType = .no
        field.separatorView.isHidden = true
        field.keyboardType = .phonePad
        field.inputTextAttributes = CustomFont.bodyRegular20.attributes.colored(color: Color.black.value)
        field.textField.defaultTextAttributes = field.inputTextAttributes
        return field
    }
    
    static func makeInviteTextField() -> OMGTextField {
        let field = OMGTextField()
        field.needShowTopLabel = false
        field.placeholder = "Sending a message with greetings from you and a link to the app in App Store"
        field.autocorrectionType = .no
        field.separatorView.isHidden = true
        field.keyboardType = .phonePad
        return field
    }
    
    static func makePhotoCommenTextField() -> OMGTextField {
        let field = OMGTextField()
       // field.needShowTopLabel = false
        field.placeholder = "Сообщение"
        field.autocorrectionType = .no
        //field.separatorView.isHidden = true
       // field.keyboardType = .phonePad
        return field
    }
    
    static func makeTelephoneTF() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Телефон"
        field.autocorrectionType = .no
        field.returnKeyType = .next
//        field.keyboardType = .phonePad ??
        return field
    }
    
    static func makeCityTF() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Город"
        field.autocorrectionType = .no
        return field
    }
    
    static func makeDescrTF() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Подпись в профиле"
        field.autocorrectionType = .no
        return field
    }
    
    static func makeGivenName() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Имя"
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }
    
    static func makeAdress() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Адрес"
        field.autocorrectionType = .no
        field.returnKeyType = .send
        return field
    }
    
    static func makeDate() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "День"
        field.autocorrectionType = .no
        
        return field
    }
    
    static func makeStartTime() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Начало"
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }
    
    static func makeTiming() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Продолжительность"
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }

    static func makeFamilyName() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Фамилия"
        field.autocorrectionType = .no
        return field
    }
    static func makeNickName() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Никнейм"
        field.autocorrectionType = .no
        field.keyboardType = .asciiCapable
        field.returnKeyType = .next
        return field
    }
    static func makeAlbumName() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Название альбома"
        field.autocorrectionType = .no
        field.returnKeyType = .done
        return field
    }
    static func specializationField() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Специализация"
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }
    
    static func doingField() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Деятельность"
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }
    
    static func costField() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Стоимость услуг, ₽ час"
        field.autocorrectionType = .no
        field.keyboardType = .numberPad
//        field.keyboardType = UIKeyboardType.phonePad ??
        return field
    }
    
    static func timeField() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Рабочие часы"
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }
    
    static func weekField() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = "Рабочие дни"
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }
    
    static func professionField() -> OMGTextField{
        let field = OMGTextField()
        field.placeholder = "Профессия"
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }
    
    static func EmailField() -> OMGTextField{
        let field = OMGTextField()
        field.placeholder = "Email"
        field.autocorrectionType = .no
        field.returnKeyType = .done
        return field
    }
    
    static func makeCityField() -> OMGTextField{
        let field = OMGTextField()
        field.placeholder = "Город"
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }
    
    static func NamingField() -> OMGTextField{
        let field = OMGTextField()
        field.placeholder = "Название"
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }
    
    static func CommentField() -> OMGTextField{
        let field = OMGTextField()
        field.placeholder = "Комментарий"
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }
    
}

extension PhoneNumberViewController: ModifyTextDelegate {
    
    func textField(_ textField: OMGTextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) {
        
            let countryCode = 7
            let formatter = NBAsYouTypeFormatter.init(regionCode: String(countryCode))
            // TODO: Хуйня ?
            if isDeletion(inRange: range, string: string) {
                let updatedText: String = self.replaceCharacters(inText: textField.text ?? String.empty,
                                                                 range: range,
                                                                 withCharacters: String.empty)
                textField.text = updatedText
                textField.textField.caretPosition = range.location
            } else {
                
                let updatedString = self.replaceCharacters(inText: textField.text ?? String.empty,
                                                           range: range,
                                                           withCharacters: string)
                if let newString = formatter?.inputString(self.deleteNonDigitSymbols(string: updatedString)) {
                    textField.text = newString
                    let newPosition = range.location + string.count + newString.count - updatedString.count
                    textField.textField.caretPosition = newPosition
                }
            
        }
    }
    
    private func replaceCharacters(inText text: String, range: NSRange, withCharacters newText: String) -> String {
        if 0 < range.length {
            let result = NSMutableString(string: text)
            result.replaceCharacters(in: range, with: newText)
            return result as String
        } else {
            let result = NSMutableString(string: text)
            result.insert(newText, at: range.location)
            return result as String
        }
    }
    
    private func isDeletion(inRange range: NSRange, string: String) -> Bool {
        return 0 < range.length && 0 == string.count
    }
    
    private func deleteNonDigitSymbols(string: String) -> String {
        return string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
    }
    
    
}

