//
//  EmptyTableHeaderFooterView.swift
//  MDW
//
//  Created by Andrew Oparin on 09/08/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class EmptyTableHeaderFooterView: TableHeaderFooterView {
    
    private lazy var containerView = UIView()
    private var containerHeight: CGFloat = 0 {
        didSet {
            self.heightConstraint?.update(offset: self.containerHeight)
        }
    }
    
    private var heightConstraint: Constraint?
    
    
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.containerView)
        self.setNeedsUpdateConstraints()
    }
    
    override func configure(viewModel: TableHeaderFooterView.ViewModelType) {
        let viewModel = viewModel as! EmptyTableHeaderFooterViewModel
        self.containerHeight = viewModel.height
        self.contentView.backgroundColor = viewModel.color
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.containerView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            self.heightConstraint = $0.height.equalTo(self.containerHeight).priority(ConstraintPriority.high).constraint
            $0.bottom.equalToSuperview()
        }
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        let viewModel = viewModel as! EmptyTableHeaderFooterViewModel
        return viewModel.height
    }
}

struct EmptyTableHeaderFooterViewModel: TableHeaderFooterViewModel {
    
    var viewType: TableHeaderFooterView.Type { return EmptyTableHeaderFooterView.self }
    let height: CGFloat
    let color: UIColor
}

extension EmptyTableHeaderFooterViewModel {
    
    static func make(height: CGFloat, color: UIColor) -> EmptyTableHeaderFooterViewModel {
        return EmptyTableHeaderFooterViewModel(height: height, color: color)
    }
}
