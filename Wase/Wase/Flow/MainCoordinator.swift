//
//  MainCoordinator.swift
//  OMG
//
//  Created by Andrew Oparin on 07/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit


final class MainCoordinator: BaseCoordinator, UserMenuCoordinatorFlowPresentable, PhotographMenuCoordinatorFlowPresentable, AuthCoordinatorFlowPresentable{


    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: UIWindow
    private let sessionManager: TaokaUserSessionManager
    private let navigationController = AirdronNavigationController()
    
    init(presenter: UIWindow,
         sessionManager: TaokaUserSessionManager,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.coordinatorFactory = coordinatorFactory
        self.sessionManager = sessionManager
        super.init()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.logout),
                                               name: Notification.Name.TaokaNotificationLogout,
                                               object: nil)
    }

    
    override  func start() {
        self.presenter.rootViewController = self.navigationController
        
        self.presenter.rootViewController = self.navigationController
        if !self.sessionManager.isRegistred {
            self.startAuthFlow(presenter: self.navigationController) { [weak self] in
                guard let self = self else { return }
                if self.sessionManager.user?.type == UserType.client {
                    self.startUserMenuFlow(presenter: self.navigationController)
                } else if self.sessionManager.user?.type == UserType.photographer && self.sessionManager.user?.city.id != "other"{
                    self.startPhotographMenuFlow(presenter: self.navigationController)
                } else {
                    self.startUserMenuFlow(presenter: self.navigationController)
                }
            }
        } else {
            if self.sessionManager.user?.type == UserType.client {
                self.startUserMenuFlow(presenter: self.navigationController)
            } else if self.sessionManager.user?.type == UserType.photographer && self.sessionManager.user?.city.id != "other"{
                self.startPhotographMenuFlow(presenter: self.navigationController)
            } else {
                self.startUserMenuFlow(presenter: self.navigationController)
            }
        }
        

    }
    
    @objc
    func logout() {
        self.childCoordinators.removeAll()
        self.start()
    }
}
