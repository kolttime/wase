//
//  NotifExtensions.swift
//  Taoka
//
//  Created by Artem Myachkov on 13/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

public extension Notification.Name {
    
    public static let TaokaNotificationLogout = Notification.Name("TaokaNotificationLogoutIdentifier")
}
