//
//  UserShort.swift
//  OMG
//
//  Created by Andrew Oparin on 08/11/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

struct UserShort: Codable {
    
    
    let id: String
    let type: UserType
    let nickname: String
    let givenName: String
    let familyName: String
    let avatar : Image?
    let city : City
    let phone: String?
    let description: String?
}

extension UserShort {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(UserShort.self, from: data)
    }
}

extension Array where Element == UserShort {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([UserShort].self, from: data)
    }
}
