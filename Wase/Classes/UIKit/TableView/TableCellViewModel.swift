//
//  TableCellViewModel.swift
//  AirdronKit
//
//  Created by Andrew Oparin on 19/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

public protocol TableCellViewModel {
    
    var cellType: TableViewCell.Type { get }
}

public extension TableCellViewModel {
    
    var reuseIdentifier: String {
        return String(describing: self.cellType)
    }
}

public protocol TableSectionViewModel {
    var cellModels: [TableCellViewModel] { get set }
    var headerViewModel: TableHeaderFooterViewModel? { get }
    var footerViewModel: TableHeaderFooterViewModel? { get }
}

public struct DefaultTableSectionViewModel: TableSectionViewModel {
    
    public var cellModels: [TableCellViewModel]
    public var headerViewModel: TableHeaderFooterViewModel?
    public var footerViewModel: TableHeaderFooterViewModel?
    
    public init(cellModels: [TableCellViewModel],
                headerViewModel: TableHeaderFooterViewModel? = nil,
                footerViewModel: TableHeaderFooterViewModel? = nil) {
        self.cellModels = cellModels
        self.headerViewModel = headerViewModel
        self.footerViewModel = footerViewModel
    }
}

// MARK: For testing
public class DefaultTableViewCell: TableViewCell {
    
    public override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! DefaultTableCellViewModel
        self.textLabel?.text = viewModel.text
    }
}

public struct DefaultTableCellViewModel: TableCellViewModel {
    
    public var cellType: TableViewCell.Type { return DefaultTableViewCell.self }
    public var text: String { return UUID().uuidString }
}
