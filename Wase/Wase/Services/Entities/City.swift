//
//  City.swift
//  Taoka
//
//  Created by Роман Макеев on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct City: Codable {
    
    let id : String
    let name : String
  
    
    init(id : String, name : String) {
        self.id = id
        self.name = name
      
    }
    
}

extension City {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(City.self, from: data)
    }
}

extension Array where Element == City {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([City].self, from: data)
    }
}

