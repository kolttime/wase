//
//  Imagewidth.swift
//  Taoka
//
//  Created by Minic Relocusov on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


public func setContentWidth(imageSrc : ImageSrc) -> URL{
    if Device.isIphone5 {
        return imageSrc.w640!
    }
    if Device.isIphone6 {
        return imageSrc.w800!
    }
    if Device.isIphone6p {
        return imageSrc.w1100!
    }
    if Device.isIphoneX {
        return imageSrc.w1100!
    }
    if Device.isIphoneXSmax {
        return imageSrc.w1100!
    }
    return imageSrc.w2000!
}

