//
//  PhotoAlbumViewController.swift
//  Taoka
//
//  Created by Minic Relocusov on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class PhotoAlbumViewController : SimpleNavigationBar {
    

    lazy var tableViewController = TableViewController()


    var id : String?
    var scrollTo : Int?
    private var apiService : ApiService
    var images : [Image]?
    let loadingView = TaokaLoadingView()
    var userId : String?
    init(userId : String? = nil, images : [Image]?, id : String?, sctollTo : Int?, apiService : ApiService){
        self.id = id
        self.scrollTo = sctollTo
        self.images = images
        self.apiService = apiService
        self.userId = userId
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func initialSetup() {
        super.initialSetup()
        //self.tabBarController?.tabBar.isHidden = true
        self.setTabBarHidden()
        self.setLargeTitle()
        //self.setTitle(title: "Портфолио")
        self.add(self.tableViewController)
        //self.scrollingView = self.tableViewController.scrollView
        //self.scrollingView.scrollView = self.tableViewController.scrollView
        self.view.backgroundColor = Color.white.value
        //self.tableViewController.scrollView.delegate = self
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = true
        self.onLargetitle = {[weak self] in
          //  self?.setOffset()
        }
        self.onNormalTitle = {[weak self] in
         //   self?.setOffset()
        }
        self.tableViewController.tableView.register(cellClass: PhotoAlbumViewCell.self)
       // self.render()
        self.setUpConstraints()
        self.fetchData()
    }
   // override func scrollViewDidScroll(_ scrollView: UIScrollView) {

//    }

   

    func fetchData(){
        if self.id != nil {
            self.loadingView.isHidden = false
            self.apiService.getAlbum(userId : self.userId, id: self.id!) {
                [weak self] response in
                switch response {
                case .success(let album):
                    self?.images = album.images
                    self?.setTitle(title: album.title)
                    //self?.setMultilineNavigationBar(topText: "Игорь", bottomText: "сука")
                    self?.loadingView.isHidden = true
                    self?.render()
                case .failure(let error):
                    print(error)
                
                }
            }
        } else {
            self.setTitle(title: "Портфолио")
            self.render()
        }
    }

    func render(){
        var viewModels : [PhotoAlbumViewCellModel] = []
        for image in self.images! {
            let viewModel = PhotoAlbumViewCellModel.init(image: image)
            viewModels.append(viewModel)
        }
        let section = DefaultTableSectionViewModel(cellModels: viewModels)
        self.tableViewController.update(viewModels: [section])
        if self.scrollTo != nil {
            self.tableViewController.tableView.scrollToRow(at: IndexPath(row: self.scrollTo!, section: 0), at: .middle, animated: false)
        }
    }
    private var offsetView = UIView()
    override func setUpConstraints() {
        super.setUpConstraints()
       // self.tableViewController.tableView.snp.removeConstraints()
        self.tableViewController.view.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.tableViewController.tableView.snp.makeConstraints{
            $0.edges.equalTo(self.tableViewController.view)
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }

    }

}
