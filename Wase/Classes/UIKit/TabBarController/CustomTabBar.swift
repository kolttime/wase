//
//  CustomTabBar.swift
//  Taoka
//
//  Created by Minic Relocusov on 09/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit

open class CustomTabBar : UITabBarController {
    var onSelect : ((Int) -> Void)?
    
    open override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        self.onSelect?(item.tag)
    }
    
    func checkTabBar(isHidden : Bool){
        if isHidden {
            self.tabBar.isHidden = true
        } else {
            self.tabBar.isHidden = false
        }
    }
    func setTabBarColor(color : UIColor){
        self.tabBar.backgroundColor = color
    }
   
}
