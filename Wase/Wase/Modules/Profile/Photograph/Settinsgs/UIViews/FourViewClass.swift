//
//  ApproveView.swift
//  OMG
//
//  Created by Minic Relocusov on 21/04/2019.
//  Copyright © 2019 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit


class FourViewClass : AirdronView {
    
    var textLabel  = UILabel()
    var textDecsr = UILabel()
    var separator = UIView()
    private lazy var image = UIImageView(image: .init(UIImage(named: "Arrow2"))!)
    
    init (textLabel : String, textDescr : String) {
        self.textLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: textLabel)
        self.textDecsr.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: textDescr)
        super.init(frame: .zero)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func set(textLabel : String, descLabel : String) {
        self.textDecsr.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: descLabel)
        self.textLabel.text = textLabel
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = Color.white.value
        self.addSubview(self.textLabel)
        self.addSubview(self.textDecsr)
        self.addSubview(self.separator)
        self.addSubview(self.image)
        self.separator.backgroundColor = .lightGray
        
        self.textDecsr.numberOfLines = 0
    
        self.setupConstraints()
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.textLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
        self.textDecsr.snp.makeConstraints{
            $0.top.equalTo(textLabel.snp.bottom).offset(4)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-44)
        }
        
        self.separator.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview()
            $0.height.equalTo(0.5)
            $0.bottom.equalToSuperview()
        }
        
        self.image.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-22)
            $0.top.equalToSuperview().offset(15.5)
            $0.width.equalTo(8)
            $0.height.equalTo(13)
        }
        
    }
    
    override var intrinsicContentSize : CGSize{
        let width = UIView.noIntrinsicMetric
        return CGSize(width: width, height: 50)
    }
}


