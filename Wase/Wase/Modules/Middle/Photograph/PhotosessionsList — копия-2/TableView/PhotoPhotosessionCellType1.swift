//
//  UserPhotosessionCell.swift
//  Taoka
//
//  Created by Minic Relocusov on 18/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotoPhotosessionsCellType1 : TableViewCell {
    var selection : ((Photosession) -> Void)?
    let baseView = UIView()
    let containerView = UIView()
    let topView = PhotoPhotosessionsTopView()
    let middleView = PhotoPhotosessionsMiddleView()
    var photosession : Photosession?
    override func initialSetup() {
        super.initialSetup()
        
        self.backgroundColor = Color.white.value
        // self.contentView.addSubview(self.label)
        self.contentView.addSubview(self.baseView)
        // self.contentView.addSubview(self.mainImageView)
        
        baseView.backgroundColor = UIColor.clear
        baseView.layer.shadowColor = UIColor.black.cgColor
        baseView.layer.shadowOffset = CGSize(width: 0, height: 0)
        baseView.layer.shadowOpacity = 0.1
        baseView.layer.shadowRadius = 8.0
        self.containerView.frame = baseView.bounds
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.masksToBounds = true
        baseView.addSubview(self.containerView)
        //  baseView.layer.shadowPath = UIBezierPath(roundedRect: baseView.bounds, cornerRadius: 10).cgPath
        baseView.layer.shouldRasterize = true
        baseView.layer.rasterizationScale = UIScreen.main.scale
        self.containerView.backgroundColor = Color.light.value
        self.containerView.addSubview(self.topView)
        self.containerView.addSubview(self.middleView)
        self.containerView.backgroundColor = Color.light.value
        self.contentView.backgroundColor = Color.white.value
        //self.containerView.dropShadow()
        
        self.setUpConstarints()
        
        // set the shadow properties
        
        
        // self.setupConstraints()
    }
    var first1 = true
    var first2 = true
    override func configure(viewModel: TableViewCell.ViewModelType) {
        var viewModel = viewModel as! PhotoPhotosessionsCellModelType1
        self.selection = viewModel.selectionHandler
        self.photosession = viewModel.photo
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        dateFormatter.locale =  Locale(identifier: "ru_MD")
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let date = dateFormatter.date(from: viewModel.photo.date)!
        
        dateFormatter.dateFormat = "dd MMMM"
        let weekDay = calendar.component(.weekday, from: date)
        var weekString = ""
        switch weekDay {
        case 1:
            weekString = "воскресенье"
        case 2:
            weekString = "понедельник"
        case 3:
            weekString = "вторник"
        case 4:
            weekString = "среда"
        case 5:
            weekString = "четверг"
        case 6:
            weekString = "пятница"
        case 7:
            weekString = "суббота"
        
        default:
            weekString = ""
        }
        let dateString = dateFormatter.string(from: date)
        self.topView.set(avatarUrl: viewModel.photo.author!.avatar?.original.absoluteString ?? "", name: "\(viewModel.photo.author!.givenName) \(viewModel.photo.author!.familyName)", date: "")
        var placeType = ""
        switch viewModel.photo.place.type {
        case .home:
            placeType = "дома"
        case .outdoors:
            placeType = "на выезде"
        case .studio:
            placeType = "в студии"
       
            
        }
        var durationString = ""
        switch viewModel.photo.duration {
        case 1:
            durationString = "1 час"
        case 2:
            durationString = "2 часа"
        case 3:
            durationString = "3 часа"
        case 4:
            durationString = "4 часа"
        default:
            durationString = "\(viewModel.photo.duration) часов"
        }
        self.middleView.set(top: "\(viewModel.photo.specialization.name!), \(placeType)", middle: "\(dateString), \(weekString)", bottom: "с \(viewModel.photo.time), \(durationString)", cost: "\(viewModel.photo.price!) ₽")
        
    }
    func setUpConstarints(){
        self.baseView.snp.makeConstraints{
            $0.top.equalTo(self.contentView).offset(8)
            $0.left.equalTo(self.contentView).offset(ViewSize.sideOffset)
            $0.right.equalTo(self.contentView).offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.contentView).offset(-8)
            // $0.height.equalTo(169)
        }
        self.containerView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.topView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(40)
        }
        self.middleView.snp.makeConstraints{
            $0.top.equalTo(self.topView.snp.bottom).offset(1)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            //$0.height.equalTo(87)
            $0.bottom.equalToSuperview()
        }
        
        
    }
    
    override func didSelect() {
        self.selection?(self.photosession!)
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        return 144
        
    }
    
    
    
    
}
struct PhotoPhotosessionsCellModelType1 : TableCellViewModel {
    var cellType: TableViewCell.Type {return PhotoPhotosessionsCellType1.self}
    var photo : Photosession
    let selectionHandler: ((Photosession) -> Void)?
}


