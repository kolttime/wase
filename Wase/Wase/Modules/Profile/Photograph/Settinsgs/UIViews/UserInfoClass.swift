//
//  ApproveView.swift
//  OMG
//
//  Created by Minic Relocusov on 21/04/2019.
//  Copyright © 2019 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit


class UserInfoClass : AirdronView {
    
    var textLabel  = UILabel()
    var separator = UIView()
    var image = UIImageView(image: .init(UIImage(named: "Arrow2"))!)
    
    init(textLabel : String) {
        self.textLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: textLabel)
        super.init(frame: .zero)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = Color.white.value
        self.addSubview(self.textLabel)
        self.addSubview(self.separator)
        self.addSubview(self.image)
        self.separator.backgroundColor = .lightGray

        self.textLabel.textColor = .black
        self.textLabel.font = UIFont.systemFont(ofSize: 20)
        self.setupConstraints()
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.textLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
    
        self.separator.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview()
            $0.height.equalTo(0.5)
            $0.bottom.equalToSuperview()
        }
        
        self.image.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-22)
            $0.centerY.equalToSuperview()
            $0.width.equalTo(8)
            $0.height.equalTo(13)
        }
        
    }
    
    override var intrinsicContentSize : CGSize{
        let width = UIView.noIntrinsicMetric
        return CGSize(width: width, height: 50)
    }
}




