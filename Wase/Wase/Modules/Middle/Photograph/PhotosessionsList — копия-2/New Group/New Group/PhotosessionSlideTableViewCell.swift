//
//  UserPhotosessionCell.swift
//  Taoka
//
//  Created by Minic Relocusov on 18/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotosessionSliddeTableViewCell : TableViewCell {
    var selection : (() -> Void)?
    let title = UILabel()
    var arrowView = UIImageView()
    
    override func initialSetup() {
        super.initialSetup()
        
        self.backgroundColor = Color.white.value
        // self.contentView.addSubview(self.label)
        self.contentView.addSubview(self.title)
        self.contentView.backgroundColor = Color.white.value
        //self.containerView.dropShadow()
        self.arrowView.image = UIImage.init(named: "Arrow2")!
        self.arrowView.contentMode = .scaleAspectFill
        self.contentView.addSubview(self.arrowView)
        self.setUpConstarints()
        
        // set the shadow properties
        
        
        // self.setupConstraints()
    }
   
    override func configure(viewModel: TableViewCell.ViewModelType) {
        var viewModel = viewModel as! PhotosessionSliddeTableViewCellModel
        self.selection = viewModel.selectionHandler
        self.title.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: viewModel.name)
        
        
    }
    func setUpConstarints(){
        self.title.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        self.arrowView.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-23)
            $0.centerY.equalToSuperview()
            $0.height.equalTo(13)
            $0.width.equalTo(8)
        }
        
    }
    
    override func didSelect() {
        self.selection?()
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        return 44
        
    }
    
    
    
    
}
struct PhotosessionSliddeTableViewCellModel : TableCellViewModel {
    var cellType: TableViewCell.Type {return PhotosessionSliddeTableViewCell.self}
    var name : String
    let selectionHandler: (() -> Void)?
}

