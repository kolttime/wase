//
//  TaokaLoadingView.swift
//  Taoka
//
//  Created by Роман Макеев on 14/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class TaokaLoadingView : AirdronView {
    
    
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func initialSetup() {
        super.initialSetup()
        
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        //actInd.center = uiView.center
        actInd.hidesWhenStopped = true
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        self.addSubview(actInd)
        self.actInd.color = Color.accent.value
        actInd.startAnimating()
        self.backgroundColor = Color.white.value
        self.setupConstraints()
        
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.actInd.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
    }
    
    
}
