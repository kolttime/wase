//
//  AuthCoordinator.swift
//  OMG
//
//  Created by Andrew Oparin on 11/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

protocol PhotographReviewCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startPhotographReviewFlow(presenter: AirdronNavigationController, completion: Action?)
}

extension PhotographReviewCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startPhotographReviewFlow(presenter: AirdronNavigationController, completion: Action?) {
        let coordinator = self.coordinatorFactory.makePhotographReviewCoordinator(presenter: presenter, coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.onCompletion = {
            completion?()
        }
        coordinator.start()
    }
}

class PhotographReviewCoordinator: BaseCoordinator {
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: PhotographReviewModuleBuilder
    private let navigationPresenter = AirdronNavigationController()
    var onCompletion: Action?
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: PhotographReviewModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        self.showUserReviewModule()
    }
    
    func showUserReviewModule() {
        let viewController = self.moduleBuilder.makePhotographReviewModule()
        self.presenter.setViewControllers([viewController], animated: false)
    }
    
}


