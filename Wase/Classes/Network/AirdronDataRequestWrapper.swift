//
//  AirdronDataRequestWrapper.swift
//  AirdronCoreTests
//
//  Created by andrew oparin on 26/11/2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation
import Alamofire

public class AirdronDataRequestWrapper {
    
    private let dataRequest: Alamofire.DataRequest
    
    public init(dataRequest: Alamofire.DataRequest) {
        self.dataRequest = dataRequest
    }
    
    public func cancel() {
        self.dataRequest.cancel()
    }
    
    public func resume() {
        self.dataRequest.resume()
    }
}
