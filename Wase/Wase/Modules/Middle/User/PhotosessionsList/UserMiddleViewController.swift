//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Artem Myachkov on 07/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit

class UserMiddleViewController: SimpleNavigationBar{
    
    var selection : ((UserType, String) -> Void)?
    var onNew : Action?
    private lazy var tableViewController : TableViewController = TableViewController()
    private var photoModels : [Photosession] = []
    
    private var apiService : ApiService
    
    init(apiService : ApiService){
        self.apiService = apiService
        super.init()
    }
    private lazy var loadingView = TaokaLoadingView()
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.setTitle(title: "Фотосессии")
        self.setLargeTitle()
        self.setBackTitle(title: "Назад")
        
        self.toastPresenter.targetView = self.view
        self.add(self.tableViewController)
        self.tableViewController.tableView.register(cellClass: UserPhotosessionsCellType1.self)
        self.tableViewController.tableView.register(cellClass: UserPhotosessionsCellType2.self)
        self.tableViewController.tableView.register(cellClass: UserPhotosessionsCellType3.self)
        self.tableViewController.tableView.register(cellClass: UserPhotosessionsCellType4.self)
       // self.tableViewController.tableView.estimatedRowHeight = 400
       // self.tableViewController.tableView.rowHeight = UITableView.automaticDimension
        self.setRighttitle(title: "Создать новую", completion: self.onNew)
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = true
        self.setUpConstraints()
        //self.render()
        self.fetchData()
    }
    
    func fetchData(){
        self.loadingView.isHidden = false
        self.apiService.getUserPhotosessions(active: true) {
            [weak self] response in
            switch response {
            case .success(let photosessions):
                print(photosessions)
                self?.photoModels = photosessions
                self?.loadingView.isHidden = true
                self?.render()
            case .failure(let error):
                print(error)
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    
    func addNew(photosession : Photosession){
        var photos : [Photosession] = [photosession]
        photos.append(contentsOf: self.photoModels)
        self.photoModels = photos
        self.render()
    }
    
    func render(){
        var viewModels2 : [TableCellViewModel] = []
        for model in self.photoModels {
            var viewModel : TableCellViewModel
            var stage = 0
            switch model.status {
            case .created:
                stage = 0
            case .prepayment:
                stage = 2
            case .selection:
                stage = 1
            default:
                print("ИИИИИ")
            
            }
            let dateFormatter = DateFormatter()
            let calendar = Calendar.current
            //let DATE = Date()
            let year = calendar.component(.year, from: Date())
            dateFormatter.locale =  Locale(identifier: "ru_MD")
            dateFormatter.dateFormat = "YYYY-MM-dd"
            let date = dateFormatter.date(from: model.date)
            let year2 = calendar.component(.year, from: date!)
            if year != year2 {
                dateFormatter.dateFormat = "dd MMMM YYYY"
            } else {
                dateFormatter.dateFormat = "dd MMMM"
            }
            let stringDate = dateFormatter.string(from: date!)
                
          
            
            let selection : ((UserType, String) -> Void)? = {[weak self] type, id in
                self?.selection?(type, id)
            }
            var durationString = ""
            switch model.duration {
                case 1:
                    durationString = "1 час"
            case 2:
                durationString = "2 часа"
            case 3:
                durationString = "3 часа"
            case 4:
                durationString = "4 часа"
            default:
                durationString = "\(model.duration) часов"
                
            }
            
                if model.offers!.count == 1 {
                    viewModel = UserPhotosessionsCellModelType1(name: model.title, date: "\(stringDate) c \(model.time), \(durationString)", urls: [""], type: "", count: model.offers![0].pending!, stage: stage, id: model.id, selection: selection)
                    viewModels2.append(viewModel)
                } else if model.offers!.count == 2 {
                    viewModel = UserPhotosessionsCellModelType2(name: model.title, date: "\(stringDate) c \(model.time), \(durationString)", urls: ["",""], count1: model.offers![0].pending!, count2: model.offers![1].pending!, stage: stage, id: model.id, selection: selection)
                    viewModels2.append(viewModel)
                }
                
                
            
        }
        let section = DefaultTableSectionViewModel(cellModels: viewModels2)
        self.tableViewController.update(viewModels: [section])
        print("PHOTOSESSIONS : \(viewModels2.count)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    override func setUpConstraints() {
        super.setUpConstraints()
        self.tableViewController.view.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
    
}




