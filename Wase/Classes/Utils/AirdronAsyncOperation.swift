//
//  AirdronAsyncOperation.swift
//  MDW
//
//  Created by Andrew Oparin on 07/09/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

open class AirdronAsyncOperation: Operation {
    
    public override init() {
        super.init()
        
        self.block = { [weak self] in
            self?.complete()
        }
    }
    
    open override func cancel() {
        super.cancel()
        _status = .finished
    }
    
    //MUST(!!) call this function in block in order to continue queue execution
    open func complete() {
        self.status = .finished
    }
    
    open var block: (() -> Void) = {}
    
    private enum Status {
        case ready, executing, finished
        var keyPath: String {
            switch self {
            case .finished:
                return "isFinished"
            case .executing:
                return "isExecuting"
            case .ready:
                return "isReady"
            }
        }
    }
    
    //https://developer.apple.com/reference/foundation/operation (Subclassing Notes)
    
    private var _status: Status = .ready
    
    private var status: Status {
        get { return _status }
        set {
            let oldValue = _status
            self.willChangeValue(forKey: _status.keyPath)
            self.willChangeValue(forKey: newValue.keyPath)
            _status = newValue
            self.didChangeValue(forKey: oldValue.keyPath)
            self.didChangeValue(forKey: _status.keyPath)
        }
    }
    
    open override func start() {
        guard !self.isCancelled else {
            self.complete()
            return
        }
        self.status = .ready
        self.main()
    }
    
    open override func main() {
        guard !self.isCancelled else {
            self.complete()
            return
        }
        self.execute()
    }
    
    func execute() {
        self.status = .executing
        self.block()
    }
    
    open override var isFinished: Bool {
        return self.status == .finished
    }
    
    open override var isExecuting: Bool {
        return self.status == .executing
    }
    
    open override var isReady: Bool {
        return self.status == .ready && super.isReady
    }
    
    open override var isAsynchronous: Bool {
        return true
    }
}
