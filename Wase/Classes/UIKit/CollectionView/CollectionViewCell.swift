//
//  CollectionViewCell.swift
//  MDW
//
//  Created by Andrew Oparin on 27/08/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

open class CollectionViewCell: UICollectionViewCell {
    
    public typealias ViewModelType = CollectionCellViewModel
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupInitialState()
        self.setupConstraints()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupInitialState()
        self.setupConstraints()
    }
    
    private func setupInitialState() {
        self.initialSetup()
    }
    
    open func initialSetup() {
        
    }
    
    open func setupConstraints() {
        
    }
    
    open func configure(viewModel: ViewModelType) {
        
    }
    
    open func didSelect() {
        
    }
}
