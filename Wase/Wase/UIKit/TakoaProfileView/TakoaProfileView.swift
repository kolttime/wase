//
//  TakoaProfileView.swift
//  Taoka
//
//  Created by Minic Relocusov on 11/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class TakoaProfileView : AirdronView {
    
    private let avatarSize: CGSize = {
        if Device.isIphone5 {
            return CGSize(width: 61, height: 61)
        }
        return CGSize(width: 80, height: 80)
    }()
    lazy var givenNameLabel = UILabel()
    //lazy var familyNameLabel = UILabel()
    lazy var cityLabel = UILabel()
    lazy var descriptionLabel = UILabel()
    private lazy var avatarImageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = Color.light.value
        view.layer.cornerRadius = self.avatarSize.width / 2
        view.clipsToBounds = true
        view.isUserInteractionEnabled = true
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    var onAvarat : Action?
    
    init(givenName : String, familyName : String, city : String, descryption : String, imgUrl : String){
        super.init(frame: .zero)
        self.avatarImageView.kf.setImage(with: URL(string: imgUrl))
        self.givenNameLabel.attributedText = CustomFont.bodyBold20.attributesWithParagraph.colored(color: Color.black.value).make(string: "\(givenName)\n\(familyName)")
        self.cityLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.accent.value).make(string: city)
        self.descriptionLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: descryption)
    }
    func set(givenName : String, familyName : String, city : String, descryption : String, imgUrl : String){
        self.avatarImageView.kf.setImage(with: URL(string: imgUrl))
        self.givenNameLabel.attributedText = CustomFont.bodyBold20.attributesWithParagraph.colored(color: Color.black.value).make(string: "\(givenName)\n\(familyName)")
        self.cityLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.accent.value).make(string: city)
        self.descriptionLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: descryption)
    }
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    public func setUp(givenName : String, familyName : String, city : String, descryption : String, imgUrl : URL){
        self.givenNameLabel.snp.removeConstraints()
      //  self.familyNameLabel.snp.removeConstraints()
        self.cityLabel.snp.removeConstraints()
        self.descriptionLabel.snp.removeConstraints()
        self.avatarImageView.snp.removeConstraints()
        self.avatarImageView.kf.setImage(with: imgUrl)
        self.givenNameLabel.text = givenName
        self.givenNameLabel.numberOfLines = 2
       // self.familyNameLabel.text = familyName
        self.cityLabel.text = city
        self.descriptionLabel.text = descryption
        self.setupConstraints()
    }
    
    @objc func onAvatrTap() {
        self.onAvarat?()
    }
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.givenNameLabel)
       // self.addSubview(self.familyNameLabel)
        self.addSubview(self.cityLabel)
        self.addSubview(self.descriptionLabel)
        self.addSubview(self.avatarImageView)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onAvatrTap))
        self.avatarImageView.addGestureRecognizer(tap)
        self.givenNameLabel.numberOfLines = 2
//        self.givenNameLabel.font = UIFont.boldSystemFont(ofSize: 20)
//        self.familyNameLabel.font = UIFont.boldSystemFont(ofSize: 20)
//        self.cityLabel.font = UIFont.systemFont(ofSize: 15)
//        self.descriptionLabel.font = UIFont.systemFont(ofSize: 15)
//        self.cityLabel.textColor = UIColor.blue
//        self.descriptionLabel.textColor = UIColor.lightGray
        self.descriptionLabel.numberOfLines = 0
        self.backgroundColor = Color.white.value
       
        self.layoutIfNeeded()
        self.setupConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.avatarImageView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalTo(self.avatarSize)
            $0.height.equalTo(self.avatarSize)
        }
        self.givenNameLabel.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalTo(self.avatarImageView.snp.right).offset(12)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
       
        self.cityLabel.snp.makeConstraints{
            $0.left.equalTo(self.avatarImageView.snp.right).offset(12)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.greaterThanOrEqualTo(self.givenNameLabel.snp.bottom)
        }
        self.descriptionLabel.snp.makeConstraints{
            $0.top.greaterThanOrEqualTo(self.cityLabel.snp.bottom)
            $0.left.equalTo(self.avatarImageView.snp.right).offset(12)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalToSuperview().offset(-10)
        }
    }

}
