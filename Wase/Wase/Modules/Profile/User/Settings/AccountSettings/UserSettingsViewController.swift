//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit
import Gallery

class UserSettingsViewController : SimpleNavigationBar {
    
    
    var backClicked : ((User) -> Void)?
    var goAccount : (() -> Void)?
    var goErrors : (() -> Void)?
    var goUserAgreement: (() -> Void)?
    var goNotif: (() -> Void)?
    var goTermsOfUse: (() -> Void)?
    var testModel : User
    var accountView : FourViewClass
    var notifView : FourViewClass
    
    var supportView : SupportAndVersionClass
    var appView : SupportAndVersionClass
    
    var personalAgreement : UserInfoClass
    
    var conditions : UserInfoClass
    
    var scrollViewController = ScrollViewController()
    var loadingView = TaokaLoadingView()
    var userModel : User
    var apiService : ApiService
    init(userModel : User, apiService : ApiService){
        self.userModel = userModel
        self.apiService = apiService
        self.accountView = FourViewClass(textLabel: "Аккаунт", textDescr: "@"+userModel.nickname)
        self.notifView = FourViewClass(textLabel: "Уведомления", textDescr: "Чтобы ничего не пропустить")
        self.supportView = SupportAndVersionClass(textLabel: "Поддержка", textDescr: "Сообщить о проблеме", versionLabel: "")
        self.appView = SupportAndVersionClass(textLabel: "Приложение", textDescr: "Версия", versionLabel: "0.1 alpha (3)")
        self.personalAgreement = UserInfoClass(textLabel: "Пользовательское соглашение")
        self.conditions = UserInfoClass(textLabel: "Условия использования")
        self.testModel = userModel
        super.init()
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func set(){
        self.accountView.set(textLabel: "Аккаунт", descLabel: "@\(self.userModel.nickname)")
        
    }
    var avatarSet = false
     func backButton(){
        
        self.backClicked?(self.userModel)
    }
    
    
    override func initialSetup() {
        super.initialSetup()
        self.view.backgroundColor = Color.white.value
        
        
        self.setLargeTitle()
        self.setTitle(title: "Настройки")
        
        self.setBackTitle(title: "Настройки")
        
        self.setTabBarHidden()
        
        supportView.versionLabel.isHidden = true
        appView.image.isHidden = true
        

        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.Account))
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.Errors))
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(self.Ussser))
        let tap5 = UITapGestureRecognizer(target: self, action: #selector(self.Notif))
        let tap6 = UITapGestureRecognizer(target: self, action: #selector(self.Conditions))
        accountView.addGestureRecognizer(tap2)
        supportView.addGestureRecognizer(tap3)
        personalAgreement.addGestureRecognizer(tap4)
        notifView.addGestureRecognizer(tap5)
        conditions.addGestureRecognizer(tap6)
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        self.view.addSubview(self.scrollViewController.scrollView)
        
        scrollViewController.scrollView.addSubview(accountView)
        scrollViewController.scrollView.addSubview(notifView)
        scrollViewController.scrollView.addSubview(supportView)
        scrollViewController.scrollView.addSubview(appView)
        scrollViewController.scrollView.addSubview(personalAgreement)
        scrollViewController.scrollView.addSubview(conditions)
        
        scrollViewController.scrollView.showsHorizontalScrollIndicator = false
        
        self.view.addSubview(self.loadingView)
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        self.loadingView.isHidden = true
        self.setUpConstraints()
    }
    
    func doInsert(){
        
    }
    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationItem.leftBarButtonItem = nil
//        self.navigationItem.setHidesBackButton(false, animated: false)
        //self.backButton()
    }
    override func viewWillAppear(_ animated: Bool) {
        //self.setBackButton()
    }
    @objc func Ussser(){
        print("acck")
        self.goUserAgreement?()
    }
    
    @objc func Notif(){
        print("notif")
        self.goNotif?()
    }
    
    @objc func Errors(){
        print("errors~~~~~")
        self.goErrors?()
    }

    @objc func Account(){
        print("~~~~~")
        self.goAccount?()
    }
    @objc func Conditions(){
        print("conditions")
        self.goTermsOfUse?()
    }
    
    override func setUpConstraints(){
        super.setUpConstraints()
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        
        accountView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.height.equalTo(84)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        notifView.snp.makeConstraints{
            $0.top.equalTo(accountView.snp.bottom)
            $0.height.equalTo(84)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        supportView.snp.makeConstraints{
            $0.top.equalTo(notifView.snp.bottom)
            $0.height.equalTo(100)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        appView.snp.makeConstraints{
            $0.top.equalTo(supportView.snp.bottom)
            $0.height.equalTo(100)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        personalAgreement.snp.makeConstraints{
            $0.top.equalTo(appView.snp.bottom)
            $0.height.equalTo(44)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        conditions.snp.makeConstraints{
            $0.top.equalTo(personalAgreement.snp.bottom)
            $0.height.equalTo(44)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-20)
        }
        
    }
}


extension UserSettingsViewController: GalleryControllerDelegate {
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Gallery.Image]) {
        controller.dismiss(animated: true, completion: nil)
        if let image = images.first {
            self.upload(image: image)
        }
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) { }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Gallery.Image]) { }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func upload(image: Gallery.Image) {
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let avatar):
               // self?.updateAvatar(image: avatar)
                self?.userModel.avatar = avatar
                
            case .failure(let error):
              //  self?.showToastErrorAlert(error)
                print(error)
            }
        }
    }
    
  
}

