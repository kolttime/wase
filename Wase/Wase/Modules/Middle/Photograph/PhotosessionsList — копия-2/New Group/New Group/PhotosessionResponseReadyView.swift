//
//  PhotosessionResponseReadyView.swift
//  Taoka
//
//  Created by Роман Макеев on 05/08/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotosessionResponseReadyView : AirdronView {
    
    
    
    private var costView = CommentView(top: false)
    private var commentView = CommentView(top : false)
    private var avatarView = UIImageView()
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.costView)
        self.addSubview(self.commentView)
        self.addSubview(self.avatarView)
        self.avatarView.backgroundColor = Color.greyL.value
        self.avatarView.layer.cornerRadius = 18
        self.avatarView.layer.masksToBounds = true
        
        
    }
    
    
    func set(cost : String, comment : String, time : String, avatarUrl : String){
        self.commentView.set(text: comment, time: time)
        self.costView.set(text: cost, time: "")
        self.avatarView.kf.setImage(with: URL(string: avatarUrl))
        self.setupConstraintss()
    }
     func setupConstraintss() {
       // super.setupConstraints()
        let costHeight = self.costView.textLabel.text!.height(width: self.costView.textLabel.textWidth() + 24, font: CustomFont.bodyRegular15.font) + 16
        self.costView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalTo(self.costView.textLabel.textWidth() + 24)
            $0.height.equalTo(costHeight)
        }
        let commentheigh = self.commentView.textLabel.text!.height(width: UIScreen.main.bounds.size.width - ViewSize.sideOffset - 50 - 12 - 12, font: CustomFont.bodyRegular15.font) + 16
        self.commentView.snp.makeConstraints{
            $0.top.equalTo(self.costView.snp.bottom).offset(4)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(50)
            $0.height.equalTo(commentheigh)
        }
        self.avatarView.snp.makeConstraints{
            $0.top.equalTo(self.commentView.snp.bottom).offset(4)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(36)
            $0.width.equalTo(36)
            $0.bottom.equalToSuperview()
        }
    }
}
