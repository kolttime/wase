import Foundation

public enum TabIndex: String {
    
    case inbox = "INBOX"
    case waiting = "WAITING"
    case active = "ACTIVE"
    case ended = "ENDED"
}

extension TabIndex: Codable {
    
    //    enum Key: CodingKey {
    //        case rawValue
    //    }
    //
    //    init(from decoder: Decoder) throws {
    //        let container = try decoder.container(keyedBy: Key.self)
    //        let rawValue = try container.decode(String.self, forKey: .rawValue)
    //        self = UserType(rawValue: rawValue)!
    //    }
    //
    //    func encode(to encoder: Encoder) throws {
    //        var container = encoder.container(keyedBy: Key.self)
    //        try container.encode(self.rawValue, forKey: .rawValue)
    //    }
    
}
