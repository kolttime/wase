//
//  AuthModuleBuilder.swift
//  Taoka
//
//  Created by Artem Myachkov on 07/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit

class PhotographMiddleModuleBuilder{
    private let apiService: ApiService
    private let sessionManager: TaokaUserSessionManager
    
    init(apiService: ApiService,
         sessionManager: TaokaUserSessionManager) {
        self.apiService = apiService
        self.sessionManager = sessionManager
    }
    
    func makePhotographMiddleModule() -> PhotographMiddleViewController {
        return PhotographMiddleViewController(apiService: self.apiService)
    }
    
    func makePhotosessionOfferModule(photoId : String) -> PhotosessionOfferView {
        return PhotosessionOfferView(apiService: self.apiService, photoId: photoId, me: self.sessionManager.user!)
    }
    
}



