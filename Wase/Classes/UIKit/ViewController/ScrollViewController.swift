//
//  ScrollViewController.swift
//  MDW
//
//  Created by Andrew Oparin on 03/09/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

open class ScrollViewController: AirdronViewController, ScrollInteraction, UIScrollViewDelegate {
    
    public var onScroll: ((UIScrollView) -> Void)?
    
    lazy public var scrollView: UIScrollView! = UIScrollView()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.scrollView)
        self.scrollView.delegate = self
        self.view.setNeedsUpdateConstraints()
        self.setupConstraints()
        
    }
    
    open func setupConstraints() {
        self.scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.onScroll?(scrollView)
    }
}
