//
//  AuthCoordinator.swift
//  OMG
//
//  Created by Andrew Oparin on 11/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

protocol PhotographMiddleCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startPhotographMiddleFlow(presenter: AirdronNavigationController)
}

extension PhotographMiddleCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startPhotographMiddleFlow(presenter: AirdronNavigationController) {
        let coordinator = self.coordinatorFactory.makePhotographMiddleCoordinator(presenter: presenter,
                                                                            coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.start()
    }
}

class PhotographMiddleCoordinator: BaseCoordinator {
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: PhotographMiddleModuleBuilder
    private let navigationPresenter = AirdronNavigationController()
    var onCompletion: Action?
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: PhotographMiddleModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        self.showPhotographMiddleModule()
    }
    
    func showPhotographMiddleModule() {
        let viewController = self.moduleBuilder.makePhotographMiddleModule()
        self.presenter.setViewControllers([viewController], animated: false)
        
        viewController.onNext = {[weak self] id in
            self?.showPhotosessionOfferModule(id : id)
        }
        
    }
    
    func showPhotosessionOfferModule(id : String){
        let viewController = self.moduleBuilder.makePhotosessionOfferModule(photoId: id)
        self.presenter.pushViewController(viewController, animated: true)
    }
    
}



