//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class PhotoFinanceSettingsViewController : SimpleNavigationBar {
    
    var descrLabel = UILabel()
    var addCard = SampleButton()
    var goToACardView : (() -> Void)?

    override init(){
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        self.addCard.mainLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.black.value).make(string: "Привязать счет")
        self.addCard.backgroundColor = Color.light.value
        self.addCard.layer.cornerRadius = 10
        self.addCard.layer.masksToBounds = true
        super.initialSetup()
        self.view.backgroundColor = Color.white.value
        
        self.descrLabel.attributedText =  CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: "Чтобы получать оплату за выполненный заказ, необходимо указать данные вашего счета в банке, привязанного к карте")
        
        self.descrLabel.numberOfLines = 0
        
        
        self.setTabBarHidden()
      
        
        self.setLargeTitle()
        
        self.setTitle(title: "Финансы")
        for navItem in(self.navigationController?.navigationBar.subviews)! {
            for itemSubView in navItem.subviews {
                if let largeLabel = itemSubView as? UILabel {
                    largeLabel.text = self.title
                    largeLabel.numberOfLines = 0
                    largeLabel.lineBreakMode = .byWordWrapping
                }
            }
        }
        
        self.view.addSubview(descrLabel)
        self.view.addSubview(addCard)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.goCard))
        addCard.addGestureRecognizer(tap1)
        
        self.setUpConstraints()
    }
    
    @objc func goCard(){
        print("caaard")
        self.goToACardView?()
    }
    
    override func setUpConstraints(){
        super.setUpConstraints()
        descrLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalToSuperview().offset(332)
        }
     
        self.addCard.snp.makeConstraints{
            $0.top.equalTo(self.descrLabel.snp.bottom).offset(12)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
        }
        
    }
}

