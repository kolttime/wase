//
//  ThreadSafeArray.swift
//  OMG
//
//  Created by Andrew Oparin on 18/11/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

class ThreadSafeArray<T> {

    private var array = [T]()
    
    private let queue = DispatchQueue(label: "Airdron.threadSafeArray", attributes: .concurrent)
    
    func append(_ newItem: T) {
        self.queue.async(flags: .barrier) {
            self.array.append(newItem)
        }
    }
    
    func getArray() -> [T] {
        var array = [T]()
        self.queue.sync {
            array = self.array
        }
        return array
    }
    
    func reset() {
        self.queue.async(flags: .barrier) {
            self.array.removeAll()
        }
    }
}
