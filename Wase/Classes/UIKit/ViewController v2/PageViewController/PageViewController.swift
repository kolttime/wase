//
//  PageViewController.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit



class AirdronPageViewController : SimpleNavigationBar {
    
    
    
    var emptyColor : UIColor = UIColor.gray
    var currentColor = UIColor.blue
    var onBack : ((Int) -> Void)?
    var currentIndex = 0
    var count : Int
    var viewControllers : [AirdronViewController] = []
    var menuBar : MenuBar?
    lazy var navigation = navigationPageViewController()
    
    
     init(count :Int) {
        self.count = count
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func initialSetup() {
        super.initialSetup()
        self.setTabBarHidden()
        self.menuBar = MenuBar.init(frame: .zero, count: self.count, emptyColor: emptyColor, currentColor: currentColor)
        //self.navigationController?.navigationItem.backBarButtonItem?.isEnabled = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.add(self.navigation)
        self.view.backgroundColor = Color.white.value
        self.setLargeTitleHidden()
       // self.navigationItem.setHidesBackButton(true, animated:false)
     //   let button = UIBarButtonItem.init(title: "< Назад", style: .done, target: self, action: #selector(self.backButton))
      //  self.navigationItem.leftBarButtonItem = button
        self.setNavigationBar()
        self.setUpConstraints()
    }
    
    func setViewContrtollers(viewControllers : [AirdronViewController], count : Int){
        self.viewControllers = []
        self.navigation.setViewControllers([viewControllers[0]], animated: true)
        self.navigation.navigationController?.setToolbarHidden(true, animated: false)
       
        
        self.viewControllers.append(viewControllers[0])
        if viewControllers.count > 1 {
            for i in 1...viewControllers.count - 1 {
                self.viewControllers.append(viewControllers[i])
            }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.backButton()
        self.menuBar?.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
    }
    
    var first = true
    override func viewDidLayoutSubviews() {
        
    }
    
    func setNavigationBar() {
        
     
        self.setMiddleButton(view: self.menuBar!, count : 5)
        
       
        
        
        let backView = UIButton()
        let arrowImage = UIImageView()
        arrowImage.contentMode = .scaleAspectFill
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.accent.value).make(string: "Назад")
        arrowImage.image = UIImage.init(named: "Arrow")!
        backView.addSubview(arrowImage)
        backView.addSubview(label)
        arrowImage.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        label.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalTo(arrowImage.snp.right).offset(5)
            $0.bottom.equalToSuperview()
            $0.width.equalTo(70)
        }
        backView.snp.makeConstraints{
            $0.height.equalTo(17)
        }
        let tapView = UIView()
        backView.addSubview(tapView)
        tapView.backgroundColor = UIColor.clear
        tapView.frame = CGRect.init(x: 0, y: 0, width: 100, height: 70)
        backView.layoutIfNeeded()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backButton(recognizer:)))
       // backView.addGestureRecognizer(tap)
        //tapView.addGestureRecognizer(tap)
        self.navigationController?.navigationBar.addGestureRecognizer(tap)
        
        let button = UIBarButtonItem.init(customView: backView)
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.setLeftBarButton(button, animated: false)
        //self.navigationItem.leftBarButtonItem?.customView?.addGestureRecognizer(tap)
        // self.navigationItem.leftBarButtonItem?.action = #selector(self.backButton)
        
        
        
       
        /*
        let button = UIButton(type: .system)
        let image = UIImage.init(named: "Arrow")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        
        button.setImage(image, for: .normal)

        button.setTitle(" Назад", for: .normal)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 100)
        button.sizeToFit()
        button.addTarget(self, action: #selector(self.backButton), for: .touchUpInside)
     //   button.titleLabel?.font = UIFont.init(name: "Arial", size: 40)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
*/
     //   self.navigationItem.setHidesBackButton(true, animated:false)
        /*
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
        
        if let imgBackArrow = UIImage(named: "Arrow") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(self.backButton))
        view.addGestureRecognizer(backTap)
        
        let leftBarButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
 */
      //  self.navigationItem.leftBarButtonItem?.image = UIImage.init(named: "Arrow")
       // self.navigationItem.leftBarButtonItem?.title = "Назад"
    }
    
    
    
    func backClicked(){
        if self.currentIndex == 0 {
            self.onBack?(self.currentIndex)
        } else {
            self.onBack?(self.currentIndex)
            self.previous()
        }

    }
    @objc func backButton(recognizer: UITapGestureRecognizer){
        let x = recognizer.location(in: self.navigationController?.navigationBar).x
        let y = recognizer.location(in: self.navigationController?.navigationBar).y
        
        if x < 120 && x > 0 && y > 0 && y < 50{
            self.backClicked()
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.deleteMiddleButtons()
    }
    func next(){
        self.currentIndex += 1
        if self.viewControllers.count == self.currentIndex {
            self.currentIndex -= 1
            return
        }
        self.menuBar?.next()
        self.navigation.pushViewController(self.viewControllers[self.currentIndex], animated: true)
    }
    func previous(){
        self.currentIndex -= 1
        if self.currentIndex < 0 {
            self.currentIndex = 0
            return
        }
        self.menuBar?.previous()
        self.navigation.popViewController(animated: true)
    }
    
    
    override func setUpConstraints(){
        super.setUpConstraints()
        self.navigation.view.snp.makeConstraints{
            $0.top.equalToSuperview().offset(self.getTopOffset())
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
       
    }
    
    
    
}
