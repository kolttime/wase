//
//  AirdronCoreDataSettings.swift
//  AirdronCore
//
//  Created by andrew oparin on 31.10.2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation

public protocol AirdronCoreDataSettings {
    
    var modelName: String { get }
}
