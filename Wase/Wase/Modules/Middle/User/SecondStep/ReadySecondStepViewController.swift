//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class ReadySecondStepViewController : SimpleNavigationBar {
    
    var onNext : ((String, Photosession) -> Void)?
    var loadingView = TaokaLoadingView()
    var whoSearch = UILabel()
    var nameLabel = UILabel()
    var descrLabel = UILabel()

    private lazy var makeNamingTextField = OMGTextField.NamingField()
    private lazy var makeCommentTextField = OMGTextField.CommentField()
    
    var readyView = SampleButton()
    var apiService : ApiService
    var photoCreation : CreatePhotosession
    init(photoCreation : CreatePhotosession ,apiService : ApiService){
        self.apiService = apiService
        self.photoCreation = photoCreation
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setBackTitle(title: "Фотографы")
        self.view.backgroundColor = Color.white.value
        view.addSubview(whoSearch)
        view.addSubview(nameLabel)
        view.addSubview(descrLabel)
        view.addSubview(readyView)
        view.addSubview(makeNamingTextField)
        view.addSubview(makeCommentTextField)
        self.toastPresenter.targetView = self.view
        self.whoSearch.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Как искать и кого")
        self.whoSearch.numberOfLines = 0
        
        self.nameLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Придумать название")
        self.nameLabel.numberOfLines = 0
        
        self.descrLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Написать комментарий")
        self.descrLabel.numberOfLines = 0
        
        self.setTabBarHidden()
        self.setTitle(title: "Готово")
        self.setLargeTitle()
        
        self.readyView.mainLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.white.value).make(string: "Начать подбор")
        
        self.readyView.backgroundColor = Color.accent.value
        self.readyView.layer.cornerRadius = 10
        self.readyView.layer.masksToBounds = true
        self.loadingView.isHidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.nextView))
        self.readyView.addGestureRecognizer(tap1)
        
        self.readyView.backgroundColor = Color.accent.value
        
        self.setBackTitle(title: "Фотографы")
        self.view.addSubview(self.loadingView)
        self.setUpConstraints()
        self.setupHandlers()
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func nextView(){
       // self.loadingView.isHidden = false
        if self.photoCreation.title != nil && self.photoCreation.title != ""{
           self.loadingView.isHidden = false
            print("photosessions : \(self.photoCreation)")
            self.apiService.createPhotosession(CreatePhoto: self.photoCreation) { [weak self] response in
                switch response {
                case .success(let result):
                    print("slkdkl")
                  //  self?.loadingView.isHidden = true
                    self?.onNext?(result.id, result)
                case .failure(let error):
                    print(error)
                    self?.showToastErrorAlert(error)
                }
            }
        }
        print("~~~~~")
    }
    
    override func setUpConstraints(){
        
        whoSearch.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalToSuperview().offset(getTopOffset())
        }
        
        nameLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalTo(whoSearch.snp.bottom).offset(16)
        }
        
        descrLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalTo(nameLabel.snp.bottom).offset(16)
        }
        readyView.snp.makeConstraints{
            $0.bottom.equalToSuperview().offset(-30)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
        }
        
        self.makeNamingTextField.snp.makeConstraints{
            $0.top.equalTo(self.descrLabel.snp.bottom).offset(ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        
        self.makeCommentTextField.snp.makeConstraints{
            $0.top.equalTo(self.makeNamingTextField.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60) 
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        
    }
    
    func setupHandlers(){
        self.makeNamingTextField.onTextDidChanged = {[weak self] text in
            self?.photoCreation.title = self?.makeNamingTextField.text
        }
        self.makeCommentTextField.onTextDidChanged = {[weak self] text in
            self?.photoCreation.description = self?.makeCommentTextField.text
        }
        self.makeNamingTextField.onBeginEditing = {[weak self] textField in
            //self?.photoCreation.title = self?.makeNamingTextField.text
            //print(self?.photoCreation.title)
        }
        self.makeCommentTextField.onBeginEditing = {[weak self] textField in
           // self?.photoCreation.description = self?.makeCommentTextField.text
        }
        self.makeNamingTextField.onReturnHandler = {[weak self] in
            self?.makeCommentTextField.becomeResponder()
        }
        self.makeCommentTextField.onReturnHandler = {[weak self] in
            self?.view.endEditing(true)
            print("redy")
        }
    }
    
}



