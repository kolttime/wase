//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit
import Gallery
import Kingfisher

class PhotographAccountSettingsViewController : SimpleNavigationBar {
    
    
    var testUser : User
    var account : (() -> Void)?
    var onLogout: Action?
    var image = UIImageView()
    var scrollViewController = ScrollViewController()
    var actInd = UIActivityIndicatorView()
    var isChanged = false
    let loadingView = TaokaLoadingView()
    var onBack : ((User) -> Void)?
    var onAvatar : Action?
    private lazy var givenNameTextField = OMGTextField.makeGivenName()
    private lazy var familyNameTextField = OMGTextField.makeFamilyName()
    private lazy var nicknameTextField = OMGTextField.makeNickName()
    private lazy var phoneTextFiled = OMGTextField.makeTelephoneTF()
    private lazy var cityTextField  = OMGTextField.makeCityTF()
    private lazy var descrTextFiled = OMGTextField.makeDescrTF()
    var descrLabel = UILabel()
    var userModel : User
    public var apiService : ApiService
    init(apiService : ApiService,userModel: User){
        self.apiService = apiService
        self.userModel = userModel
        self.testUser = userModel
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    func onChange(){
        if self.givenNameTextField.text != userModel.givenName{
            self.isChanged = true
            self.userModel.givenName = self.givenNameTextField.text ?? ""
        }
        if self.familyNameTextField.text != userModel.familyName{
            self.isChanged = true
            self.userModel.familyName = self.familyNameTextField.text ?? ""
        }
        if self.nicknameTextField.text != userModel.nickname{
            self.isChanged = true
            self.userModel.nickname = self.nicknameTextField.text ?? ""
            }
        if self.phoneTextFiled.text != userModel.phone{
            self.isChanged = true
            self.userModel.phone = self.phoneTextFiled.text
        }
        if self.cityTextField.text != userModel.city.name{
            self.isChanged = true
        }
        if self.descrTextFiled.text != userModel.description{
            self.isChanged = true
            self.userModel.description = self.descrTextFiled.text
        }
        
//        self.apiService.getCheckNickname(nickname: nicknameTextField.text!) {[weak self] response in
//            switch response {
//            case .success(let available):
//                print(available.available)
//            case .failure(let error):
//                print("HERE ERROR \(error)")
//            }
//        }
     
    }

    
    func makeSpace(){
        self.phoneTextFiled.text! += " "
    }
    
    override func initialSetup() {
        
        self.phoneTextFiled.onTextDidChanged = { [weak self] response in
            print(response.text.count)
        }
    
        super.initialSetup()
        self.view.backgroundColor = Color.white.value
        self.setTabBarHidden()
        self.toastPresenter.targetView = self.view
        self.givenNameTextField.text = userModel.givenName
        self.familyNameTextField.text = userModel.familyName
        self.nicknameTextField.text = userModel.nickname
        self.phoneTextFiled.text = userModel.phone
        self.cityTextField.text = userModel.city.name
        self.descrTextFiled.text = userModel.description
        
         self.descrLabel.attributedText = CustomFont.tech11.attributesWithParagraph.colored(color: Color.black.value).make(string: "Используйте буквы и знаки препинания, до 80 символов")
        
        self.descrLabel.textColor = .gray
        self.descrLabel.numberOfLines = 0
        
        self.setRighttitle(title: "Выйти", completion: self.onLogout)
        self.image.layer.cornerRadius = 48
        self.image.backgroundColor = Color.light.value
        image.layer.masksToBounds = true
        if self.userModel.avatar != nil {
            self.image.kf.setImage(with: URL(string: self.userModel.avatar!.original.absoluteString))
        } else {
            self.image.image = UIImage(named: "BringPhoto")
        }
        
        self.setLargeTitle()
        self.setTitle(title: "Аккаунт")
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        self.view.addSubview(self.scrollViewController.scrollView)
        scrollViewController.scrollView.addSubview(image)
        scrollViewController.scrollView.addSubview(givenNameTextField)
        scrollViewController.scrollView.addSubview(familyNameTextField)
        scrollViewController.scrollView.addSubview(nicknameTextField)
        scrollViewController.scrollView.addSubview(phoneTextFiled)
        scrollViewController.scrollView.addSubview(cityTextField)
        scrollViewController.scrollView.addSubview(descrTextFiled)
        scrollViewController.scrollView.addSubview(descrLabel)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.avatarTap))
        self.image.isUserInteractionEnabled = true
        self.image.addGestureRecognizer(tap)
        image.contentMode = .scaleAspectFill
        //self.view.addSubview(self.loadingView)
       
        self.setUpConstraints()
        self.setupHandlers()
    }
    
    
    @objc func avatarTap(){
        self.onAvatar?()
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        print(phoneTextFiled.text)
    }
    
    override func setUpConstraints(){
        super.setUpConstraints()
        
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        image.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.centerX.equalToSuperview()
            $0.width.equalTo(96)
            $0.height.equalTo(96)
        }
        
        givenNameTextField.snp.makeConstraints{
            $0.top.equalTo(self.image.snp.bottom).offset(ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        familyNameTextField.snp.makeConstraints{
            $0.top.equalTo(self.givenNameTextField.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        nicknameTextField.snp.makeConstraints{
            $0.top.equalTo(self.familyNameTextField.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        phoneTextFiled.snp.makeConstraints{
            $0.top.equalTo(self.nicknameTextField.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        cityTextField.snp.makeConstraints{
            $0.top.equalTo(self.phoneTextFiled.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        descrTextFiled.snp.makeConstraints{
            $0.top.equalTo(self.cityTextField.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        
        descrLabel.snp.makeConstraints{
            $0.top.equalTo(self.descrTextFiled.snp.bottom).offset(6)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalToSuperview().offset(-20)
        }

}
    
    override func viewWillDisappear(_ animated: Bool) {
        print(self.userModel.description)
        onChange()
        if HelpFunctions.userModelChanged(first: self.userModel, second: self.testUser) {
            self.apiService.patchProfile(userModel: userModel) {  [weak self] response in
                switch response {
                case .success(let user):
                    print("Заебись")
                    
                case .failure(let error):
                    print(error)
                    self?.showToastErrorAlert(error)
                }
            }
        } 
        
        onBack?(userModel)
        print(userModel.type.rawValue)
    }
}


extension PhotographAccountSettingsViewController{
    func setupHandlers(){
        

        self.givenNameTextField.onBeginEditing = {[weak self] textfield in
            print("givenName")
        }
        self.givenNameTextField.onReturnHandler = {[weak self] in
            self?.familyNameTextField.becomeResponder()
        }
        self.familyNameTextField.onReturnHandler = {[weak self] in
            self?.nicknameTextField.becomeResponder()
        }
        self.nicknameTextField.onReturnHandler = {[weak self] in
            self?.phoneTextFiled.becomeResponder()
        }
        self.phoneTextFiled.onReturnHandler = {[weak self] in
            self?.cityTextField.becomeResponder()
        }
        self.cityTextField.onReturnHandler = {[weak self] in
            self?.descrTextFiled.becomeResponder()
        }
        self.descrTextFiled.onTextDidChanged = {[weak self] text in
            self?.userModel.description = text.text
        }
        self.descrTextFiled.onReturnHandler = {[weak self] in
            self?.view.endEditing(true)
            print("done")
        }
        
    }
}


extension PhotographAccountSettingsViewController: GalleryControllerDelegate {
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Gallery.Image]) {
        controller.dismiss(animated: true, completion: nil)
        if let image = images.first {
            self.upload(image: image)
        }
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) { }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Gallery.Image]) { }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func upload(image: Gallery.Image) {
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let avatar):
               // self?.updateAvatar(image: avatar)
                self?.userModel.avatar = avatar
                //self?.actInd.isHidden = false
                
                //self?.actInd.isHidden = false
                self?.image.kf.indicatorType = .activity
                self?.image.kf.setImage(with: URL(string: avatar.original.absoluteString)!)
              
                
                
            case .failure(let error):
               // self?.showToastErrorAlert(error)
                print(error)
            }
        }
    }
    
    
}
