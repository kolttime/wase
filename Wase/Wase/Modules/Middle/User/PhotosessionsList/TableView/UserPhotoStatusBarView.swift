//
//  UserPhotoStatusBarView.swift
//  Taoka
//
//  Created by Minic Relocusov on 19/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class UserPhotoStatusBarView : AirdronView {
    
    
    
    let label = UILabel()
    var statusBar : StatusBarView
    var stage :Int
    
    
    init(stage : Int){
        self.stage = stage
        self.statusBar = StatusBarView(height: Float(UIScreen.main.bounds.width - (2 * ViewSize.sideOffset) - 24), stage: stage)
        super.init(frame: .zero)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = Color.white.value
        self.addSubview(self.label)
        self.addSubview(self.statusBar)
        var str : String = ""
        switch self.stage {
        case 0:
            str = "Подбор не начат"
        case 1:
            str = "Идет подбор"
        case 2:
            str = "Ожидается подтверждение фотосессии"
        case 3:
            str = "Ожидается фоосессия"
        case 4:
            str = "Ожилается отзыв о фотосессии"
        case 5:
            str = "Ожидаются результаты фотоссессии"
        case 6:
            str = "Отмена модератором"
        default:
            print("Упс...")
        }
        self.label.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.greyL.value).make(string: str)
        
        self.setupConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.label.snp.makeConstraints{
            $0.top.equalToSuperview().offset(5)
            $0.left.equalToSuperview().offset(12)
        }
        self.statusBar.snp.makeConstraints{
            $0.top.equalTo(self.label.snp.bottom).offset(8)
            $0.left.equalToSuperview().offset(12)
            $0.right.equalToSuperview().offset(-12)
            $0.height.equalTo(8)
        }
    }
    
    
    
}
