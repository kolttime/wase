//
//  AirdronError.swift
//  AirdronCore
//
//  Created by andrew oparin on 07.10.2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation

public enum AirdronErrorCode: Int {
    
    case standardError = 996
    case unknownError = 999
    case parseError = 998
    case dataIsEmptyError = 997
    case jsonSerializeError = 995
    case unknownResponseFormat = 994
    case requestCanceled = -999
}

public struct AirdronError: Error {
    
    public let code: Int
    public let reason: String
    
    public init(code: Int, reason: String) {
        self.code = code
        self.reason = reason
    }
}

extension AirdronError {
    
    public init(code: AirdronErrorCode, reason: String) {
        self.init(code: code.rawValue,
                  reason: reason)
    }
}

extension AirdronError {
    
    public init(error: Error) {
        self.init(code: .standardError, reason: error.localizedDescription)
    }
}

extension AirdronError {
    
    public init(error: NSError) {
        self.init(code: error.code, reason: error.localizedDescription)
    }
}

extension AirdronError: Equatable {
    
    public static func ==(lhs: AirdronError, rhs: AirdronError) -> Bool {
        return lhs.code == rhs.code
    }
}

extension AirdronError {
    
    public static func unknownError(code: Int = AirdronErrorCode.unknownError.rawValue) -> AirdronError {
        return AirdronError(code: code,
                            reason: "Unknown error")
    }
    
    public static func parseError() -> AirdronError {
        return AirdronError(code: AirdronErrorCode.parseError,
                            reason: "Parse error")
    }
    
    public static func dataIsEmptyError() -> AirdronError {
        return AirdronError(code: AirdronErrorCode.dataIsEmptyError,
                            reason: "Data is empty error")
    }
    
    public static func jsonSerializeError() -> AirdronError {
        return AirdronError(code: AirdronErrorCode.jsonSerializeError,
                            reason: "Json serialize error")
    }
    
    public static func unknownResponseFormatError() -> AirdronError {
        return AirdronError(code: AirdronErrorCode.jsonSerializeError,
                            reason: "Unknown response format error")
    }
}

extension AirdronError {
    
    public static func backendError(_ statusCode: Int, responseData: Data?) -> AirdronError {
        guard let data = responseData else { return AirdronError.unknownError(code: statusCode) }
        return AirdronError(code: statusCode, reason: backendErrorReason(data: data))
    }
    
    public static func backendErrorReason(data: Data) -> String {
        
        let serializer = JSONSerializer()
        
        do {
            let serializedData = try serializer.deserialize(data: data)
            guard let json = serializedData as? JsonDictionary else {
                throw AirdronError.jsonSerializeError()
            }
            print(json)
            let description = json.map { "\($1)" }.joined(separator: ". ")
            return description
        } catch {
            let description = String(data: data, encoding: String.Encoding.utf8) ?? "cant parse error"
            print(description)
            return description
        }
    }
}
