//
//  AirdronTabbarController.swift
//  MDW
//
//  Created by Andrew Oparin on 20/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

public typealias AirdronTabResponder = AirdronTabDataSource & UIViewController

public protocol AirdronTabDataSource: class {
    
    var airdronTabbarController: AirdronTabbarController? { get set }
    //var airdronTabItem: AirdronTabItem { get set }
    func needShowTabbar() -> Bool
}

open class AirdronTabbarController: AirdronViewController  {
    
    
    private var currentSelectedTabIndex: Int = 0
    
    
    
    public var viewControllers : [UIViewController] = []
    public var testtabbarController  = CustomTabBar()
    
    
    public func setViewControllers(viewControllers : [AirdronTabResponder]){
        
        self.viewControllers = viewControllers
        let first = UITabBarItem.init(title: "Обзор", image: UIImage(named: "Today")!, tag: 0)
        let second = UITabBarItem.init(title: "Фотосессии", image: UIImage(named: "Collection")!, tag: 1)
        let third = UITabBarItem.init(title: "Профиль", image: UIImage(named: "Profile")!, tag: 2)
        
        //UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.grayColor()], for:.Normal)
//
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Color.accent.value], for:.selected)
//
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Color.grey77.value], for: .disabled)
        
        
        self.testtabbarController.tabBar.tintColor = Color.accent.value
        
        viewControllers[0].tabBarItem = first
        viewControllers[1].tabBarItem = second
        viewControllers[2].tabBarItem = third
        self.testtabbarController.viewControllers = viewControllers
        
        
        
    }
    private var tabItems: [AirdronTabItem] = [] {
        didSet {
            self.stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
            self.tabItems.forEach { self.stackView.addArrangedSubview($0) }
        }
    }
    
    public var separatorColor: UIColor = UIColor.gray {
        didSet {
            
            UITabBar.appearance().shadowImage = UIImage.colorForNavBar(color: self.separatorColor)
            
        }
    }
    
    public var tabbarColor: UIColor = UIColor.red {
        didSet {
            
            //self.tabBarItem.barTintColor = self.tabbarColor
        }
    }
    
    
    public var onSelectedTab: ((Int) -> Void)?
    
    public func selectTab(atIndex index: Int) {
        self.onSelectedTab?(index)
        self.checkShowingTabbar()
    }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.testtabbarController.onSelect = {
            [weak self] index in
                self?.onSelectedTab?(index)
        }
        
        UITabBar.appearance().shadowImage = UIImage.colorForNavBar(color: self.separatorColor)
    
        self.add(self.testtabbarController)
        self.setupConstraints()
        self.view.setNeedsUpdateConstraints()
    }
    
    
    
    //open override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    //     self.onSelectedTab?(item.tag)
    //  }
    
    
    func setupConstraints() {
        
    }
    
    
    
    public func checkShowingTabbar() {
        
        var vc: AirdronTabResponder? = self.viewControllers[self.currentSelectedTabIndex] as! AirdronTabResponder
        if let topVc = vc as? AirdronNavigationController {
            vc = topVc.topViewController as? AirdronTabResponder
        }
        self.testtabbarController.checkTabBar(isHidden: !(vc?.needShowTabbar() ?? true))
        //self.tabBar.isHidden = !(vc?.needShowTabbar() ?? true)
        
    }
    
    
    
}

extension AirdronTabbarController {
    
    
}
extension UIImage {
    class func colorForNavBar(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        //    Or if you need a thinner border :
        //    let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 0.5)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}
