//
//  ClearButton.swift
//  OMG
//
//  Created by Andrew Oparin on 17/11/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

class ClearButton: AirdronControl {
    
    private lazy var circleView = CircleView()
    private lazy var clearImageView = UIImageView(image: #imageLiteral(resourceName: "clear").withRenderingMode(.alwaysTemplate))
    
    override func initialSetup() {
        super.initialSetup()
        self.circleView.isUserInteractionEnabled = false
        self.clearImageView.isUserInteractionEnabled = false
        self.addSubview(self.circleView)
        self.addSubview(self.clearImageView)
        self.invalidateIntrinsicContentSize()
    }
    
    var circleColor: UIColor = Color.light.value {
        didSet {
            self.circleView.mainColor = self.circleColor
        }
    }
    
    var cancelColor: UIColor = Color.black.value {
        didSet {
            self.clearImageView.tintColor = self.cancelColor
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if self.isHighlighted {
                self.circleView.alpha = 0.5
            } else {
                self.circleView.alpha = 1.0
            }
        }
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.circleView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.size.equalTo(CGSize(width: 18, height: 18))
        }
        self.clearImageView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 38, height: 38)
    }
}
