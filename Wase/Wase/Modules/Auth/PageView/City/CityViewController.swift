//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class CityViewController : AirdronViewController {
    
    lazy var tableViewController = TableViewController()
    
    var onNext : ((String) -> Void)?
    
    var mainLabel = UILabel()
    
    var errorViewController : ErrorViewController?
    
    private let loadingView = TaokaLoadingView()
    private var cityViewModels: [SearchableCountryTableCellViewModel] = []
    private var userCreation : UserCreation
    private var sessionmanager : TaokaUserSessionManager
    private var apiService : ApiService

    
    init(userCreation : UserCreation, apiService : ApiService, sessionManager : TaokaUserSessionManager){
        self.userCreation = userCreation
        self.apiService = apiService
        self.sessionmanager = sessionManager
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidDisappear(_ animated: Bool) {
        guard let errorViewController = self.errorViewController else {return}
        errorViewController.view.center.y = self.view.frame.maxY
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.errorViewController.view.isHidden = true
        self.view.backgroundColor = Color.white.value
        
        view.addSubview(mainLabel)
        view.addSubview(tableViewController.tableView)
        view.addSubview(self.loadingView)
        self.loadingView.isHidden = true
        
       // self.add(self.errorViewController)
        
        mainLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.make(string: "Из какого вы города?")
        mainLabel.numberOfLines = 0
        self.tableViewController.tableView.register(cellClass: SearchableCountryTableViewCell.self)
      
       // print(self.errorViewController.mainLabel.text)
        self.setUpConstraints()
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.nextView))
        self.fetchData()
       //HERE errorViewController.cancel.addGestureRecognizer(tap1)
        

    }
    
    
    func fetchData(){
        self.loadingView.isHidden = false
        self.apiService.getCities(){[weak self] response in
            switch response {
            case .success(let cities):
                print(cities)
                self?.render(cities: cities)
                self?.loadingView.isHidden = true
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    @objc func nextView(){
        self.onNext?("volgograd")
    }
    
    func setUpConstraints(){
        
        
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        mainLabel.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        
        tableViewController.tableView.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(32)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        
        
    }
    var onError : Action?
    private func render(cities: [City]) {
        var viewModels : [SearchableCountryTableCellViewModel] = []
        for i in 0...cities.count - 1 {
            let selection : ((String) -> Void) = {[weak self] city in
                
                self?.userCreation.city = cities[i].id
                self?.loadingView.isHidden = false
                if self?.userCreation.type == .other || self?.userCreation.city ==  "other" {
                    self?.loadingView.isHidden = true
                if self?.userCreation.type == .other && self?.userCreation.city != "other"{
                    self?.errorViewController = ErrorViewController.init(type: 0)
                } else if self?.userCreation.type != .other && self?.userCreation.city == "other" {
                    self?.errorViewController = ErrorViewController.init(type: 1)
                } else if self?.userCreation.type == .other && self?.userCreation.city == "other" {
                    self?.errorViewController = ErrorViewController.init(type: 2)
                }
                    guard let self = self else {return}
                    guard let errorViewController = self.errorViewController else {return}
                    self.view.addSubview(errorViewController.view)
                    self.view.bringSubviewToFront(errorViewController.view)
                    let viewFrame = self.view.frame
                    errorViewController.view.frame = CGRect(x: viewFrame.minX, y: viewFrame.maxY, width: viewFrame.width, height: viewFrame.height)
                    UIView.animate(withDuration: 1, animations: {
                        errorViewController.view.frame = viewFrame
                    })
                    errorViewController.cancelAction = {[weak self] email, proffesion, city in
                        self?.loadingView.isHidden = true
                        self?.apiService.activation(userActivation: self!.userCreation, email: email, cityName: city, professionName: proffesion) {[weak self] response in
                            switch response {
                            case .success(let user):
                                self?.sessionmanager.save(user: user)
                                 self?.onError?()
                                self?.loadingView.isHidden = false
                                print("Success")
                            case .failure(let error):
                                print(error)
                            }
                        }
                    }
                }
                // self?.errorViewController.view.isHidden = false
                else {
                    self?.apiService.activation(userActivation: self!.userCreation, email: "", cityName: "", professionName: "") {[weak self] result in
                        print(result)
                        switch result {
                        case .success(let response):
                            print(response)
                            self?.sessionmanager.save(user: response)
                             self?.onNext?(response.city.id)
                            self?.loadingView.isHidden = true
                            
                        case .failure(let error):
                            print(error)
                            self?.loadingView.isHidden = false
                        }
                    }
                }
                //self?.onNext?(city)
            }
            
            
            let viewModel = SearchableCountryTableCellViewModel(cityLabel: cities[i].name, selectionHandler: selection)
            viewModels.append(viewModel)
        }
        
        let section = DefaultTableSectionViewModel(cellModels: viewModels)
        self.tableViewController.update(viewModels: [section])
    }
    
}


