
//
//  ApproveView.swift
//  OMG
//
//  Created by Minic Relocusov on 21/04/2019.
//  Copyright © 2019 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit


class FinanceButtonView : AirdronView {
    
    var financeBut = AddWorksButton()
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = .lightGray
        
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = true;
        

        self.setupConstraints()
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        
    }
    
    override var intrinsicContentSize : CGSize{
        let width = UIView.noIntrinsicMetric
        return CGSize(width: width, height: 50)
    }
}




