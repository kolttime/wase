//
//  AuthModuleBuilder.swift
//  Taoka
//
//  Created by Artem Myachkov on 07/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit

class AuthModuleBuilder{
    
    private let apiService: ApiService
    private let sessionManager: TaokaUserSessionManager
    
    init(apiService: ApiService,
         sessionManager: TaokaUserSessionManager) {
        self.apiService = apiService
        self.sessionManager = sessionManager
    }
    
    func makeLaunchModule() -> LaunchViewController {
        return LaunchViewController(apiService: self.apiService)
    }

    func makePhoneNumberModule() -> PhoneNumberViewController {
        return PhoneNumberViewController(apiService: self.apiService)
    }
    func makePhoneCodeModule(phonenumber : String, key : String) -> PhoneCodeViewController {
        return PhoneCodeViewController(phoneNumber: phonenumber, apiService : apiService, key : key,  sessionManager: self.sessionManager)
    }
    func makePageViewModule(initialViewControllers : [AirdronViewController], count : Int) -> AirdronPageViewController {
        let PageViewControllers = AirdronPageViewController(count: count)
        PageViewControllers.setViewContrtollers(viewControllers: initialViewControllers, count: count)
        return PageViewControllers
    }
    func makeIntroductionModule() -> IntroductionViewController{
        return IntroductionViewController(apiService: self.apiService)
    }
    func makeProfessionModule() -> ProfessionViewController{
        return ProfessionViewController()
    }
    func makeToDoModule() -> ToDoViewController{
        return ToDoViewController()
    }
    
    func makeCityModule(userCreation : UserCreation, vc: ErrorViewController) -> CityViewController{
        return CityViewController(userCreation: userCreation, apiService: self.apiService, sessionManager: self.sessionManager)
    }

    func makeNotificationModule() -> NotificationViewController{
        return NotificationViewController()
    }
    
    func makeErrorModule(type : Int) -> ErrorViewController{
        return ErrorViewController(type : type)
    }
    
    func makeReadyModule(userCreation : UserCreation) -> ReadyViewController{
        return ReadyViewController(userCreation: userCreation, apiService: self.apiService)
    }
    
    func makeInstagramModule() -> InstagramLoginController {
        return InstagramLoginController(apiService: self.apiService, sessionManager: self.sessionManager)
    }
    
}
