//
//  User.swift
//  OMG
//
//  Created by Andrew Oparin on 06/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

struct User: Codable {
    
    var id: String?
    var type: UserType
    var nickname: String
    var givenName: String
    var familyName: String
    var avatar : Image?
    var city : City
    var phone: String?
    var description: String?
    var instagram: String?
    var status: StatusType?
    var services : Services?
    
    
    func toDictionary() -> JsonDictionary {
        var parameters : [String : Any] = [:]
        
        parameters["nickname"] = self.nickname
        parameters["given_name"] = self.givenName
        parameters["family_name"] = self.familyName
        if self.avatar != nil {
            parameters["avatar"] = self.avatar!.id
        }
        parameters["city"] = self.city.id
        parameters["description"] = self.description
        
        if self.type != .client {
            if self.services != nil {
                var serv : [String : Any] = [:]
                serv["specializations"] = self.services!.specializations.map { $0.id! }
                var t = "0"
                var wHours = self.services!.workHours
                if (wHours.count) < 11 {
                    
                    t.append(wHours)
                    wHours = t
                }
                serv["work_hours"] = t
                serv["work_days"] = self.services?.workDays
                serv["price"] = self.services!.price ?? 0
                serv["vacant"] = self.services!.vacant
                serv["mobile"] = self.services!.mobile!
                parameters["services"] = serv
            }
        }
        
        
        
        return parameters
    }
    
    init(id: String,
        type: UserType,
        nickname: String,
        givenName: String,
        familyName: String,
        avatar : Image?,
        city : City,
        phone: String?,
        description: String?,
        instagram: String,
        status: StatusType?,
        services : Services?
         ) {
        self.id = id
        self.type = type
        self.nickname = nickname
        self.givenName = givenName
        self.familyName = familyName
        self.phone = phone
        self.description = description
        self.instagram = instagram
        self.status = status
        self.services = services
        self.avatar = avatar
        self.city = city
    }

}



extension User {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(User.self, from: data)
    }
}

extension Array where Element == User {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([User].self, from: data)
    }
}
