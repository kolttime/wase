//
//  AuthCoordinator.swift
//  OMG
//
//  Created by Andrew Oparin on 11/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//
// testcommit
import Foundation
import Gallery


protocol PhotographProfileCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startPhotographProfileFlow(presenter: AirdronNavigationController)
}

extension PhotographProfileCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startPhotographProfileFlow(presenter: AirdronNavigationController) {
        let coordinator = self.coordinatorFactory.makePhotographProfileCoordinator(presenter: presenter,
                                                                                  coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.start()
    }
}
class GalleryMultiselectImageSelector: GalleryControllerDelegate {
    
    var onSelectImages: (([Gallery.Image]) -> Void)?
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Gallery.Image]) {
        controller.dismiss(animated: true, completion: nil)
        onSelectImages?(images)
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) { }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Gallery.Image]) { }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
class PhotographProfileCoordinator: BaseCoordinator {
    private let galleryImageSelector = GalleryMultiselectImageSelector()
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: PhotographProfileModuleBuilder
    private let navigationPresenter = AirdronNavigationController()
    var onCompletion: Action?
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: PhotographProfileModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
 
    override func start() {
        self.showPhotographProfileModule()
    }
    
    func showPhotographProfileModule() {
        let viewController = self.moduleBuilder.makePhotographProfileModule()
        viewController.onSettings = {[weak self] user in
            self?.showPhotoProfileSettingModule(userModel: user, vc : viewController)
        }
        viewController.onAlbum = {[weak self] id in
            self?.showPhotoAlbumModule(images: nil, id: id, scrollTo: nil)
        }
        viewController.onImages = {[weak self] images, index in
            self?.showPhotoAlbumModule(images: images, id: nil, scrollTo: index)
        }
        viewController.onSelectCell = {[weak self] in
            //self?.showPhotoAlbumModule()
        }
        viewController.onPhotosession = {[weak self] in
           // self?.showPhotosessionsModule()
        }
        viewController.onAdd = {[weak self] in
            self?.showAddAlbumModule(vc: viewController)
        }
        
        self.presenter.setViewControllers([viewController], animated: false)
    }
    
    
    func showAddAlbumModule(vc : PhotographProfileViewController){
        let viewController = self.moduleBuilder.makeAddAlbumModule()
        viewController.onAddImages = { [weak self, weak viewController] in
            self?.showImagePicker { images in
                viewController?.add(images: images)
            }
            viewController?.onBackWithImages = {[weak self] images in
                vc.addImages(images: images)
                self?.presenter.popViewController(animated: true)
            }
            viewController?.onBackWithAlbum = {[weak self] album in
                vc.addAlbum(album: album)
                self?.presenter.popViewController(animated: true)
            }
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showPhotoProfileSettingModule(userModel : User, vc : PhotographProfileViewController){
        let viewController = self.moduleBuilder.makePhotoProfileSettings(userModel: userModel)
        viewController.goAccount = {[weak self] in
            self?.showPhotoAccountSettingsModule(vc: viewController, mainVc: vc)
        }
        viewController.goNotif = {[weak self] in
            self?.showPhotoNotifSettingsModule()
        }
        viewController.goServices = {[weak self] in
            self?.showPhotoServiceSettingsModule(vc: viewController, mainVc: vc)
        }
        viewController.goFinance = {[weak self] in
            self?.showPhotoFinanceSettingModule()
        }
        viewController.goErrors = {[weak self] in
            self?.showPhotoErrorModule()
        }
        viewController.goUserAgreement = {[weak self] in
            self?.showPhotoUserAgreement()
        }
        viewController.goTermsOfUse = {[weak self] in
            self?.showPhotoTermsOfUseModule()
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    
    func showPhotoFinanceSettingModule(){
        let viewController = self.moduleBuilder.makePhotoFinanceSettings()
        viewController.goToACardView = {[weak self] in
            self?.showCardAddingModule()
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showPhotoErrorModule(){
        let viewController = self.moduleBuilder.makePhotoErrorsSettings()
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showPhotoTermsOfUseModule(){
        let viewController = self.moduleBuilder.makeTermsOfUseSettings()
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showPhotoNotifSettingsModule(){
        let viewController = self.moduleBuilder.makeNotifSettings()
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showPhotoUserAgreement(){
        let viewController = self.moduleBuilder.makeUserEgreementSettings()
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showPhotoAccountSettingsModule(vc : PhotographSettingsViewController, mainVc : PhotographProfileViewController){
        let viewController = self.moduleBuilder.makePhotoAccountSettings(userModel: vc.userModel)
        viewController.onAvatar = {[weak self] in
            guard let self = self else { return }
            self.showImagePicker(viewController: viewController)
        }
        viewController.onLogout = {[weak self] in
            viewController.apiService.logout()
        }
        viewController.onBack = {[weak self] model in
            vc.userModel = model
            vc.set()
            mainVc.set(userModel: model)
            print(model)

        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showPhotoServiceSettingsModule(vc : PhotographSettingsViewController, mainVc : PhotographProfileViewController){
        let viewController = self.moduleBuilder.makePhotoServiceSettings(userModel: vc.userModel)
        viewController.onBack = {[weak self] model, isChanged in
            vc.userModel = model
            vc.set()
            mainVc.set(userModel: model)
            print(model)
            print(isChanged)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showPhotoAlbumModule(images : [Image]?, id : String?, scrollTo : Int?){
        let viewController = self.moduleBuilder.makePhotoAlbum(images : images, id: id, scrollTo: scrollTo)
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showImagePicker(selectedImagesHandler: @escaping (([Gallery.Image]) -> Void)) {
        let galleryController = GalleryController()
        Config.tabsToShow = [.imageTab]
        Config.Camera.imageLimit = 10
        galleryController.delegate = self.galleryImageSelector
        self.galleryImageSelector.onSelectImages = { images in
            selectedImagesHandler(images)
        }
        self.presenter.present(galleryController, animated: true) { [unowned galleryController] in
           // Appsee.markView(asSensitive: galleryController.view)
        }
    }
    func showCardAddingModule(){
        let viewController = self.moduleBuilder.makeCardAddingModel()
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showImagePicker(viewController: PhotographAccountSettingsViewController) {
        let galleryController = GalleryController()
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.Camera.imageLimit = 1
        galleryController.delegate = viewController
        self.presenter.present(galleryController, animated: true) { [unowned galleryController] in
            
        }
    }
    
}



