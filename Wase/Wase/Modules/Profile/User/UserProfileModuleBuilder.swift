//
//  AuthModuleBuilder.swift
//  Taoka
//
//  Created by Artem Myachkov on 07/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit

class UserProfileModuleBuilder{
    
    public let apiService: ApiService
    private let sessionManager: TaokaUserSessionManager
    
    init(apiService: ApiService,
         sessionManager: TaokaUserSessionManager) {
        self.apiService = apiService
        self.sessionManager = sessionManager
    }
    func makeUserProfileModule() -> UserProfileViewController {
        return UserProfileViewController(apiService: self.apiService)
    }
    func makeUserProfileSettings(userModel : User) -> UserSettingsViewController {
        return UserSettingsViewController(userModel: userModel, apiService: self.apiService)
    }
    func makeUserAgreementSettings() -> photoAgreementViewController{
        return photoAgreementViewController(apiService: self.apiService)
    }

    func makeUserNotifSettings() -> NotificationSettingsViewController {
        return NotificationSettingsViewController()
    }
    
    func makeUserErrorsSettings() -> PhotoSendErrorViewController {
        return PhotoSendErrorViewController()
    }
    func makeUserAccountSettings(userModel : User) -> PhotographAccountSettingsViewController {
        return  PhotographAccountSettingsViewController(apiService: self.apiService, userModel: userModel)
    }

    func makeUserTermsOfUseSettings() -> PhotoTermsOfUseViewController{
        return PhotoTermsOfUseViewController(apiService: self.apiService)
    }
    func makePhotoAlbum(images : [Image]?, id : String?, scrollTo : Int?) -> PhotoAlbumViewController {
        return PhotoAlbumViewController( images : images,  id: id, sctollTo: scrollTo , apiService: self.apiService )

    }
    
}

