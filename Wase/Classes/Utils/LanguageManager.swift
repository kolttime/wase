//
//  LanguageManager.swift
//  MDW
//
//  Created by Andrew Oparin on 29/08/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

public enum LanguageType: String {
    
    case en = "en"
    case ru = "ru"
    
    var defaultCallingCode: String {
        switch self {
        case .en:
            return "1"
        case .ru:
            return "7"
        }
    }
}

public final class LanguageManager {
    
    public static let kDefaultLanguage = LanguageType.en
    
    public static func defaultLanguage() -> LanguageType {
        var defaultLanguage: LanguageType = kDefaultLanguage
        guard let preferredLanguage = Bundle.main.preferredLocalizations.first else {
            return kDefaultLanguage
        }
        let availableLanguages: [String] = self.availableLanguages()
        if (availableLanguages.contains(preferredLanguage)) {
            defaultLanguage = LanguageType(rawValue: preferredLanguage)!
        }
        else {
            defaultLanguage = kDefaultLanguage
        }
        return defaultLanguage
    }
    
    public static func availableLanguages() -> [String] {
        var availableLanguages = Bundle.main.localizations
        if let indexOfBase = availableLanguages.index(of: "Base") {
            availableLanguages.remove(at: indexOfBase)
        }
        return availableLanguages
    }
}
