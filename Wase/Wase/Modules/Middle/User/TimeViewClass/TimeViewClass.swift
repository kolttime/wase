//
//  ApproveView.swift
//  OMG
//
//  Created by Minic Relocusov on 21/04/2019.
//  Copyright © 2019 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit


class TimeViewClass : AirdronView {
    
    var textLabel  = UILabel()
    var textDecsr = UILabel()
    var separator = UIView()
    var descText : String?
    var onEdditing : Action?
    var onEndEdditing : Action!
    var didSet = false
    override func initialSetup() {
        super.initialSetup()
        
         self.textLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: "")
         self.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: "")
        self.backgroundColor = Color.white.value
        self.addSubview(self.textLabel)
        self.addSubview(self.textDecsr)
        self.addSubview(self.separator)
        self.separator.backgroundColor = Color.light.value
        self.onEndEdditing = {[weak self] in
            self?.separator.backgroundColor = Color.light.value
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onBeginEdditing))
        self.addGestureRecognizer(tap)
        self.setupConstraints()
    }
    @objc func onBeginEdditing(){
        self.separator.backgroundColor = Color.black.value
        self.onEdditing?()
    }
    
    func set(text : String){
        self.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: text)
        self.textDecsr.isHidden = false
        self.textLabel.isHidden = true
    }
    
    func setDefault(text : String){
        self.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.grey.value).make(string: text)
        self.textDecsr.isHidden = false
        self.textLabel.isHidden = true
    }
    
    func setText(text : String){
        self.textDecsr.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: text)
        self.textDecsr.isHidden = false
        self.textLabel.isHidden = false
        self.textLabel.attributedText = CustomFont.tech13.attributesWithParagraph.colored(color: Color.grey.value).make(string: text)
        self.didSet = true
        
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.textLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
        
        self.textDecsr.snp.makeConstraints{
            $0.top.equalTo(textLabel.snp.bottom).offset(4)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-44)
        }
        
        self.separator.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(1)
            $0.top.equalTo(textDecsr.snp.bottom).offset(7)
        }
    
        
    }
    
    override var intrinsicContentSize : CGSize{
        let width = UIView.noIntrinsicMetric
        return CGSize(width: width, height: 50)
    }
}


extension TimeViewClass {
    public static func makeDateAlbum() -> TimeViewClass{
        let field = TimeViewClass()
        field.textLabel.text = "Дата фотосессии"
        field.textDecsr.text = "Дата фотосессии"
        return field
    }
    public static func makeTypeAlbum() -> TimeViewClass{
        let field = TimeViewClass()
        field.textLabel.text = "Тип фотосессии"
        field.textDecsr.text = "Тип фотосессии"
        return field
    }
}


