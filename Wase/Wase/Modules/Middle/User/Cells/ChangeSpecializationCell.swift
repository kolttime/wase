//
//  SearchableCountryTableViewCell.swift
//  OMG
//
//  Created by Andrew Oparin on 13/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

class ChangeSpecializationCell: TableViewCell {
    var selection : ((Bool?, Specialization?) -> Void)?
    var cityModel: ChangeSpecializationCellModel?
    var specialization : Specialization?
    var added = false
    var switchView = UISwitch(frame:CGRect(x: 150, y: 150, width: 0, height: 0))
    var onSwitchOn : Action?
    var onSwitchOff : Action?
    
    private lazy var mainLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.mainLabel)
        self.contentView.addSubview(self.switchView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.mainLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(24)
        }
        self.switchView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! ChangeSpecializationCellModel
        self.cityModel = viewModel
        if viewModel.hasSpec {
            self.switchView.setOn(true, animated: false)
        }
        self.specialization = viewModel.specialization
        self.selection = viewModel.selectionHandler
        self.mainLabel.text = viewModel.specialization.name
        self.switchView.addTarget(self, action: #selector(toggleSwitch), for: UIControl.Event.valueChanged)
        self.switchView.onTintColor = .blue
        //if viewModel.specialization
        self.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: self.mainLabel.text ?? "")
        
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 60
    }
    
 
    
    @objc func toggleSwitch(sender: UISwitch) {
      self.selection?(self.switchView.isOn, self.specialization)
    }
    
}

struct ChangeSpecializationCellModel: TableCellViewModel {
    var cellType: TableViewCell.Type { return ChangeSpecializationCell.self}
    var specialization : Specialization
    var hasSpec : Bool
    let selectionHandler: ((Bool?, Specialization?) -> Void)?
}




