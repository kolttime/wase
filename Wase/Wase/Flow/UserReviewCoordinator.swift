
//
//  AuthCoordinator.swift
//  OMG
//
//  Created by Andrew Oparin on 11/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

protocol UserReviewCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startUserReviewFlow(presenter: AirdronNavigationController, profileHandler: Action?)
}

extension UserReviewCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startUserReviewFlow(presenter: AirdronNavigationController, profileHandler: Action?) {
        let coordinator = self.coordinatorFactory.makeUserReviewCoordinator(presenter: presenter,
                                                                          coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.profileHandler = {
            profileHandler?()
        }
        coordinator.start()
    }
}

class UserReviewCoordinator: BaseCoordinator {
    
    var profileHandler: Action?
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: UserReviewModuleBuilder
    private let navigationPresenter = AirdronNavigationController()
    var onCompletion: Action?
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: UserReviewModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        self.showUserReviewModule()
    }
 
    func showUserReviewModule() {
        let viewController = self.moduleBuilder.makeUserReviewModule()
        self.presenter.setViewControllers([viewController], animated: false)
    }
    
}


