//
//  AddAddPhotoCollectionViewCell.swift
//  OMG
//
//  Created by Andrew Oparin on 16/11/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit
import Photos

class AddPhotoCollectionViewCell: CollectionViewCell {
    
    var cellSelectionHandler: ((UIImage?) -> Void)?
    var clearHandler: ((PHAsset, UICollectionViewCell) -> Void)?
    
    private weak var observer: Observable<UIImage?>?
    private weak var errorObserver: Observable<NSError?>?
    private weak var uploadObserver: Observable<UploadType>?
    
    private lazy var originalImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = Color.light.value
        return imageView
    }()
    
    private lazy var errorImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "icn_chat_error"))
        imageView.contentMode = .center
        imageView.isHidden = true
        return imageView
    }()
    
    private lazy var circularProgressView: CircularProgressView = {
        let view = CircularProgressView(side: 22)
        view.beginRotation()
        view.setProgress(0.6, animated: false)
        return view
    }()
    
    private lazy var clearButton = ClearButton()
    private var asset: PHAsset?
    
    private lazy var handlerView = UIView()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.handlerView)
        self.contentView.addSubview(self.originalImageView)
        self.contentView.addSubview(self.circularProgressView)
        self.contentView.addSubview(self.errorImageView)
        self.contentView.addSubview(self.clearButton)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectionHandler))
        self.handlerView.addGestureRecognizer(tap)
        self.clearButton.addTarget(self,
                                   action: #selector(self.clearTouch),
                                   for: .touchUpInside)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.originalImageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.handlerView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.errorImageView.snp.makeConstraints { $0.edges.equalToSuperview() }
        self.clearButton.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.right.equalToSuperview()
        }
        self.circularProgressView.snp.makeConstraints {
            $0.center.equalTo(self.clearButton)
            $0.size.equalTo(CGSize.init(width: 22, height: 22))
        }
    }
    
    override func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! AddPhotoCollectionCellViewModel
        self.originalImageView.image = viewModel.image.observer.value
        self.observer = viewModel.image.observer
        self.uploadObserver = viewModel.image.uploadObserver
        self.handleUploadState(type: viewModel.image.uploadObserver.value)
        
        self.cellSelectionHandler = viewModel.cellSelectionHandler
        
        viewModel.image.observer.add(observer: self) { [weak self] _, newImage in
            self?.originalImageView.image = newImage
        }
        
        viewModel.image.uploadObserver.add(observer: self) { [weak self] _, type in
            self?.handleUploadState(type: type)
        }
        self.clearHandler = viewModel.clearHandler
        self.asset = viewModel.image.asset
    }
    
    private func handleUploadState(type: UploadType) {
        switch type {
        case .initial:
            self.circularProgressView.isHidden = false
            self.errorImageView.isHidden = true
            self.clearButton.circleColor = UIColor.clear
            self.clearButton.cancelColor = Color.grey.value
            self.circularProgressView.color = Color.grey.value
        case .beforeExportImage:
            self.circularProgressView.isHidden = false
            self.errorImageView.isHidden = true
            self.clearButton.circleColor = UIColor.clear
            self.clearButton.cancelColor = Color.grey.value
            self.circularProgressView.color = Color.grey.value
        case .afterExportImage:
            self.circularProgressView.isHidden = false
            self.errorImageView.isHidden = true
            self.clearButton.circleColor = UIColor.clear
            self.clearButton.cancelColor = Color.white.value
            self.circularProgressView.color = Color.white.value
        case .error:
            self.errorImageView.isHidden = false
            self.circularProgressView.isHidden = true
        case .complete:
            self.errorImageView.isHidden = true
            self.clearButton.cancelColor = Color.black.value
            self.clearButton.circleColor = Color.light.value
            self.circularProgressView.isHidden = true
        }
    }
    
    @objc
    func selectionHandler() {
        self.cellSelectionHandler?(self.originalImageView.image)
    }
    
    @objc
    func clearTouch() {
        guard let asset = self.asset else { return }
        self.clearHandler?(asset, self)
    }
    
    deinit {
        self.observer?.remove(observer: self)
        self.errorObserver?.remove(observer: self)
        self.uploadObserver?.remove(observer: self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.errorImageView.isHidden = true
        self.observer?.remove(observer: self)
        self.errorObserver?.remove(observer: self)
        self.uploadObserver?.remove(observer: self)
    }
}

extension AddPhotoCollectionViewCell: WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel, width: CGFloat) -> CGFloat {
        return width
    }
}

struct AddPhotoCollectionCellViewModel: CollectionCellViewModel {
    
    var cellType: CollectionViewCell.Type { return AddPhotoCollectionViewCell.self }
    let image: UploadedImage
    let cellSelectionHandler: ((UIImage?) -> Void)?
    let clearHandler: ((PHAsset, UICollectionViewCell) -> Void)?
}
