//
//  ApproveView.swift
//  OMG
//
//  Created by Minic Relocusov on 21/04/2019.
//  Copyright © 2019 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit



class AvatarView : AirdronView {
    
    var textLabel  = UILabel()
    //var image = UIImageView(image: .init(UIImage(named: "Arrow2"))!)
    var image = UIImageView()
    
    init(){
        super.init(frame: .zero)
    }
    
    init (textLabel : String) {
        self.textLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: textLabel)
        super.init(frame: .zero)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.image.layer.cornerRadius = 18
        self.image.layer.masksToBounds = true
        self.image.backgroundColor = Color.greyL.value
        self.image.contentMode = .scaleAspectFill
        self.backgroundColor = Color.white.value
        self.addSubview(self.textLabel)
        self.addSubview(self.image)
        self.setupConstraints()
    }
    
    func set(imageUrl : String, nameLabel : String){
        self.image.kf.setImage(with: URL(string: imageUrl))
        self.textLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.make(string: nameLabel)
    }
    override func setupConstraints() {
        super.setupConstraints()
        
        self.image.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalToSuperview()
            $0.width.equalTo(36)
            $0.height.equalTo(36)
        }
        self.textLabel.snp.makeConstraints{
            $0.left.equalTo(image.snp.right).offset(6)
            $0.centerY.equalToSuperview()
        }
    }

}





