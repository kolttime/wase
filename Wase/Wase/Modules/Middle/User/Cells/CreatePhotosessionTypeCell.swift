//
//  SearchableCountryTableViewCell.swift
//  OMG
//
//  Created by Andrew Oparin on 13/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

class CreatePhotosessionTypeCell: TableViewCell {
    var selection : ((Specialization?) -> Void)?
    var cityModel: CreatePhotosessionTableCellViewModel?
    var specialization : Specialization?
    var imageq = UIImageView(image: .init(UIImage(named: "Arrow2"))!)
    
    private lazy var mainLabel = UILabel()
    private lazy var descrLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.mainLabel)
        self.contentView.addSubview(self.descrLabel)
        self.contentView.addSubview(self.imageq)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.mainLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(24)
        }
        
        self.descrLabel.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(2)
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        
        self.imageq.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-22)
            $0.top.equalToSuperview().offset(15)
            $0.width.equalTo(8)
            $0.height.equalTo(13)
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! CreatePhotosessionTableCellViewModel
        self.cityModel = viewModel
        self.selection = viewModel.selectionHandler
        self.specialization = viewModel.specialization
        self.mainLabel.text = viewModel.specialization.name
        self.descrLabel.text = viewModel.specialization.description
        self.mainLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.colored(color: Color.black.value).make(string: self.mainLabel.text ?? "")
        self.descrLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: self.descrLabel.text ?? "")
        self.descrLabel.numberOfLines = 0
        
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 80
    }
    
    override func didSelect() {
        self.selection?(self.specialization)
       // self.backgroundColor = Color.grey10.value
    }
}

struct CreatePhotosessionTableCellViewModel: TableCellViewModel {
    var cellType: TableViewCell.Type { return CreatePhotosessionTypeCell.self }
    var specialization : Specialization
    let selectionHandler: ((Specialization?) -> Void)?
}


