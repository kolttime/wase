//
//  UITextView.swift
//  MDW
//
//  Created by Andrew Oparin on 16/08/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

public extension UITextView {
    
    public static func makeStaticTextView() -> UITextView {
        let textView = UITextView()
        textView.textAlignment = .left
        textView.bounces = false
        textView.isScrollEnabled = false
        textView.isSelectable = true
        textView.isEditable = false
        textView.spellCheckingType = .no
        textView.textContainerInset = UIEdgeInsets.zero
        textView.textContainer.lineFragmentPadding = 0
        return textView
    }
}
