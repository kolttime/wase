//
//  Predicates.swift
//  AirdronCore
//
//  Created by andrew oparin on 10/02/2018.
//  Copyright © 2018 airdron. All rights reserved.
//

import Foundation
import CoreData

public enum Predicates {
    
    case id(value: Identifier)
    
    public var predicate: NSPredicate {
        switch self {
        case .id(let value):
            return NSPredicate(format: "id == %d", value)
        }
    }
}
