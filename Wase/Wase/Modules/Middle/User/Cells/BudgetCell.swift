//
//  SearchableCountryTableViewCell.swift
//  OMG
//
//  Created by Andrew Oparin on 13/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

class BudgetCell: TableViewCell {
    var selection : ((Budget) -> Void)?
    var cityModel: BudgetTableCellViewModel?
    var budget : Budget?
    var imageq = UIImageView(image: .init(UIImage(named: "Arrow2"))!)
    
    private lazy var mainLabel = UILabel()
    private lazy var descrLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.mainLabel)
        self.contentView.addSubview(self.imageq)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.mainLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(24)
        }

        self.imageq.snp.makeConstraints{
            $0.right.equalToSuperview().offset(-22)
            $0.top.equalToSuperview().offset(15)
            $0.width.equalTo(8)
            $0.height.equalTo(13)
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! BudgetTableCellViewModel
        self.cityModel = viewModel
        self.budget = viewModel.budget
        self.selection = viewModel.selectionHandler
        self.mainLabel.text = viewModel.budget.name
        self.mainLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.colored(color: Color.black.value).make(string: self.mainLabel.text ?? "")
        
    }
    
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        return 60
    }
    
    override func didSelect() {
        self.selection?(self.budget!)
        self.backgroundColor = Color.light.value
    }
}

struct BudgetTableCellViewModel: TableCellViewModel {
    var cellType: TableViewCell.Type { return BudgetCell.self }
    var budget : Budget
    let selectionHandler: ((Budget?) -> Void)?
}



