//
//  Specializations.swift
//  Taoka
//
//  Created by Роман Макеев on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation

struct Offers: Codable {
    
    var type : UserType?
    var pending : Int?
    
    
    init() {
        self.type = nil
        self.pending = nil
    }
    
    init(type : UserType?, pending : Int?) {
        self.type = type
        self.pending = pending
    }
    
    
    //    func toDictionary() -> JsonDictionary {
    //
    //        var dict : [String : Any] = [:]
    //        dict["type"] = self.type?.rawValue
    //        return dict
    //
    //    }
}

extension Offers {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Offers.self, from: data)
    }
}

extension Array where Element == Offers {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Offers].self, from: data)
    }
}




