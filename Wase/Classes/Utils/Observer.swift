//
//  Observer.swift
//  MDW
//
//  Created by Andrew Oparin on 08/09/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation

public class Observable<T> {
    
    public typealias ObserveAction = (_ oldValue: T, _ newValue: T) -> Void
    
    public var observers = [ObjectIdentifier: ObserveAction]()
    
    public var value: T {
        didSet {
            observers.values.forEach { (action) in
                action(oldValue, value)
            }
        }
    }
    
    public func add(observer: AnyObject,
                    observeAction: @escaping ObserveAction) {
        observers[ObjectIdentifier(observer)] = observeAction
    }
    
    public func remove(observer: AnyObject) {
        observers.removeValue(forKey: ObjectIdentifier(observer))
    }
    
    public init(value: T) {
        self.value = value
    }
    
    private var onDeinit: (() -> Void)?
    
    deinit {
        self.onDeinit?()
    }
}
