//
//  MDWCodeTextField.swift
//  MDW
//
//  Created by Andrew Oparin on 06/08/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

class OMGCodeTextField: AirdronView {
    
    private let contentHeight: CGFloat = 50
    weak var delegate: CodeFieldViewDelegate? {
        didSet {
            self.codeField.delegate = self.delegate
        }
    }
    private lazy var codeField = CodeFieldView()
    
    func resignResponder() {
        self.codeField.resignFirstResponder()
    }
    
    func firstResponder() {
        self.codeField.becomeFirstResponder()
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.codeField)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.codeFieldTap))
        self.addGestureRecognizer(tap)
        self.invalidateIntrinsicContentSize()
        self.setNeedsUpdateConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.codeField.snp.makeConstraints {
//            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-5)
            $0.left.equalToSuperview().offset(24)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: self.contentHeight)
    }
    
    func showError() {
        self.shake()
        self.codeField.reset()
        self.codeField.setCode("")
    }
    
    @objc
    private func codeFieldTap() {
        self.codeField.becomeFirstResponder()
    }
}

