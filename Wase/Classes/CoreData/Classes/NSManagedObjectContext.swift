//
//  NSManagedObjectContext.swift
//  AirdronCore
//
//  Created by andrew oparin on 10/02/2018.
//  Copyright © 2018 airdron. All rights reserved.
//

import Foundation
import CoreData

public extension NSManagedObjectContext {
    
    @discardableResult
    public func saveOrRollback() -> Bool {
        guard self.hasChanges else {
            return true
        }
        do {
            try save()
            return true
        } catch {
            print("Error while saving context \(error)")
            rollback()
            return false
        }
    }
    
    public func performSaveOrRollback() {
        perform {
            _ = self.saveOrRollback()
        }
    }
    
    public func performChanges(block: @escaping (NSManagedObjectContext) -> Void) {
        perform {
            block(self)
            _ = self.saveOrRollback()
        }
    }
    
    public func performChangesAndWait(block: @escaping (NSManagedObjectContext) throws -> Void) throws {
        var exception: Error? = nil
        performAndWait {
            do {
                try block(self)
                try self.save()
            } catch {
                exception = error
            }
        }
        if let exception = exception {
            throw exception
        }
    }
    
    public func insertObject<A: ManagedObject>() -> A where A: ManagedObjectType {
        guard let obj = NSEntityDescription.insertNewObject(forEntityName: A.entityName, into: self) as? A else { fatalError("Wrong object type") }
        return obj
    }
    
    func deleteAllObjects() throws {
        if let entitesByName = self.persistentStoreCoordinator?.managedObjectModel.entitiesByName {
            var deletedObjectIds = [NSManagedObjectID]()
            for (_, entityDescription) in entitesByName {
                if let objectIDs = try self.deleteAllObjectsForEntityViaBatchRequest(entity: entityDescription) {
                    deletedObjectIds.append(contentsOf: objectIDs)
                }
            }
            let changes = [NSDeletedObjectsKey: deletedObjectIds]
            NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [self])
            self.refreshAllObjects()
        }
    }
    
    func deleteAllObjectsForEntityViaBatchRequest(entity: NSEntityDescription) throws -> [NSManagedObjectID]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entity
        let batchDelete = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        batchDelete.resultType = .resultTypeObjectIDs
        let result = try self.execute(batchDelete) as? NSBatchDeleteResult
        return result?.result as? [NSManagedObjectID]
    }
}
