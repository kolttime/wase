//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class photoAgreementViewController : SimpleNavigationBar {
    
    private let loadingView = TaokaLoadingView()
    var onNext : (() -> Void)?
    var crutchTF = OMGTextField.makeDate()
    var mainLabel = UILabel()
    var apiLabel = UILabel()
    var apiService : ApiService
    
    init(apiService : ApiService){
        self.apiService = apiService

        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var scrollViewController = ScrollViewController()
    
    func fetchData(){
        self.loadingView.isHidden = false
        self.apiService.privacyPolicy(){[weak self] response in
            switch response {
            case .success(let rr):
                print(rr)
                self?.apiLabel.text = rr.text
                self?.loadingView.isHidden = true
            case .failure(let error):
                print(error)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.apiLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "")
        
        view.addSubview(self.loadingView)
        self.loadingView.isHidden = true
        
        self.apiLabel.numberOfLines = 0

        
        self.crutchTF.isHidden = true
        self.view.backgroundColor = Color.white.value
        self.mainLabel.font = UIFont.boldSystemFont(ofSize: 30)
        self.mainLabel.textColor = .black
        self.mainLabel.text = "Пользовательское соглашение"
        self.mainLabel.numberOfLines = 0
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        
        self.view.addSubview(self.scrollViewController.scrollView)
        self.scrollViewController.scrollView.addSubview(mainLabel)
        self.scrollViewController.scrollView.addSubview(crutchTF)
        self.scrollViewController.scrollView.addSubview(apiLabel)
        
        self.setTabBarHidden()
        self.setLargeTitleHidden()

        
        self.setUpConstraints()
        
        self.fetchData()
    }
    
    @objc func nextView(){
        print("~~~~~")
        self.onNext?()
    }
    
    override func setUpConstraints(){
        
        self.scrollViewController.scrollView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        self.mainLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalToSuperview()
        }
        
        self.crutchTF.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
        self.apiLabel.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(11)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-16)
            $0.bottom.equalToSuperview().offset(-20)
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
}



