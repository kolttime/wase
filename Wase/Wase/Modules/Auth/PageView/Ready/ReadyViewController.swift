//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class ReadyViewController : AirdronViewController {
    
    var onNext : (() -> Void)?
    
    var mainLabel = UILabel()
    
    var readyView = SampleButton()

    var readyIntro = UILabel()
  
    private var userCreation : UserCreation
    private var apiService : ApiService
    
    
    init(userCreation : UserCreation, apiService : ApiService){
        self.userCreation = userCreation
        self.apiService = apiService
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func makeIntroSupplier(){
        self.apiService.getReadyPageSupplier(){[weak self] response in
            switch response {
            case .success(let rr):
                self?.readyIntro.text = rr.text
                print(rr)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func makeIntroClien(){
        self.apiService.getReadyPageClient(){[weak self] response in
            switch response {
            case .success(let rr):
                self?.readyIntro.text = rr.text
                print(rr)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fetchData(){
        switch userCreation.type {
        case UserType(rawValue: "PHOTOGRAPHER") as! UserType:
            makeIntroSupplier()
        case UserType(rawValue: "MAKEUP_ARTIST") as! UserType:
            makeIntroSupplier()
        case UserType(rawValue: "CLIENT") as! UserType:
            makeIntroClien()
        default:
            print("deff")
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        view.addSubview(mainLabel)
        view.addSubview(readyView)
        view.addSubview(readyIntro)
        mainLabel.font = UIFont.systemFont(ofSize: 35)
        mainLabel.textColor = .black
        mainLabel.text = "Готово"
        mainLabel.numberOfLines = 0

        self.readyIntro.numberOfLines = 0
        self.readyIntro.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.white.value).make(string: "")
        
        self.readyView.mainLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.white.value).make(string: "Готово")
    
        self.readyView.backgroundColor = Color.accent.value
        self.readyView.layer.cornerRadius = 10
        self.readyView.layer.masksToBounds = true

        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.nextView))
        self.readyView.addGestureRecognizer(tap1)
        
        self.readyView.backgroundColor = Color.accent.value
        self.fetchData()
        print(userCreation)
        self.setUpConstraints()
        
        
    }
    
    @objc func nextView(){
        print("~~~~~")
        self.onNext?()
    }
    
    func setUpConstraints(){
        
        mainLabel.snp.makeConstraints{
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
        readyView.snp.makeConstraints{
            $0.bottom.equalToSuperview().offset(-30)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.height.equalTo(56)
        }
        readyIntro.snp.makeConstraints{
            $0.top.equalTo(mainLabel.snp.bottom).offset(ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
    }
}


