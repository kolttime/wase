//
//  UserPhotoTopView.swift
//  Taoka
//
//  Created by Minic Relocusov on 18/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit


class UserphotoTopView : AirdronView {
    
    
    let separatorView = UIView()
    let nameLabel : UILabel = UILabel()
    let dateLabel : UILabel = UILabel()
    let arrow : UIImageView = UIImageView()
    let img : Bool = false
    
    
    
    func set(nameLabel : String, dateLabel : String, img : Bool){
        if !img {
            self.arrow.isHidden = true
        }
        self.nameLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.make(string: nameLabel)
        self.dateLabel.attributedText = CustomFont.tech13.attributesWithParagraph.make(string: dateLabel)
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.nameLabel)
        self.addSubview(self.dateLabel)
        self.addSubview(self.separatorView)
        self.separatorView.backgroundColor = Color.light.value
        self.backgroundColor = Color.white.value
        arrow.image = UIImage(named: "Arrow2")!
        self.addSubview(self.arrow)
        
        
        self.setupConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.nameLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(12)
        }
        self.dateLabel.snp.makeConstraints{
            $0.top.equalTo(self.nameLabel.snp.bottom)
            $0.left.equalToSuperview().offset(12)
        }
        self.separatorView.snp.makeConstraints{
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
        }
        if self.img {
            self.arrow.snp.makeConstraints{
                $0.top.equalToSuperview().offset(15)
                $0.right.equalToSuperview().offset(-17)
            }
        }
    }
}
