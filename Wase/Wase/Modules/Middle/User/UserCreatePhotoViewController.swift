//
//  IntroductionViewController.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class UserCreatePhotoViewController : AirdronViewController {
    
    var onNext : ((Specialization?) -> Void)?

    private let loadingView = TaokaLoadingView()
    lazy var tableViewController = TableViewController()
    
    private var photoTypeModels: [CreatePhotosessionTableCellViewModel] = []


    public var apiService : ApiService
    init(apiService : ApiService){
        self.apiService = apiService
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        
    
        view.addSubview(self.loadingView)
        self.loadingView.isHidden = false
        self.view.addSubview(tableViewController.tableView)
        
        self.tableViewController.tableView.register(cellClass: CreatePhotosessionTypeCell.self)
        self.tableViewController.tableView.register(headerFooterViewClass: MainLabelHeaderCell.self)
        
        let headerModel = MainLabelHeaderViewModel(mainLabel: "Какая фотосессия вам нужна?")
        self.setUpConstraints()
//        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.nextView))
        self.fetchData()
    }
    
//    @objc func nextView(){
//        print("~~~~~")
//        self.onNext?()
//    }
    
    func fetchData(){
        self.loadingView.isHidden = false
        self.apiService.specialization(){[weak self] response in
            switch response {
            case .success(let model):
                self?.loadingView.isHidden = true
                print(model)
                let models = model
                self!.render(models: models)
            case .failure(let error):
                print(error)
            }
        }
    }
    func render(models : [Specialization]){
        var viewModels : [CreatePhotosessionTableCellViewModel] = []
        for model in models {
            let selection : ((Specialization?) -> Void) = {[weak self]  type in
                print(type)
                self?.onNext?(type)
                print()
            }
            
            let viewModel = CreatePhotosessionTableCellViewModel(specialization : model, selectionHandler: selection)
            viewModels.append(viewModel)
        }

        let headerModel = MainLabelHeaderViewModel(mainLabel: "Какая фотосессия вам нужна?")
        let section = DefaultTableSectionViewModel(cellModels: viewModels, headerViewModel: headerModel, footerViewModel: nil)
        self.tableViewController.update(viewModels: [section])
        
    }
    func setUpConstraints(){

        tableViewController.tableView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.loadingView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableViewController.tableView.reloadData()
    }
    
}

