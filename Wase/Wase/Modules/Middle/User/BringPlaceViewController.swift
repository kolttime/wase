//
//  File.swift
//  Taoka
//
//  Created by Minic Relocusov on 10/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class BringPlaceViewController : AirdronViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var activeField = 0
    private lazy var keyObserver = KeyboardObserver()
    
    var slideView = SlideView()
    var districtPicker = UIPickerView()
    var atHome = checkPlanView()
    var travel = checkPlanView()
    var studio = checkPlanView()
    
    var selectedDistrict : District?
    
    var cityType = ["Район", "За городом"]
    
    var activePicker = 0
    
    var inCity : [District] = []
    var outCity : [District] = []
    
    private var apiService : ApiService
    private var userModel : User
    
    init(apiService : ApiService, userModel : User) {
        self.apiService = apiService
        self.userModel = userModel
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var txtfld = OMGTextField.makeAdress()

    var onnHome : ((LocationType, String) -> Void)?
    var onnAdress : ((LocationType, String) -> Void)?
    var onnStudio : ((LocationType) -> Void)?
    
    let mainLabel = UILabel()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.slideView.setRecognizer(height: 304)
        self.slideView.defaultCenter = self.view.frame.maxY + (250)

        self.districtPicker.delegate = self
        self.districtPicker.dataSource = self
        
        self.slideView.addSubview(districtPicker)
        self.slideView.addSubview(txtfld)
        
        self.districtPicker.isHidden = false
        self.txtfld.isHidden = true
        
        atHome.nameLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "Дома")
        atHome.descrLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Когда у себя хорошо")
        
        travel.nameLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "На выезде")
        travel.descrLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Когда место очень важно")
        
        studio.nameLabel.attributedText = CustomFont.bodyRegular20.attributesWithParagraph.make(string: "В студии")
        studio.descrLabel.attributedText = CustomFont.bodyRegular15.attributesWithParagraph.colored(color: Color.grey.value).make(string: "Когда нужно дополнительное оборудование или подготовленный интерьер")
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.onHome))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.onTravel))
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.onStudio))
        self.atHome.addGestureRecognizer(tap1)
        self.travel.addGestureRecognizer(tap2)
        self.studio.addGestureRecognizer(tap3)
        
        let fff  : Action? = {[weak self] in
            switch self?.activeField {
            case 1:
                print("First")
                if self!.txtfld.text != ""{
                    self!.onnHome!(LocationType.home, self!.txtfld.text!)
                    self?.slideView.close?()
                }
            case 2:
                guard let self = self else {return}
                self.onnAdress!(LocationType.outdoors, (self.selectedDistrict?.id!)!)
                self.slideView.close?()
                print("Second")
                print(self.selectedDistrict?.name)
            default:
                print("fff")
            }
    
        }
        
        self.slideView.setRightTitle(title: "Выбрать", completion: fff)
        
        self.mainLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.colored(color: Color.black.value).make(string: "В каком месте хотите провести?")
        
        mainLabel.numberOfLines = 0
        
        self.districtPicker.isHidden = false
    
        self.view.addSubview(mainLabel)
        self.view.addSubview(atHome)
        self.view.addSubview(travel)
        self.view.addSubview(studio)
        self.view.addSubview(slideView)
        self.slideView.animate = {[weak self] in
            self?.view.animateLayout(duration: 0.5)
        }
        self.setUpConstraints()
        self.setupHandlers()
        self.fetchData()
    }
    
    func fetchData(){
        self.apiService.takeDistricts(id: userModel.city.id){[weak self] response in
            switch response {
            case .success(let rr):
                for i in rr{
                    if i.type == DistrictType.inCity{
                        self?.inCity.append(i)
                    } else {
                        self?.outCity.append(i)
                    }
                }
                self?.selectedDistrict = rr[0]
                print(rr)
                print(self?.inCity)
                print(self?.outCity)
                self?.districtPicker.reloadAllComponents()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func renderHome(){
        self.districtPicker.isHidden = true
        self.txtfld.isHidden = false
        self.studio.image.tintColor = Color.greyL.value
        self.atHome.image.tintColor = Color.black.value
        self.travel.image.tintColor = Color.greyL.value
        self.travel.backgroundColor = Color.white.value
        self.atHome.backgroundColor = Color.light.value
        self.studio.backgroundColor = Color.white.value
        
    }
    
    func renderAdress(){
        self.districtPicker.isHidden = false
        self.txtfld.isHidden = true
        self.studio.image.tintColor = Color.greyL.value
        self.atHome.image.tintColor = Color.greyL.value
        self.travel.image.tintColor = Color.black.value
        self.travel.backgroundColor = Color.light.value
        self.atHome.backgroundColor = Color.white.value
        self.studio.backgroundColor = Color.white.value
        
    }
    
    @objc func onHome(){
        self.slideView.setHeight(height: 416)
        self.slideView.open?()
        self.txtfld.becomeResponder()
        print("Home")
        self.renderHome()
        activeField = 1
    }
    
    @objc func onTravel(){
        self.slideView.setHeight(height: 304)
        self.slideView.open?()
        print("Travel")
        self.renderAdress()
        activeField = 2
    }

    @objc func onStudio(){
        print("Studio")
        self.studio.image.tintColor = Color.black.value
        self.atHome.image.tintColor = Color.greyL.value
        self.travel.image.tintColor = Color.greyL.value
        self.travel.backgroundColor = Color.white.value
        self.atHome.backgroundColor = Color.white.value
        self.studio.backgroundColor = Color.light.value
        activeField = 3
        self.onnStudio?(LocationType.studio)
    }
    private var keyHeight = CGFloat.zero
    func setupHandlers(){
        self.keyObserver.onKeyboardWillShow = {[weak self] frame, d in
            self?.keyHeight = frame.height
            self?.slideView.setHeight(height: frame.height + 138)
            self?.slideView.open?()
        }
        self.txtfld.onTextDidChanged = {[weak self] result in
            self?.slideView.layoutSubviews()
            print(result.text)
        }
        self.txtfld.onReturnHandler = {[weak self] in
            self?.view.endEditing(true)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        switch activeField{
        case 1:
            print("from vs")
        case 2:
            print("sas")
        default:
            print("none")
        }
        
    }
    
    func setUpConstraints(){
        self.mainLabel.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.atHome.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.height.equalTo(64)
            $0.right.equalToSuperview()
        $0.top.equalTo(mainLabel.snp.bottom).offset(ViewSize.sideOffset)
        }
        self.slideView.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            self.slideView.topConstr = $0.top.equalTo(self.view.snp.bottom).constraint
            $0.height.equalTo(500)
        }
        self.travel.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.height.equalTo(64)
            $0.right.equalToSuperview()
            $0.top.equalTo(atHome.snp.bottom).offset(10)
        }
        self.studio.snp.makeConstraints{
            $0.left.equalToSuperview()
            $0.height.equalTo(104)
            $0.right.equalToSuperview()
            $0.top.equalTo(travel.snp.bottom).offset(10)
        }
        self.districtPicker.snp.makeConstraints{
            $0.left.equalTo(self.slideView.snp.left)
            $0.top.equalTo(self.slideView.snp.top).offset(54)
            $0.right.equalTo(self.slideView.snp.right).offset(-23)
        }
        self.txtfld.snp.makeConstraints{
            $0.top.equalTo(self.slideView.snp.top).offset(49)
            $0.left.equalTo(slideView.snp.left).offset(ViewSize.sideOffset)
            $0.right.equalTo(slideView.snp.right).offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(60)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return 2
        }
        else {
            if activePicker == 0{
                return inCity.count
            } else {
                return outCity.count
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0{
            return cityType[row]
        }
        if component == 1{
            if activePicker == 0{
                return inCity[row].name
            } else {
                return outCity[row].name
            }
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0{
            print(cityType[row])
            activePicker = row
            self.districtPicker.reloadAllComponents()
        }
        if component == 1{
            if activePicker == 0{
                if inCity.count == 0{
                    print("None")
                } else {print(inCity[row].name)
                    selectedDistrict = inCity[row]
                }
                
            } else {
                if outCity.count == 0{
                    print("None")
                    } else {print(outCity[row].name)
                    selectedDistrict = outCity[row]
                }
            }
        }
    }
    
}






