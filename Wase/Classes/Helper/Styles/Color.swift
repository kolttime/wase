//
//  Color.swift
//  OMG
//
//  Created by Andrew Oparin on 04/10/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

enum Color {
    
    case white
    case black
    case grey
    case greyL
    case light
    case accent
    case red
    case green
    
    
    var value: UIColor {
        switch self {
        case .white:
            return .white
        case .black:
            return .black
        case .grey:
            return .init(hashRgbaValue: 0x89898FFF)
        case .greyL:
            return .init(hashRgbaValue: 0xC4C4C8FF)
        case .light:
            return .init(hashRgbaValue: 0xEFEFF4FF)
        case .accent:
            return .init(hashRgbaValue: 0x004099FF)
        case .red:
            return .init(hashRgbaValue: 0xE52E2EFF)
        case .green:
            return .init(hashRgbaValue: 0x23B26AFF)
        
        }
    }
}
