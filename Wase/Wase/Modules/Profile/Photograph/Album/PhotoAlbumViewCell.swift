//
//  PhotoAlbumViewCell.swift
//  Taoka
//
//  Created by Minic Relocusov on 15/07/2019.
//  Copyright © 2019 Minic Relocusov. All rights reserved.
//

import Foundation
import UIKit

class PhotoAlbumViewCell : TableViewCell {
    
    
    
    private lazy var albumImageView = UIImageView()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.layoutIfNeeded()
        self.contentView.layoutSubviews()
        self.contentView.addSubview(self.albumImageView)
        self.albumImageView.contentMode = .scaleToFill
        self.setupConstraints()
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! PhotoAlbumViewCellModel
       // self.albumImageView.kf.setImage(with: setContentWidth(imageSrc: viewModel.image.srcset!))
        self.albumImageView.kf.setImage(with: viewModel.image.original)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.albumImageView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-3)
        }
    }
    override class func height(for viewModel: ViewModelType,
                               tableView: UITableView) -> CGFloat {
        //return (((UIScreen.main.bounds.size.width - 40) - 3) / 4.0) * 2.2
        let viewModel = viewModel as! PhotoAlbumViewCellModel
        let prop : Float = Float(Float(viewModel.image.height) / Float(viewModel.image.width))
        let width = Float(UIScreen.main.bounds.size.width)
        print("Width : \(viewModel.image.width)\nHeight : \(viewModel.image.height)\nProp : \(prop)\nScreenWidth \(width)\nSreenHeight : \(width * prop)")
        return CGFloat(   prop * width )
        
    }
}
struct PhotoAlbumViewCellModel : TableCellViewModel {
    var cellType: TableViewCell.Type {return PhotoAlbumViewCell.self}
    var image : Image
}
