//
//  AirdronTabItem.swift
//  MDW
//
//  Created by Andrew Oparin on 21/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

open class AirdronTabItem: AirdronView {
    
    class Style {
        
        //static let font =UIFont(name: "Arial")
    }
    
    public var onSelectTab: Action?
    
    private let textHeight: CGFloat = 15
    private let imageHeight: CGFloat = 35
    
    public lazy var imageView = UIImageView()
    public lazy var textLabel = UILabel()
    
    public var selectedImage: UIImage?
    public var normalImage: UIImage?
    
    public var text: String? = ""
    
   // public var font = Style.font
    
    public var selectedImageColor: UIColor = UIColor.black
    public var normalImageColor: UIColor = UIColor.gray
    public var selectedTextColor: UIColor = UIColor.black
    public var normalTextColor: UIColor = UIColor.gray
    
    
    open override func initialSetup() {
        super.initialSetup()
        self.updateState(isSelected: self.isSelected)
        self.addSubview(self.imageView)
        self.addSubview(self.textLabel)
        self.imageView.isUserInteractionEnabled = false
        self.textLabel.isUserInteractionEnabled = false
        self.imageView.contentMode = .center
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tap))
        self.addGestureRecognizer(tap)
        self.setNeedsLayout()
    }
    
    @objc
    func tap() {
        self.onSelectTab?()
    }
    
    public var isSelected = false {
        didSet {
            self.updateState(isSelected: self.isSelected)
            self.setNeedsLayout()
        }
    }
    
    func updateState(isSelected: Bool) {
        var textAttributes: [NSAttributedString.Key: Any] = [:]
        //textAttributes[NSAttributedString.Key.font] = self.font
        if isSelected {
            self.imageView.image = self.selectedImage?.withRenderingMode(.alwaysTemplate)
            self.imageView.tintColor = self.selectedImageColor
            textAttributes[NSAttributedString.Key.foregroundColor] = self.selectedTextColor
        } else {
            self.imageView.image = self.normalImage?.withRenderingMode(.alwaysTemplate)
            self.imageView.tintColor = self.normalImageColor
            textAttributes[NSAttributedString.Key.foregroundColor] = self.normalTextColor
        }
        if let text = self.text {
            self.textLabel.attributedText = NSAttributedString(string: text, attributes: textAttributes)
        } else {
            self.textLabel.attributedText = nil
        }
        self.textLabel.textAlignment = .center
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        self.imageView.frame.size = CGSize(width: self.bounds.width, height: self.imageHeight)
        self.imageView.frame.origin = CGPoint.zero
        self.textLabel.frame.size = CGSize(width: self.bounds.width, height: self.textHeight)
        self.textLabel.frame.origin = CGPoint(x: 0, y: self.imageView.frame.maxY)
    }
}
