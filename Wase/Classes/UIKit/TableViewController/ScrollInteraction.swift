//
//  ScrollInteraction.swift
//  MDW
//
//  Created by Andrew Oparin on 30/07/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import UIKit

public protocol ScrollInteraction: class {
    
    var scrollView: UIScrollView! { get }
    var onScroll: ((UIScrollView) -> Void)? { get set }
}


