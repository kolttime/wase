//
//  LaunchViewController.swift
//  Taoka
//
//  Created by Artem Myachkov on 07/07/2019.
//  Copyright © 2019 Artem Myachkov. All rights reserved.
//

import Foundation
import UIKit

class ErrorViewController: AirdronViewController, UIScrollViewDelegate{
    
    var mainLabel = UILabel()
    private var descLabel = UILabel()
    private lazy var professionTextField = OMGTextField.professionField()
    private lazy var emailField = OMGTextField.EmailField()
    private lazy var cityTextField = OMGTextField.makeCityField()
    private lazy var keyObserver = KeyboardObserver()
    private lazy var subscribe = SampleButton()
    var cancel = SampleButton()
    let descText : String
    var scrollViewController = ScrollViewController()
    private var activeField = UIView() {
        didSet {
            
                self.setScrollOffset(offset: 100)
            
            
        }
    }
    var type : Int
    
     init(type : Int) {
        self.type = type
        if type == 0 {
            self.descText = "Мы пока еще работаем над расширенными возможностями в приложении и скоро будем рады видеть новых специалистов. \n\nРасскажите чем вы занимаетесь, будем держать в курсе новостей."
        } else if type == 1 {
            self.descText = "Пока мы работаем в определенном списке городов и скоро планируем расширяться. \n\nРасскажите откуда вы, будем держать в курсе новостей. "
        } else {
            self.descText = "Мы пока еще работаем над расширенными возможностями в приложении и скоро будем рады видеть новых специалистов. \n\n Расскажите чем вы занимаетесь, будем держать в курсе новостей."
        }
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainLabel.attributedText = CustomFont.largeTitle.attributesWithParagraph.make(string: "Извините, но")
        self.mainLabel.numberOfLines = 0
        
        self.view.backgroundColor = Color.white.value
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.descLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.make(string: self.descText)
        self.descLabel.numberOfLines = 0
        self.subscribe.mainLabel.attributedText = CustomFont.bodySemibold17.attributesWithParagraph.colored(color: Color.black.value).make(string: "Подписаться")
        self.subscribe.backgroundColor = Color.light.value
        self.subscribe.layer.cornerRadius = 10
        self.subscribe.layer.masksToBounds = true

        self.cancel.mainLabel.attributedText = CustomFont.bodyRegular17.attributesWithParagraph.colored(color: Color.black.value).make(string: "Пропустить")
        self.cancel.backgroundColor = Color.white.value
        self.cancel.layer.cornerRadius = 10
        self.cancel.layer.masksToBounds = true
        self.cancel.addTarget(self, action: #selector(self.onCancel), for: .touchUpInside)
        self.subscribe.addTarget(self, action: #selector(self.onNext), for: .touchUpInside)
        self.navigationItem.setHidesBackButton(true, animated:true) //??
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        scrollViewController.scrollView.addSubview(mainLabel)
        scrollViewController.scrollView.addSubview(self.descLabel)
        scrollViewController.scrollView.addSubview(cityTextField)
        scrollViewController.scrollView.addSubview(professionTextField)
        scrollViewController.scrollView.addSubview(emailField)
        scrollViewController.scrollView.addSubview(subscribe)
        scrollViewController.scrollView.addSubview(cancel)
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        let rec = UIGestureRecognizer(target: self, action: #selector(self.gesture))
        if !Device.isIphone5 {
            self.scrollViewController.scrollView.addGestureRecognizer(rec)
        }
        scrollViewController.scrollView.delegate = self
        self.view.addSubview(self.scrollViewController.view)
        if self.type == 0 {
            self.cityTextField.isHidden = true
            self.SetupConstraints0()
        } else if self.type == 1 {
            self.professionTextField.isHidden = true
            self.SetupConstraints1()
        } else {
            self.SetupConstraints2()
        }
        self.setupHandlers()
        
    }
    @objc func gesture(){
        self.view.endEditing(true)
    }
    var cancelAction : ((String, String, String) -> Void)?
    var nextAction : ((String, String, String) -> Void)?
    @objc func onNext(){
        let email = self.emailField.text ?? ""
        let proffesion = self.professionTextField.text ?? ""
        let city = self.cityTextField.text ?? ""
        self.cancelAction?(email, proffesion, city)
    }
    @objc func onCancel(){
        let email = self.emailField.text ?? ""
        let proffesion = self.professionTextField.text ?? ""
        let city = self.cityTextField.text ?? ""
        self.cancelAction?(email, proffesion, city)
    }
    
    
    private func SetupConstraints0(){
        if !Device.isIphone5 {
            self.scrollViewController.view.snp.makeConstraints{
                $0.top.equalToSuperview()
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
                $0.bottom.equalToSuperview()
            }
            
            mainLabel.snp.makeConstraints{
                $0.top.equalTo(self.scrollViewController.view)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            }
            self.descLabel.snp.makeConstraints{
                $0.top.equalTo(self.mainLabel.snp.bottom).offset(16)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            }
            
            professionTextField.snp.makeConstraints{
                $0.bottom.equalTo(emailField.snp.bottom).offset(-60)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }
            
            emailField.snp.makeConstraints{
                $0.bottom.equalTo(self.subscribe.snp.top).offset(-30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }
            
            subscribe.snp.makeConstraints{
                $0.bottom.equalTo(cancel.snp.top).offset(-10)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
            }
            
            cancel.snp.makeConstraints{
                $0.bottom.equalTo(self.scrollViewController.view).offset(-40)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
            }
        
        } else {
            self.scrollViewController.view.snp.makeConstraints{
                $0.top.equalToSuperview()
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
                $0.bottom.equalToSuperview()
            }
            
            mainLabel.snp.makeConstraints{
                $0.top.equalTo(self.scrollViewController.view)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            }
            self.descLabel.snp.makeConstraints{
                $0.top.equalTo(self.mainLabel.snp.bottom).offset(16)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            }
            
            professionTextField.snp.makeConstraints{
                $0.top.equalTo(descLabel.snp.bottom).offset(60)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }
            
            emailField.snp.makeConstraints{
                $0.bottom.equalTo(self.professionTextField.snp.bottom).offset(30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }
            
            subscribe.snp.makeConstraints{
                $0.top.equalTo(emailField.snp.top).offset(30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
            }
            
            cancel.snp.makeConstraints{
                $0.top.equalTo(self.subscribe.snp.bottom).offset(30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
                $0.bottom.equalToSuperview()
            }
        }
    }
    private func SetupConstraints1(){
        if !Device.isIphone5 {
            self.scrollViewController.view.snp.makeConstraints{
                $0.top.equalToSuperview()
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
                $0.bottom.equalToSuperview()
            }
            
            mainLabel.snp.makeConstraints{
                $0.top.equalTo(self.scrollViewController.view)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            }
            self.descLabel.snp.makeConstraints{
                $0.top.equalTo(self.mainLabel.snp.bottom).offset(16)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            }
            
            cityTextField.snp.makeConstraints{
                $0.bottom.equalTo(emailField.snp.bottom).offset(-60)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }
            
            emailField.snp.makeConstraints{
                $0.bottom.equalTo(self.subscribe.snp.top).offset(-30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }
            
            subscribe.snp.makeConstraints{
                $0.bottom.equalTo(cancel.snp.top).offset(-10)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
            }
            
            cancel.snp.makeConstraints{
                $0.bottom.equalTo(self.scrollViewController.view).offset(-40)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
            }
        } else {
            self.scrollViewController.view.snp.makeConstraints{
                $0.top.equalToSuperview()
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
                $0.bottom.equalToSuperview()
            }
            
            mainLabel.snp.makeConstraints{
                $0.top.equalTo(self.scrollViewController.view)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            }
            self.descLabel.snp.makeConstraints{
                $0.top.equalTo(self.mainLabel.snp.bottom).offset(16)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            }
            
            cityTextField.snp.makeConstraints{
                $0.top.equalTo(descLabel.snp.bottom).offset(60)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }
            
            emailField.snp.makeConstraints{
                $0.bottom.equalTo(self.cityTextField.snp.bottom).offset(30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }
            
            subscribe.snp.makeConstraints{
                $0.top.equalTo(emailField.snp.top).offset(30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
            }
            
            cancel.snp.makeConstraints{
                $0.top.equalTo(self.subscribe.snp.bottom).offset(30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
                $0.bottom.equalToSuperview()
            }
        }
        
        
    }
    private func SetupConstraints2(){
        if !Device.isIphone5 {
            self.scrollViewController.view.snp.makeConstraints{
                $0.top.equalToSuperview()
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
                $0.bottom.equalToSuperview()
            }
            
            mainLabel.snp.makeConstraints{
                $0.top.equalTo(self.scrollViewController.view)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            }
            self.descLabel.snp.makeConstraints{
                $0.top.equalTo(self.mainLabel.snp.bottom).offset(16)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            }
            cityTextField.snp.makeConstraints{
                $0.bottom.equalTo(self.professionTextField.snp.bottom).offset(-60)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }
            professionTextField.snp.makeConstraints{
                $0.bottom.equalTo(emailField.snp.bottom).offset(-60)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }

            emailField.snp.makeConstraints{
                $0.bottom.equalTo(self.subscribe.snp.top).offset(-30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }

            subscribe.snp.makeConstraints{
                $0.bottom.equalTo(cancel.snp.top).offset(-10)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
            }

            cancel.snp.makeConstraints{
                $0.bottom.equalTo(self.scrollViewController.view).offset(-40)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
            }
        } else {
            self.scrollViewController.view.snp.makeConstraints{
                $0.top.equalToSuperview()
                $0.left.equalToSuperview()
                $0.right.equalToSuperview()
                $0.bottom.equalToSuperview()
            }
            
            mainLabel.snp.makeConstraints{
                $0.top.equalTo(self.scrollViewController.view)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            }
            self.descLabel.snp.makeConstraints{
                $0.top.equalTo(self.mainLabel.snp.bottom).offset(16)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            }
            
            cityTextField.snp.makeConstraints{
                $0.top.equalTo(descLabel.snp.bottom).offset(60)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }
            
            professionTextField.snp.makeConstraints{
                $0.top.equalTo(cityTextField.snp.bottom).offset(30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }
            
            
            emailField.snp.makeConstraints{
                $0.bottom.equalTo(self.professionTextField.snp.bottom).offset(30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            }
            
            subscribe.snp.makeConstraints{
                $0.top.equalTo(emailField.snp.top).offset(30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
            }
            
            cancel.snp.makeConstraints{
                $0.top.equalTo(self.subscribe.snp.bottom).offset(30)
                $0.left.equalToSuperview().offset(ViewSize.sideOffset)
                $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
                $0.height.equalTo(56)
                $0.bottom.equalToSuperview()
            }
        }

        
    }
    var first = true
    var defaultCenter = CGFloat.zero
    var keyFrame = CGRect.init(x: 0.0, y: 451.0, width: 375.0, height: 216.0)
    func setScrollOffset(offset : CGFloat = 0){
        let keyMin = UIScreen.main.bounds.height - self.keyFrame.height
        let textFrame  = self.scrollViewController.scrollView.convert(self.activeField.frame, to: self.view)
        
        print("KeyMin: \(keyMin)\ntextFrame: \(textFrame.maxY + offset)")
        
        if (textFrame.maxY + offset ) > keyMin {
            
            let scrollOffset = (textFrame.maxY + offset) - keyMin
            if Device.isIphone5 {
                self.scrollViewController.scrollView.setContentOffset(CGPoint(x: self.scrollViewController.scrollView.contentOffset.x, y: scrollOffset), animated: true)
            } else {
                if first {
                    self.defaultCenter = self.view.center.y
                    self.first = false
                }
                UIView.animate(withDuration: 0.5) {
                    self.view.center.y -= scrollOffset
                }
            }
        } else {
            if Device.isIphone5 {
                self.scrollViewController.scrollView.setContentOffset(CGPoint.zero, animated: true)
            } else {
                if self.first {
                    self.defaultCenter = self.view.center.y
                    self.first = false
                }
                UIView.animate(withDuration: 0.5) {
                    self.view.center.y = self.defaultCenter
                }
            }
        }
    }
    func setupHandlers(){
        self.keyObserver.onKeyboardDidShow = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            self?.keyFrame = frame
            // self?.scrollViewController.scrollView.contentSize.height = self?.scrollViewController.scrollView.contentSize.height ?? 0 + y
        }
        
        self.keyObserver.onKeyboardWillHide = { [weak self] frame, duration in
            self?.scrollViewController.scrollView.contentInset.bottom = 0
            UIView.animate(withDuration: 0.5, animations: {
                if !Device.isIphone5 {
                    self?.view.center.y = self?.defaultCenter ?? 0
                } else {
                    self?.scrollViewController.scrollView.setContentOffset(CGPoint.zero, animated: true)
                }
            })
        }
        self.cityTextField.onBeginEditing = {[weak self] textField in
            self?.activeField = textField
        }
        self.professionTextField.onBeginEditing = {[weak self] textField in
            self?.activeField = textField
        }
        self.cityTextField.onReturnHandler = {[weak self] in
            self?.professionTextField.becomeResponder()
        }
        self.professionTextField.onReturnHandler = {[weak self] in
            self?.emailField.becomeResponder()
        }
        self.emailField.onBeginEditing = {[weak self] textField in
            guard let self = self else {return}
            self.activeField = textField
            
        }
        
        
    }

}

