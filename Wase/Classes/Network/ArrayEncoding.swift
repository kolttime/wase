//
//  ArrayEncoding.swift
//  OMG
//
//  Created by Andrew Oparin on 13/11/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import Foundation
import Alamofire

private let arrayParametersKey = "arrayParametersKey"

extension Array {
    func asAlamofireParameters() -> Parameters {
        return [arrayParametersKey: self]
    }
}

public struct ArrayEncoding: ParameterEncoding {
    
    public let options: JSONSerialization.WritingOptions
    
    public init(options: JSONSerialization.WritingOptions = []) {
        self.options = options
    }
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var urlRequest = try urlRequest.asURLRequest()
        
        guard let parameters = parameters,
            let array = parameters[arrayParametersKey] else {
                return urlRequest
        }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: array, options: options)
            
            if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
            
            urlRequest.httpBody = data
            
        } catch {
            throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
        }
        
        return urlRequest
    }
}
