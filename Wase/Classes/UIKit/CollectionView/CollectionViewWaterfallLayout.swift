//
//  CollectionViewWaterfallLayout.swift
//  MDW
//
//  Created by Andrew Oparin on 27/08/2018.
//  Copyright © 2018 Andrew Oparin. All rights reserved.
//

import UIKit

open class AirdronCollectionBackgroundDecorationAttributes: UICollectionViewLayoutAttributes {
    
    public var color: UIColor = .clear
    
    open override func copy(with zone: NSZone? = nil) -> Any {
        let newAttributes = super.copy(with: zone) as! AirdronCollectionBackgroundDecorationAttributes
        newAttributes.color = self.color.copy(with: zone) as! UIColor
        return newAttributes
    }
    open override var representedElementKind: String? {
        return AirdronCollectionBackgroundDecorationView.kind
    }
}

open class AirdronCollectionBackgroundDecorationView: UICollectionReusableView {
    
    public static let kind = String(describing: AirdronCollectionBackgroundDecorationView.self)
    
    open override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        let layoutAttributes = layoutAttributes as! AirdronCollectionBackgroundDecorationAttributes
        self.backgroundColor = layoutAttributes.color
    }
}

@objc
public protocol AirdronCollectionViewWaterfallLayoutDelegate: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, columnWidth: CGFloat, heightForItemAtIndexPath indexPath: IndexPath) -> CGFloat
    
    @objc
    optional func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, columnCountForSection section: Int) -> Int
    
    @objc
    optional func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, heightForHeaderInSection section: Int) -> CGFloat
    
    @objc
    optional func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, heightForFooterInSection section: Int) -> CGFloat
    
    @objc
    optional func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, insetForSection section: Int) -> UIEdgeInsets
    
    @objc
    optional func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, insetForHeaderInSection section: Int) -> UIEdgeInsets
    
    @objc
    optional func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, insetForFooterInSection section: Int) -> UIEdgeInsets
    
    @objc
    optional func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, minimumInteritemSpacingForSection section: Int) -> CGFloat
    
    @objc
    optional func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, minimumColumnSpacingForSection section: Int) -> CGFloat
    
    @objc
    optional func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, colorForSectionItemsBackground section: Int) -> UIColor
    
    @objc
    optional func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, insetForBackgroundInSection section: Int) -> UIEdgeInsets
}

open class AirdronCollectionViewWaterfallLayout: UICollectionViewLayout {
    
    public override init() {
        super.init()
        self.register(AirdronCollectionBackgroundDecorationView.self,
                      forDecorationViewOfKind: AirdronCollectionBackgroundDecorationView.kind)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.register(AirdronCollectionBackgroundDecorationView.self,
                      forDecorationViewOfKind: AirdronCollectionBackgroundDecorationView.kind)
    }
    
    // MARK: Private constants
    /// How many items to be union into a single rectangle
    fileprivate let unionSize = 20
    
    // MARK: Public Properties
    
    open var columnCount: Int = 3 {
        didSet {
            invalidateIfNotEqual(oldValue as AnyObject, newValue: columnCount as AnyObject)
        }
    }
    
    open var minimumColumnSpacing: CGFloat = 10.0 {
        didSet {
            invalidateIfNotEqual(oldValue as AnyObject, newValue: minimumColumnSpacing as AnyObject)
        }
    }
    open var minimumInteritemSpacing: CGFloat = 10.0 {
        didSet {
            invalidateIfNotEqual(oldValue as AnyObject, newValue: minimumInteritemSpacing as AnyObject)
        }
    }
    open var headerHeight: CGFloat = 0.0 {
        didSet {
            invalidateIfNotEqual(oldValue as AnyObject, newValue: headerHeight as AnyObject)
        }
    }
    open var footerHeight: CGFloat = 0.0 {
        didSet {
            invalidateIfNotEqual(oldValue as AnyObject, newValue: footerHeight as AnyObject)
        }
    }
    open var headerInset: UIEdgeInsets = UIEdgeInsets.zero {
        didSet {
            invalidateIfNotEqual(NSValue(uiEdgeInsets: oldValue), newValue: NSValue(uiEdgeInsets: headerInset))
        }
    }
    
    open var footerInset: UIEdgeInsets = UIEdgeInsets.zero {
        didSet {
            invalidateIfNotEqual(NSValue(uiEdgeInsets: oldValue), newValue: NSValue(uiEdgeInsets: footerInset))
        }
    }
    open var sectionInset: UIEdgeInsets = UIEdgeInsets.zero {
        didSet {
            invalidateIfNotEqual(NSValue(uiEdgeInsets: oldValue), newValue: NSValue(uiEdgeInsets: sectionInset))
        }
    }
    
    open var backgroundInset: UIEdgeInsets = UIEdgeInsets.zero {
        didSet {
            invalidateIfNotEqual(NSValue(uiEdgeInsets: oldValue), newValue: NSValue(uiEdgeInsets: headerInset))
        }
    }
    
    open weak var delegate: AirdronCollectionViewWaterfallLayoutDelegate?
    
    // MARK: Private Properties
    fileprivate var columnHeights = [Int: [CGFloat]]()
    fileprivate var sectionMaxY = [CGFloat]()
     var sectionItemAttributes = [[UICollectionViewLayoutAttributes]]()
    fileprivate var allItemAttributes = [UICollectionViewLayoutAttributes]()
    fileprivate var headersAttribute = [Int: UICollectionViewLayoutAttributes]()
    fileprivate var footersAttribute = [Int: UICollectionViewLayoutAttributes]()
    fileprivate var unionRects = [CGRect]()
    
    // MARK: UICollectionViewLayout Methods
    override open func prepare() {
        super.prepare()
        
        guard let collectionView = collectionView else {
            return
        }
        
        assert(delegate!.conforms(to: AirdronCollectionViewWaterfallLayoutDelegate.self), "UICollectionView's delegate should conform to PSWaterfallLayoutDelegate protocol")
        
        // Initialize variables
        headersAttribute.removeAll(keepingCapacity: false)
        footersAttribute.removeAll(keepingCapacity: false)
        unionRects.removeAll(keepingCapacity: false)
        columnHeights.removeAll(keepingCapacity: false)
        allItemAttributes.removeAll(keepingCapacity: false)
        sectionItemAttributes.removeAll(keepingCapacity: false)
        sectionMaxY.removeAll(keepingCapacity: false)
        
        let numberOfSections = collectionView.numberOfSections
        
        if numberOfSections == 0 {
            return
        }
        
        // Create attributes
        var top: CGFloat = 0
        var attributes: UICollectionViewLayoutAttributes
        sectionMaxY = [CGFloat](repeating: 0, count: numberOfSections)
        
        for section in 0..<numberOfSections {
            
            let columnCount: Int
            if let count = self.delegate?.collectionView?(collectionView,
                                                          layout: self,
                                                          columnCountForSection: section), count >= 1 {
                columnCount = count
            } else {
                columnCount = self.columnCount
            }
            
            self.columnHeights[section] = [CGFloat](repeating: 0, count: columnCount)
            
            /*
             * 1. Get section-specific metrics (minimumInteritemSpacing, sectionInset)
             */
            var minimumInteritemSpacing: CGFloat
            if let height = delegate?.collectionView?(collectionView, layout: self, minimumInteritemSpacingForSection: section) {
                minimumInteritemSpacing = height
            } else {
                minimumInteritemSpacing = self.minimumInteritemSpacing
            }
            
            var sectionInset: UIEdgeInsets
            if let inset = delegate?.collectionView?(collectionView, layout: self, insetForSection: section) {
                sectionInset = inset
            } else {
                sectionInset = self.sectionInset
            }
            
            var minimumColumnSpacing: CGFloat
            if let spacing = delegate?.collectionView?(collectionView, layout: self, minimumColumnSpacingForSection: section) {
                minimumColumnSpacing = spacing
            } else {
                minimumColumnSpacing = self.minimumColumnSpacing
            }
            let width = collectionView.frame.size.width - sectionInset.left - sectionInset.right
            let count = CGFloat(columnCount)
            let itemWidth = floor((width - (count - 1) * minimumColumnSpacing) / (count))
            
            /*
             * 2. Section header
             */
            var headerHeight: CGFloat
            if let height = delegate?.collectionView?(collectionView,
                                                      layout: self,
                                                      heightForHeaderInSection: section)
            {
                headerHeight = height
            } else {
                headerHeight = self.headerHeight
            }
            
            var headerInset: UIEdgeInsets
            if let inset = delegate?.collectionView?(collectionView,
                                                     layout: self,
                                                     insetForHeaderInSection: section) {
                headerInset = inset
                
            } else {
                headerInset = self.headerInset
            }
            
            top += headerInset.top
            
            if headerHeight > 0 {
                attributes = UICollectionViewLayoutAttributes(
                    forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                    with: IndexPath(item: 0, section: section))
                
                attributes.frame = CGRect(x: headerInset.left,
                                          y: top,
                                          width: collectionView.frame.size.width - (headerInset.left + headerInset.right),
                                          height: headerHeight)
                
                headersAttribute[section] = attributes
                allItemAttributes.append(attributes)
                
                top = attributes.frame.maxY + headerInset.bottom
            }
            
            top += sectionInset.top
            
            /*
             * 3. Section items
             */
            let itemCount = collectionView.numberOfItems(inSection: section)
            var itemAttributes = [UICollectionViewLayoutAttributes]()
            
            // Item will be put into shortest column.
            for idx in 0..<itemCount {
                let indexPath = IndexPath(item: idx, section: section)
                let columnIndex = shortestColumnIndex(section: section)
                
                let xOffset = sectionInset.left + (itemWidth + minimumColumnSpacing) * CGFloat(columnIndex)
                let yOffset = columnHeights[section]![columnIndex] + top
                let itemHeight: CGFloat = floor((delegate?.collectionView(collectionView,
                                                                          layout: self,
                                                                          columnWidth: itemWidth,
                                                                          heightForItemAtIndexPath: indexPath))!)
                
                attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = CGRect(x: xOffset, y: yOffset, width: itemWidth, height: itemHeight)
                itemAttributes.append(attributes)
                allItemAttributes.append(attributes)
                columnHeights[section]![columnIndex] = attributes.frame.maxY + minimumInteritemSpacing - top
            }
            
            sectionItemAttributes.append(itemAttributes)
            
            /*
             * 4. Background
             */
            let columnIndex = longestColumnIndex(section: section)
            let itemsHeight = columnHeights[section]![columnIndex] - minimumInteritemSpacing
            
            if let color = self.delegate?.collectionView?(collectionView,
                                                          layout: self,
                                                          colorForSectionItemsBackground: section) {
                
                let backgroundInset: UIEdgeInsets
                if let inset = delegate?.collectionView?(collectionView,
                                                         layout: self,
                                                         insetForBackgroundInSection: section)
                {
                    backgroundInset = inset
                } else {
                    backgroundInset = self.backgroundInset
                }
                
                let kind = AirdronCollectionBackgroundDecorationView.kind
                let attr = AirdronCollectionBackgroundDecorationAttributes(forDecorationViewOfKind: kind,
                                                                           with: IndexPath(item: 0, section: section))
                attr.frame = CGRect(x: backgroundInset.left,
                                    y: top - sectionInset.top + backgroundInset.top,
                                    width: collectionView.frame.size.width - backgroundInset.left - backgroundInset.right,
                                    height: itemsHeight + sectionInset.bottom + sectionInset.top - backgroundInset.top - backgroundInset.bottom)
                attr.color = color
                attr.zIndex = -1
                allItemAttributes.append(attr)
            }
            
            /*
             * 5. Section footer
             */
            var footerHeight: CGFloat
            top += itemsHeight + sectionInset.bottom
            
            if let height = delegate?.collectionView?(collectionView, layout: self, heightForFooterInSection: section) {
                footerHeight = height
            } else {
                footerHeight = self.footerHeight
            }
            
            var footerInset: UIEdgeInsets
            if let inset = delegate?.collectionView?(collectionView, layout: self, insetForFooterInSection: section) {
                footerInset = inset
            } else {
                footerInset = self.footerInset
            }
            
            top += footerInset.top
            
            if footerHeight > 0 {
                attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, with: IndexPath(item: 0, section: section))
                attributes.frame = CGRect(x: footerInset.left, y: top, width: collectionView.frame.size.width - (footerInset.left + footerInset.right), height: footerHeight)
                
                footersAttribute[section] = attributes
                allItemAttributes.append(attributes)
                
                top = attributes.frame.maxY + footerInset.bottom
            }
            
            sectionMaxY[section] = top
            
        }
        
        // Build union rects
        var idx = 0
        let itemCounts = allItemAttributes.count
        while idx < itemCounts {
            let rect1 = allItemAttributes[idx].frame
            idx = min(idx + unionSize, itemCounts) - 1
            let rect2 = allItemAttributes[idx].frame
            unionRects.append(rect1.union(rect2))
            idx += 1
        }
    }
    
    override open var collectionViewContentSize: CGSize {
        let numberOfSections = collectionView?.numberOfSections
        if numberOfSections == 0 {
            return CGSize.zero
        }
        
        var contentSize = collectionView?.bounds.size
        if let height = sectionMaxY.last {
            contentSize?.height = height
        }
        return contentSize!
    }
    
    override open func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if indexPath.section >= sectionItemAttributes.count {
            return nil
        }
        
        if indexPath.item >= sectionItemAttributes[indexPath.section].count {
            return nil
        }
        
        return sectionItemAttributes[indexPath.section][indexPath.item]
    }
    
    override open func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        var attribute: UICollectionViewLayoutAttributes?
        
        if elementKind == UICollectionView.elementKindSectionHeader {
            attribute = headersAttribute[indexPath.section]
        } else if elementKind == UICollectionView.elementKindSectionFooter {
            attribute = footersAttribute[indexPath.section]
        }
        
        return attribute
    }
    
    override open func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var begin: Int = 0
        var end: Int = unionRects.count
        var attrs = [UICollectionViewLayoutAttributes]()
        
        for i in 0..<unionRects.count {
            if rect.intersects(unionRects[i]) {
                begin = i * unionSize
                break
            }
        }
        for i in (0..<unionRects.count).reversed() {
            if rect.intersects(unionRects[i]) {
                end = min((i + 1) * unionSize, allItemAttributes.count)
                break
            }
        }
        for i in begin ..< end {
            let attr = allItemAttributes[i]
            if rect.intersects(attr.frame) {
                attrs.append(attr)
            }
        }
        return Array(attrs)
    }
    
    override open func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        let oldBounds = collectionView?.bounds
        if newBounds.width != oldBounds!.width {
            return true
        }
        return false
    }
    
    // MARK: Private Methods
    fileprivate func shortestColumnIndex(section: Int) -> Int {
        var index: Int = 0
        var shortestHeight = CGFloat.greatestFiniteMagnitude
        
        for (idx, height) in columnHeights[section]!.enumerated() where height < shortestHeight {
            shortestHeight = height
            index = idx
        }
        return index
    }
    
    fileprivate func longestColumnIndex(section: Int) -> Int {
        var index: Int = 0
        var longestHeight: CGFloat = 0
        for (idx, height) in columnHeights[section]!.enumerated() where height > longestHeight {
            longestHeight = height
            index = idx
        }
        
        return index
    }
    
    fileprivate func invalidateIfNotEqual(_ oldValue: AnyObject, newValue: AnyObject) {
        if !oldValue.isEqual(newValue) {
            invalidateLayout()
        }
    }
}
